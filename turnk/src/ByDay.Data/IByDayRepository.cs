﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Data
{
    public interface IByDayRepository<T> where T : class
    {
        T Find(object id);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(object id);
        IQueryable<T> All { get; }
        IQueryable<T> FindBy(Expression<Func<T, bool>> filter,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy,
            string includeProperties = "");
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
    }
}
