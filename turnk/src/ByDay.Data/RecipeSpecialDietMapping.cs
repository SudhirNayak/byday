//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ByDay.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class RecipeSpecialDietMapping
    {
        public int Id { get; set; }
        public int RecipeId { get; set; }
        public int SpecialDietId { get; set; }
    
        public virtual SpecialDiet SpecialDiet { get; set; }
        public virtual Recipe Recipe { get; set; }
    }
}
