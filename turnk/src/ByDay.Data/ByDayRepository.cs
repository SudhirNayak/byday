﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace ByDay.Data
{
    public class ByDayRepository
    {
    }
    /// <summary>
    /// Class Used for store entity classes repository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ByDayRepository<T> : IByDayRepository<T> where T : class
    {


        private readonly Ex2ByDayEntities _context;
        private IDbSet<T> _entities;

       
        /// <summary>
        ///Paratmetries Constrotor
        /// </summary>
        /// <param name="context">Object context</param>
        public ByDayRepository(Ex2ByDayEntities context)
        {

            _context = context;
        }

        /// <summary>
        /// Method Used for find data according to Id
        /// </summary>
        /// <param name="id">used for search item through ID</param>
        /// <returns>data of searching element</returns>
        public T Find(object id)
        {
            return Entities.Find(id);
        }

        /// <summary>
        /// Used for Add data On database by object
        /// </summary>
        /// <param name="entity">store related data</param>
        public void Add(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                Entities.Add(entity);

                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw WrappedDbEntityValidationException(dbEx);
            }
        }

        /// <summary>
        /// Used for Store data collection On Database
        /// </summary>
        /// <param name="entities">Used fore store collection Data</param>
        public void BulkAdd(List<T> entities)
        {
            try
            {
                if (entities == null)
                    throw new ArgumentNullException("entity");
                foreach (var entity in entities)
                {
                    Entities.Add(entity);
                }


                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw WrappedDbEntityValidationException(dbEx);
            }
        }

        /// <summary>
        /// Used for Update requested entity on database
        /// </summary>
        /// <param name="entity">store data of requested entity</param>
        public void Update(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw WrappedDbEntityValidationException(dbEx);
            }
        }

        /// <summary>
        /// Used for call delete method to search given  Id data on database
        /// </summary>
        /// <param name="id">used for delete data by id</param>
        public void Delete(object id)
        {
            Delete(Find(id));
        }

        /// <summary>
        /// Used for delete entiry from database
        /// </summary>
        /// <param name="entity">store requested entity data for delete</param>
        public void Delete(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                Entities.Remove(entity);

                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw WrappedDbEntityValidationException(dbEx);
            }
        }

        /// <summary>
        /// Used for select data from database
        /// </summary>
        public virtual IQueryable<T> All
        {
            get
            {
                return Entities;
            }
        }

        /// <summary>
        /// Used for set Type of Entities
        /// </summary>
        private IDbSet<T> Entities
        {
            get { return _entities ?? (_entities = _context.Set<T>()); }
        }

        /// <summary>
        /// Used for select data according to given parameter from database
        /// </summary>
        /// <param name="filter">used for give searching parameter</param>
        /// <param name="orderBy">set order of selected data</param>
        /// <param name="includeProperties">add properyies</param>
        /// <returns></returns>
        public IQueryable<T> FindBy(Expression<Func<T, bool>> filter, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, string includeProperties = "")
        {
            var query = All;

            if (filter != null)
                query = query.Where(filter);

            query = includeProperties.
              Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).
              Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            return orderBy != null ? orderBy(query) : query;
        }

        public int ExecWithStoreProcedure(string query, params object[] parameters)
        {            
            return _context.Database.ExecuteSqlCommand("EXEC " + query, parameters);
        }

        /// <summary>
        /// Used for Select data according to given condition
        /// </summary>
        /// <param name="predicate">used for data selection </param>
        /// <returns>data for releted search</returns> 
        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            var query = All.Where(predicate);

            return query;
        }

        /// <summary>
        /// Used for handle Data exception in entities
        /// </summary>
        /// <param name="dbEx">store database exception</param>
        /// <returns>exception detail</returns>
        private Exception WrappedDbEntityValidationException(DbEntityValidationException dbEx)
        {
            var msg = string.Empty;
            const string msgFmt = "Property: {0} Error: {1}";
            var sep = Environment.NewLine;
            msg = dbEx.EntityValidationErrors.
              Aggregate(msg, (current1, validationResult) => validationResult.ValidationErrors.
                Aggregate(current1, (current, ve) => current + (sep + string.Format(msgFmt, ve.PropertyName, ve.ErrorMessage))));
            return new Exception(msg, dbEx);
        }

        public dynamic ExecWithStoreProcedureSync(string sp_name,ref string lastSyncId, params object[] parameters)
        {
            return GetDataSet(sp_name,ref lastSyncId, parameters);
        }

        public DataSet GetDataSet(string sp_name,ref string lastSyncId,params object[] paramValue )
        {
            //SqlConnection sqlConn = new SqlConnection("Data Source = tcp:dv93zw6ag3.database.windows.net,1433; Initial Catalog = Byady; User Id = byday1@dv93zw6ag3; Password = Byday@123;");
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConn"].ConnectionString);
            SqlCommand sqlcomm = new SqlCommand();
            sqlcomm.Connection = sqlConn;
            DataSet ds = new DataSet();
            using (sqlConn)
            {
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        // This will be your input parameter and its value
                        sqlcomm.Parameters.AddWithValue("@UserId", paramValue[0]);
                        sqlcomm.Parameters.AddWithValue("@SyncId", paramValue[1]);
                        sqlcomm.Parameters.AddWithValue("@Schema", paramValue[2]);

                        // You can retrieve values of `output` variables
                        var returnParam = new SqlParameter
                        {
                            ParameterName = "@lastSyncId",
                            Direction = ParameterDirection.Output,
                            Size = 1000
                        };
                        sqlcomm.Parameters.Add(returnParam);
                        // Name of stored procedure
                        sqlcomm.CommandText = "GetData";
                        da.SelectCommand = sqlcomm;
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.Fill(ds);
                        lastSyncId = Convert.ToString(returnParam.Value);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }
            return ds;
        }

    }
}