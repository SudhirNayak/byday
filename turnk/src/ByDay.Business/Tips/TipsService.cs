﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.Tips
{
  public  class TipsService
    {
        ByDayRepository<Tip> _tipsRepo = new ByDayRepository<Tip>(new Ex2ByDayEntities());
        Tip tip = new Tip();
        public bool Insert(string tips)
        {
           
            tip.Description = tips;
            tip.CreatedAt = DateTime.UtcNow;
            _tipsRepo.Add(tip);
            return true;
        }
        public bool Update(string tips,int Id)
        {
            tip = _tipsRepo.Find(Id);

            if (tip != null)
            {
                tip.Description = tips;
                _tipsRepo.Update(tip);
            }

            return true;
        }

        public bool Delete(int Id)
        {
            tip = _tipsRepo.Find(Id);

            if (tip != null)
            {
                tip.IsDeleted=true;
                tip.ModifiedAt = DateTime.UtcNow;
                _tipsRepo.Update(tip);
            }

            return true;
        }
        public IQueryable<Tip> GetMealTip()
        {
            return _tipsRepo.All.AsQueryable().Where(x=>x.IsDeleted==false);
        }
        public IQueryable<Tip> GetMealTipById(int id)
        {
            return _tipsRepo.All.Where(x => x.Id == id);
        }
          }
}
