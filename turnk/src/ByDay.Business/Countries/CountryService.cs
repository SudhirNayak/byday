﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.Countries
{
   public class CountryService
    {
        ByDayRepository<ByDay.Data.Country> _countryRepo = new ByDayRepository<ByDay.Data.Country>(new Ex2ByDayEntities());
        public IQueryable<Country> Countries()
        {
            return _countryRepo.All;
        }

    }
}
