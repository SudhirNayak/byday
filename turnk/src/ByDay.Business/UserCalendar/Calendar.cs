﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.UserCalendar
{
    public class Calendar
    {

        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string TimeZone { get; set; }
        public int ColorId { get; set; }
        public int Owner { get; set; }

        public Nullable<bool> IsPrimary { get; set; }


    }
}
