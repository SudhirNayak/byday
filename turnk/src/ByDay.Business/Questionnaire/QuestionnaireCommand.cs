﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.Questionnaire
{
   public class QuestionnaireCommand
    {
        public QuestionnaireCommand()
        {
            Answer = new List<Questionnairecom>();
        }
      public List<Questionnairecom> Answer { get; set; }
    }

    public class Questionnairecom
    {
        public int QuestionId { get; set; }
        public string Type { get; set; }

        public int UserId { get; set; }

        public string Value { get; set; }

        public int[] SelectedOptions { get; set; }

        public OptionParts[] SelectedOptionsParts { get; set; }
    }


    public class OptionParts
    {


        public int OptionId { get; set; }
        public int PartId { get; set; }
    }
}
