﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ByDay.Data;

namespace ByDay.Business.Questionnaire
{
    public class QuestionnaireService
    {

        ByDayRepository<ByDay.Data.Questionnaire> _QuestionarreRepository = new ByDayRepository<ByDay.Data.Questionnaire>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.UserResponseText> _UserResQuestRepository = new ByDayRepository<ByDay.Data.UserResponseText>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.Recipe> _recipe = new ByDayRepository<ByDay.Data.Recipe>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.SyncLog> _syncLogRepository = new ByDayRepository<ByDay.Data.SyncLog>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.Category> _categoryRepository = new ByDayRepository<ByDay.Data.Category>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.RecipeCategoryMapping> _recipeCategoryRepository = new ByDayRepository<ByDay.Data.RecipeCategoryMapping>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.UserResponseM> _UserResMulQuestRepository = new ByDayRepository<ByDay.Data.UserResponseM>(new Ex2ByDayEntities());
        Data.UserResponseM responseM = new Data.UserResponseM();

        public IQueryable<Data.Questionnaire> GetQuestionnaire()
        {
            var questions = _QuestionarreRepository.All.Select(x => x).Where(y => y.IsActive == true);

            return questions;
        }
        public IQueryable<Data.UserResponseText> GetQuestionnaireTextType(int userId)
        {
            var answers = _UserResQuestRepository.All.Select(x => x).Where(y => y.UserId == userId);
            return answers;
        }
        public IQueryable<Data.UserResponseM> GetQuestionnaireSelectionType(int userId)
        {
            var answersMul = _UserResMulQuestRepository.All.Select(x => x).Where(y => y.UserId == userId);
            return answersMul;
        }

        public void DeleteExistAnswerM(Questionnairecom ans)
        {
            var answerExists = _UserResMulQuestRepository.FindBy(x => x.UserId == ans.UserId && x.QuestionId == ans.QuestionId).ToList();
            foreach (var answerEx in answerExists)
            {
                if (answerEx != null)
                    _UserResMulQuestRepository.Delete(answerEx);
            }

        }

        public bool PostQuestionnTextType(Questionnairecom ans)
        {
            if (ans.Type == "R")
            {
                var categories = _categoryRepository.All.ToList();
                foreach (var recipe in ans.Value.Split(',').ToList())
                {
                    var res = _recipe.FindBy(t => t.RecipeName.ToLower() == recipe.Trim().ToLower()).ToList();
                    var res1 = new Recipe()
                    {
                        UserId = ans.UserId,
                        RecipeName = recipe.Trim()
                    };
                    if (res.Count == 0)
                    {
                        _recipe.Add(res1);
                        _syncLogRepository.Add(new SyncLog()
                        {
                            UserId = ans.UserId,
                            Entity = "Recipe",
                            EntityRowId = res1.Id,
                            ActionType = "INSERT",
                            SyncDate = DateTime.UtcNow
                        });
                        var category = categories.Where(t => recipe.Trim().ToLower().Contains(t.Name.Trim().ToLower())).ToList();

                        if (category != null && category.Count() > 0)
                        {
                            foreach (var item in category)
                            {
                                var mapping = new RecipeCategoryMapping()
                                {
                                    RecipeId = res1.Id,
                                    CategoryId = item.Id,
                                    CreatedAt = DateTime.UtcNow,
                                    ModifiedAt = DateTime.UtcNow
                                };
                                _recipeCategoryRepository.Add(mapping);
                                    
                                _syncLogRepository.Add(new SyncLog()
                                {
                                    UserId = ans.UserId,
                                    Entity = "RecipeCategory",
                                    EntityRowId = mapping.Id,
                                    ActionType = "INSERT",
                                    SyncDate = DateTime.UtcNow
                                });
                            }
                        }
                    }
                }
            }
            else
            {
                var answerExists = _UserResQuestRepository.FindBy(x => x.UserId == ans.UserId && x.QuestionId == ans.QuestionId).ToList();
                foreach (var answerEx in answerExists)
                {
                    if (answerEx != null)
                        _UserResQuestRepository.Delete(answerEx);
                }
                Data.UserResponseText response = new Data.UserResponseText();
                response.QuestionId = ans.QuestionId;
                response.UserId = ans.UserId;
                response.Response = ans.Value;
                _UserResQuestRepository.Add(response);
            }
            return true;
        }

        public bool PostQuestionSingleSel(Questionnairecom ans)
        {
            DeleteExistAnswerM(ans);
            if (ans.SelectedOptions.Count() != 0)
            {
                responseM.QuestionId = ans.QuestionId;
                responseM.UserId = ans.UserId;
                responseM.OptionId = ans.SelectedOptions.First();
                responseM.UserId = ans.UserId;
                responseM.PartId = 1;
                _UserResMulQuestRepository.Add(responseM);
            }
            return true;
        }
        public bool PostQuestionMulSel(Questionnairecom _ans)
        {
            DeleteExistAnswerM(_ans);
            foreach (int selectionM in _ans.SelectedOptions)
            {
                responseM.QuestionId = _ans.QuestionId;
                responseM.UserId = _ans.UserId;
                responseM.OptionId = selectionM; ;
                responseM.UserId = _ans.UserId;
                responseM.PartId = 1;
                _UserResMulQuestRepository.Add(responseM);
            }
            return true;
        }
        public bool PostQuestionMulSingleSel(Questionnairecom _ans)
        {
             DeleteExistAnswerM(_ans);
            foreach (OptionParts selectionP in _ans.SelectedOptionsParts)
            {
                responseM.QuestionId = _ans.QuestionId;
                responseM.UserId = _ans.UserId;
                responseM.OptionId = selectionP.OptionId;
                responseM.PartId = selectionP.PartId;
                _UserResMulQuestRepository.Add(responseM);
            }
            return true;
        }
    }
}
