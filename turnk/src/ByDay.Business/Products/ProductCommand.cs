﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.Products
{
  public  class ProductCommand
    {
        public int Id { get; set; }
        public int SubCategoryId { get; set; }
        public string Name { get; set; }
        public string ProductName { get; set; }
        public string URL { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string Path { get; set; }
        public string Type { get; set; }
        public bool IsActive { get; set; }
        public bool IsCustomizable { get; set; }
        public int CategoryId { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public List<ProductImage> ProductImages { get; set; }

    }

    public class ProductImage
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string URL { get; set; }
    }
}
