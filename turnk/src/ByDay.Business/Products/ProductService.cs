﻿using ByDay.Business.Categories;

using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.Products
{
  public  class ProductService
    {
        ByDayRepository<ByDay.Data.Product> _productRepo = new ByDayRepository<ByDay.Data.Product>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.ProductImage> _productImageRepo = new ByDayRepository<ByDay.Data.ProductImage>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.ProductCategory> _productCategoryRepo = new ByDayRepository<ByDay.Data.ProductCategory>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.ProductSubCategory> _productSubCategoryRepo = new ByDayRepository<ByDay.Data.ProductSubCategory>(new Ex2ByDayEntities());
        CategoryService _categoryService = new CategoryService();
        public IQueryable<ByDay.Data.Product> GetAll()
        {
            return _productRepo.All.Where(x => x.IsActive);
        }
        public IQueryable<ByDay.Data.Product> GetSelectProduct(string searchText)
        {
            return _productRepo.All.Where(x => x.IsActive && x.Name.Contains(searchText));
        }
        public IQueryable<ByDay.Data.Product> GetProducts(ProductCommand command)
        {
            var subCategories =
                _productSubCategoryRepo.FindBy(x => x.CategoryId == command.CategoryId).Select(x => x.Id).ToList();
            return _productRepo.FindBy(x => subCategories.Contains(x.SubCategoryId) && x.Type == command.Type && x.IsActive);
        }
        public IQueryable<ByDay.Data.Product> GetProductById(int id)
        {
            return _productRepo.FindBy(x => x.Id ==id);
        }
        public IQueryable<ProductCategory> GetCategories()
        {
            return _categoryService.All();
        }

        public IQueryable<ProductSubCategory> GetSubCategories()
        {
            return _categoryService.AllSubCategory();
        }
        
        public IQueryable<ProductSubCategory> GetSubCategories(int categoryId)
        {
            return _categoryService.AllSubCategory().Where(x => x.CategoryId == categoryId);
        }


        public IQueryable<ProductSubCategory> GetSubCategoriesById(int subCategoryId)
        {
            return _categoryService.AllSubCategory().Where(x => x.Id == subCategoryId);
        }
        public IQueryable<ProductCategory> GetCategoriesById(int id)
        {
            return _categoryService.All().Where(x => x.Id == id);
        }
        public void Insert(ProductCommand productCommand)
        {
            var product = new Product
            {
                Name = productCommand.Name,
                Price = productCommand.Price,
                SubCategoryId = productCommand.SubCategoryId,
                URL = productCommand.URL,
                Type = productCommand.Type,
                CreatedAt = DateTime.UtcNow,
                IsActive = true,
               IsCustomizable=productCommand.IsCustomizable
            };
            _productRepo.Add(product);

            for (int i = 0; i < productCommand.ProductImages.Count; i++)
            {
                _productImageRepo.Add(new ByDay.Data.ProductImage()
                {
                    ProductId = product.Id,
                    URL = productCommand.ProductImages[i].URL
                });
            }
        }
        public void Update(ProductCommand productCommand)
        {

            var existProduct = _productRepo.FindBy(x => x.Id == productCommand.Id).FirstOrDefault();

            if (existProduct.Id != 0)
            {
                existProduct.Id = productCommand.Id;
                existProduct.Name = productCommand.Name;
                existProduct.Price = productCommand.Price;
                existProduct.SubCategoryId = productCommand.SubCategoryId;
                existProduct.URL = productCommand.URL;
                existProduct.Type = productCommand.Type;
                existProduct.ModifiedAt = DateTime.UtcNow;
                existProduct.IsActive = true;
                existProduct.IsCustomizable = productCommand.IsCustomizable;

                _productRepo.Update(existProduct);
                var res = _productImageRepo.FindBy(t => t.ProductId == existProduct.Id).ToList();
                foreach (var item in res)
                    _productImageRepo.Delete(item);

                for (int i = 0; i < productCommand.ProductImages.Count; i++)
                {
                    _productImageRepo.Add(new ByDay.Data.ProductImage()
                    {
                        ProductId = existProduct.Id,
                        URL = productCommand.ProductImages[i].URL
                    });
                }
            }
        }
        public ProductCategory InsertProductCategory(string Name)
        {
            var ExistProductCategory = GetCategories().Where(x => x.Name == Name);
            var productCategory = new ProductCategory();
            if (ExistProductCategory.Count() == 0)
            {
                productCategory = new ProductCategory
                {
                    Name = Name
                };
                _productCategoryRepo.Add(productCategory);
            }
            return productCategory;
        }
        public ProductSubCategory InsertProductSubCategory(string Name, int categoryId)
        {
            var ExistProductSubCategory = GetSubCategories().Where(x => x.Name == Name);
            var productSubCategory = new ProductSubCategory();
            if (ExistProductSubCategory.Count() == 0)
            {
                productSubCategory = new ProductSubCategory
                {
                    CategoryId = categoryId,
                    Name = Name
                };
                _productSubCategoryRepo.Add(productSubCategory);
            }
            return productSubCategory;
        }



        public bool Delete(int Id)
        {
        var product = _productRepo.Find(Id);

            if (product != null)
            {
                product.IsActive = false;
                product.ModifiedAt = DateTime.UtcNow;
                _productRepo.Update(product);
            }
            
            return true;
        }

        //public void Insert(ProductCommand productCommand,string Name)
        //{
        //    var product = new Product
        //    {
        //        Name = Name,
        //        Price = productCommand.Price,
        //        SubCategoryId = productCommand.SubCategoryId,
        //        URL = productCommand.URL,
        //        Type = productCommand.Type,
        //        CreatedAt = DateTime.UtcNow,
        //        IsActive = true
        //    };
        //    _productRepo.Add(product);

        //}
    }
}
