﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.Categories
{
   public class CategoryService
    {

        ByDayRepository<ByDay.Data.ProductCategory> _productCategoryRepo = new ByDayRepository<ByDay.Data.ProductCategory>(new Ex2ByDayEntities());

        ByDayRepository<ByDay.Data.ProductSubCategory> _productSubCategoryRepo = new ByDayRepository<ByDay.Data.ProductSubCategory>(new Ex2ByDayEntities());

        public IQueryable<ProductCategory> All()
        {
            return _productCategoryRepo.All;
        }
        public IQueryable<ProductSubCategory> AllSubCategory()
        {
            return _productSubCategoryRepo.All;
        }
    }
}
