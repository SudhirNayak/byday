﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.UserNote
{
    public class UserNoteService
    {
        ByDayRepository<ByDay.Data.UserNote> _userNoteRepository = new ByDayRepository<ByDay.Data.UserNote>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.NoteCategory> _userNoteCategoryRepository = new ByDayRepository<ByDay.Data.NoteCategory>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.SyncLog> _syncLogRepository = new ByDayRepository<ByDay.Data.SyncLog>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.UserScratchpad> _scratchRepository = new ByDayRepository<ByDay.Data.UserScratchpad>(new Ex2ByDayEntities());

        public IQueryable<Data.UserNote> GetUserNotes(int id)
        {
            var userNote = _userNoteRepository.FindBy(x => x.UserId == id).ToList();

            return userNote.AsQueryable();

        }

        public NotesSyncResponse SyncedNotes(UserNotesViewModel userNotes, int userId, int lastSyncedId)
        {
            NotesSyncResponse response = new NotesSyncResponse();

            if (userNotes.scratchPad != null)
            {
                var scratchpad = _scratchRepository.FindBy(t => t.UserId == userId).FirstOrDefault();
                if (scratchpad != null)
                {
                    scratchpad.Content = userNotes.scratchPad;
                    scratchpad.ModifiedAt = DateTime.UtcNow;
                    _scratchRepository.Update(scratchpad);
                }
                else
                {
                    scratchpad = new Data.UserScratchpad();
                    scratchpad.Content = userNotes.scratchPad;
                    scratchpad.ModifiedAt = DateTime.UtcNow;
                    scratchpad.CreatedAt = DateTime.UtcNow;
                    scratchpad.UserId = userId;
                    _scratchRepository.Add(scratchpad);
                }
                response.log = UserNoteSyncLog(userId, scratchpad.Id, "UserScratchpad", "UPDATE");
                _syncLogRepository.Add(response.log);
            }
            if (CheckLogSyncId(lastSyncedId, userId) > 0)
            {
                response.Code = 1;
                return response;
            }
            SyncedCategory(userNotes.categoryModel, userId, lastSyncedId, ref response);
            response.CreateCategory = userNotes.categoryModel.CreateCategory;
            foreach (var note in userNotes.CreatedUserNote)
            {
                var res = UserNote(note);
                var category = userNotes.categoryModel.CreateCategory.FirstOrDefault(t => t.tempId == note.tempId);
                if (category != null)
                    res.NoteCategoryId = category.Id;
                _userNoteRepository.Add(res);
                note.Id = res.Id;
                response.log = UserNoteSyncLog(userId, res.Id, "UserNote", "INSERT");
                _syncLogRepository.Add(response.log);
            }
            response.CreatedUserNote = userNotes.CreatedUserNote;
            foreach (var note in userNotes.ModifiedUserNote)
            {
                var res = UserNote(note);
                var category = userNotes.categoryModel.CreateCategory.FirstOrDefault(t => t.tempId == note.tempId);
                if (category != null)
                    res.NoteCategoryId = category.Id;
                _userNoteRepository.Update(res);
                response.log = UserNoteSyncLog(userId, res.Id, "UserNote", "UPDATE");
                _syncLogRepository.Add(response.log);
            }
            foreach (var note in userNotes.DeletedUserNote)
            {
                _userNoteRepository.Delete(note.Id);
                response.log = UserNoteSyncLog(userId, note.Id, "UserNote", "DELETE");
                _syncLogRepository.Add(response.log);

            }
            return response;
        }

        public NotesSyncResponse SyncedCategory(UserNoteCategoryViewModel userCategory, int userId, int lastSyncedId, ref NotesSyncResponse response)
        {
            foreach (var category in userCategory.CreateCategory)
            {
                var res = UserNoteCategory(category);
                _userNoteCategoryRepository.Add(res);
                category.Id = res.Id;
                response.log = UserNoteSyncLog(userId, res.Id, "Category", "INSERT");
                _syncLogRepository.Add(response.log);
            }
            foreach (var category in userCategory.ModifiedCategory)
            {
                var res = UserNoteCategory(category);
                _userNoteCategoryRepository.Update(res);
                response.log = UserNoteSyncLog(userId, res.Id, "Category", "UPDATE");
                _syncLogRepository.Add(response.log);
            }
            foreach (var category in userCategory.DeleteCategory)
            {
                _userNoteCategoryRepository.Delete(category.Id);
                response.log = UserNoteSyncLog(userId, category.Id, "Category", "DELETE");
                _syncLogRepository.Add(response.log);

            }
            return response;
        }
        public ByDay.Data.UserNote UserNote(UserNotesModel note)
        {
            var usernote = new Data.UserNote()
            {
                Id = note.Id,
                UserId = note.UserId,
                Title = note.Title,
                Content = note.Content,
                NoteCategoryId = note.NoteCategoryId == 0 ? null : note.NoteCategoryId,
                CreatedAt = DateTime.UtcNow,
                ModifiedAt = DateTime.UtcNow,
                IsDeleted = false
            };
            return usernote;
        }
        public ByDay.Data.NoteCategory UserNoteCategory(UserNoteCategory category)
        {
            var userCategory = new Data.NoteCategory()
            {
                Name = category.Name,
                Colour = category.Colour,
                CreatedAt = DateTime.UtcNow,
                ModifiedAt = DateTime.UtcNow,
                IsDeleted = false
            };
            return userCategory;
        }
        public ByDay.Data.SyncLog UserNoteSyncLog(int userId, int rowId, string entity, string actionType)
        {
            var obj = new SyncLog()
            {
                UserId = userId,
                Entity = entity,
                EntityRowId = rowId,
                ActionType = actionType,
                SyncDate = DateTime.UtcNow
            };
            return obj;
        }

        public int CheckLogSyncId(long id, long userId)
        {
            return _syncLogRepository.FindBy(t => t.Id > id && t.UserId == userId && (t.Entity == "UserNote" || t.Entity == "Category")).Count();
        }

        public dynamic GetData(int userId, int syncId, string schema, ref string lastSyncId)
        {
            return _syncLogRepository.ExecWithStoreProcedureSync("GetData", ref lastSyncId, new object[] { userId, syncId, schema });
        }
    }

    public class UserNotesModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int? NoteCategoryId { get; set; }
        public int localId { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public bool IsDeleted { get; set; }
        public int UserId { get; set; }
        public int tempId { get; set; }
        public int VersionNumber { get; set; }
    }

    public class UserNotesViewModel
    {
        public List<UserNotesModel> CreatedUserNote { get; set; }
        public List<UserNotesModel> ModifiedUserNote { get; set; }
        public List<UserNotesModel> DeletedUserNote { get; set; }
        public UserNoteCategoryViewModel categoryModel { get; set; }
        public string scratchPad { get; set; }
    }

    public class NotesSyncResponse
    {
        public int lastSyncId { get; set; }
        public int Code { get; set; }
        public SyncLog log { get; set; }
        public List<UserNoteCategory> CreateCategory { get; set; }
        public List<UserNotesModel> CreatedUserNote { get; set; }


    }

    public class UserNoteCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public bool IsDeleted { get; set; }
        public string Colour { get; set; }
        public int tempId { get; set; }
        public int VersionNumber { get; set; }

    }
    public class UserNoteCategoryViewModel
    {
        public List<UserNoteCategory> CreateCategory { get; set; }
        public List<UserNoteCategory> ModifiedCategory { get; set; }
        public List<UserNoteCategory> DeleteCategory { get; set; }
    }

    public class SyncNotesResult
    {
        public DataTable NotesIU { get; set; }
        public DataTable NotesD { get; set; }
        public DataTable CategoryIU { get; set; }
        public DataTable CategoryD { get; set; }
        public DataTable UserScratchPad { get; set; }
    }
}
