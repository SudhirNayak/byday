﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.DeviceSyncUser
{
   public class DeviceSyncService
    {


        ByDayRepository<ByDay.Data.DeviceSync> _deviceSyncRepository = new ByDayRepository<ByDay.Data.DeviceSync>(new Ex2ByDayEntities());


        public IQueryable<ByDay.Data.DeviceSync> GetPushTokens(int userId)
        {
            var pushTokens=  _deviceSyncRepository.All;
            var pushTokenByUser = pushTokens.Select(x => x).Where(x=>x.UserId == userId);
            return pushTokenByUser;
        }
    }
}
