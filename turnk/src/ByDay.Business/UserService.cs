﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.Business
{
    /// <summary>
    /// Class Used for select, insert,update,delete user information from database
    /// </summary>
    public class UserService
    {
        private readonly ByDayRepository<ByDayUser> _userRepository;
        public UserService()
        {
            _userRepository = new ByDayRepository<ByDayUser>(new Ex2ByDayEntities());
        }

        public bool CreateUser()
        {
            return true;
        }

        /// <summary>
        /// This method Used for getting User detail by User Name and Password.
        /// </summary>
        /// <param name="userName">search according userName</param>
        /// <param name="Password">search according password</param>
        /// <returns>UserDetail</returns>
        public ByDayUser Login(string userName, string Password)
        {
            return _userRepository.FindBy(x => x.EmailId == userName && x.Password == Password).FirstOrDefault();
        }

        public IQueryable<ByDayUser> GetByUser()
        {
            return _userRepository.All.Select(x => x);

        }
        /// <summary>
        /// This method used for getting User Detail by UserName.
        /// </summary>
        /// <param name="email">used for search according to username</param>
        /// <returns></returns>
        public ByDayUser GetByUserName(string email)
        {
            return _userRepository.FindBy(x => x.EmailId == email && x.RoleDefinedId==1).FirstOrDefault();
        }

        public IQueryable<ByDayUser> GetAllUser()
        {
            return _userRepository.FindBy(x => x.RoleDefinedId==2);

        }

        public IQueryable<ByDayUser> GetSelectUser(string searchText)
        {
            return _userRepository.FindBy(x => x.RoleDefinedId == 2 && x.Name.Contains(searchText)).OrderBy(x=>x.Name);
        }
    }
}