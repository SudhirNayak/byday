﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.MealPlans
{
    public class MealPlanCommand
    {
        public int Id { get; set; }
        public string TempId { get; set; }
        public int RecipeId { get; set; }

        public int UserId { get; set; }
        public Nullable<System.DateTime> RecipeDate { get; set; }
        public string Guid { get; set; }
        public string OneTimeComment { get; set; }

        public bool IsDeleted { get; set; }
        public string TempRecipeId { get; set; }
        public int SyncStatus { get; set; }
        public int VersionNumber { get; set; }

    }
}
