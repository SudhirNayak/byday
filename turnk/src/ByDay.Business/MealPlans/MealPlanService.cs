﻿using ByDay.Data;
using ByDay.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.MealPlans
{
   public  class MealPlanService
    {
        private readonly ByDayRepository<MealPlan> _mealRepository;
        ByDayRepository<ByDay.Data.Recipe> _recipeRepo = new ByDayRepository<ByDay.Data.Recipe>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.RecipeImage> _recipeImageRepo = new ByDayRepository<ByDay.Data.RecipeImage>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.RecipeUserFeedback> _recipeUserFeddbackRepo = new ByDayRepository<ByDay.Data.RecipeUserFeedback>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.Tip> _tipRepo = new ByDayRepository<ByDay.Data.Tip>(new Ex2ByDayEntities());


        public MealPlanService()
        {
            _mealRepository = new ByDayRepository<MealPlan>(new Ex2ByDayEntities());
                  }

        public IQueryable<MealPlan> Get(int id)
        {
            var meals = _mealRepository.FindBy(x => x.UserId == id).ToList();
           //  List<MealPlan> events = new List<MealPlan>();
           return meals.AsQueryable();
        }


        public IQueryable<MealPlan> GetUnSyncedMealPlan(string userID, DateTime syncedDate)
        {
            var meals = Get(FieldConverter.To<int>(userID));
            var mealsResult = meals.GroupBy(x => x.Id).Select(b => b.FirstOrDefault());

            List<MealPlan> mealPlan = new List<MealPlan>();
            mealPlan.AddRange(mealsResult.Where(cr => cr.CreatedAt != null).Where(x => x.CreatedAt.Value > syncedDate && x.IsDeleted == false));
            mealPlan.AddRange(mealsResult.Select(a => a).Where(y => y.ModifiedAt != null).Where(x => x.ModifiedAt.Value > syncedDate && x.IsDeleted == false));
            mealPlan.AddRange(mealsResult.Select(b => b).Where(x => x.ModifiedAt != null && x.CreatedAt != null).Where(y => y.CreatedAt.Value <= syncedDate && y.ModifiedAt > syncedDate && y.IsDeleted == true));

            var mealsResults = mealPlan.GroupBy(x => x.Id).Select(b => b.FirstOrDefault());
            return mealsResults.OrderBy(y => y.Id).AsQueryable();
        }

        public IQueryable<ByDay.Data.Recipe> GetRecipeName(int recipeId)

        {
            return _recipeRepo.All.Select(x => x).Where(x=>x.Id==recipeId).OrderBy(y => y.RecipeName);
        }
        public IQueryable<ByDay.Data.RecipeImage> GetRecipeImage(int recipeId)

        {
            return _recipeImageRepo.All.Select(x => x).Where(x => x.RecipeId == recipeId).OrderBy(y => y.Path);
           
        }
        
            public IQueryable<ByDay.Data.RecipeUserFeedback> GetIsFavourite(int recipeId)

        {
            return _recipeUserFeddbackRepo.All.Select(x => x).Where(x => x.RecipeId == recipeId);
        }

        public CreatedUpdateedDeletedMealResponse Insert(MealPlanCommand command)
        {
            CreatedUpdateedDeletedMealResponse response = new CreatedUpdateedDeletedMealResponse();
            var mealPlanDetail = new MealPlan()
            {
                RecipeId = command.RecipeId,
                UserId= command.UserId,
                RecipeDate=command.RecipeDate,
                Guid=command.Guid,
                OneTimeComment=command.OneTimeComment,
                IsDeleted=command.IsDeleted,
                CreatedAt= DateTime.UtcNow
            };
            _mealRepository.Add(mealPlanDetail);

            if (mealPlanDetail.Id != 0)
            {
                response.Id = mealPlanDetail.Id;
                response.TempId = command.TempId;
                response.Success = true;
            }
            return response;
        }

        public CreatedUpdateedDeletedMealResponse Update(MealPlanCommand command)
        {
            CreatedUpdateedDeletedMealResponse response = new CreatedUpdateedDeletedMealResponse();

            var currentMeals = _mealRepository.FindBy(x => x.Id == command.Id).FirstOrDefault();


            currentMeals.Id = command.Id;
            currentMeals.RecipeId = command.RecipeId;
            currentMeals.UserId = command.UserId;
            currentMeals.RecipeDate = command.RecipeDate;
            currentMeals.Guid = command.Guid;
            currentMeals.OneTimeComment = command.OneTimeComment;
            currentMeals.IsDeleted = command.IsDeleted;
            currentMeals.ModifiedAt = DateTime.UtcNow;
            
            _mealRepository.Update(currentMeals);

            if (currentMeals.ModifiedAt != null)
            {
                response.Id = currentMeals.Id;
                response.Success = true;


            }
            return response;
        }

        public CreatedUpdateedDeletedMealResponse Delete(MealPlanCommand command)
        {
            CreatedUpdateedDeletedMealResponse response = new CreatedUpdateedDeletedMealResponse();

            var currentMeals = _mealRepository.FindBy(x => x.Id == command.Id).ToList();


            foreach (var data in currentMeals)
            {
                data.IsDeleted = true;
                data.ModifiedAt = DateTime.UtcNow;
                _mealRepository.Update(data);

                response.Id = command.Id;
                response.Success = true;
            }

         
            return response;
        }

        public IQueryable<Tip>  GetMealTip()
        {
            return _tipRepo.FindBy(t => t.IsDeleted == false).AsQueryable();
        }
    }
}
