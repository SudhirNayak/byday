﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.MealPlans
{
   public class MealUnSyncedResponse
    {

        public MealUnSyncedResponse()
        {
            CreatedPlans = new List<CreatedUpdateedDeletedMealResponse>();
            ModifiedPlans = new List<CreatedUpdateedDeletedMealResponse>();
            DeletedPlans = new List<CreatedUpdateedDeletedMealResponse>();

        }
        public string LastSync
        {
            get
            {
                return DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            }
        }


        public List<CreatedUpdateedDeletedMealResponse> CreatedPlans { get; set; }
        public List<CreatedUpdateedDeletedMealResponse> ModifiedPlans { get; set; }
        public List<CreatedUpdateedDeletedMealResponse> DeletedPlans { get; set; }

       
    }
    public class CreatedUpdateedDeletedMealResponse
    {
        public string TempId { get; set; }

        public int Id { get; set; }

        public bool Success { get; set; }
    }
}
