﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.MealPlans
{
    public class MealUnSyncedCommand
    {
        public MealUnSyncedCommand()
        {
            CreatedPlans = new List<MealPlanCommand>();
            ModifiedPlans = new List<MealPlanCommand>();
            DeletedPlans = new List<MealPlanCommand>();

        }
        public List<MealPlanCommand> CreatedPlans { get; set; }
        public List<MealPlanCommand> ModifiedPlans { get; set; }
        public List<MealPlanCommand> DeletedPlans { get; set; }
    }
}
