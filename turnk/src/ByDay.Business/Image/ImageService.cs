﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.Image
{
    public class ImageService
    {
        ByDayRepository<ByDay.Data.RecipeImage> _recipeImageRepository = new ByDayRepository<ByDay.Data.RecipeImage>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.SyncLog> _syncLogRepository = new ByDayRepository<ByDay.Data.SyncLog>(new Ex2ByDayEntities());

        public ImageSyncResponse SyncedModel(ByDay.Business.RecipeDetailUnSynced.RecipeImage recipeImage, int userId)
        {
            ImageSyncResponse response = new ImageSyncResponse();
            if (recipeImage != null)
            {
                var objModel = MapRecipeImage(recipeImage);
                _recipeImageRepository.Add(objModel);
                recipeImage.Id = objModel.Id;
                var log = ImageSyncLog(userId, objModel.Id, "RecipeImage", "INSERT");
                _syncLogRepository.Add(log);
                response.Code = objModel.Id;
                response.image = recipeImage;
                response.lastSyncId = log.Id;
            }
            return response;
        }

        public ImageSyncResponse UpdateModel(ByDay.Business.RecipeDetailUnSynced.RecipeImage recipeImage, int userId)
        {
            ImageSyncResponse response = new ImageSyncResponse();
            if (recipeImage != null)
            {
                var objModel = _recipeImageRepository.Find(recipeImage.Id);
                objModel.Path = recipeImage.Path;
                _recipeImageRepository.Update(objModel);
                var log = ImageSyncLog(userId, objModel.Id, "RecipeImage", "UPDATE");
                _syncLogRepository.Add(log);
                response.Code = objModel.Id;
                response.image = recipeImage;
                response.lastSyncId = log.Id;
            }

            return response;
        }



        public ByDay.Data.SyncLog ImageSyncLog(int userId, int rowId, string entity, string actionType)
        {
            var obj = new SyncLog()
            {
                UserId = userId,
                Entity = entity,
                EntityRowId = rowId,
                ActionType = actionType,
                SyncDate = DateTime.UtcNow
            };
            return obj;
        }

        public int CheckLogSyncId(long id, long userId, string Entity)
        {
            return _syncLogRepository.FindBy(t => t.Id > id && t.UserId == userId && t.Entity == Entity).Count();
        }

        public RecipeImage MapRecipeImage(ByDay.Business.RecipeDetailUnSynced.RecipeImage recipeImage)
        {
            var recipeImageObj = new Data.RecipeImage()
            {
                Id = recipeImage.Id,
                CreatedAt = DateTime.UtcNow,
                CreatedBy = recipeImage.CreatedBy,
                ModifiedAt = recipeImage.ModifiedAt,
                ModifiedBy = recipeImage.ModifiedBy,
                Name = recipeImage.Name,
                Path = recipeImage.Path,
                RecipeId = recipeImage.RecipeId,
                IsCover = recipeImage.IsCover
            };
            return recipeImageObj;
        }

    }

    public class ImageSyncResponse
    {
        public int lastSyncId { get; set; }
        public int Code { get; set; }
        public ByDay.Business.RecipeDetailUnSynced.RecipeImage image { get; set; }

    }
}
