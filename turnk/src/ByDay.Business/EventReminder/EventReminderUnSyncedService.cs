﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ByDay.Business.EventDetail;
using System.Threading.Tasks;
using ByDay.Data;

namespace ByDay.Business.EventReminder
{
  public class EventReminderUnSyncedService
    {

        private readonly ByDayRepository<Reminder> _eventReminderRepository;

        public EventReminderUnSyncedService()
        {
            _eventReminderRepository = new ByDayRepository<Reminder>(new Ex2ByDayEntities());
        }

        public CreatedRemindersResponse Insert(EventReminderCommand eventReminderCommand,List<CreatedEventResponse> resEvent)
        {
            CreatedRemindersResponse response = new CreatedRemindersResponse();
            Reminder eventReminder = new Reminder();

            if (eventReminderCommand.EventId == 0)
            {
                foreach (var data in resEvent)
                {
                    if (eventReminderCommand.TempEventId == data.TempId)
                    {
                        eventReminder = new ByDay.Data.Reminder()
                        {
                            EventId = data.Id,
                            Method = eventReminderCommand.Method,
                            Duration = eventReminderCommand.Duration,
                            CreatedAt = DateTime.UtcNow
                        };
                    }
                }
            }
            else
            {
                eventReminder = new ByDay.Data.Reminder()
                {
                    EventId = eventReminderCommand.EventId,
                    Method = eventReminderCommand.Method,
                    Duration = eventReminderCommand.Duration,
                    CreatedAt = DateTime.UtcNow
                };
            }

            _eventReminderRepository.Add(eventReminder);

            if (eventReminder.Id != 0)
            {
                response.TempId = eventReminderCommand.TempId;
                response.TempEventId = eventReminderCommand.TempEventId;
                response.Id = eventReminder.Id;
                response.Success = true;

            }
            return response;
        }

        public List<ModifiedRemindersResponse> Get(long eventId)
        {
            ModifiedRemindersResponse response = new ModifiedRemindersResponse();
            List<ModifiedRemindersResponse> currentEventReminders = _eventReminderRepository.FindBy(x => x.EventId == eventId).Select(x => new ModifiedRemindersResponse() {
                Id=x.Id
             }).ToList();
            return currentEventReminders;
        }


        public ModifiedRemindersResponse Update(EventReminderCommand eventReminderCommand)
        {
            ModifiedRemindersResponse response = new ModifiedRemindersResponse();
            var currentEventReminder = _eventReminderRepository.FindBy(x => x.Id == eventReminderCommand.Id).FirstOrDefault();
            currentEventReminder.EventId = eventReminderCommand.EventId;
            currentEventReminder.Method = eventReminderCommand.Method;
            currentEventReminder.Duration = eventReminderCommand.Duration;
            currentEventReminder.ModifiedAt = DateTime.UtcNow;


            //var eventReminder = new ByDay.Data.Reminder()
            //{
            //    Id = eventReminderCommand.Id,
            //    EventId = eventReminderCommand.EventId,
            //    Method = eventReminderCommand.Method,
            //    Duration=eventReminderCommand.Duration,
            //    ModifiedAt = DateTime.Now

            //};
            _eventReminderRepository.Update(currentEventReminder);

            if (currentEventReminder.ModifiedAt != null)
            {
                response.Id = currentEventReminder.Id;
                response.Success = true;

            }
            return response;
        }

        public DeletedRemindersResponse Delete(EventReminderCommand eventReminderCommand)
        {
            DeletedRemindersResponse response = new DeletedRemindersResponse();
            var currentEventReminder = _eventReminderRepository.FindBy(x => x.Id == eventReminderCommand.Id).ToList();
            foreach (var data in currentEventReminder)
            {
                //data.IsDeleted = true;
                //data.ModifiedAt = DateTime.UtcNow;
                _eventReminderRepository.Delete(data);
                response.Id = data.Id;
                response.Success = true;
            }
            return response;
        }


    }
}
