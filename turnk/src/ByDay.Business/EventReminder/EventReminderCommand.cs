﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.EventReminder
{
  public class EventReminderCommand
    {

        public int Id { get; set; }
        public int EventId { get; set; }
        public int Method { get; set; }
        public Nullable<int> Duration { get; set; }

        public string TempId { get; set; }

        public string TempEventId { get; set; }

        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public bool IsDeleted { get; set; }
        public int VersionNumber { get; set; }

        // public virtual Event Event { get; set; }
        // public virtual ReminderMethod ReminderMethod { get; set; }


    }
}
