﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.EventReminder
{
 public class EventReminderService
    {
     
        private readonly ByDayRepository<Reminder> _reminderRepository;
     
        public EventReminderService()
        {     
            _reminderRepository = new ByDayRepository<Reminder>(new Ex2ByDayEntities());
        }


        public void Insert(EventReminderCommand remind, Event eventDetail)
        {
            Reminder reminder = new Reminder();
            reminder.EventId = eventDetail.Id;
            reminder.Method = remind.Method;
            reminder.Duration = remind.Duration;
            reminder.CreatedAt = DateTime.UtcNow;
            _reminderRepository.Add(reminder);
            eventDetail.Reminders.Add(reminder);
        }
    }
}
