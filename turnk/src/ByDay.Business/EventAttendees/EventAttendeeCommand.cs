﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.EventAttendees
{
  public class EventAttendeeCommand
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public int CalendarId { get; set; }
        public bool IsOrganiser { get; set; }

        public string TempId { get; set; }

        public string TempEventId { get; set; }

        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public bool IsDeleted { get; set; }
        public int UserId { get; set; }
        public int VersionNumber { get; set; }
        //   public virtual Calendar Calendar { get; set; }

        //   public virtual Event Event { get; set; }


    }
}
