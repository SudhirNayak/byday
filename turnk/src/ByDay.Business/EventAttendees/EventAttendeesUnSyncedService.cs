﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ByDay.Business.EventDetail;
using ByDay.Data;

namespace ByDay.Business.EventAttendees
{
    public class EventAttendeesUnSyncedService
    {

        private readonly ByDayRepository<EventAttendee> _eventAttendeeRepository;

        public EventAttendeesUnSyncedService()
        {
            _eventAttendeeRepository = new ByDayRepository<EventAttendee>(new Ex2ByDayEntities());
        }

        public CreatedAttendeesResponse Insert(EventAttendeeCommand eventAttendeeCommand, List<CreatedEventResponse> resEvent)
        {
            CreatedAttendeesResponse response = new CreatedAttendeesResponse();
            EventAttendee eventAttendee = new EventAttendee();

            if (eventAttendeeCommand.EventId == 0)
            {
                foreach (var data in resEvent)
                {
                    if (eventAttendeeCommand.TempEventId == data.TempId)
                    {
                        eventAttendee = new ByDay.Data.EventAttendee()
                        {

                            EventId = data.Id,
                            CalendarId = eventAttendeeCommand.CalendarId,
                            IsOrganiser = eventAttendeeCommand.IsOrganiser,
                            IsDeleted = eventAttendeeCommand.IsDeleted,
                            CreatedAt = DateTime.UtcNow

                        };
                    }
                }
            }
            else
            {

                eventAttendee = new ByDay.Data.EventAttendee()
                {

                    EventId = eventAttendeeCommand.EventId,
                    CalendarId = eventAttendeeCommand.CalendarId,
                    IsOrganiser = eventAttendeeCommand.IsOrganiser,
                    IsDeleted = eventAttendeeCommand.IsDeleted,
                    CreatedAt = DateTime.UtcNow

                };
            }
            _eventAttendeeRepository.Add(eventAttendee);

            if (eventAttendee.Id != 0)
            {
                response.TempId = eventAttendeeCommand.TempId;
                response.TempEventId = eventAttendeeCommand.TempEventId;
                response.Id = eventAttendee.Id;
                response.Success = true;

            }
            return response;
        }

        public List<ModifiedAttendeesResponse> Get(long eventId)
        {
            List<ModifiedAttendeesResponse> currentAttendeeReminders = _eventAttendeeRepository.FindBy(x => x.EventId == eventId).Select(x => new ModifiedAttendeesResponse()
            {
                Id = x.Id
            }).ToList();
            return currentAttendeeReminders;
        }

        public ModifiedAttendeesResponse Update(EventAttendeeCommand eventAttendeeCommand)
        {
            ModifiedAttendeesResponse response = new ModifiedAttendeesResponse();
            var currentEvent = _eventAttendeeRepository.FindBy(x => x.Id == eventAttendeeCommand.Id).FirstOrDefault();

            currentEvent.EventId = eventAttendeeCommand.EventId;
            currentEvent.CalendarId = eventAttendeeCommand.CalendarId;
            currentEvent.IsOrganiser = eventAttendeeCommand.IsOrganiser;
            currentEvent.IsDeleted = eventAttendeeCommand.IsDeleted;
            currentEvent.ModifiedAt = DateTime.UtcNow;
            //var eventAttendee = new ByDay.Data.EventAttendee()
            //{
            //    Id = eventAttendeeCommand.Id,
            //    EventId = eventAttendeeCommand.EventId,
            //    CalendarId = eventAttendeeCommand.CalendarId,
            //    IsOrganiser = eventAttendeeCommand.IsOrganiser,
            //    IsDeleted = eventAttendeeCommand.IsDeleted,
            //    CreatedAt = createdDate,
            //    ModifiedAt = DateTime.Now

            //};
            _eventAttendeeRepository.Update(currentEvent);

            if (currentEvent.ModifiedAt != null)
            {
                response.Id = currentEvent.Id;
                response.Success = true;

            }
            return response;
        }

        public DeletedAttendeesResponse Delete(EventAttendeeCommand eventAttendeeCommand)
        {
            DeletedAttendeesResponse response = new DeletedAttendeesResponse();
            var currentEventAttendee = _eventAttendeeRepository.FindBy(x => x.Id == eventAttendeeCommand.Id).ToList();
            foreach (var data in currentEventAttendee)
            {
                //data.IsDeleted = true;
                //data.ModifiedAt = DateTime.UtcNow;
                _eventAttendeeRepository.Delete(data);

                response.Id = data.Id;
                response.Success = true;
            }
            return response;
        }


    }
}
