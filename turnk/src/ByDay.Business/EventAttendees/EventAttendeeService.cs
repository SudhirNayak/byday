﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ByDay.Data;

namespace ByDay.Business.EventAttendees
{
 public  class EventAttendeeService
    {
      
        private readonly ByDayRepository<EventAttendee> _eventAttendeeRepository;
    
        public EventAttendeeService()
        {
            _eventAttendeeRepository = new ByDayRepository<EventAttendee>(new Ex2ByDayEntities());
        }

        public void Insert(EventAttendeeCommand eventAttendeeCommand,Event eventDetail)
        {
            EventAttendee eventAttendee = new EventAttendee();
            eventAttendee.EventId = eventDetail.Id;
            eventAttendee.CalendarId = eventAttendeeCommand.CalendarId;
            eventAttendee.IsOrganiser = eventAttendeeCommand.IsOrganiser;
            eventAttendee.CreatedAt = DateTime.UtcNow;
            _eventAttendeeRepository.Add(eventAttendee);
            eventDetail.EventAttendees.Add(eventAttendee);

        }
    }
}
