﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.RecipeUserFeedBack
{
  public class RecipeUserFeedBackService
    {
        ByDayRepository<ByDay.Data.RecipeUserFeedback> _recipeFeedBackRepo = new ByDayRepository<ByDay.Data.RecipeUserFeedback>(new Ex2ByDayEntities());


        public int UpdateUserFeedBack(Business.RecipeDetailUnSynced.RecipeUserFeedBack item)
        {
       
            Data.RecipeUserFeedback feedback = new Data.RecipeUserFeedback();
          
            if (item.Id != 0)
            {
                feedback = _recipeFeedBackRepo.Find(item.Id);

                feedback.UserId = item.UserId;
                feedback.RecipeId = item.RecipeId;
                feedback.IsFavourite = item.IsFavourite;
                feedback.IsHidden = item.IsHidden;
                feedback.Rating = item.Rating;
                feedback.Comments = item.Comments;
                feedback.Guid = item.Guid;
                // feedback.IsDeleted = item.IsDeleted;
                feedback.ModifiedAt = DateTime.UtcNow;
                _recipeFeedBackRepo.Update(feedback);
            }
            else
            {
               
                feedback.UserId = item.UserId;
                feedback.RecipeId = item.RecipeId;
                feedback.IsFavourite = item.IsFavourite;
                feedback.IsHidden = item.IsHidden;
                feedback.Rating = item.Rating;
                feedback.Comments = item.Comments;
                feedback.Guid = item.Guid;
                feedback.IsDeleted = item.IsDeleted;
                feedback.CreatedAt = DateTime.UtcNow;
                _recipeFeedBackRepo.Add(feedback);

            }
            return feedback.Id;
        }

    }
}
