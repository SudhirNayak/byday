﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.RecipeDetailUnSynced
{
   public class UnSyncedNewRecipeModel
    {


        public UnSyncedNewRecipeModel()
        {

            Recipes = new List<RecipeRespCommand>();
            Ingredients = new List<RespIngredient>();
            UserSpecificRecipeDetails = new List<RecipeRespUserFeedBack>();
            Categories = new List<RecipeRespCategory>();
            Images = new List<RecipeRespImage>();
        }
        public string LastSync
        {
            get
            {
                return DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            }
        }
        public List<RecipeRespCommand> Recipes { get; set; }
        public List<RespIngredient> Ingredients { get; set; }
        public List<RecipeRespUserFeedBack> UserSpecificRecipeDetails { get; set; }
        public List<RecipeRespCategory> Categories { get; set; }
        public List<RecipeRespImage> Images { get; set; }
    }


    public class RecipeRespCommand
    {
     
        public int Id { get; set; }
        public int UserId { get; set; }
        public string RecipeName { get; set; }
        public string Description { get; set; }

        public string Instructions { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public string Guid { get; set; }
        public string Url { get; set; }

        public Nullable<int> CookingTime { get; set; }
        public Nullable<int> PrepTime { get; set; }
        public Nullable<int> NumServings { get; set; }

        public string Simplicity { get; set; }

        public bool IsAdmin { get; set; }
        public Nullable<bool> IsDeleted { get; set; }

  

    }

    public class RecipeRespCategory
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int RecipeId { get; set; }
        public string Guid { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }

    public class RecipeRespImage
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public int RecipeId { get; set; }
        public string Path { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public string Guid { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }

    public class RecipeRespUserFeedBack
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RecipeId { get; set; }
        public bool IsFavourite { get; set; }
        public bool IsHidden { get; set; }
        public Nullable<int> Rating { get; set; }
        public string Comments { get; set; }
        public string Guid { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public Nullable<bool> IsDeleted { get; set; }

    }
    public class RespIngredient
    {
        public int Id { get; set; }
        public int RecipeId { get; set; }
        public string Name { get; set; }
        public Nullable<int> Quantity { get; set; }
        public string QuantityType { get; set; }
        public string Guid { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }
}
