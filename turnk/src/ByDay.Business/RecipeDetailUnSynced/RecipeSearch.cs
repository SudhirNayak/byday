﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.RecipeDetailUnSynced
{
   public class RecipeSearch
    {
        public List<string> Keys { get; set; }
        public int Method { get; set; }
        public int Scope { get; set; }
        public PageDetail PageDetail { get; set; }


    }

    public class PageDetail
    {
       public int PageSize { get; set; }
       public int LastRecord { get; set; }
    }

    public class RecipeBasedSearch
    {
        public List<RecipeBSearch> Recipe { get; set; }
    }
    public class RecipeBSearch
    {
        public int RecipeId { get; set; }
        public long LastSync { get; set; }
    }
}
