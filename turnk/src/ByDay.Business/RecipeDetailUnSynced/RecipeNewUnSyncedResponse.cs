﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.RecipeDetailUnSynced
{
   public class RecipeNewUnSyncedResponse
    {
        public RecipeNewUnSyncedResponse()
        {
            CreatedRecipes = new List<CreUpDelRecipeNewResponse>();
            ModifiedRecipes = new List<CreUpDelRecipeNewResponse>();
            DeletedRecipes = new List<CreUpDelRecipeNewResponse>();
            CreatedIngredients = new List<CreUpDelRecipeNewResponse>();
            ModifiedIngredients = new List<CreUpDelRecipeNewResponse>();
            DeletedIngredients = new List<CreUpDelRecipeNewResponse>();
            CreatedCategories = new List<CreUpDelRecipeNewResponse>();
            DeletedCategories = new List<CreUpDelRecipeNewResponse>();
            CreatedUserSpecificRecipeDetails = new List<CreUpDelRecipeNewResponse>();
            ModifiedUserSpecificRecipeDetails = new List<CreUpDelRecipeNewResponse>();
            DeletedUserSpecificRecipeDetails = new List<CreUpDelRecipeNewResponse>();
            CreatedImages = new List<CreUpDelRecipeNewResponse>();
            ModifiedImages = new List<CreUpDelRecipeNewResponse>();
            DeletedImages = new List<CreUpDelRecipeNewResponse>();

        }
        public string LastSync
        {
            get
            {
                return DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            }
        }


        public List<CreUpDelRecipeNewResponse> CreatedRecipes { get; set; }
        public List<CreUpDelRecipeNewResponse> ModifiedRecipes { get; set; }
        public List<CreUpDelRecipeNewResponse> DeletedRecipes { get; set; }

        public List<CreUpDelRecipeNewResponse> CreatedIngredients { get; set; }
        public List<CreUpDelRecipeNewResponse> ModifiedIngredients { get; set; }
        public List<CreUpDelRecipeNewResponse> DeletedIngredients { get; set; }

        public List<CreUpDelRecipeNewResponse> CreatedCategories { get; set; }
        public List<CreUpDelRecipeNewResponse> DeletedCategories { get; set; }
       
        
        public List<CreUpDelRecipeNewResponse> CreatedUserSpecificRecipeDetails { get; set; }
        public List<CreUpDelRecipeNewResponse> ModifiedUserSpecificRecipeDetails { get; set; }
        public List<CreUpDelRecipeNewResponse> DeletedUserSpecificRecipeDetails { get; set; }



        public List<CreUpDelRecipeNewResponse> CreatedImages { get; set; }
        public List<CreUpDelRecipeNewResponse> ModifiedImages { get; set; }
        public List<CreUpDelRecipeNewResponse> DeletedImages { get; set; }

        
    }
    public class CreUpDelRecipeNewResponse
    {
        public string TempId { get; set; }

        public int Id { get; set; }

        public bool Success { get; set; }
    }
}

