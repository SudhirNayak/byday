﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.RecipeDetailUnSynced
{
  public  class RecipeNewUnSyncedCommand
    {
        public RecipeNewUnSyncedCommand()
        {
            CreatedRecipes = new List<RecipeCommand>();
            ModifiedRecipes = new List<RecipeCommand>();
            DeletedRecipes = new List<RecipeCommand>();
            CreatedIngredients = new List<Ingredient>();
            ModifiedIngredients = new List<Ingredient>();
            DeletedIngredients = new List<Ingredient>();
            CreatedCategories = new List<RecipeCategory>();
            DeletedCategories = new List<RecipeCategory>();
            CreatedUserSpecificRecipeDetails = new List<RecipeUserFeedBack>();
            ModifiedUserSpecificRecipeDetails = new List<RecipeUserFeedBack>();
            DeletedUserSpecificRecipeDetails = new List<RecipeUserFeedBack>();
            CreatedImages = new List<RecipeImage>();
            ModifiedImages = new List<RecipeImage>();
            DeletedImages = new List<RecipeImage>();

        }
        public List<RecipeCommand> CreatedRecipes { get; set; }
        public List<RecipeCommand> ModifiedRecipes { get; set; }
        public List<RecipeCommand> DeletedRecipes { get; set; }

        public List<Ingredient> CreatedIngredients { get; set; }
        public List<Ingredient> ModifiedIngredients { get; set; }
        public List<Ingredient> DeletedIngredients { get; set; }

        public List<RecipeCategory> CreatedCategories { get; set; }
        public List<RecipeCategory> DeletedCategories { get; set; }

        public List<RecipeUserFeedBack> CreatedUserSpecificRecipeDetails { get; set; }
        public List<RecipeUserFeedBack> ModifiedUserSpecificRecipeDetails { get; set; }
        public List<RecipeUserFeedBack> DeletedUserSpecificRecipeDetails { get; set; }

        public List<RecipeImage> CreatedImages { get; set; }
        public List<RecipeImage> ModifiedImages { get; set; }
        public List<RecipeImage> DeletedImages { get; set; }
    }
}
