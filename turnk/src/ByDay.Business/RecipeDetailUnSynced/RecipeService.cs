﻿using ByDay.Data;
using ByDay.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.RecipeDetailUnSynced
{
    public class RecipeService
    {
        ByDayRepository<ByDay.Data.Recipe> _recipeRepo = new ByDayRepository<ByDay.Data.Recipe>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.RecipeUserFeedback> _recipeFeedBackRepo = new ByDayRepository<ByDay.Data.RecipeUserFeedback>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.RecipeImage> _recipeImageRepo = new ByDayRepository<ByDay.Data.RecipeImage>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.Ingredient> _ingredientRepo = new ByDayRepository<ByDay.Data.Ingredient>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.RecipeCategoryMapping> _recipeCategoryRepo = new ByDayRepository<ByDay.Data.RecipeCategoryMapping>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.RecipeIngredientMapping> _ingredientMappingRepo = new ByDayRepository<ByDay.Data.RecipeIngredientMapping>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.SyncLog> _syncLogRepository = new ByDayRepository<ByDay.Data.SyncLog>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.MealPlan> _planRepository = new ByDayRepository<ByDay.Data.MealPlan>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.BulletinBoard> _bulletinRepository = new ByDayRepository<ByDay.Data.BulletinBoard>(new Ex2ByDayEntities());

        ByDayRepository<ByDay.Data.Category> _categoryRepo = new ByDayRepository<ByDay.Data.Category>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.Type> _typeRepo = new ByDayRepository<ByDay.Data.Type>(new Ex2ByDayEntities());



        public IQueryable<ByDay.Data.Recipe> Get(int id)

        {
            var recipes = _recipeRepo.FindBy(x => x.UserId == id).ToList();
            return recipes.AsQueryable();
        }
        public IQueryable<ByDay.Data.Recipe> Get()

        {
            var recipes = _recipeRepo.All;
            return recipes.AsQueryable();
        }
        public IQueryable<Recipe> GetUnSyncedRecipe(string userID, DateTime syncedDate)
        {
            var recipes = Get(FieldConverter.To<int>(userID));
            var recipesResult = recipes.GroupBy(x => x.Id).Select(b => b.FirstOrDefault());

            List<Recipe> mealPlan = new List<Recipe>();
            mealPlan.AddRange(recipesResult.Where(cr => cr.CreatedAt != null).Where(x => x.CreatedAt.Value > syncedDate && x.IsDeleted == false));
            mealPlan.AddRange(recipesResult.Select(a => a).Where(y => y.ModifiedAt != null).Where(x => x.ModifiedAt.Value > syncedDate && x.IsDeleted == false));
            mealPlan.AddRange(recipesResult.Select(b => b).Where(x => x.ModifiedAt != null && x.CreatedAt != null).Where(y => y.CreatedAt.Value <= syncedDate && y.ModifiedAt > syncedDate && y.IsDeleted == true));

            var mealsResults = mealPlan.GroupBy(x => x.Id).Select(b => b.FirstOrDefault());
            return mealsResults.OrderBy(y => y.Id).AsQueryable();
        }

        public IQueryable<Data.Ingredient> GetUnSyncedIngredient(string userID, DateTime syncedDate)
        {

            var recipes = Get(FieldConverter.To<int>(userID));
            var ingredientResult = recipes.GroupBy(x => x.Id).Select(b => b.FirstOrDefault());
            List<Data.Ingredient> selectedIngredient = new List<Data.Ingredient>();
            List<RecipeIngredientMapping> ingreMap = new List<RecipeIngredientMapping>();

            List<Data.Ingredient> ResIngredient = new List<Data.Ingredient>();
            foreach (var data in ingredientResult)
            {
                ingreMap.AddRange(data.RecipeIngredientMappings.ToList());
            }
            foreach (var ingre in ingreMap)
            {

                selectedIngredient.AddRange(_ingredientRepo.FindBy(x => x.Id == ingre.IngredientId));
            }

            ResIngredient.AddRange(selectedIngredient.Where(cr => cr.CreatedAt != null).Where(x => x.CreatedAt.Value > syncedDate && x.IsDeleted == false));
            ResIngredient.AddRange(selectedIngredient.Select(a => a).Where(y => y.ModifiedAt != null).Where(x => x.ModifiedAt.Value > syncedDate && x.IsDeleted == false));
            ResIngredient.AddRange(selectedIngredient.Select(b => b).Where(x => x.ModifiedAt != null && x.CreatedAt != null).Where(y => y.CreatedAt.Value <= syncedDate && y.ModifiedAt > syncedDate && y.IsDeleted == true));
            var Results = ResIngredient.GroupBy(x => x.Id).Select(y => y.FirstOrDefault());
            return Results.OrderBy(y => y.Id).AsQueryable();

        }


        public IQueryable<Data.RecipeUserFeedback> GetUnSyncedUserFeedBack(string userID, DateTime syncedDate)
        {

            var recipes = Get(FieldConverter.To<int>(userID));
            var recipesResult = recipes.GroupBy(x => x.Id).Select(b => b.FirstOrDefault());
            List<Data.RecipeUserFeedback> selectedUserFeedBack = new List<Data.RecipeUserFeedback>();

            List<Data.RecipeUserFeedback> ResUserFeedBack = new List<Data.RecipeUserFeedback>();

            foreach (var rec in recipesResult)
            {
                selectedUserFeedBack.AddRange(_recipeFeedBackRepo.FindBy(x => x.RecipeId == rec.Id).ToList());
            }
            ResUserFeedBack.AddRange(selectedUserFeedBack.Where(cr => cr.CreatedAt != null).Where(x => x.CreatedAt.Value > syncedDate && x.IsDeleted == false));
            ResUserFeedBack.AddRange(selectedUserFeedBack.Select(a => a).Where(y => y.ModifiedAt != null).Where(x => x.ModifiedAt.Value > syncedDate && x.IsDeleted == false));
            ResUserFeedBack.AddRange(selectedUserFeedBack.Select(b => b).Where(x => x.ModifiedAt != null && x.CreatedAt != null).Where(y => y.CreatedAt.Value <= syncedDate && y.ModifiedAt > syncedDate && y.IsDeleted == true));
            var Results = ResUserFeedBack.GroupBy(x => x.Id).Select(y => y.FirstOrDefault());
            return Results.OrderBy(y => y.Id).AsQueryable();

        }


        public IQueryable<Data.RecipeCategoryMapping> GetUnSyncedCategory(string userID, DateTime syncedDate)
        {

            var recipes = Get(FieldConverter.To<int>(userID));
            var recipesResult = recipes.GroupBy(x => x.Id).Select(b => b.FirstOrDefault());
            List<Data.RecipeCategoryMapping> selectedCategory = new List<Data.RecipeCategoryMapping>();

            List<Data.RecipeCategoryMapping> ResCategory = new List<Data.RecipeCategoryMapping>();

            foreach (var ingre in recipesResult)
            {
                selectedCategory.AddRange(_recipeCategoryRepo.FindBy(x => x.RecipeId == ingre.Id).ToList());
            }
            ResCategory.AddRange(selectedCategory.Where(cr => cr.CreatedAt != null).Where(x => x.CreatedAt.Value > syncedDate && x.IsDeleted == false));
            ResCategory.AddRange(selectedCategory.Select(a => a).Where(y => y.ModifiedAt != null).Where(x => x.ModifiedAt.Value > syncedDate && x.IsDeleted == false));
            ResCategory.AddRange(selectedCategory.Select(b => b).Where(x => x.ModifiedAt != null && x.CreatedAt != null).Where(y => y.CreatedAt.Value <= syncedDate && y.ModifiedAt > syncedDate && y.IsDeleted == true));
            var Results = ResCategory.GroupBy(x => x.Id).Select(y => y.FirstOrDefault());
            return Results.OrderBy(y => y.Id).AsQueryable();

        }

        public IQueryable<Data.RecipeImage> GetUnSyncedRecipeImage(string userID, DateTime syncedDate)
        {

            var recipes = Get(FieldConverter.To<int>(userID));
            var recipesResult = recipes.GroupBy(x => x.Id).Select(b => b.FirstOrDefault());
            List<Data.RecipeImage> selectedImage = new List<Data.RecipeImage>();

            List<Data.RecipeImage> ResImage = new List<Data.RecipeImage>();

            foreach (var img in recipesResult)
            {
                selectedImage.AddRange(_recipeImageRepo.FindBy(x => x.RecipeId == img.Id).ToList());
            }
            ResImage.AddRange(selectedImage.Where(cr => cr.CreatedAt != null).Where(x => x.CreatedAt.Value > syncedDate && x.IsDeleted == false));
            ResImage.AddRange(selectedImage.Select(a => a).Where(y => y.ModifiedAt != null).Where(x => x.ModifiedAt.Value > syncedDate && x.IsDeleted == false));
            ResImage.AddRange(selectedImage.Select(b => b).Where(x => x.ModifiedAt != null && x.CreatedAt != null).Where(y => y.CreatedAt.Value <= syncedDate && y.ModifiedAt > syncedDate && y.IsDeleted == true));
            var Results = ResImage.GroupBy(x => x.Id).Select(y => y.FirstOrDefault());
            return Results.OrderBy(y => y.Id).AsQueryable();

        }



        public IQueryable<Recipe> GetUnSyncedRecipeByRecipeId(int recipeId, DateTime syncedDate)
        {
            var recipes = Get();
            var recipesResult = recipes.Where(y => y.Id == recipeId).GroupBy(x => x.Id).Select(b => b.FirstOrDefault());

            List<Recipe> mealPlan = new List<Recipe>();
            mealPlan.AddRange(recipesResult.Where(cr => cr.CreatedAt != null).Where(x => x.CreatedAt.Value > syncedDate && x.IsDeleted == false));
            mealPlan.AddRange(recipesResult.Select(a => a).Where(y => y.ModifiedAt != null).Where(x => x.ModifiedAt.Value > syncedDate && x.IsDeleted == false));
            mealPlan.AddRange(recipesResult.Select(b => b).Where(x => x.ModifiedAt != null && x.CreatedAt != null).Where(y => y.CreatedAt.Value <= syncedDate && y.ModifiedAt > syncedDate && y.IsDeleted == true));

            var mealsResults = mealPlan.GroupBy(x => x.Id).Select(b => b.FirstOrDefault());
            return mealsResults.OrderBy(y => y.Id).AsQueryable();
        }
        public IEnumerable<RecipeResponseImage> GetImagesOfRecipe(Recipe recipeDetails)
        {

            List<RecipeResponseImage> response = new List<RecipeResponseImage>();
            var recipeImages = recipeDetails.RecipeImages.ToList();
            foreach (var recipeImg in recipeImages)
            {

                var data = new RecipeResponseImage()
                {
                    Id = recipeImg.Id,

                    Name = recipeImg.Name,
                    Path = recipeImg.Path,
                    RecipeId = recipeImg.RecipeId
                };
                response.Add(data);
            }

            return response;
        }


        public IQueryable<Category> GetRecipeCategory()
        {
            return _categoryRepo.All;
        }

        public IQueryable<ByDay.Data.Type> GetSimplicityLevel(Nullable<int> id)
        {
            return _typeRepo.FindBy(x => x.Type1 == "5" && x.Id == id);
        }

        public IQueryable<Data.RecipeUserFeedback> GetUserFevoriteId(int userId)
        {
            var res = _recipeFeedBackRepo.FindBy(x => x.UserId == userId && x.IsFavourite == true).ToList();

            return res.AsQueryable();
        }



        public CreatedUpdateedDeletedRecipeResponse Insert(RecipeCommand command)
        {
            CreatedUpdateedDeletedRecipeResponse response = new CreatedUpdateedDeletedRecipeResponse();
            var recipeDetail = new Recipe()
            {
                UserId = command.UserId,
                RecipeName = command.RecipeName,
                Description = command.Description,
                Instructions = command.Instructions,
                CookingTime = command.CookingTime,
                CreatedAt = DateTime.UtcNow,
                ModifiedAt = command.ModifiedAt,
                Guid = command.Guid,
                Url = command.Url,
                // Direction=command.Direction,
                IsDeleted = command.IsDeleted
            };
            _recipeRepo.Add(recipeDetail);
            foreach (var item in command.UserSpecificRecipeDetails)
            {
                Data.RecipeUserFeedback feedback = new Data.RecipeUserFeedback();
                feedback.UserId = command.UserId;
                feedback.RecipeId = recipeDetail.Id;
                feedback.IsFavourite = item.IsFavourite;
                feedback.IsHidden = item.IsHidden;
                feedback.Rating = item.Rating;
                feedback.Comments = item.Comments;
                feedback.Guid = item.Guid;
                feedback.CreatedAt = DateTime.UtcNow;
                feedback.IsDeleted = item.IsDeleted;

                _recipeFeedBackRepo.Add(feedback);
            }
            // _recipeRepo.Add(recipeDetail);
            foreach (var item in command.Ingredients)
            {
                Data.Ingredient ingre = new Data.Ingredient();
                if (item.Name != null)
                {
                    ingre.Name = item.Name;
                    ingre.Guid = item.Guid;
                    ingre.CreatedAt = DateTime.UtcNow;
                    ingre.IsDeleted = item.IsDeleted;
                    _ingredientRepo.Add(ingre);
                    RecipeIngredientMapping q = new RecipeIngredientMapping();
                    q.IngredientId = ingre.Id;
                    q.RecipeId = recipeDetail.Id;
                    // recipeDetail.RecipeIngredientMappings.Add(q);
                    _ingredientMappingRepo.Add(q);
                }
            }
            foreach (var item in command.Images)
            {
                Data.RecipeImage image = new Data.RecipeImage();
                image.Name = item.Name;
                image.RecipeId = recipeDetail.Id;
                image.Path = item.Path;
                image.CreatedAt = DateTime.UtcNow;
                image.Guid = item.Guid;
                image.IsDeleted = item.IsDeleted;
                _recipeImageRepo.Add(image);
            }
            foreach (var item in command.Categories)
            {
                RecipeCategoryMapping category = new RecipeCategoryMapping();
                category.CategoryId = item.CategoryId;
                category.RecipeId = recipeDetail.Id;
                category.Guid = item.Guid;
                category.CreatedAt = DateTime.UtcNow;
                // recipeDetail.RecipeCategoryMappings.Add(category);
                _recipeCategoryRepo.Add(category);
            }


            if (recipeDetail.Id != 0)
            {
                response.Id = recipeDetail.Id;
                response.TempId = command.TempId;
                response.Success = true;
            }
            return response;
        }



        public CreatedUpdateedDeletedRecipeResponse Update(RecipeCommand command)
        {
            CreatedUpdateedDeletedRecipeResponse response = new CreatedUpdateedDeletedRecipeResponse();
            var recipe = _recipeRepo.Find(command.Id);
            if (recipe.IsAdmin == false)
            {
                recipe.UserId = command.UserId;
                recipe.RecipeName = command.RecipeName;
                // recipe.Description = command.Description;
                recipe.CookingTime = command.CookingTime;
                recipe.Instructions = command.Instructions;
                recipe.ModifiedAt = DateTime.UtcNow;
                recipe.Guid = command.Guid;
                recipe.Url = command.Url;
                recipe.IsDeleted = command.IsDeleted;


                // _recipeRepo.Add(recipeDetail);


                foreach (var item in command.UserSpecificRecipeDetails)
                {
                    Data.RecipeUserFeedback feedback = new Data.RecipeUserFeedback();
                    feedback = _recipeFeedBackRepo.Find(item.Id);
                    feedback.Id = item.Id;
                    feedback.UserId = command.UserId;
                    feedback.RecipeId = recipe.Id;
                    feedback.IsFavourite = item.IsFavourite;
                    feedback.IsHidden = item.IsHidden;
                    feedback.Rating = item.Rating;
                    feedback.Comments = item.Comments;
                    feedback.Guid = item.Guid;
                    feedback.ModifiedAt = DateTime.UtcNow;
                    feedback.IsDeleted = item.IsDeleted;

                    _recipeFeedBackRepo.Update(feedback);
                }
                // _recipeRepo.Add(recipeDetail);
                foreach (var item in command.Ingredients)
                {
                    Data.Ingredient ingre = new Data.Ingredient();
                    ingre = _ingredientRepo.Find(item.Id);
                    if (item.Name != null)
                    {
                        ingre.Id = item.Id;
                        ingre.Name = item.Name;
                        ingre.Guid = item.Guid;
                        ingre.ModifiedAt = DateTime.UtcNow;
                        ingre.IsDeleted = item.IsDeleted;
                        _ingredientRepo.Update(ingre);

                    }
                }
                foreach (var item in command.Images)
                {
                    Data.RecipeImage image = new Data.RecipeImage();
                    image = _recipeImageRepo.Find(item.Id);
                    image.Id = item.Id;
                    image.Name = item.Name;
                    image.RecipeId = recipe.Id;
                    image.Path = item.Path;
                    image.ModifiedAt = DateTime.UtcNow;
                    image.Guid = item.Guid;
                    image.IsDeleted = item.IsDeleted;
                    _recipeImageRepo.Update(image);
                }


                foreach (var item in command.Categories)
                {
                    RecipeCategoryMapping category = new RecipeCategoryMapping();
                    category = _recipeCategoryRepo.Find(item.Id);
                    category.Id = item.Id;
                    category.CategoryId = item.CategoryId;
                    category.RecipeId = recipe.Id;
                    category.Guid = item.Guid;
                    category.ModifiedAt = item.ModifiedAt;

                    _recipeCategoryRepo.Update(category);
                }


                if (recipe.Id != 0)
                {
                    response.Id = recipe.Id;
                    response.TempId = command.TempId;
                    response.Success = true;
                }
                _recipeRepo.Update(recipe);
            }

            return response;
        }

        public CreatedUpdateedDeletedRecipeResponse Delete(RecipeCommand command)
        {
            CreatedUpdateedDeletedRecipeResponse response = new CreatedUpdateedDeletedRecipeResponse();
            var recipe = _recipeRepo.Find(command.Id);
            if (recipe.IsAdmin == false)
            {

                recipe.IsDeleted = true;
                recipe.ModifiedAt = DateTime.UtcNow;
                _recipeRepo.Update(recipe);
            }
            if (recipe.Id != 0)
            {
                response.Id = recipe.Id;
                response.TempId = command.TempId;
                response.Success = true;
            }

            return response;
        }




        public CreUpDelRecipeNewResponse InsertRecipe(RecipeCommand command)
        {
            CreUpDelRecipeNewResponse response = new CreUpDelRecipeNewResponse();
            var recipeDetail = new Recipe()
            {
                UserId = command.UserId,
                RecipeName = command.RecipeName,
                Description = command.Description,
                Instructions = command.Instructions,
                CookingTime = command.CookingTime,
                CreatedAt = DateTime.UtcNow,
                // ModifiedAt = command.ModifiedAt,
                Guid = command.Guid,
                Url = command.Url,
                // Direction=command.Direction,
                IsDeleted = command.IsDeleted
            };
            _recipeRepo.Add(recipeDetail);
            if (recipeDetail.Id != 0)
            {
                response.Id = recipeDetail.Id;
                response.TempId = command.TempId;
                response.Success = true;
            }
            return response;
        }

        public CreUpDelRecipeNewResponse UpdateRecipe(RecipeCommand command)
        {
            CreUpDelRecipeNewResponse response = new CreUpDelRecipeNewResponse();
            var recipe = _recipeRepo.Find(command.Id);
            if (recipe.IsAdmin == false)
            {
                recipe.UserId = command.UserId;
                recipe.RecipeName = command.RecipeName;
                recipe.Description = command.Description;
                recipe.CookingTime = command.CookingTime;
                recipe.Instructions = command.Instructions;
                recipe.ModifiedAt = DateTime.UtcNow;
                recipe.Guid = command.Guid;
                recipe.Url = command.Url;
                recipe.IsDeleted = command.IsDeleted;
                if (recipe.Id != 0)
                {
                    response.Id = recipe.Id;
                    response.TempId = command.TempId;
                    response.Success = true;
                }
                _recipeRepo.Update(recipe);
            }

            return response;
        }

        public CreUpDelRecipeNewResponse DeleteRecipe(RecipeCommand command)
        {
            CreUpDelRecipeNewResponse response = new CreUpDelRecipeNewResponse();
            var recipe = _recipeRepo.Find(command.Id);
            if (recipe.IsAdmin == false)
            {
                recipe.IsDeleted = true;
                recipe.ModifiedAt = DateTime.UtcNow;
                _recipeRepo.Update(recipe);
            }
            if (recipe.Id != 0)
            {
                response.Id = recipe.Id;
                response.TempId = command.TempId;
                response.Success = true;
            }
            return response;
        }

        public CreUpDelRecipeNewResponse InsertIngredient(Ingredient command, List<CreUpDelRecipeNewResponse> recipeDetail)
        {
            CreUpDelRecipeNewResponse response = new CreUpDelRecipeNewResponse();
            Data.Ingredient ingre = new Data.Ingredient();

            foreach (var recipe in recipeDetail)
            {
                if (command.TempId == recipe.TempId)
                {
                    if (command.Name != null)
                    {
                        ingre.Name = command.Name;
                        ingre.Guid = command.Guid;
                        ingre.CreatedAt = DateTime.UtcNow;
                        ingre.IsDeleted = command.IsDeleted;
                        _ingredientRepo.Add(ingre);
                        RecipeIngredientMapping q = new RecipeIngredientMapping();
                        q.IngredientId = ingre.Id;
                        q.RecipeId = recipe.Id;
                        // recipeDetail.RecipeIngredientMappings.Add(q);
                        _ingredientMappingRepo.Add(q);
                    }

                }
            }

            if (command.RecipeId != 0)
            {
                if (command.Name != null)
                {
                    ingre.Name = command.Name;
                    ingre.Guid = command.Guid;
                    ingre.CreatedAt = DateTime.UtcNow;
                    ingre.IsDeleted = command.IsDeleted;
                    _ingredientRepo.Add(ingre);
                    RecipeIngredientMapping q = new RecipeIngredientMapping();
                    q.IngredientId = ingre.Id;
                    q.RecipeId = command.RecipeId;
                    _ingredientMappingRepo.Add(q);
                }
            }

            if (ingre.Id != 0)
            {
                response.Id = ingre.Id;
                response.TempId = command.TempId;
                response.Success = true;
            }
            return response;
        }


        public CreUpDelRecipeNewResponse UpdateIngredient(Ingredient item)
        {
            CreUpDelRecipeNewResponse response = new CreUpDelRecipeNewResponse();
            Data.Ingredient ingre = new Data.Ingredient();
            ingre = _ingredientRepo.Find(item.Id);
            if (item.Name != null)
            {
                ingre.Id = item.Id;
                ingre.Name = item.Name;
                ingre.Guid = item.Guid;
                ingre.ModifiedAt = DateTime.UtcNow;
                ingre.IsDeleted = item.IsDeleted;
                _ingredientRepo.Update(ingre);
            }
            if (ingre.Id != 0)
            {
                response.Id = ingre.Id;
                response.Success = true;
            }
            return response;
        }

        public CreUpDelRecipeNewResponse DeleteIngredient(Ingredient command)
        {
            CreUpDelRecipeNewResponse response = new CreUpDelRecipeNewResponse();
            var ingredient = _ingredientRepo.Find(command.Id);
            ingredient.IsDeleted = true;
            ingredient.ModifiedAt = DateTime.UtcNow;
            _ingredientRepo.Update(ingredient);

            if (ingredient.Id != 0)
            {
                response.Id = ingredient.Id;
                response.TempId = command.TempId;
                response.Success = true;
            }
            return response;
        }


        public CreUpDelRecipeNewResponse InsertCategory(RecipeCategory item, List<CreUpDelRecipeNewResponse> recipeDetail)
        {
            CreUpDelRecipeNewResponse response = new CreUpDelRecipeNewResponse();
            RecipeCategoryMapping category = new RecipeCategoryMapping();
            foreach (var recipe in recipeDetail)
            {
                if (item.TempId == recipe.TempId)
                {
                    category.CategoryId = item.CategoryId;
                    category.RecipeId = recipe.Id;
                    category.Guid = item.Guid;
                    category.CreatedAt = DateTime.UtcNow;
                    _recipeCategoryRepo.Add(category);
                }
            }
            if (item.RecipeId != 0)
            {
                category.CategoryId = item.CategoryId;
                category.RecipeId = item.RecipeId;
                category.Guid = item.Guid;
                category.CreatedAt = DateTime.UtcNow;
                _recipeCategoryRepo.Add(category);
            }
            if (category.Id != 0)
            {
                response.Id = category.Id;
                response.TempId = item.TempId;
                response.Success = true;
            }
            return response;
        }



        public CreUpDelRecipeNewResponse DeleteCategory(RecipeCategory item)
        {
            CreUpDelRecipeNewResponse response = new CreUpDelRecipeNewResponse();
            var category = _recipeCategoryRepo.Find(item.Id);
            category.IsDeleted = true;
            category.ModifiedAt = DateTime.UtcNow;
            _recipeCategoryRepo.Update(category);

            if (category.Id != 0)
            {
                response.Id = category.Id;
                response.TempId = item.TempId;
                response.Success = true;
            }
            return response;
        }


        public CreUpDelRecipeNewResponse InsertUserFeedBack(RecipeUserFeedBack item, List<CreUpDelRecipeNewResponse> recipeDetail)
        {
            CreUpDelRecipeNewResponse response = new CreUpDelRecipeNewResponse();
            Data.RecipeUserFeedback feedback = new Data.RecipeUserFeedback();
            foreach (var recipe in recipeDetail)
            {
                if (item.TempId == recipe.TempId)
                {
                    feedback.UserId = item.UserId;
                    feedback.RecipeId = recipe.Id;
                    feedback.IsFavourite = item.IsFavourite;
                    feedback.IsHidden = item.IsHidden;
                    feedback.Rating = item.Rating;
                    feedback.Comments = item.Comments;
                    feedback.Guid = item.Guid;
                    feedback.CreatedAt = DateTime.UtcNow;
                    feedback.IsDeleted = item.IsDeleted;
                    _recipeFeedBackRepo.Add(feedback);
                }
            }
            if (item.RecipeId != 0)
            {
                feedback.UserId = item.UserId;
                feedback.RecipeId = item.RecipeId;
                feedback.IsFavourite = item.IsFavourite;
                feedback.IsHidden = item.IsHidden;
                feedback.Rating = item.Rating;
                feedback.Comments = item.Comments;
                feedback.Guid = item.Guid;
                feedback.CreatedAt = DateTime.UtcNow;
                feedback.IsDeleted = item.IsDeleted;
                _recipeFeedBackRepo.Add(feedback);
            }
            if (feedback.Id != 0)
            {
                response.Id = feedback.Id;
                response.TempId = item.TempId;
                response.Success = true;
            }
            return response;
        }


        public CreUpDelRecipeNewResponse UpdateUserFeedBack(RecipeUserFeedBack item)
        {
            CreUpDelRecipeNewResponse response = new CreUpDelRecipeNewResponse();
            Data.RecipeUserFeedback feedback = new Data.RecipeUserFeedback();
            feedback = _recipeFeedBackRepo.Find(item.Id);
            feedback.UserId = item.UserId;
            feedback.RecipeId = item.RecipeId;
            feedback.IsFavourite = item.IsFavourite;
            feedback.IsHidden = item.IsHidden;
            feedback.Rating = item.Rating;
            feedback.Comments = item.Comments;
            feedback.Guid = item.Guid;
            feedback.ModifiedAt = DateTime.UtcNow;
            _recipeFeedBackRepo.Update(feedback);

            if (feedback.Id != 0)
            {
                response.Id = feedback.Id;
                response.TempId = item.TempId;
                response.Success = true;
            }
            return response;
        }



        public CreUpDelRecipeNewResponse DeleteUserFeedBack(RecipeUserFeedBack command)
        {
            CreUpDelRecipeNewResponse response = new CreUpDelRecipeNewResponse();
            var userFeedBack = _recipeFeedBackRepo.Find(command.Id);

            userFeedBack.IsDeleted = true;
            userFeedBack.ModifiedAt = DateTime.UtcNow;
            _recipeFeedBackRepo.Update(userFeedBack);

            if (userFeedBack.Id != 0)
            {
                response.Id = userFeedBack.Id;
                response.TempId = command.TempId;
                response.Success = true;
            }

            return response;
        }


        public CreUpDelRecipeNewResponse InsertRecipeImage(RecipeImage item, List<CreUpDelRecipeNewResponse> recipeDetail)
        {
            CreUpDelRecipeNewResponse response = new CreUpDelRecipeNewResponse();
            Data.RecipeImage image = new Data.RecipeImage();
            foreach (var recipe in recipeDetail)
            {
                if (item.TempId == recipe.TempId)
                {
                    image.Name = item.Name;
                    image.RecipeId = recipe.Id;
                    image.Path = item.Path;
                    image.CreatedAt = DateTime.UtcNow;
                    image.Guid = item.Guid;
                    image.IsDeleted = item.IsDeleted;
                    _recipeImageRepo.Add(image);
                }
            }
            if (item.RecipeId != 0)
            {
                image.Name = item.Name;
                image.RecipeId = item.RecipeId;
                image.Path = item.Path;
                image.CreatedAt = DateTime.UtcNow;
                image.Guid = item.Guid;
                image.IsDeleted = item.IsDeleted;
                _recipeImageRepo.Add(image);
            }
            if (image.Id != 0)
            {
                response.Id = image.Id;
                response.TempId = item.TempId;
                response.Success = true;
            }
            return response;
        }


        public CreUpDelRecipeNewResponse UpdateRecipeImage(RecipeImage item)
        {
            CreUpDelRecipeNewResponse response = new CreUpDelRecipeNewResponse();
            Data.RecipeImage image = new Data.RecipeImage();
            image = _recipeImageRepo.Find(item.Id);
            image.Id = item.Id;
            image.Name = item.Name;
            image.RecipeId = item.RecipeId;
            image.Path = item.Path;
            image.ModifiedAt = DateTime.UtcNow;
            image.Guid = item.Guid;
            image.IsDeleted = item.IsDeleted;
            _recipeImageRepo.Update(image);
            if (image.Id != 0)
            {
                response.Id = image.Id;
                response.TempId = item.TempId;
                response.Success = true;
            }
            return response;
        }



        public CreUpDelRecipeNewResponse DeleteRecipeImage(RecipeImage command)
        {
            CreUpDelRecipeNewResponse response = new CreUpDelRecipeNewResponse();
            var recipeImage = _recipeImageRepo.Find(command.Id);

            recipeImage.IsDeleted = true;
            recipeImage.ModifiedAt = DateTime.UtcNow;
            _recipeImageRepo.Update(recipeImage);

            if (recipeImage.Id != 0)
            {
                response.Id = recipeImage.Id;
                response.TempId = command.TempId;
                response.Success = true;
            }

            return response;
        }


        #region Recipe
        public RecipeSyncResponse SyncedRecipes(RecipeViewModel1 recipeModel, int userId, int lastSyncedId)
        {
            RecipeSyncResponse response = new RecipeSyncResponse();

            if (CheckLogSyncId(lastSyncedId, userId) > 0)
            {
                response.Code = 1;
                return response;
            }
            foreach (var recipe in recipeModel.Recipes)
            {
                var res = new ByDay.Data.Recipe()
                {
                    Id = recipe.Id,
                    Guid = recipe.Guid,
                    UserId = recipe.UserId,
                    RecipeName = recipe.RecipeName,
                    Url = recipe.Url,
                    Instructions = recipe.Instructions,
                    CookingTime = recipe.CookingTime,
                    ModifiedAt = DateTime.Now,

                };
                if (recipe.SyncStatus == 0)
                {
                    res.CreatedAt = DateTime.UtcNow;
                    _recipeRepo.Add(res);
                    recipe.Id = res.Id;
                    response.log = UserRecipeSyncLog(userId, res.Id, "Recipe", "INSERT");
                }
                if (recipe.SyncStatus == 1)
                {
                    _recipeRepo.Update(res);
                    response.log = UserRecipeSyncLog(userId, res.Id, "Recipe", "UPDATE");
                }
                if (recipe.SyncStatus == 2)
                {
                    var category = _recipeCategoryRepo.FindBy(t => t.RecipeId == res.Id).ToList();
                    if (category != null)
                    {
                        foreach (var item in category)
                        {
                            _recipeCategoryRepo.Delete(item.Id);
                            response.log = UserRecipeSyncLog(userId, item.Id, "RecipeCategory", "DELETE");
                            _syncLogRepository.Add(response.log);

                        }


                    }
                    var image = _recipeImageRepo.FindBy(t => t.RecipeId == res.Id).ToList();
                    if (image != null)
                    {
                        foreach (var item in image)
                        {
                            _recipeImageRepo.Delete(item.Id);
                            response.log = UserRecipeSyncLog(userId, item.Id, "RecipeImage", "DELETE");
                            _syncLogRepository.Add(response.log);
                        }

                    }
                    var ingredient = _ingredientMappingRepo.FindBy(t => t.RecipeId == res.Id).ToList();
                    if (ingredient != null)
                    {
                        foreach (var item in ingredient)
                        {
                            _ingredientMappingRepo.Delete(item.Id);
                            response.log = UserRecipeSyncLog(userId, item.Id, "RecipeIngredient", "DELETE");
                            _syncLogRepository.Add(response.log);
                        }


                    }
                    var feedback = _recipeFeedBackRepo.FindBy(t => t.RecipeId == res.Id).ToList();
                    if (feedback != null)
                    {
                        foreach (var item in feedback)
                        {
                            _recipeFeedBackRepo.Delete(item.Id);
                            response.log = UserRecipeSyncLog(userId, item.Id, "RecipeUserFeedback", "DELETE");
                            _syncLogRepository.Add(response.log);
                        }


                    }
                    var mealPlan = _planRepository.FindBy(t => t.RecipeId == res.Id).ToList();
                    if (mealPlan != null)
                    {
                        foreach (var item in mealPlan)
                        {
                            _planRepository.Delete(item.Id);
                            response.log = UserRecipeSyncLog(userId, item.Id, "MealPlan", "DELETE");
                            _syncLogRepository.Add(response.log);
                        }


                    }
                    var bulletinBoard = _bulletinRepository.FindBy(t => t.RecipeId == res.Id).ToList();
                    if (bulletinBoard != null)
                    {
                        foreach (var item in bulletinBoard)
                        {
                            _bulletinRepository.Delete(item.Id);
                            response.log = UserRecipeSyncLog(userId, item.Id, "BulletinBoard", "DELETE");
                            _syncLogRepository.Add(response.log);
                        }


                    }
                    _recipeRepo.Delete(recipe.Id);
                    response.log = UserRecipeSyncLog(userId, res.Id, "Recipe", "DELETE");
                }
                _syncLogRepository.Add(response.log);
            }
            response.recipeCommand = recipeModel.Recipes;
            foreach (var recipeCategory in recipeModel.RecipeCategories)
            {
                var res = new ByDay.Data.RecipeCategoryMapping()
                {
                    Id = recipeCategory.Id,
                    RecipeId = recipeCategory.RecipeId == 0 ? recipeModel.Recipes.FirstOrDefault(t => t.TempId == recipeCategory.TempRecipeId).Id : recipeCategory.RecipeId,
                    CategoryId = recipeCategory.CategoryId,
                    Guid = recipeCategory.Guid,
                    ModifiedAt = DateTime.UtcNow
                };
                if (recipeCategory.SyncStatus == 0)
                {
                    res.CreatedAt = DateTime.UtcNow;
                    _recipeCategoryRepo.Add(res);
                    recipeCategory.Id = res.Id;
                    response.log = UserRecipeSyncLog(userId, res.Id, "RecipeCategory", "INSERT");
                }
                if (recipeCategory.SyncStatus == 2)
                {
                    _recipeCategoryRepo.Delete(res.Id);
                    response.log = UserRecipeSyncLog(userId, res.Id, "RecipeCategory", "DELETE");
                }
                _syncLogRepository.Add(response.log);
            }
            response.recipeCategory = recipeModel.RecipeCategories;
            foreach (var recipeImage in recipeModel.Images)
            {
                var res = new ByDay.Data.RecipeImage()
                {
                    RecipeId = recipeImage.RecipeId == 0 ? recipeModel.Recipes.FirstOrDefault(t => t.TempId == recipeImage.TempRecipeId).Id : recipeImage.RecipeId,
                    Name = recipeImage.Name,
                    Path = recipeImage.Path,
                    Guid = recipeImage.Guid,
                    IsCover = recipeImage.IsCover,
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow
                };
                if (recipeImage.SyncStatus == 0)
                {
                    res.CreatedAt = DateTime.UtcNow;
                    _recipeImageRepo.Add(res);
                    recipeImage.Id = res.Id;
                    response.log = UserRecipeSyncLog(userId, res.Id, "RecipeImage", "INSERT");
                }
                if (recipeImage.SyncStatus == 2)
                {
                    _recipeImageRepo.Delete(recipeImage.Id);
                    response.log = UserRecipeSyncLog(userId, recipeImage.Id, "RecipeImage", "DELETE");
                }
                _syncLogRepository.Add(response.log);
            }
            response.recipeImage = recipeModel.Images;
            foreach (var recipeIngredient in recipeModel.Ingredients)
            {
                var res = new ByDay.Data.Ingredient()
                {
                    Name = recipeIngredient.Name,
                    Guid = recipeIngredient.Guid,
                    ModifiedAt = DateTime.UtcNow
                };
                var recipeIngred = new ByDay.Data.RecipeIngredientMapping()
                {
                    RecipeId = recipeIngredient.RecipeId == 0 ? recipeModel.Recipes.FirstOrDefault(t => t.TempId == recipeIngredient.TempRecipeId).Id : recipeIngredient.RecipeId
                };
                if (recipeIngredient.SyncStatus == 0)
                {
                    res.CreatedAt = DateTime.UtcNow;
                    _ingredientRepo.Add(res);

                    recipeIngred.IngredientId = res.Id;
                    _ingredientMappingRepo.Add(recipeIngred);
                    recipeIngredient.Id = recipeIngred.Id;
                    response.log = UserRecipeSyncLog(userId, recipeIngred.Id, "RecipeIngredient", "INSERT");
                }
                if (recipeIngredient.SyncStatus == 1)
                {
                    var ingredientId = _ingredientMappingRepo.Find(recipeIngredient.Id).IngredientId;
                    var ingredient = _ingredientRepo.Find(ingredientId);
                    ingredient.Name = recipeIngredient.Name;
                    _ingredientRepo.Update(ingredient);
                    response.log = UserRecipeSyncLog(userId, recipeIngredient.Id, "RecipeIngredient", "UPDATE");
                }
                if (recipeIngredient.SyncStatus == 2)
                {
                    var ingredientId = _ingredientMappingRepo.Find(recipeIngredient.Id).IngredientId;
                    _ingredientMappingRepo.Delete(recipeIngredient.Id);
                    _ingredientRepo.Delete(ingredientId);
                    response.log = UserRecipeSyncLog(userId, recipeIngredient.Id, "RecipeIngredient", "DELETE");

                }
                _syncLogRepository.Add(response.log);
            }
            response.recipeIngredient = recipeModel.Ingredients;
            foreach (var recipeFeedback in recipeModel.recipeFeedback)
            {
                var recipefeedback1 = new ByDay.Data.RecipeUserFeedback()
                {
                    Id = recipeFeedback.Id,
                    UserId = recipeFeedback.UserId,
                    RecipeId = recipeFeedback.RecipeId == 0 ? recipeModel.Recipes.FirstOrDefault(t => t.TempId == recipeFeedback.TempRecipeId).Id : recipeFeedback.RecipeId,
                    IsFavourite = recipeFeedback.IsFavourite,
                    IsHidden = recipeFeedback.IsHidden,
                    Rating = recipeFeedback.Rating,
                    Comments = recipeFeedback.Comments,
                    Guid = recipeFeedback.Guid,
                    ModifiedAt = DateTime.UtcNow,
                    IsDeleted = recipeFeedback.IsDeleted
                };
                if (recipeFeedback.SyncStatus == 0)
                {
                    recipefeedback1.CreatedAt = DateTime.UtcNow;
                    _recipeFeedBackRepo.Add(recipefeedback1);
                    recipeFeedback.Id = recipefeedback1.Id;
                    response.log = UserRecipeSyncLog(userId, recipeFeedback.Id, "RecipeUserFeedback", "INSERT");
                }

                else if (recipeFeedback.SyncStatus == 1)
                {
                    _recipeFeedBackRepo.Update(recipefeedback1);
                    response.log = UserRecipeSyncLog(userId, recipeFeedback.Id, "RecipeUserFeedback", "UPDATE");
                }

                else if (recipeFeedback.SyncStatus == 2)
                {
                    _recipeFeedBackRepo.Delete(recipefeedback1.Id);
                    response.log = UserRecipeSyncLog(userId, recipeFeedback.Id, "RecipeUserFeedback", "DELETE");
                }
                _syncLogRepository.Add(response.log);

            }
            response.recipeFeedback = recipeModel.recipeFeedback;
            foreach (var plan in recipeModel.MealPlans)
            {
                var res = new ByDay.Data.MealPlan()
                {
                    Id = plan.Id,
                    RecipeId = plan.RecipeId == 0 ? recipeModel.Recipes.FirstOrDefault(t => t.TempId == plan.TempRecipeId).Id : plan.RecipeId,
                    UserId = userId,
                    RecipeDate = plan.RecipeDate,
                    ModifiedAt = DateTime.UtcNow,
                    Guid = plan.Guid,
                    OneTimeComment = plan.OneTimeComment
                };

                if (plan.SyncStatus == 0)
                {
                    res.CreatedAt = DateTime.UtcNow;
                    _planRepository.Add(res);
                    plan.Id = res.Id;
                    response.log = UserRecipeSyncLog(userId, plan.Id, "MealPlan", "INSERT");
                }
                if (plan.SyncStatus == 1)
                {
                    var planObj = _planRepository.Find(plan.Id);
                    planObj.OneTimeComment = plan.OneTimeComment;
                    planObj.RecipeId = plan.RecipeId;
                    _planRepository.Update(planObj);
                    response.log = UserRecipeSyncLog(userId, plan.Id, "MealPlan", "UPDATE");
                }
                if (plan.SyncStatus == 2)
                {
                    _planRepository.Delete(plan.Id);
                    response.log = UserRecipeSyncLog(userId, plan.Id, "MealPlan", "DELETE");
                }
                _syncLogRepository.Add(response.log);
            }
            response.mealPlan = recipeModel.MealPlans;

            foreach (var board in recipeModel.BulletinBoard)
            {
                var res = new ByDay.Data.BulletinBoard()
                {
                    Id = board.Id,
                    RecipeId = board.RecipeId == 0 ? recipeModel.Recipes.FirstOrDefault(t => t.TempId == board.TempRecipeId).Id : board.RecipeId,
                    UserId = userId,
                    Guid = board.Guid
                };

                if (board.SyncStatus == 0)
                {
                    _bulletinRepository.Add(res);
                    board.Id = res.Id;
                    response.log = UserRecipeSyncLog(userId, board.Id, "BulletinBoard", "INSERT");
                }
                //if (board.SyncStatus == 1)
                //{
                //    var boardObj = _bulletinRepository.Find(board.Id);
                //    boardObj.Guid = board.Guid;
                //    boardObj.RecipeId = board.RecipeId;
                //    _bulletinRepository.Update(boardObj);
                //    response.log = UserRecipeSyncLog(userId, board.Id, "BulletinBoard", "UPDATE");
                //}
                if (board.SyncStatus == 2)
                {
                    _bulletinRepository.Delete(board.Id);
                    response.log = UserRecipeSyncLog(userId, board.Id, "BulletinBoard", "DELETE");
                }
                _syncLogRepository.Add(response.log);
            }
            response.bulletinBoard = recipeModel.BulletinBoard;
            return response;
        }

        public dynamic GetData(int userId, int syncId, string schema, ref string lastSyncId)
        {
            return _syncLogRepository.ExecWithStoreProcedureSync("GetData", ref lastSyncId, new object[] { userId, syncId, schema });
        }
        public int CheckLogSyncId(long id, long userId)
        {
            return _syncLogRepository.FindBy(t => t.Id > id && t.UserId == userId && (t.Entity == "RecipeIngredient" || t.Entity == "Ingredient" || t.Entity == "Recipe" || t.Entity == "RecipeImage" || t.Entity == "RecipeCategory")).Count();
        }

        public ByDay.Data.SyncLog UserRecipeSyncLog(int userId, int rowId, string entity, string actionType)
        {
            var obj = new SyncLog()
            {
                UserId = userId,
                Entity = entity,
                EntityRowId = rowId,
                ActionType = actionType,
                SyncDate = DateTime.UtcNow
            };
            return obj;
        }
        #endregion




































    }
}
