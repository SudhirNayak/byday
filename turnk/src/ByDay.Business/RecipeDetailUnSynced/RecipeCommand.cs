﻿using ByDay.Business.EventDetail;
using ByDay.Business.MealPlans;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.RecipeDetailUnSynced
{
    public class RecipeCommand
    {

        public RecipeCommand()
        {
            UserSpecificRecipeDetails = new List<RecipeUserFeedBack>();
            Ingredients = new List<Ingredient>();
            Categories = new List<RecipeCategory>();
            Images = new List<RecipeImage>();
        }
        public int Id { get; set; }
        public string TempId { get; set; }
        public int UserId { get; set; }
        public string RecipeName { get; set; }
        public string Instructions { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }

        public Nullable<int> CookingTime { get; set; }
        public string Guid { get; set; }
        public string Url { get; set; }
        // public string Direction { get; set; }
        public bool IsDeleted { get; set; }

        public List<RecipeUserFeedBack> UserSpecificRecipeDetails { get; set; }

        public List<Ingredient> Ingredients { get; set; }
        public List<RecipeCategory> Categories { get; set; }
        public List<RecipeImage> Images { get; set; }
        public int SyncStatus { get; set; }
        public int VersionNumber { get; set; }

    }

    public class RecipeCategory
    {
        public int Id { get; set; }
        public string TempRecipeId { get; set; }
        public string TempId { get; set; }
        public int CategoryId { get; set; }
        public int RecipeId { get; set; }
        public string Guid { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public int SyncStatus { get; set; }
        public int VersionNumber { get; set; }

    }

   
    public class RecipeImage
    {

        public int Id { get; set; }

        public string TempId { get; set; }
        public string Name { get; set; }
        public int RecipeId { get; set; }
        public string Path { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        // public Nullable<bool> IsActive { get; set; }
        public string Guid { get; set; }
        public bool IsDeleted { get; set; }
        public int SyncStatus { get; set; }
        public string TempRecipeId { get; set; }
        public bool IsCover { get; set; }
        public int VersionNumber { get; set; }
    }

    public class RecipeUserFeedBack
    {
        public int Id { get; set; }
        public string TempId { get; set; }
        public int UserId { get; set; }
        public int RecipeId { get; set; }
        public bool IsFavourite { get; set; }
        public bool IsHidden { get; set; }
        public Nullable<int> Rating { get; set; }
        public string Comments { get; set; }
        public string Guid { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public bool IsDeleted { get; set; }
        public int SyncStatus { get; set; }
        public string TempRecipeId { get; set; }
        public int VersionNumber { get; set; }

    }
    public class Ingredient
    {
        public int Id { get; set; }

        public int RecipeId { get; set; }

        public string TempId { get; set; }
        public string Name { get; set; }
        public string Guid { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public bool IsDeleted { get; set; }
        public int SyncStatus { get; set; }
        public string TempRecipeId { get; set; }
        public int VersionNumber { get; set; }

    }

    public class RecipeViewModel1
    {
        public List<RecipeCommand> Recipes { get; set; }
        public List<RecipeCategory> RecipeCategories { get; set; }
        public List<RecipeImage> Images { get; set; }
        public List<Ingredient> Ingredients { get; set; }
        public List<RecipeUserFeedBack> recipeFeedback { get; set; }
        public List<MealPlanCommand> MealPlans { get; set; }
        public List<BulletinBoard> BulletinBoard { get; set; }

    }

    public class RecipeSyncResponse
    {
        public int lastSyncId { get; set; }
        public int Code { get; set; }
        public ByDay.Data.SyncLog log { get; set; }
        public List<RecipeCommand> recipeCommand { get; set; }
        public List<RecipeCategory> recipeCategory { get; set; }
        public List<RecipeImage> recipeImage { get; set; }
        public List<Ingredient> recipeIngredient { get; set; }
        public List<RecipeUserFeedBack> recipeFeedback { get; set; }
        public List<MealPlanCommand> mealPlan { get; set; }
        public List<BulletinBoard> bulletinBoard { get; set; }

    }

    public class SyncRecipeResult
    {
        public DataTable RecipeIU { get; set; }
        public DataTable RecipeD { get; set; }
        public DataTable CategoryI { get; set; }
        public DataTable CategoryD { get; set; }
        public DataTable ImageI { get; set; }
        public DataTable ImageD { get; set; }
        public DataTable RecipeIngredientIU { get; set; }
        public DataTable RecipeIngredientD { get; set; }
        public DataTable RecipeFeedbackIU { get; set; }
        public DataTable RecipeFeedbackD { get; set; }

        public DataTable MealPlanIU { get; set; }
        public DataTable MealPlanD { get; set; }

        public DataTable AdminRecipeI { get; set; }
        public DataTable AdminRecipeImagesI { get; set; }
        public DataTable AdminRecipeIngredientI { get; set; }
        public DataTable AdminRecipeFeedbackI { get; set; }
        public DataTable AdminRecipeCategoryI { get; set; }

        public DataTable BulletinBoardI { get; set; }
        public DataTable BulletinBoardD { get; set; }
    }

    public class BulletinBoard
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RecipeId { get; set; }
        public string Guid { get; set; }
        public int SyncStatus { get; set; }
        public string TempRecipeId { get; set; }
        public string TempId { get; set; }
        public int VersionNumber { get; set; }

    }
}