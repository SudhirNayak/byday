﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.RecipeDetailUnSynced
{
 public   class RecipeUnSyncedResponse
    {

        public RecipeUnSyncedResponse()
        {
            CreatedRecipes = new List<CreatedUpdateedDeletedRecipeResponse>();
            ModifiedRecipes = new List<CreatedUpdateedDeletedRecipeResponse>();
            DeletedRecipes = new List<CreatedUpdateedDeletedRecipeResponse>();

        }
        public string LastSync
        {
            get
            {
                return DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            }
        }


        public List<CreatedUpdateedDeletedRecipeResponse> CreatedRecipes { get; set; }
        public List<CreatedUpdateedDeletedRecipeResponse> ModifiedRecipes { get; set; }
        public List<CreatedUpdateedDeletedRecipeResponse> DeletedRecipes { get; set; }


    }
    public class CreatedUpdateedDeletedRecipeResponse
    {
        public string TempId { get; set; }

        public int Id { get; set; }

        public bool Success { get; set; }
    }
}

