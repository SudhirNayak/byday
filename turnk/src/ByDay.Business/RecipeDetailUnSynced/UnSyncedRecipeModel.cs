﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.RecipeDetailUnSynced
{
  public  class UnSyncedRecipeModel
    {

        public UnSyncedRecipeModel()
        {

            Recipes = new List<RecipeResponseCommand>();
        }
        public string LastSync
        {
            get
            {
                return DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            }
        }
        public List<RecipeResponseCommand> Recipes { get; set; }


    }

    public class RecipeResponseCommand
    {
        public RecipeResponseCommand()
        {
            Ingredients = new List<ResponseIngredient>();
            UserSpecificRecipeDetails = new List<RecipeResponseUserFeedBack>();
            Categories = new List<RecipeResponseCategory>();
            Images = new List<RecipeResponseImage>();
        }
        public int Id { get; set; }
        public int UserId { get; set; }
        public string RecipeName { get; set; }
        public string Description { get; set; }

        public string Instructions { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public string Guid { get; set; }
        public string Url { get; set; }

        public Nullable<int> CookingTime { get; set; }
        public Nullable<int> PrepTime { get; set; }
        public Nullable<int> NumServings { get; set; }

        public string Simplicity { get; set; }

        //   public string Direction { get; set; }
        public bool IsAdmin { get; set; }
        public Nullable<bool> IsDeleted { get; set; }

        public List<ResponseIngredient> Ingredients { get; set; }

        public List<RecipeResponseUserFeedBack> UserSpecificRecipeDetails { get; set; }

        
        public List<RecipeResponseCategory> Categories { get; set; }
        public List<RecipeResponseImage> Images { get; set; }

    }

    public class RecipeResponseCategory
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int RecipeId { get; set; }
        public string Guid { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }

    public class RecipeResponseImage
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public int RecipeId { get; set; }
        public string Path { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
      //  public Nullable<int> CreatedBy { get; set; }
       // public Nullable<int> ModifiedBy { get; set; }
        // public Nullable<bool> IsActive { get; set; }
        public string Guid { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }

    public class RecipeResponseUserFeedBack
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RecipeId { get; set; }
        public bool IsFavourite { get; set; }
        public bool IsHidden { get; set; }
        public Nullable<int> Rating { get; set; }
        public string Comments { get; set; }
        public string Guid { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public Nullable<bool> IsDeleted { get; set; }

    }
    public class ResponseIngredient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Quantity { get; set; }
        public string QuantityType { get; set; }
        public string Guid { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }


}
