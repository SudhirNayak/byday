﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.RecipeDetailUnSynced
{
  public class RecipeUnSyncedCommand
    {
        public RecipeUnSyncedCommand()
        {
            CreatedRecipes = new List<RecipeCommand>();
            ModifiedRecipes = new List<RecipeCommand>();
            DeletedRecipes = new List<RecipeCommand>();

        }
        public List<RecipeCommand> CreatedRecipes { get; set; }
        public List<RecipeCommand> ModifiedRecipes { get; set; }
        public List<RecipeCommand> DeletedRecipes { get; set; }
    }
}
