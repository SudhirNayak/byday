﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.UserScratchPad
{
   public  class ScratchPadService
    {
        ByDayRepository<ByDay.Data.UserScratchpad> _userScratchPadRepository = new ByDayRepository<ByDay.Data.UserScratchpad>(new Ex2ByDayEntities());



        public IQueryable<Data.UserScratchpad> GetUserScratchPad(int id)
        {
            var user = _userScratchPadRepository.FindBy(x => x.UserId == id).ToList();

            return user.AsQueryable();

        }

        public ScratchPad UpdateUserScratchPad(ScratchPad scratchPad )
        {
           ScratchPad ScratchPadResponse = new ScratchPad();
            UserScratchpad scratch = new UserScratchpad();
            scratch = _userScratchPadRepository.Find(scratchPad.Id);
            scratch.UserId = scratch.UserId;
            scratch.Content = scratchPad.Content;
            scratch.ModifiedAt = DateTime.UtcNow;
            scratch.IsDeleted = scratchPad.IsDeleted;
           _userScratchPadRepository.Update(scratch);
            if(scratch.Id!=0)
            {
                ScratchPadResponse.Id = scratch.Id;
                ScratchPadResponse.UserId = scratch.UserId;
                ScratchPadResponse.Content = scratch.Content;
                ScratchPadResponse.CreatedAt = scratch.CreatedAt;
                ScratchPadResponse.ModifiedAt = scratch.ModifiedAt;
                ScratchPadResponse.IsDeleted = scratch.IsDeleted;
            }
            return ScratchPadResponse;
            }



    }
}
