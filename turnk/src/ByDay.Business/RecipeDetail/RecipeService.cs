﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ByDay.Data;
using ByDay.Business.RecipeDetail;
using System.IO;


namespace ByDay.Business.RecipeDetail
{
    public class RecipeService
    {
        ByDayRepository<ByDay.Data.ByDayUser> _userRepo = new ByDayRepository<ByDay.Data.ByDayUser>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.Recipe> _mealRepo = new ByDayRepository<ByDay.Data.Recipe>(new Ex2ByDayEntities());

        ByDayRepository<RecipePermissionMapping> _permissionsRepo = new ByDayRepository<RecipePermissionMapping>(new Ex2ByDayEntities());
        ByDayRepository<Ingredient> _ingredientRepo = new ByDayRepository<Ingredient>(new Ex2ByDayEntities());

        ByDayRepository<RecipeImage> _imageRepo = new ByDayRepository<RecipeImage>(new Ex2ByDayEntities());
        ByDayRepository<ImagePermissionMapping> _imagePerm = new ByDayRepository<ImagePermissionMapping>(new Ex2ByDayEntities());

        ByDayRepository<AgeAppropriate> _ageAppropriatenessRepo = new ByDayRepository<AgeAppropriate>(new Ex2ByDayEntities());
        ByDayRepository<RecipeAgeAppropriateMapping> _ageAppropriatenessMap = new ByDayRepository<RecipeAgeAppropriateMapping>(new Ex2ByDayEntities());
        ByDayRepository<RecipeCuisinMapping> _cusineMap = new ByDayRepository<RecipeCuisinMapping>(new Ex2ByDayEntities());


        ByDayRepository<Permission> _premissionTypeRepo = new ByDayRepository<Permission>(new Ex2ByDayEntities());
        ByDayRepository<Category> _categoryRepo = new ByDayRepository<Category>(new Ex2ByDayEntities());
        ByDayRepository<SpecialDiet> _specialDietRepo = new ByDayRepository<SpecialDiet>(new Ex2ByDayEntities());


        ByDayRepository<RecipeCategoryMapping> _categoryMapRepo = new ByDayRepository<RecipeCategoryMapping>(new Ex2ByDayEntities());


        ByDayRepository<RecipeSpecialDietMapping> _specailDietMapRepo = new ByDayRepository<RecipeSpecialDietMapping>(new Ex2ByDayEntities());


        ByDayRepository<ByDay.Data.Type> _quantityRepo = new ByDayRepository<ByDay.Data.Type>(new Ex2ByDayEntities());
        ByDayRepository<RecipeIngredientMapping> _mealsIngredientRepo = new ByDayRepository<RecipeIngredientMapping>(new Ex2ByDayEntities());
        ByDayRepository<RecipeCategoryMapping> _mealsCategoryRepo = new ByDayRepository<RecipeCategoryMapping>(new Ex2ByDayEntities());
        ByDayRepository<IngredientTypeMapping> _ingreTypeRepoMap = new ByDayRepository<IngredientTypeMapping>(new Ex2ByDayEntities());
        ByDayRepository<ImagePermissionMapping> _imagePermissionMapRepo = new ByDayRepository<ImagePermissionMapping>(new Ex2ByDayEntities());
        DateTime now = DateTime.UtcNow;
        public IQueryable<ByDay.Data.Recipe> GetAll()

        {
            return _mealRepo.All.Select(x => x).Where(y=>y.IsAdmin==true && y.IsDeleted==false).OrderBy(y=>y.RecipeName);
        }

        public IQueryable<AgeAppropriate> GetAgeGroups()
        {
            return _ageAppropriatenessRepo.All;
        }

        public List<int> GetSelectedAgeOfRecipe(Recipe recipeDetails, int id)
        {
            return recipeDetails.RecipeAgeAppropriateMappings.Where(x => x.RecipeId == id).Select(x => x.AgeAppropriateId).ToList();
        }

        public List<int> GetSelectedCusineOfRecipe(Recipe recipeDetails, int id)
        {
            return recipeDetails.RecipeCuisinMappings.Where(x => x.RecipeId == id).Select(x => Convert.ToInt32(x.CuisinId)).ToList();
        }



        public List<int> GetSelectedCategoryOfRecipe(Recipe recipeDetails, int id)
        {
         return recipeDetails.RecipeCategoryMappings.Where(x => x.RecipeId == id).Select(x => x.CategoryId).ToList();
        }
        

        public List<int> GetSelectedSpecialDiet(Recipe recipeDetails, int id)
        {
            return recipeDetails.RecipeSpecialDietMappings.Where(x => x.RecipeId == id).Select(x => x.SpecialDietId).ToList();
        }

        public List<int> GetSelectedPermissionOfRecipe(Recipe recipe, int id)
        {
          return recipe.RecipePermissionMappings.Where(x => x.RecipeId == id).Select(x => x.PermissionId).ToList();
        }

        public IQueryable<Permission> GetPermissionType()
        {
            return _premissionTypeRepo.All;
        }

        public int Insert(RecipeCommands recipeCommand)
        {
            var recipeDetail = new Recipe
            {
                RecipeName = recipeCommand.RecipeName.Trim(),                
                WeekendPreparable = recipeCommand.WeekendPreparable,
                SpiceLevelId = recipeCommand.SpiceLevelId,
                Instructions = recipeCommand.Instructions,
                Description = recipeCommand.Description,
                ReadyWhenGetHome = recipeCommand.ReadyWhenGetHome,
                QuickFixMatch = recipeCommand.QuickFixMatch ?? 0,
                IsMostLike = string.Join(",", recipeCommand.IsMostLikeIds),
                LeftOverRecipe= string.Join(",", recipeCommand.LeftOverRecipeGroupId),
                NumServings = recipeCommand.NumServings ?? 0,
                Cost=recipeCommand.Cost??0,
                PickyEaterTip = recipeCommand.PickyEaterTip,
                GeneralTips = recipeCommand.GeneralTips,
                CookingTime = recipeCommand.CookingTime ?? 0,
                PrepTime = recipeCommand.PrepTime ?? 0,
                UniqueToDatabase = recipeCommand.UniqueToDatabase,
               // CuisineId = recipeCommand.CuisineId,
                Top10PercentOfCuisine = recipeCommand.Top10PercentOfCuisine,
               // LeftOverRecipeId = recipeCommand.LeftoverRecipeId ?? 0,
                Freezable = recipeCommand.Freezable,
                SimplicityId = recipeCommand.SimplicityId,
               // BackupPlanWorthiness = recipeCommand.BackupPlanWorthiness,
                IngredientPreferenceId=recipeCommand.IngredientPreferenceId,
                Publish = recipeCommand.Publish,
                UserId = recipeCommand.UserId,
                CreatedBy=recipeCommand.UserId,
                IsAdmin= true,
                CreatedAt= now
            };
            foreach (var id in recipeCommand.AgeGroupIds)
            {
                RecipeAgeAppropriateMapping ageAppropriate = new RecipeAgeAppropriateMapping();
                ageAppropriate.AgeAppropriateId = id;
                recipeDetail.RecipeAgeAppropriateMappings.Add(ageAppropriate);
            }


            foreach (var id in recipeCommand.CusineGroupId)
            {
                RecipeCuisinMapping cuisineMapping = new RecipeCuisinMapping();
                cuisineMapping.CuisinId = id;
                recipeDetail.RecipeCuisinMappings.Add(cuisineMapping);
            }



            foreach (var id in recipeCommand.CategoryId)
            {
                RecipeCategoryMapping category = new RecipeCategoryMapping();
                category.CategoryId = id;
                recipeDetail.RecipeCategoryMappings.Add(category);
            }
            foreach (var id in recipeCommand.SpecialDietId)
            {
                RecipeSpecialDietMapping specailDiet = new RecipeSpecialDietMapping();
                specailDiet.SpecialDietId = id;
                recipeDetail.RecipeSpecialDietMappings.Add(specailDiet);
            }

            foreach (var item in recipeCommand.Ingrideints)
            {
                Ingredient ingre = new Ingredient();
                if (item.IngredientName != null)
                {
                    ingre.Name = item.IngredientName;
                    ingre.IngredientType = item.IngredientType;
                    ingre.Quantity = item.QuantityId;
                    ingre.QuantityType = item.QuanityTypeId;
                    ingre.Cost = item.IngredientCost;
                    ingre.Tip = item.Tip;
                    ingre.HardToFind = item.HardToFind;
                    ingre.Ommitable = item.Omittable;

                    if (item.IngridientTypeId != null)
                    {
                        foreach (var id in item.IngridientTypeId)
                        {
                            IngredientTypeMapping ingreMaping = new IngredientTypeMapping();
                            ingreMaping.TypeId = id;
                            ingre.IngredientTypeMappings.Add(ingreMaping);

                        }
                    }
                    _ingredientRepo.Add(ingre);
                    RecipeIngredientMapping q = new RecipeIngredientMapping();
                    q.IngredientId = ingre.Id;
                    recipeDetail.RecipeIngredientMappings.Add(q);
                }
            }

            foreach (var id in recipeCommand.PermissionGroupIds)
            {
                RecipePermissionMapping permission = new RecipePermissionMapping();
                permission.PermissionId = id;
                recipeDetail.RecipePermissionMappings.Add(permission);
            }
            _mealRepo.Add(recipeDetail);
            return recipeDetail.Id;
        }


        public void Update(RecipeCommands recipeCommand)
        {
            var recipe = _mealRepo.Find(recipeCommand.Id);
            recipe.RecipeName = recipeCommand.RecipeName.Trim();
            recipe.WeekendPreparable = recipeCommand.WeekendPreparable;
            recipe.SpiceLevelId = recipeCommand.SpiceLevelId;
            recipe.Instructions = recipeCommand.Instructions;
            recipe.Description = recipeCommand.Description;
            recipe.ReadyWhenGetHome = recipeCommand.ReadyWhenGetHome;
            recipe.QuickFixMatch = recipeCommand.QuickFixMatch ?? 0;
            recipe.IsMostLike = string.Join(",", recipeCommand.IsMostLikeIds);
            recipe.LeftOverRecipe = string.Join(",", recipeCommand.LeftOverRecipeGroupId);
            recipe.NumServings = recipeCommand.NumServings ?? 0;
            recipe.Cost = recipeCommand.Cost ?? 0;
            recipe.PickyEaterTip = recipeCommand.PickyEaterTip;
            recipe.GeneralTips = recipeCommand.GeneralTips;
            recipe.CookingTime = recipeCommand.CookingTime ?? 0;
            recipe.PrepTime = recipeCommand.PrepTime ?? 0;
            recipe.UniqueToDatabase = recipeCommand.UniqueToDatabase;
           // recipe.CuisineId = recipeCommand.CuisineId;
            recipe.Top10PercentOfCuisine = recipeCommand.Top10PercentOfCuisine;
           // recipe.LeftOverRecipeId = recipeCommand.LeftoverRecipeId ?? 0;
            recipe.Freezable = recipeCommand.Freezable;
            recipe.SimplicityId = recipeCommand.SimplicityId;
          //  recipe.BackupPlanWorthiness = recipeCommand.BackupPlanWorthiness;
            recipe.Publish = recipeCommand.Publish;
            recipe.IngredientPreferenceId = recipeCommand.IngredientPreferenceId;
            recipe.UserId = recipeCommand.UserId;
            recipe.ModifiedBy = recipeCommand.UserId;
            recipe.ModifiedAt = now;

            var ageGroupExists = _ageAppropriatenessMap.FindBy(x => x.RecipeId == recipe.Id).ToList();
            foreach (var ageGroupEx in ageGroupExists)
            {
                if (ageGroupEx != null)
                    _ageAppropriatenessMap.Delete(ageGroupEx);
            }

            foreach (var id in recipeCommand.AgeGroupIds)
            {
                RecipeAgeAppropriateMapping ageAppropriate = new RecipeAgeAppropriateMapping();
                ageAppropriate.AgeAppropriateId = id;
                ageAppropriate.RecipeId = recipe.Id;
                recipe.RecipeAgeAppropriateMappings.Add(ageAppropriate);          
            }


            var cusineExists = _cusineMap.FindBy(x => x.RecipeId == recipe.Id).ToList();
            foreach (var cusineEx in cusineExists)
            {
                if (cusineEx != null)
                    _cusineMap.Delete(cusineEx);
            }

            foreach (var id in recipeCommand.CusineGroupId)
            {
                RecipeCuisinMapping cusineMaping = new RecipeCuisinMapping();
                cusineMaping.CuisinId = id;
                cusineMaping.RecipeId = recipe.Id;
                recipe.RecipeCuisinMappings.Add(cusineMaping);
            }



            var categoryExists = _categoryMapRepo.FindBy(x => x.RecipeId == recipe.Id).ToList();
            foreach (var categoryEx in categoryExists)
            {
                if (categoryEx != null)
                    _categoryMapRepo.Delete(categoryEx);
            }
                foreach (var id in recipeCommand.CategoryId)
                {
                    RecipeCategoryMapping categoryMapping = new RecipeCategoryMapping();
                    categoryMapping.CategoryId = id;
                    categoryMapping.RecipeId = recipe.Id;
                    recipe.RecipeCategoryMappings.Add(categoryMapping);
                }


            var specailDietExists = _specailDietMapRepo.FindBy(x => x.RecipeId == recipe.Id).ToList();
            foreach (var specailEx in specailDietExists)
            {
                if (specailEx != null)
                   _specailDietMapRepo.Delete(specailEx);
            }
            foreach (var id in recipeCommand.SpecialDietId)
            {
                RecipeSpecialDietMapping specailDietMapping = new RecipeSpecialDietMapping();
                specailDietMapping.SpecialDietId = id;
                specailDietMapping.RecipeId = recipe.Id;
                recipe.RecipeSpecialDietMappings.Add(specailDietMapping);
            }


            // delete all ingredient of current recipe
            var ingRecipeId = _mealsIngredientRepo.FindBy(x => x.RecipeId == recipeCommand.Id);
            var ingRecipeExists = ingRecipeId.Select(x => x.IngredientId).ToList();

            foreach (int ingredientId in ingRecipeExists )
            {
                var ingreTypeExists = _ingreTypeRepoMap.FindBy(x => x.IngredientId == ingredientId).ToList();
                foreach (var ingreTypeEx in ingreTypeExists)
                {
                    if (ingreTypeEx != null)
                        _ingreTypeRepoMap.Delete(ingreTypeEx);
                }
                var ingreRecipeExists = _mealsIngredientRepo.FindBy(x => x.IngredientId == ingredientId).ToList();
                foreach (var ingreRecipeEx in ingreRecipeExists)
                {
                    if (ingreRecipeEx != null)
                        _mealsIngredientRepo.Delete(ingreRecipeEx);
                }
              var  ingreExists = _ingredientRepo.FindBy(x => x.Id == ingredientId).ToList();
                foreach (var ingreEx in ingreExists)
                {
                    if (ingreEx != null)
                        _ingredientRepo.Delete(ingreEx);
                }
            }

            // add all ingredient of current recipe.
            foreach (var item in recipeCommand.Ingrideints)
            {
                Ingredient ingre = new Ingredient();
                if (item.IngredientName != null)
                {
                    ingre.Name = item.IngredientName;
                    ingre.IngredientType = item.IngredientType;
                    ingre.Quantity = item.QuantityId;
                    ingre.QuantityType = item.QuanityTypeId;
                    ingre.Cost = item.IngredientCost;
                    ingre.Tip = item.Tip;
                    ingre.HardToFind = item.HardToFind;
                    ingre.Ommitable = item.Omittable;

                    if (item.IngridientTypeId != null)
                    {
                        foreach (var id in item.IngridientTypeId)
                        {
                            IngredientTypeMapping ingreMaping = new IngredientTypeMapping();
                            ingreMaping.TypeId = id;
                            ingre.IngredientTypeMappings.Add(ingreMaping);

                        }
                    }
                    _ingredientRepo.Add(ingre);
                    RecipeIngredientMapping q = new RecipeIngredientMapping();
                    q.IngredientId = ingre.Id;
                    recipe.RecipeIngredientMappings.Add(q);
                }
            }

            var permissionExists = _permissionsRepo.FindBy(x => x.RecipeId == recipe.Id).ToList(); 
           foreach (var permissionEx in permissionExists)
              {
                if(permissionEx!=null)
                 _permissionsRepo.Delete(permissionEx);
              }
            foreach (var id in recipeCommand.PermissionGroupIds)
             {
                RecipePermissionMapping permission = new RecipePermissionMapping();
                permission.PermissionId = id;
                permission.RecipeId = recipe.Id;
                recipe.RecipePermissionMappings.Add(permission);
            }
            _mealRepo.Update(recipe);
        }


        public List<RecipeIngredientMapping> GetRecipeIngredient()
        {
            return _mealsIngredientRepo.All.Select(x => x).ToList();
        }

        public bool DeleteRecipe(int Id)
        {
            var recipe = _mealRepo.Find(Id);
            if (recipe != null)
            {
                recipe.IsDeleted = true;
                recipe.ModifiedAt = DateTime.UtcNow;
                _mealRepo.Update(recipe);
            }
               return true;
        }
        //Deleted ingredient in database
        public int DeleteIngredient(int ingredientId, int recipeId)
        {
                    foreach (var ingreTypeExists in _ingreTypeRepoMap.FindBy(x => x.IngredientId == ingredientId))
                     {
                                 if (ingreTypeExists != null)
                                _ingreTypeRepoMap.Delete(ingreTypeExists);
                     }
                    foreach (var ingreRecipeExists in _mealsIngredientRepo.FindBy(x => x.IngredientId == ingredientId))
                    {
                        if (ingreRecipeExists != null)
                    _mealsIngredientRepo.Delete(ingreRecipeExists);
                    }

                    foreach (var ingreEx in _ingredientRepo.FindBy(x => x.Id == ingredientId))
                    {
                        if (ingreEx != null)
                            _ingredientRepo.Delete(ingreEx);
                    }

            int ingreExCount = _mealsIngredientRepo.FindBy(x => x.RecipeId == recipeId).Count();
            return ingreExCount;
        }

        //Deleted Upload image in database
        public void DeleteUploadImage(int imageId)
        {
            var imagePermExists = _imagePermissionMapRepo.FindBy(x => x.ImageId == imageId).ToList();
            foreach (var imagePerm in imagePermExists)
            {
                if (imagePerm != null)
                    _imagePermissionMapRepo.Delete(imagePerm);
            }

            var imageExists = _imageRepo.FindBy(x => x.Id == imageId).ToList();

            string imagePath = "";
         

            foreach (var imageEx in  imageExists)
            {
                imagePath = imageEx.Path;
                if (imageEx != null)
                    _imageRepo.Delete(imageEx);
            }
            

           // string filePath = System.Web.HttpContext.Current.Server.MapPath("/RecipeImages/" + imageName);

            string filePath = System.Web.HttpContext.Current.Server.MapPath(imagePath);
            FileInfo file = new FileInfo(filePath);
            if (file.Exists)
            {
                file.Delete();
            }
            
        }

        public Recipe GetMeal(int id)
        {
            var repo = _mealRepo.Find(id);

            return _mealRepo.Find(id);
        }

   

        public void UpdateUploadedImage(ImageCommand imageCommand, int recipeId,int userId)
        {
            var image = _imageRepo.Find(imageCommand.Id);          
            if (image != null)
            {
                string imagePath = image.Path;
                image.Name = imageCommand.Name;
                image.Path = imageCommand.Path;
                image.ModifiedAt = now;
                image.ModifiedBy = userId;
                var imagePermissionMappings = image.ImagePermissionMappings;
                foreach (var item in imagePermissionMappings)
                {
                    _imagePermissionMapRepo.Delete(item.Id);
                }
                foreach (var id in imageCommand.PermissionIds)
                {
                    ImagePermissionMapping permission = new ImagePermissionMapping();
                    permission.PermissionId = id;
                    image.ImagePermissionMappings.Add(permission);
                }
                _imageRepo.Update(image);

                string filePath = System.Web.HttpContext.Current.Server.MapPath(imagePath);
                FileInfo file = new FileInfo(filePath);
                if (file.Exists)
                {
                    file.Delete();
                }


            }
            else
            {
                UploadImage(imageCommand,userId);
            }
        }

        
          public void UpdateUploadedImagePermission(ImageCommand imageCommand, int recipeId)
           {
            var image = _imageRepo.Find(imageCommand.Id);
            if (image != null)
            {
                image.Name = imageCommand.Name;
                image.Path = imageCommand.Path;
                var imagePermissionMappings = image.ImagePermissionMappings;
                foreach (var item in imagePermissionMappings)
                {
                    _imagePermissionMapRepo.Delete(item.Id);
                }
                foreach (var id in imageCommand.PermissionIds)
                {
                    ImagePermissionMapping permission = new ImagePermissionMapping();
                    permission.PermissionId = id;
                    image.ImagePermissionMappings.Add(permission);
                }
                _imageRepo.Update(image);
            }
                   }

        public void UploadImage(ImageCommand imageCommand, int userId)
        {
            var image = new RecipeImage
            {
                RecipeId = imageCommand.MealId,
                Name = imageCommand.Name,
                Path = imageCommand.Path,
                CreatedAt=now,
                CreatedBy=userId
            };


            foreach (var id in imageCommand.PermissionIds)
            {
                ImagePermissionMapping permission = new ImagePermissionMapping();
                permission.PermissionId = id;
                image.ImagePermissionMappings.Add(permission);
            }
            _imageRepo.Add(image);

        }



        public IQueryable<ByDay.Data.Type> GetQuanities()
        {
            return _quantityRepo.FindBy(x => x.Type1 == "1");
        }

        public IQueryable<ByDay.Data.Type> GetQuanityTypes()
        {
            return _quantityRepo.FindBy(x => x.Type1 == "2");
        }

        public IQueryable<ByDay.Data.Type> GetIngridientType()
        {
            return _quantityRepo.FindBy(x => x.Type1 == "3");
        }

        public IQueryable<ByDay.Data.Category> GetCategory()
        {
            return _categoryRepo.All;
        }

        


        public IQueryable<ByDay.Data.SpecialDiet> GetSpecialDiet()
        {
            return _specialDietRepo.All;
        }

        public IQueryable<ByDay.Data.Type> GetSpicyLevel()
        {
            return _quantityRepo.FindBy(x => x.Type1 == "4");
        }
        public IQueryable<ByDay.Data.Type> GetSimplicityLevel()
        {
            return _quantityRepo.FindBy(x => x.Type1 == "5");
        }


        public IQueryable<ByDay.Data.Type> GetCuisines()
        {
            return _quantityRepo.FindBy(x => x.Type1 == "6");
        }



        public IQueryable<ByDay.Data.Type> GetIngredientCost()
        {
            return _quantityRepo.FindBy(x => x.Type1 == "7");
        }



        public IQueryable<ByDay.Data.Type> GetIngredientPreference()
        {
            return _quantityRepo.FindBy(x => x.Type1 == "8");
        }




        public IQueryable<Recipe> GetRecipeName(string RecipeName)
        {
            return _mealRepo.All.Select(x => x).Where(x => x.RecipeName == RecipeName && x.IsDeleted==false);


        }

        public IQueryable<Recipe> SearchRecipies(string searchText)
        {
            return _mealRepo.All.Select(x => x).Where(x => x.RecipeName.Contains(searchText) && x.IsDeleted == false && x.IsAdmin == true);

        }


        public IQueryable<Ingredient> SearchIngredients(string searchText)
        {
            return _ingredientRepo.All.Select(x => x).Where(x => x.Name.Contains(searchText)).Distinct();
        }



        public IQueryable<Ingredient> SearchIngredientsType(string id)
        {
            return _ingredientRepo.All.Select(x => x).Where(x => x.Name==id);

        }
        public IQueryable<Recipe> SearchRecipiesResult(string searchText)
        {

            return _mealRepo.All.Select(x => x).Where(x => x.RecipeName.Contains(searchText) && x.IsDeleted == false && x.IsAdmin == true);

        }
    }
}
