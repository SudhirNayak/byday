﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ByDay.Business.RecipeDetail
{
    public class ImageCommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public int MealId { get; set; }
        public List<int> PermissionIds { get; set; }
        public virtual ByDay.Data.Recipe Meal { get; set; }
        public virtual Permission PermissionType { get; set; }
    }
}