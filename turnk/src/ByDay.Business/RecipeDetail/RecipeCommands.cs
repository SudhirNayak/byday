﻿using ByDay.Data;
using System;
using System.Collections.Generic;

namespace ByDay.Business.RecipeDetail
{
    public class RecipeCommands
    {

        public RecipeCommands()
        {
            this.images = new HashSet<ImageCommand>();
            this.MealsCategories = new HashSet<RecipeCategoryMapping>();
            this.permissions = new HashSet<RecipePermissionMapping>();
            this.Ingrideints = new List<ByDay.Business.RecipeDetail.IngridientCommand>();
        }
        public int UserId { get; set; }
        public List<int> IsMostLikeIds { get; set; }
        public int? RecipeId { get; set; }
        public string RecipeName { get; set; }
        public int? SimplicityId { get; set; }
       // public int? SplicyLevelId { get; set; }
        public List<int> CategoryId { get; set; }

        public List<int> SpecialDietId { get; set; }
        public List<int> IngridientId { get; set; }
        public int? QuanityTypeId { get; set; }
        public int? QuantityId { get; set; }
        public int Id { get; set; }
        public int? SpiceLevelId { get; set; }
        public string Instructions { get; set; }
        public string Description { get; set; }
        public bool ReadyWhenGetHome { get; set; }
        public Nullable<int> QuickFixMatch { get; set; }
        public string IsMostLike { get; set; }
        public int? NumServings { get; set; }
        public Nullable<int> Cost { get; set; }
        public string PickyEaterTip { get; set; }
        public string GeneralTips { get; set; }
        public int? IngredientCost { get; set; }
        public int? CookingTime { get; set; }
        public int? PrepTime { get; set; }
        public bool? UniqueToDatabase { get; set; }
        public List<AgeAppropriate> AgeAppropriateness { get; set; }
        public int? CuisineId { get; set; }
        public bool? Top10PercentOfCuisine { get; set; }
        public bool? Freezable { get; set; }
        public byte SimplicityLevel { get; set; }
       // public bool? BackupPlanWorthiness { get; set; }
        public bool? Publish { get; set; }
        public int? LeftoverRecipeId { get; set; }
        //public int RecipePermissions { get; set; }
        public bool? WeekendPreparable { get; set; }
        //public virtual Cuisine Cuisine { get; set; }

        public string IngredientName { get; set; }

        public bool? Omittable { get; set; }
        public string Tip { get; set; }
        public bool? HardToFind { get; set; }
        public List<int> AgeGroupIds { get; set; }
        public List<int> PermissionGroupIds { get; set; }
        public List<string> IsMostLikeGroupId { get; set; }

        public string LeftOverRecipe { get; set; }
        public List<int> LeftOverRecipeGroupId { get; set; }

        public List<int> CusineGroupId { get; set; }

        public List<ByDay.Data.SpecialDiet> SpecialDiet { get; set; }

        public Nullable<int> IngredientPreferenceId { get; set; }
        public virtual ICollection<ImageCommand> images { get; set; }
     
        public virtual ICollection<RecipeCategoryMapping> MealsCategories { get; set; }
     
        public virtual ICollection<RecipePermissionMapping> permissions { get; set; }

        public List<ByDay.Business.RecipeDetail.IngridientCommand> Ingrideints { get; set; }



    }
}