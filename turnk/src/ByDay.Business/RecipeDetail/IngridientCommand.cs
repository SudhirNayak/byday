﻿using System.Collections.Generic;

namespace ByDay.Business.RecipeDetail
{
    public class IngridientCommand
    {


        //[Required(ErrorMessage = "This field is required.",AllowEmptyStrings =false)]
        public string IngredientName { get; set; }

        public int? Id { get; set; }
        public List<int> IngridientTypeId { get; set; }

        public int? QuanityTypeId { get; set; }
        public int? QuantityId { get; set; }
        public IEnumerable<ByDay.Data.Type> Quantities { get; set; }
        public IEnumerable<ByDay.Data.Type> QuantityType { get; set; }
        public int? IngredientCost { get; set; }
        public bool? Omittable { get; set; }
        public string Tip { get; set; }
        public bool? HardToFind { get; set; }
        public string IngredientType { get; set; }

        public IEnumerable<ByDay.Data.Type> IngridientType { get; set; }


    }
}