﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.Task
{
    public class TaskService
    {
        ByDayRepository<ByDay.Data.Task> _taskRepository = new ByDayRepository<ByDay.Data.Task>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.TaskCategory> _taskCategoryRepository = new ByDayRepository<ByDay.Data.TaskCategory>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.SyncLog> _syncLogRepository = new ByDayRepository<ByDay.Data.SyncLog>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.TaskPriority> _taskpriorityRepository = new ByDayRepository<ByDay.Data.TaskPriority>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.TaskAttendee> _taskAttendeeRepository = new ByDayRepository<ByDay.Data.TaskAttendee>(new Ex2ByDayEntities());

        public UserTaskVModel GetTasks(int id)
        {
            UserTaskVModel model = new UserTaskVModel();
            var userTasks = _taskRepository.FindBy(x => x.UserId == id).ToList();

            model.UserTask = userTasks.Select(x => new TaskViewModel()
            {
                CategoryId = x.CategoryId,
                Comments = x.Comments,
                CompleteAt = x.CompleteAt,
                CreatedAt = x.CreatedAt,
                DueDate = x.DueDate,
                Id = x.Id,
                PriorityId = x.PriorityId,
                RDate = x.RDate,
                RemindMe = x.RemindMe,
                RRule = x.RRule,
                Title = x.Title,
                UserId = x.UserId,
            }).ToList();

            model.UserTaskAttendee = (from t in _taskAttendeeRepository.All.ToList()
                                      join m in model.UserTask on t.TaskId equals m.Id
                                      select new TaskAttendeeViewModel()
                                      {
                                          Id = t.Id,
                                          TaskId = t.TaskId,
                                          UserId = t.UserId
                                      }).ToList();

            model.UserTaskCategory = (from t in _taskCategoryRepository.All.ToList()
                                      join m in model.UserTask on t.Id equals m.CategoryId
                                      select new TaskCategoryViewModel()
                                      {
                                          Id = t.Id,
                                          UserId = t.UserId,
                                          CreatedAt = t.CreatedAt,
                                          IsActive = t.IsActive,
                                          ModifiedAt = t.ModifiedAt,
                                          Title = t.Title
                                      }).ToList();

            model.UserTaskPriority = (from t in _taskpriorityRepository.All.ToList()
                                      join m in model.UserTask on t.Id equals m.PriorityId
                                      select new TaskPriorityViewModel()
                                      {
                                          Id = t.Id,
                                          Title = t.Title,
                                          Colour = t.Colour
                                      }).ToList();
            return model;
        }


        public TaskSyncResponse SyncedTasks(UserTaskViewModel userTaskSyncModel, int userId, int lastSyncedId)
        {
            TaskSyncResponse response = new TaskSyncResponse();
            _syncLogRepository.Add(response.log);

            if (CheckLogSyncId(lastSyncedId, userId, "Task") > 0)
            {
                response.Code = 1;
                return response;
            }

            return response;
        }

        public TaskSyncResponse SyncedModel(UserTaskViewModel userTaskSyncModel, int userId, int lastSyncedId)
        {
            TaskSyncResponse response = new TaskSyncResponse();

            #region TaskCategorySYnc
            if (userTaskSyncModel.CreateUserTaskCategory != null)
            {
                foreach (var category in userTaskSyncModel.CreateUserTaskCategory)
                {
                    var res = UserTaskCategory(category);
                    res.CreatedAt = DateTime.UtcNow;
                    _taskCategoryRepository.Add(res);

                    category.Id = res.Id;
                    response.log = UserTaskSyncLog(userId, res.Id, "TaskCategory", "INSERT");
                    _syncLogRepository.Add(response.log);
                }

            }
            response.CreateUserTaskCategory = userTaskSyncModel.CreateUserTaskCategory;
            if (userTaskSyncModel.UpdateUserTaskCategory != null)
            {
                foreach (var category in userTaskSyncModel.UpdateUserTaskCategory)
                {
                    var res = UserTaskCategory(category);
                    res.CreatedAt = DateTime.UtcNow;
                    res.ModifiedAt = DateTime.UtcNow;
                    _taskCategoryRepository.Update(res);
                    response.log = UserTaskSyncLog(userId, res.Id, "TaskCategory", "UPDATE");
                    _syncLogRepository.Add(response.log);
                }
            }

            if (userTaskSyncModel.DeleteUserTaskCategory != null)
            {
                foreach (var category in userTaskSyncModel.DeleteUserTaskCategory)
                {
                    _taskCategoryRepository.Delete(category.Id);
                    response.log = UserTaskSyncLog(userId, category.Id, "TaskCategory", "DELETE");
                    _syncLogRepository.Add(response.log);
                }
            }
            #endregion

            #region TaskPrioritySync
            if (userTaskSyncModel.CreateUserTaskPriority != null)
            {
                foreach (var priority in userTaskSyncModel.CreateUserTaskPriority)
                {
                    var res = UserTaskPriority(priority);
                    _taskpriorityRepository.Add(res);
                    priority.Id = res.Id;
                    response.log = UserTaskSyncLog(userId, res.Id, "TaskPriority", "INSERT");
                    _syncLogRepository.Add(response.log);
                }
            }
            response.CreateUserTaskPriority = userTaskSyncModel.CreateUserTaskPriority;

            if (userTaskSyncModel.UpdateUserTaskPriority != null)
            {
                foreach (var priority in userTaskSyncModel.UpdateUserTaskPriority)
                {
                    var res = UserTaskPriority(priority);
                    _taskpriorityRepository.Update(res);
                    response.log = UserTaskSyncLog(userId, res.Id, "TaskPriority", "UPDATE");
                    _syncLogRepository.Add(response.log);
                }
            }

            if (userTaskSyncModel.DeleteUserTaskPriority != null)
            {
                foreach (var priority in userTaskSyncModel.DeleteUserTaskPriority)
                {
                    _taskpriorityRepository.Delete(priority.Id);
                    response.log = UserTaskSyncLog(userId, priority.Id, "TaskPriority", "DELETE");
                    _syncLogRepository.Add(response.log);
                }
            }
            #endregion

            #region TaskSYnc
            if (userTaskSyncModel.CreateUserTask != null)
            {
                foreach (var task in userTaskSyncModel.CreateUserTask)
                {
                    var res = UserTask(task);
                    var category = userTaskSyncModel.CreateUserTaskCategory.FirstOrDefault(t => t.tempId == task.tempId);
                    if (category != null)
                        res.CategoryId = category.Id;
                    category = userTaskSyncModel.UpdateUserTaskCategory.FirstOrDefault(t => t.tempId == task.tempId);
                    if (category != null)
                        res.CategoryId = category.Id;

                    if (userTaskSyncModel.CreateUserTaskPriority != null)
                    {
                        var priority = userTaskSyncModel.CreateUserTaskPriority.FirstOrDefault(t => t.tempId == task.tempId);
                        if (priority != null)
                            res.PriorityId = priority.Id;
                    }
                    if (userTaskSyncModel.CreateUserTaskPriority != null)
                    {
                        var priority = userTaskSyncModel.UpdateUserTaskPriority.FirstOrDefault(t => t.tempId == task.tempId);
                        if (priority != null)
                            res.PriorityId = priority.Id;
                    }
                    _taskRepository.Add(res);
                    task.Id = res.Id;
                    response.log = UserTaskSyncLog(userId, res.Id, "Task", "INSERT");
                    _syncLogRepository.Add(response.log);
                }

            }
            response.CreateUserTask = userTaskSyncModel.CreateUserTask;
            if (userTaskSyncModel.UpdateUserTask != null)
            {
                foreach (var task in userTaskSyncModel.UpdateUserTask)
                {
                    var res = UserTask(task);
                    var category = userTaskSyncModel.CreateUserTaskCategory.FirstOrDefault(t => t.tempId == task.tempId);
                    if (category != null)
                        res.CategoryId = category.Id;
                    category = userTaskSyncModel.UpdateUserTaskCategory.FirstOrDefault(t => t.tempId == task.tempId);
                    if (category != null)
                        res.CategoryId = category.Id;

                    if (userTaskSyncModel.CreateUserTaskPriority != null)
                    {
                        var priority = userTaskSyncModel.CreateUserTaskPriority.FirstOrDefault(t => t.tempId == task.tempId);
                        if (priority != null)
                            res.PriorityId = priority.Id;
                    }
                    if (userTaskSyncModel.CreateUserTaskPriority != null)
                    {
                        var priority = userTaskSyncModel.UpdateUserTaskPriority.FirstOrDefault(t => t.tempId == task.tempId);
                        if (priority != null)
                            res.PriorityId = priority.Id;
                    }
                    _taskRepository.Update(res);
                    response.log = UserTaskSyncLog(userId, res.Id, "Task", "UPDATE");
                    _syncLogRepository.Add(response.log);
                }
            }
            if (userTaskSyncModel.DeleteUserTask != null)
            {
                foreach (var task in userTaskSyncModel.DeleteUserTask)
                {
                    _taskRepository.Delete(task.Id);
                    response.log = UserTaskSyncLog(userId, task.Id, "Task", "DELETE");
                    _syncLogRepository.Add(response.log);
                }
            }
            #endregion

            #region TaskAttendeeSync
            if (userTaskSyncModel.CreateUserTaskAttendee != null)
            {
                foreach (var taskAttendee in userTaskSyncModel.CreateUserTaskAttendee)
                {
                    var res = UserTaskAttendee(taskAttendee);
                    if (userTaskSyncModel.CreateUserTask != null)
                    {
                        var task = userTaskSyncModel.CreateUserTask.FirstOrDefault(t => t.tempId == taskAttendee.tempId);
                        if (task != null)
                            res.TaskId = task.Id;
                    }
                    if (userTaskSyncModel.UpdateUserTask != null)
                    {
                        var task = userTaskSyncModel.UpdateUserTask.FirstOrDefault(t => t.tempId == taskAttendee.tempId);
                        if (task != null)
                            res.TaskId = task.Id;
                    }
                    _taskAttendeeRepository.Add(res);
                    taskAttendee.Id = res.Id;
                    response.log = UserTaskSyncLog(userId, res.Id, "TaskAttendees", "INSERT");
                    _syncLogRepository.Add(response.log);
                }
            }
            response.CreateUserTaskAttendee = userTaskSyncModel.CreateUserTaskAttendee;
            if (userTaskSyncModel.UpdateUserTaskAttendee != null)
            {
                foreach (var taskAttendee in userTaskSyncModel.UpdateUserTaskAttendee)
                {
                    var res = UserTaskAttendee(taskAttendee);
                    _taskAttendeeRepository.Update(res);
                    response.log = UserTaskSyncLog(userId, res.Id, "TaskAttendees", "UPDATE");
                    _syncLogRepository.Add(response.log);
                }
            }
            if (userTaskSyncModel.DeleteUserTaskAttendee != null)
            {
                foreach (var taskAttendee in userTaskSyncModel.DeleteUserTaskAttendee)
                {
                    _taskAttendeeRepository.Delete(taskAttendee.Id);
                    response.log = UserTaskSyncLog(userId, taskAttendee.Id, "TaskAttendees", "DELETE");
                    _syncLogRepository.Add(response.log);
                }
            }
            #endregion

            return response;
        }


        public ByDay.Data.SyncLog UserTaskSyncLog(int userId, int rowId, string entity, string actionType)
        {
            var obj = new SyncLog()
            {
                UserId = userId,
                Entity = entity,
                EntityRowId = rowId,
                ActionType = actionType,
                SyncDate = DateTime.UtcNow
            };
            return obj;
        }

        public int CheckLogSyncId(long id, long userId, string Entity)
        {
            return _syncLogRepository.FindBy(t => t.Id > id && t.UserId == userId && t.Entity == Entity).Count();
        }

        public dynamic GetData(int userId, int syncId, string schema, ref string lastSyncId)
        {
            return _syncLogRepository.ExecWithStoreProcedureSync("GetData", ref lastSyncId, new object[] { userId, syncId, schema });
        }

        public ByDay.Data.Task UserTask(TaskViewModel task)
        {
            var taskObj = new Data.Task()
            {
                Id = task.Id,
                CategoryId = task.CategoryId,
                Comments = task.Comments,
                Title = task.Title,
                RRule = task.RRule,
                RemindMe = task.RemindMe,
                RDate = task.RDate,
                PriorityId = task.PriorityId,
                DueDate = task.DueDate,
                CreatedAt = task.CreatedAt,
                CompleteAt = task.CompleteAt,
                UserId = task.UserId
            };
            return taskObj;
        }

        public ByDay.Data.TaskAttendee UserTaskAttendee(TaskAttendeeViewModel taskAttendee)
        {
            var taskAttendeeObj = new Data.TaskAttendee()
            {
                Id = taskAttendee.Id,
                TaskId = taskAttendee.TaskId,
                UserId = taskAttendee.UserId
            };
            return taskAttendeeObj;
        }

        public ByDay.Data.TaskCategory UserTaskCategory(TaskCategoryViewModel taskCategory)
        {
            var taskCategoryObj = new Data.TaskCategory()
            {
                Id = taskCategory.Id,
                UserId = taskCategory.UserId,
                CreatedAt = taskCategory.CreatedAt,
                IsActive = taskCategory.IsActive == null ? true : taskCategory.IsActive,
                ModifiedAt = taskCategory.ModifiedAt,
                Title = taskCategory.Title,
                Colour = taskCategory.Colour
            };
            return taskCategoryObj;
        }

        public ByDay.Data.TaskPriority UserTaskPriority(TaskPriorityViewModel taskPriority)
        {
            var taskPriorityObj = new Data.TaskPriority()
            {
                Id = taskPriority.Id,
                Title = taskPriority.Title,
                Colour = taskPriority.Colour
            };
            return taskPriorityObj;
        }
    }

    public class UserTaskViewModel
    {
        public List<TaskViewModel> CreateUserTask { get; set; }
        public List<TaskViewModel> UpdateUserTask { get; set; }
        public List<TaskViewModel> DeleteUserTask { get; set; }

        public List<TaskPriorityViewModel> CreateUserTaskPriority { get; set; }
        public List<TaskPriorityViewModel> UpdateUserTaskPriority { get; set; }
        public List<TaskPriorityViewModel> DeleteUserTaskPriority { get; set; }

        public List<TaskCategoryViewModel> CreateUserTaskCategory { get; set; }
        public List<TaskCategoryViewModel> UpdateUserTaskCategory { get; set; }
        public List<TaskCategoryViewModel> DeleteUserTaskCategory { get; set; }

        public List<TaskAttendeeViewModel> CreateUserTaskAttendee { get; set; }
        public List<TaskAttendeeViewModel> UpdateUserTaskAttendee { get; set; }
        public List<TaskAttendeeViewModel> DeleteUserTaskAttendee { get; set; }

    }

    public class UserTaskVModel
    {
        public List<TaskViewModel> UserTask { get; set; }

        public List<TaskPriorityViewModel> UserTaskPriority { get; set; }

        public List<TaskCategoryViewModel> UserTaskCategory { get; set; }

        public List<TaskAttendeeViewModel> UserTaskAttendee { get; set; }
    }

    public class TaskViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public Nullable<int> PriorityId { get; set; }
        public Nullable<System.DateTime> DueDate { get; set; }
        public int localId { get; set; }
        public string Comments { get; set; }
        public string RRule { get; set; }
        public Nullable<System.DateTime> RDate { get; set; }
        public Nullable<bool> RemindMe { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> CompleteAt { get; set; }
        public int UserId { get; set; }
        public int tempId { get; set; }
    }
    public class TaskPriorityViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Colour { get; set; }
        public int tempId { get; set; }
        public int localId { get; set; }
    }
    public class TaskCategoryViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int UserId { get; set; }
        public int tempId { get; set; }
        public string Colour { get; set; }
        public int localId { get; set; }
    }
    public class TaskAttendeeViewModel
    {
        public int Id { get; set; }
        public Nullable<int> TaskId { get; set; }
        public Nullable<int> UserId { get; set; }
        public int tempId { get; set; }
        public int localId { get; set; }
    }

    public class TaskSyncResponse
    {
        public int lastSyncId { get; set; }
        public int Code { get; set; }
        public SyncLog log { get; set; }
        public List<TaskViewModel> CreateUserTask { get; set; }
        public List<TaskPriorityViewModel> CreateUserTaskPriority { get; set; }
        public List<TaskCategoryViewModel> CreateUserTaskCategory { get; set; }
        public List<TaskAttendeeViewModel> CreateUserTaskAttendee { get; set; }

    }

    public class SyncTaskResult
    {
        public DataTable TaskIU { get; set; }
        public DataTable TaskD { get; set; }
        public DataTable TaskCategoryIU { get; set; }
        public DataTable TaskCategoryD { get; set; }
        public DataTable TaskAttendeeIU { get; set; }
        public DataTable TaskAttendeeD { get; set; }
        public DataTable TaskPriorityIU { get; set; }
        public DataTable TaskPriorityD { get; set; }
    }


}
