﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.Orders
{
  public  class OrderCommand
    {

        public string Payment_method_nonce { get; set; }
        public int OrderId { get; set; }
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public int Qty { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverEmail { get; set; }
        public string ReceiverNum { get; set; }
        public string ReceiverAddress { get; set; }
        public string City { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public Nullable<int> PostalCode { get; set; }
        public string LandMark { get; set; }
        public string Message { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public string ImageType { get; set; }

        public int AddressId { get; set; }
        public int StatusId { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
    }
}
