﻿using ByDay.Data;
using System;
using System.Linq;

namespace ByDay.Business.Orders
{
    public class OrderService
    {
        ByDayRepository<OrderListing> _orderRepo = new ByDayRepository<OrderListing>(new Ex2ByDayEntities());
        ByDayRepository<Status> _statusRepo = new ByDayRepository<Status>(new Ex2ByDayEntities());
        public int Insert(OrderCommand orderCommand)
        {
            var order = new OrderListing
            {
                City = orderCommand.City,
                CountryId = orderCommand.CountryId,
                LandMark = orderCommand.LandMark,
                Message = orderCommand.Message,
                OrderDate = DateTime.UtcNow,
                PostalCode = orderCommand.PostalCode,
                ProductId = orderCommand.ProductId,
                Qty = orderCommand.Qty,
                ReceiverAddress = orderCommand.ReceiverAddress,
                ReceiverEmail = orderCommand.ReceiverEmail,
                ReceiverName = orderCommand.ReceiverName,
                ReceiverNum = orderCommand.ReceiverNum,
                StateId = orderCommand.StateId,
                TotalPrice = orderCommand.TotalPrice,
                UserId = orderCommand.UserId,
                AddressId = orderCommand.AddressId,
                StatusId = orderCommand.StatusId,
                ImageName = orderCommand.ImageName,
                ImagePath = orderCommand.ImagePath,
                ImageType = orderCommand.ImageType

            };
            _orderRepo.Add(order);

            return order.Id;

        }


        public IQueryable<OrderListing> SearchOrderResult(string searchText)
        {
            // return _orderRepo.All.Select(x => x).Where(x => x..Contains(searchText));
            return _orderRepo.All.Select(x => x);

        }

        public IQueryable<OrderListing> SearchOrder(string searchText)
        {
            return _orderRepo.All.Select(x => x).Where(x => x.ByDayUser.Name.Contains(searchText));

        }


        public IQueryable<OrderListing> GetOrderById(int id)
        {

            return _orderRepo.All.Select(x => x).Where(y => y.Id == id);

        }

        public IQueryable<OrderListing> GetAllOrder()
        {
            return _orderRepo.All.Select(x => x);

        }

        public IQueryable<Status> GetAllStatus()
        {
            return _statusRepo.All.Select(x => x);

        }

        public void UpdateOrderOfStatus(int statusId, int Id)
        {

            var orders = _orderRepo.Find(Id);

            orders.StatusId = statusId;
            // orders.ModifiedAt = DateTime.UtcNow;
            _orderRepo.Update(orders);

        }



        //public decimal GetAmountOfProduct(int UserId, int ProductId)
        //{
        //    var totalPrice = _orderRepo.All.Where(y => y.UserId == UserId && y.ProductId == ProductId).Select(x=>x.TotalPrice).ToList();
        //    decimal price = 0;
        //    if(totalPrice.Count!=0)
        //    {
        //        price = Convert.ToDecimal(totalPrice[0]);
        //    }
        //    return price;
        //}
    }
}
