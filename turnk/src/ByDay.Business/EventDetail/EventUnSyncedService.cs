﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.EventDetail
{
    public class EventUnSyncedService
    {
        private readonly ByDayRepository<Event> _eventRepository;
        private readonly ByDayRepository<EventAttendee> _eventAttendeeRepository;
        private readonly ByDayRepository<Reminder> _eventReminderRepository;
        private readonly ByDayRepository<ByDay.Data.SyncLog> _syncLogRepository;

        public EventUnSyncedResponse eventResponse = new EventUnSyncedResponse();
        public EventUnSyncedService()
        {
            _eventRepository = new ByDayRepository<Event>(new Ex2ByDayEntities());
            _eventAttendeeRepository = new ByDayRepository<EventAttendee>(new Ex2ByDayEntities());
            _eventReminderRepository = new ByDayRepository<Reminder>(new Ex2ByDayEntities());
            _syncLogRepository = new ByDayRepository<ByDay.Data.SyncLog>(new Ex2ByDayEntities());
        }

        public CreatedEventResponse Insert(EventCommand eventCommand)
        {
            CreatedEventResponse response = new CreatedEventResponse();
            var eventDetail = new Event()
            {
                TypeId = eventCommand.TypeId,
                Title = eventCommand.Title,
                AssociatedEntityId = eventCommand.AssociatedEntityId,
                IsAllDayEvent = eventCommand.IsAllDayEvent,
                HasAlarm = eventCommand.HasAlarm,
                HasAttendees = eventCommand.HasAttendees,
                Description = eventCommand.Description,
                StartDate = eventCommand.StartDate,
                EndDate = eventCommand.EndDate,
                Duration = eventCommand.Duration,
                StartTimeZone = eventCommand.StartTimeZone,
                EndTimeZone = eventCommand.EndTimeZone,
                Location = eventCommand.Location,
                ExDate = eventCommand.ExDate,
                ExRule = eventCommand.ExRule,
                OriginalAllDay = eventCommand.OriginalAllDay,
                OriginalId = eventCommand.OriginalId,
                OriginalInstanceTime = eventCommand.OriginalInstanceTime,
                RDateUTC = eventCommand.RDateUTC,
                RRule = eventCommand.RRule,
                CreatedAt = DateTime.UtcNow,
                CalendarId = eventCommand.OwnerCalenderId
            };
            _eventRepository.Add(eventDetail);

            if(eventDetail.Id!=0)
            {
                response.Id = eventDetail.Id;
                response.TempId = eventCommand.TempID;
                response.Success = true;
                

            }
            return response;
        }



        public ModifiedEventResponse Update(EventCommand eventCommand)
        {
            ModifiedEventResponse response = new ModifiedEventResponse();
            var currentEvent = _eventRepository.FindBy(x => x.Id == eventCommand.Id).FirstOrDefault();


            currentEvent.Id = eventCommand.Id;
            currentEvent.TypeId = eventCommand.TypeId;
            currentEvent.Title = eventCommand.Title;
            currentEvent.AssociatedEntityId = eventCommand.AssociatedEntityId;
            currentEvent.IsAllDayEvent = eventCommand.IsAllDayEvent;
            currentEvent.HasAlarm = eventCommand.HasAlarm;
            currentEvent.HasAttendees = eventCommand.HasAttendees;
            currentEvent.Description = eventCommand.Description;
            currentEvent.StartDate = eventCommand.StartDate;
            currentEvent.EndDate = eventCommand.EndDate;
            currentEvent.Duration = eventCommand.Duration;
            currentEvent.StartTimeZone = eventCommand.StartTimeZone;
            currentEvent.EndTimeZone = eventCommand.EndTimeZone;
            currentEvent.Location = eventCommand.Location;
            currentEvent.ExDate = eventCommand.ExDate;
            currentEvent.ExRule = eventCommand.ExRule;
            currentEvent.OriginalAllDay = eventCommand.OriginalAllDay;
            currentEvent.OriginalId = eventCommand.OriginalId;
            currentEvent.OriginalInstanceTime = eventCommand.OriginalInstanceTime;
            currentEvent.RDateUTC = eventCommand.RDateUTC;
            currentEvent.RRule = eventCommand.RRule;
            currentEvent.ModifiedAt = DateTime.UtcNow;
            currentEvent.CalendarId = eventCommand.OwnerCalenderId;



            //var eventDetail = new Event()
            //{
            //    Id= eventCommand.Id,
            //    TypeId = eventCommand.TypeId,
            //    Title = eventCommand.Title,
            //    AssociatedEntityId = eventCommand.AssociatedEntityId,
            //    IsAllDayEvent = eventCommand.IsAllDayEvent,
            //    HasAlarm = eventCommand.HasAlarm,
            //    HasAttendees = eventCommand.HasAttendees,
            //    Description = eventCommand.Description,
            //    StartDate = eventCommand.StartDate,
            //    EndDate = eventCommand.EndDate,
            //    Duration = eventCommand.Duration,
            //    StartTimeZone = eventCommand.StartTimeZone,
            //    EndTimeZone = eventCommand.EndTimeZone,
            //    Location = eventCommand.Location,
            //    ExDate = eventCommand.ExDate,
            //    ExRule = eventCommand.ExRule,
            //    OriginalAllDay = eventCommand.OriginalAllDay,
            //    OriginalId = eventCommand.OriginalId,
            //    OriginalInstanceTime = eventCommand.OriginalInstanceTime,
            //    RDateUTC = eventCommand.RDateUTC,
            //    RRule = eventCommand.RRule,
            //    ModifiedAt = DateTime.Now
            //};
            _eventRepository.Update(currentEvent);

            if (currentEvent.ModifiedAt!=null)
            {
                response.Id = currentEvent.Id;
                response.Success = true;


            }
            return response;
        }


        public DeletedEventResponse Delete(EventCommand eventCommand)
        {
            DeletedEventResponse _deleteEvent = new DeletedEventResponse();
            var currentEvent = _eventRepository.FindBy(x => x.Id == eventCommand.Id).ToList();

            var currentEventAttendee = _eventAttendeeRepository.FindBy(x => x.EventId == eventCommand.Id).ToList();
            foreach(var data in currentEventAttendee)
            {
                //data.IsDeleted = true;
                //data.ModifiedAt = DateTime.UtcNow;
                _eventAttendeeRepository.Delete(data);
            }
            var currentReminder = _eventReminderRepository.FindBy(x => x.EventId == eventCommand.Id).ToList();
            foreach (var data in currentReminder)
            {
                //data.IsDeleted = true;

                //data.ModifiedAt = DateTime.UtcNow;


                _eventReminderRepository.Delete(data);
            }
            foreach (var data in currentEvent)
            {
                //data.IsDeleted = true;
                //data.ModifiedAt = DateTime.UtcNow;
                _eventRepository.Delete(data);

                _deleteEvent.Id = eventCommand.Id;
                _deleteEvent.Success = true;
            }
         
            return _deleteEvent;
        }

        public long InsertSync(int rowId, string tableName,string actionName,long userId)
        {
            ByDay.Data.SyncLog syncLog = new Data.SyncLog();
            syncLog.UserId = Convert.ToInt32(userId);
            syncLog.ActionType = actionName;
            syncLog.Entity = tableName;
            syncLog.SyncDate = DateTime.UtcNow;
            syncLog.EntityRowId = rowId;
            _syncLogRepository.Add(syncLog);
            return syncLog.Id;
        }

        public int CheckLogSyncId(long id, long userId)
        {
            return _syncLogRepository.FindBy(t => t.Id > id && t.UserId == userId).Count();
        }

        public dynamic GetData(int userId, int syncId, string schema, ref string lastSyncId)
        {
            return _syncLogRepository.ExecWithStoreProcedureSync("GetData", ref lastSyncId, new object[] { userId, syncId, schema });
        }
    }
}
