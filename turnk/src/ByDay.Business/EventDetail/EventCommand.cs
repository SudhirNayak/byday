﻿using System;
using System.Collections.Generic;
using ByDay.Business.EventAttendees;
using ByDay.Business.EventReminder;
namespace ByDay.Business.EventDetail
{
    public class EventCommand
    {
        public EventCommand()
        {
            this.EventAttendees = new List<EventAttendeeCommand>();
            this.EventReminder = new List<EventReminderCommand>();
        }
        public int Id { get; set; }
        public string TempID { get; set; }
        public int TypeId { get; set; }
        public string Title { get; set; }
        public Nullable<int> AssociatedEntityId { get; set; }
        public bool IsAllDayEvent { get; set; }
        public bool HasAlarm { get; set; }
        public bool HasAttendees { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Duration { get; set; }
        public string StartTimeZone { get; set; }
        public string EndTimeZone { get; set; }
        public string Location { get; set; }
        public Nullable<System.DateTime> ExDate { get; set; }
        public string ExRule { get; set; }
        public bool OriginalAllDay { get; set; }
        public Nullable<int> OriginalId { get; set; }
        public Nullable<System.DateTime> OriginalInstanceTime { get; set; }
        public Nullable<System.DateTime> RDateUTC { get; set; }
        public string RRule { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public Nullable<System.DateTime> LastDate { get; set; }

        public bool IsDeleted { get; set; }
        public List<EventReminderCommand> EventReminder { get; set; }
        public List<EventAttendeeCommand> EventAttendees { get; set; }

        /*New Section*/
        public List<EventAttendeeCommand> CreatedAttendees { get; set; }
        public List<EventAttendeeCommand> ModifiedAttendees { get; set; }
        public List<EventAttendeeCommand> DeletedAttendees { get; set; }
        public List<EventReminderCommand> CreatedReminders { get; set; }
        public List<EventReminderCommand> ModifiedReminders { get; set; }
        public List<EventReminderCommand> DeletedReminders { get; set; }
        public List<long> Attendees { get; set; }
        public int OwnerCalenderId { get; set; }
        public int VersionNumber { get; set; }
    }

    public class SyncLog
    {

    }
}
