﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ByDay.Business.EventAttendees;
using ByDay.Business.EventReminder;

namespace ByDay.Business.EventDetail
{
  public  class EventUnSyncedCommand
    {
        public EventUnSyncedCommand()
        {
            CreatedEvents = new List<EventCommand>();
            ModifiedEvents = new List<EventCommand>();
            DeletedEvents = new List<EventCommand>();

            //CreatedAttendees = new List<EventAttendeeCommand>();
            //ModifiedAttendees = new List<EventAttendeeCommand>();
            //DeletedAttendees = new List<EventAttendeeCommand>();

            //CreatedReminders = new List<EventReminderCommand>();
            //ModifiedReminders = new List<EventReminderCommand>();
            //DeletedReminders = new List<EventReminderCommand>();
        }

        public List<EventCommand> CreatedEvents { get; set; }
        public List<EventCommand> ModifiedEvents { get; set; }
        public List<EventCommand> DeletedEvents { get; set; }

        //public List<EventAttendeeCommand> CreatedAttendees { get; set; }
        //public List<EventAttendeeCommand> ModifiedAttendees { get; set; }
        //public List<EventAttendeeCommand> DeletedAttendees { get; set; }

        //public List<EventReminderCommand> CreatedReminders { get; set; }
        //public List<EventReminderCommand> ModifiedReminders { get; set; }
        //public List<EventReminderCommand> DeletedReminders { get; set; }
        //public List<EventAttendeeCommand> Attendees { get; set; }
    }
}
