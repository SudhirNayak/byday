﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.EventDetail
{
    public class EventUnSyncedResponse
    {
        public EventUnSyncedResponse()
        {
            CreatedEvents = new List<CreatedEventResponse>();
            ModifedEvents = new List<ModifiedEventResponse>();
            DeletedEvents = new List<DeletedEventResponse>();

            CreatedAttendees = new List<CreatedAttendeesResponse>();
            ModifiedAttendees = new List<ModifiedAttendeesResponse>();
            DeletedAttendees = new List<DeletedAttendeesResponse>();
            CreatedReminder = new List<CreatedRemindersResponse>();
            ModifiedReminder = new List<ModifiedRemindersResponse>();
            DeletedReminder = new List<DeletedRemindersResponse>();
        }


        public string LastSync
        {
            get
            {
                return DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fff"); 
            }
        }



        public List<CreatedEventResponse> CreatedEvents { get; set; }
        public List<ModifiedEventResponse> ModifedEvents { get; set; }
        public List<DeletedEventResponse> DeletedEvents { get; set; }
        public List<CreatedAttendeesResponse> CreatedAttendees { get; set; }
        public List<ModifiedAttendeesResponse> ModifiedAttendees { get; set; }

        public List<DeletedAttendeesResponse> DeletedAttendees { get; set; }

        public List<CreatedRemindersResponse> CreatedReminder { get; set; }
        public List<ModifiedRemindersResponse> ModifiedReminder { get; set; }
        public List<DeletedRemindersResponse> DeletedReminder { get; set; }

        public int code { get; set; }
        public long LastLogId { get; set; }

    }
    public class CreatedEventResponse
    {
        public string TempId { get; set; }

        public int Id { get; set; }

        public bool Success { get; set; }
        public int VersionNumber { get; set; }

    }
    public class ModifiedEventResponse
    {
        public int Id { get; set; }
        public bool Success { get; set; }
        public int VersionNumber { get; set; }
    }

    public class DeletedEventResponse
    {
        public int Id { get; set; }
        public bool Success { get; set; }
        public int VersionNumber { get; set; }
    }

    public class CreatedAttendeesResponse
    {
        public string TempId { get; set; }
        public string TempEventId { get; set; }
        public int Id { get; set; }
        public bool Success { get; set; }
        public int VersionNumber { get; set; }
    }

    public class ModifiedAttendeesResponse
    {
        public int Id { get; set; }
        public bool Success { get; set; }
        public int VersionNumber { get; set; }
    }
    public class DeletedAttendeesResponse
    {
        public int Id { get; set; }
        public bool Success { get; set; }
        public int VersionNumber { get; set; }
    }

    public class CreatedRemindersResponse
    {
        public string TempId { get; set; }
        public string TempEventId { get; set; }
        public int Id { get; set; }
        public bool Success { get; set; }
        public int VersionNumber { get; set; }
    }

    public class ModifiedRemindersResponse
    {
        public int Id { get; set; }
        public bool Success { get; set; }
        public int VersionNumber { get; set; }
    }
    public class DeletedRemindersResponse
    {
        public int Id { get; set; }
        public bool Success { get; set; }
        public int VersionNumber { get; set; }
    }
}
