﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ByDay.Data;
using ByDay.Business.EventAttendees;
using ByDay.Business.EventReminder;
using ByDay.Utility;

namespace ByDay.Business.EventDetail
{
    public class EventService
    {
        private readonly ByDayRepository<Event> _eventRepository;
        private readonly ByDayRepository<Calendar> _calendarRepository;
        private readonly ByDayRepository<EventAttendee> _eventAttendeeRepository;

        EventAttendeeService _eventAttendeeService = new EventAttendeeService();
        EventReminderService _eventReminderService = new EventReminderService();
        public EventService()
        {
            _eventRepository = new ByDayRepository<Event>(new Ex2ByDayEntities());
            _calendarRepository = new ByDayRepository<Calendar>(new Ex2ByDayEntities());
            _eventAttendeeRepository = new ByDayRepository<EventAttendee>(new Ex2ByDayEntities());
        }

        public Event Insert(EventCommand eventCommand)
        {
            var eventDetail = new Event()
            {
                TypeId = eventCommand.TypeId,
                Title = eventCommand.Title,
                AssociatedEntityId = eventCommand.AssociatedEntityId,
                IsAllDayEvent = eventCommand.IsAllDayEvent,
                HasAlarm = eventCommand.HasAlarm,
                HasAttendees = eventCommand.HasAttendees,
                Description = eventCommand.Description,
                StartDate = eventCommand.StartDate,
                EndDate = eventCommand.EndDate,
                Duration = eventCommand.Duration,
                StartTimeZone = eventCommand.StartTimeZone,
                EndTimeZone = eventCommand.EndTimeZone,
                Location = eventCommand.Location,
                ExDate = eventCommand.ExDate,
                ExRule = eventCommand.ExRule,
                OriginalAllDay = eventCommand.OriginalAllDay,
                OriginalId = eventCommand.OriginalId,
                OriginalInstanceTime = eventCommand.OriginalInstanceTime,
                RDateUTC = eventCommand.RDateUTC,
                RRule = eventCommand.RRule,
                CreatedAt = DateTime.UtcNow,
                LastDate = eventCommand.LastDate,
                CalendarId = eventCommand.OwnerCalenderId
            };
            _eventRepository.Add(eventDetail);

            foreach (var attendee in eventCommand.EventAttendees)
            {
                if (attendee.CalendarId != 0)
                {
                    _eventAttendeeService.Insert(attendee, eventDetail);
                }
            }

            foreach (var remind in eventCommand.EventReminder)
            {
                if (remind.Method != 0)
                {
                    _eventReminderService.Insert(remind, eventDetail);
                }
            }
            return eventDetail;
        }

        public IQueryable<Event> Get()
        {
            return _eventRepository.All.Select(x => x);
        }

        public IQueryable<Event> Get(int id)
        {
            var calendars = _calendarRepository.FindBy(x => x.Owner == id).ToList();
            List<EventAttendee> attendees = new List<EventAttendee>();
            List<Event> events = new List<Event>();

            foreach (var cal in calendars)
            {
                attendees.AddRange(_eventAttendeeRepository.FindBy(x => x.CalendarId == cal.Id).Where(x => x.CalendarId == cal.Id).Distinct().ToList());
            }

            foreach (var att in attendees)
            {
               events.AddRange(_eventRepository.FindBy(x => x.Id == att.EventId).Where(x => x.Id == att.EventId).Distinct().ToList());

            }
            return events.AsQueryable();
        }

        public IQueryable<Event> GetUnSyncedEvent(string userID, DateTime syncedDate,string newlastSynce)
        {
           

         //   string correctString = newlastSynce.Replace("T", " ");
           // DateTime newLastSynced = DateTime.Parse(correctString, System.Globalization.CultureInfo.InvariantCulture);

          //  DateTime newLastSynced = DateTime.ParseExact(newlastSynce, "MM/dd/yyyy HH:mm:ss tt",
            //                           System.Globalization.CultureInfo.InvariantCulture);



            var events = Get(FieldConverter.To<int>(userID));
            // events=events.Where(x=>x.CreatedAt!= null).Select(y => y).Where(x => x.CreatedAt.Value > syncedDate || (x.ModifiedAt== null?false : x.ModifiedAt.Value>syncedDate));
            var eventResult = events.GroupBy(x => x.Id).Select(b => b.FirstOrDefault());
            List<Event> eventssData = new List<Event>();
            eventssData.AddRange(eventResult.Where(cr => cr.CreatedAt != null).Where(x => x.CreatedAt.Value > syncedDate && x.IsDeleted==false));
            eventssData.AddRange(eventResult.Select(a => a).Where(y => y.ModifiedAt != null).Where(x => x.ModifiedAt.Value > syncedDate  && x.IsDeleted==false));
            eventssData.AddRange(eventResult.Select(b => b).Where(x => x.ModifiedAt != null && x.CreatedAt != null).Where(y => y.CreatedAt.Value <= syncedDate && y.ModifiedAt > syncedDate && y.IsDeleted == true));

            var eventResults = eventssData.GroupBy(x => x.Id).Select(b => b.FirstOrDefault());
            return eventResults.OrderBy(y=>y.Id).AsQueryable();
        }


      

       public IQueryable<EventAttendee> GetUnSyncedEventAttendee(string userID,DateTime syncedDate)
        {
            var events = Get(FieldConverter.To<int>(userID));
            var eventResult = events.GroupBy(x => x.Id).Select(b => b.FirstOrDefault());         
            List<EventAttendee> selectedEvent = new List<EventAttendee>();
        
            foreach (var data in eventResult)
            {
              var  eventAttendee = data.EventAttendees.ToList();
                selectedEvent.AddRange(eventAttendee.Where(cr => cr.CreatedAt != null).Where(x => x.CreatedAt.Value > syncedDate && x.IsDeleted==false));
                selectedEvent.AddRange(eventAttendee.Select(a => a).Where(y => y.ModifiedAt != null).Where(x => x.ModifiedAt.Value > syncedDate && x.IsDeleted==false));

                selectedEvent.AddRange(eventAttendee.Select(b => b).Where(x => x.ModifiedAt != null && x.CreatedAt != null).Where(y => y.CreatedAt.Value <= syncedDate && y.ModifiedAt > syncedDate && y.IsDeleted == true));

            }
            var eventResults = selectedEvent.GroupBy(x => x.Id).Select(y => y.FirstOrDefault());
            return eventResults.OrderBy(y => y.Id).AsQueryable();
        }



        public IQueryable<Reminder> GetUnSyncedEventReminder(string userID, DateTime syncedDate)
        {
            var events = Get(FieldConverter.To<int>(userID));
            var eventResult = events.GroupBy(x => x.Id).Select(b => b.FirstOrDefault());
       
            List<Reminder> eventReminder = new List<Reminder>();

            foreach (var cd in eventResult)
            {
               var reminder = cd.Reminders.ToList();
                eventReminder.AddRange(reminder.Where(cr => cr.CreatedAt != null).Where(x => x.CreatedAt.Value > syncedDate && x.IsDeleted==false));
                eventReminder.AddRange(reminder.Select(a => a).Where(y => y.ModifiedAt != null).Where(x => x.ModifiedAt.Value > syncedDate && x.IsDeleted==false));

                eventReminder.AddRange(reminder.Select(b => b).Where(x => x.ModifiedAt != null && x.CreatedAt != null).Where(y => y.CreatedAt.Value <= syncedDate && y.ModifiedAt > syncedDate && y.IsDeleted == true));

            }
            var eventResults = eventReminder.GroupBy(x => x.Id).Select(y => y.FirstOrDefault());
           return eventResults.OrderBy(y => y.Id).AsQueryable();
        }


        //public IQueryable<Reminder> GetUnSyncedEventReminder(string userID, DateTime syncedDate)
        //{
        //    var events = Get(FieldConverter.To<int>(userID));

        //    var d = events.Select(v => v.Reminders).ToList();

        //    List<Reminder> er = new List<Reminder>();

        //    foreach (var cd in events)
        //    {

        //        er = cd.Reminders.ToList();

        //        er = er.Where(cr => cr.CreatedAt != null).Where(x => x.CreatedAt.Value > syncedDate).ToList();
        //        er= er.Select(a =>a).Where(y=>y.ModifiedAt != null).Where(x =>x.ModifiedAt.Value > syncedDate).ToList();

        //    }


        //    //List<EventAttendee> e = new List<EventAttendee>();

        //    //foreach (var c in d)
        //    //{
        //    //    e = c.Where(cd => cd.CreatedAt != null).Select(b=>b).Where(x => x.CreatedAt.Value > syncedDate).Where(y=>y.ModifiedAt!=null).Where(z=>z.ModifiedAt.Value>syncedDate).ToList();
        //    //}
        //    //List<EventAttendee> eventAttendeeResult = e.GroupBy(x => x.Id).Select(v => v.FirstOrDefault()).ToList();

        //    return er.AsQueryable();
        //}

    }


}