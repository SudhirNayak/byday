﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using ByDay.Business.UserCalendar;
namespace ByDay.Business.User
{
    public  class UserCommand
    {

        public UserCommand()
        {
    //       this.User = new User();
      //      this.Calendar = new Calendar();
        }
       
        public int Id { get; set; }
        [Required]
        public string EmailId { get; set; }
        [Required]
        public string Name { get; set; }
        public string PhotoUrl { get; set; }

        public string TimeZone { get; set; }
        public string PushToken { get; set; }
      //  public User User { get; set; }
      // public Calendar Calendar { get; set; }
    }
}
