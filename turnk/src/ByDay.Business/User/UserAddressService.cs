﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ByDay.Data;

namespace ByDay.Business.User
{
    public class UserAddressService
    {

        ByDayRepository<ByDay.Data.UserAddress> _userRepository = new ByDayRepository<ByDay.Data.UserAddress>(new Ex2ByDayEntities());



        public UserAddress Insert(UserAddressCommand userCommand)
        {
            var userDetail = new UserAddress()
            {
               UserId=userCommand.UserId,
               Address=userCommand.Address,
               City=userCommand.City,
               PostalCode=userCommand.PostalCode,
               LandMark=userCommand.LandMark,
               StateId=userCommand.StateId,
               CountryId=userCommand.CountryId,
               IsTemp=userCommand.IsTemp,
              CreatedAt= DateTime.UtcNow

            };
            _userRepository.Add(userDetail);
            return userDetail;
            }

        public IQueryable<UserAddress>  GetUserAddress(int id)
        {
            var user = _userRepository.FindBy(x => x.UserId == id && x.IsTemp==false ).ToList();

            return user.AsQueryable();
            
        }

    }
}
