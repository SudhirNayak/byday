﻿using ByDay.Data;
using System.Collections.Generic;
using System.Linq;
using ByDay.Utility;
using ByDay.Utility.Enums;
using System;

namespace ByDay.Business.User
{
    public class UserSignUpService
    {
        private readonly ByDayRepository<ByDayUser> _userRepository;
        private readonly ByDayRepository<Calendar> _calendarRepository;
        private readonly ByDayRepository<DeviceSync> _deviceSyncRepository;
        private readonly ByDayRepository<Color> _colorRepository;
        private readonly ByDayRepository<ByDay.Data.UserScratchpad> _userScratchPadRepository;
        public UserSignUpService()
        {
            _userRepository = new ByDayRepository<ByDayUser>(new Ex2ByDayEntities());
            _calendarRepository = new ByDayRepository<Calendar>(new Ex2ByDayEntities());
            _deviceSyncRepository = new ByDayRepository<DeviceSync>(new Ex2ByDayEntities());
            _colorRepository = new ByDayRepository<Color>(new Ex2ByDayEntities());
            _userScratchPadRepository = new ByDayRepository<ByDay.Data.UserScratchpad>(new Ex2ByDayEntities());

        }

        public ByDayUser UserSignUp(UserCommand userCommand, ref bool firstTimeUserLogin)
        {
            firstTimeUserLogin = false;
            var user = _userRepository.FindBy(x => x.EmailId == userCommand.EmailId).FirstOrDefault();
            if (user == null)
            {
                firstTimeUserLogin = true;
                user = new ByDayUser { EmailId = userCommand.EmailId, Name = userCommand.Name, PhotoUrl = userCommand.PhotoUrl, RoleDefinedId = FieldConverter.To<int>(ActionId.RoleDefinedId), CreatedAt = DateTime.Now, ModifiedAt = DateTime.Now };
                _userRepository.Add(user);
                if (userCommand.PushToken != null)
                {
                    var pushBackToken = new DeviceSync { UserId = user.Id, PushToken = userCommand.PushToken };
                    _deviceSyncRepository.Add(pushBackToken);
                }
            }
            return user;
        }

        public List<Calendar> CreateDefaultCalender(ByDayUser user, UserCommand userCommand)
        {
            var calendars = _calendarRepository.FindBy(x => x.Owner == user.Id).ToList();
            if (calendars == null || calendars.Count <= 0)
            {
                Calendar calendar = new Calendar { DisplayName = ConfigUtility.GetConfigValueFromWebConfig<string>(Constants.DisplayName), TimeZone = userCommand.TimeZone, Owner = user.Id, ColorId = FieldConverter.To<int>(ActionId.ColorId), IsPrimary = FieldConverter.To<bool>(ActionId.IsPrimary) };
                _calendarRepository.Add(calendar);
                calendars = _calendarRepository.FindBy(x => x.Owner == user.Id).ToList();
            }
            return calendars;
        }


        public List<UserScratchpad> CreateScrachpad(ByDayUser user)
        {
            var scratchPad = _userScratchPadRepository.FindBy(x => x.UserId == user.Id).ToList();
            if (scratchPad == null || scratchPad.Count <= 0)
            {
                UserScratchpad userScratch = new UserScratchpad
                {
                    UserId = user.Id,
                    CreatedAt = DateTime.UtcNow
                };

                _userScratchPadRepository.Add(userScratch);
                scratchPad = _userScratchPadRepository.FindBy(x => x.UserId == user.Id).ToList();
            }
            return scratchPad;
        }




        public IQueryable<Color> GetColor()
        {
            return _colorRepository.All.Select(x => x).OrderBy(x => x.Name);
        }
    }
}