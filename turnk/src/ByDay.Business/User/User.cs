﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;

namespace ByDay.Business.User
{
    public  class User
    {
        ByDayRepository<ByDay.Data.UserFamilyMember> _userFamilyRepository = new ByDayRepository<ByDay.Data.UserFamilyMember>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.ByDayUser> _userRepository = new ByDayRepository<ByDay.Data.ByDayUser>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.AppFeedback> _userFeedbackRepository = new ByDayRepository<ByDay.Data.AppFeedback>(new Ex2ByDayEntities());
        ByDayRepository<ByDay.Data.SyncLog> _syncLogRepository = new ByDayRepository<ByDay.Data.SyncLog>(new Ex2ByDayEntities());

        public int Id { get; set; }
        [Required]
        public string EmailId { get; set; }
        [Required]
        public string Name { get; set; }
        public string PhotoUrl { get; set; }

        //  public string PushToken { get; set; }

        public int AddUserMember(int user_id, dynamic data)
        {
            string email = Convert.ToString(data.MemberEmailId);
            var user = _userRepository.FindBy(x => x.EmailId == email).FirstOrDefault();
            if (user != null)
            {
                var userRequest = _userFamilyRepository.FindBy(x => (x.SenderId == user_id && x.MemberId == user.Id)|| (x.SenderId == user.Id && x.MemberId == user_id)).FirstOrDefault();
                if (userRequest == null)
                {
                    var newUserRequest = new UserFamilyMember()
                    {
                        SenderId = user_id,
                        MemberId = user.Id,
                        Status = "P",
                        MemberFirstName = Convert.ToString(data.MemberFirstName),
                        MemberlastName = Convert.ToString(data.MemberlastName),
                        ModifiedAt = DateTime.Now.ToUniversalTime(),
                        CreatedAt = DateTime.Now.ToUniversalTime()
                    };
                    _userFamilyRepository.Add(newUserRequest);
                    _syncLogRepository.Add(UserSyncLog(user_id, newUserRequest.Id, "UserFamilyMembers", "INSERT"));
                    _syncLogRepository.Add(UserSyncLog(user.Id, newUserRequest.Id, "UserFamilyMembers", "INSERT"));
                    return 0;
                }
                else if (userRequest.Status == "A")
                    return 1;
                else
                    return 2;
            }
            else
                return 3;
        }

        public int DeleteRequest(int user_id, int member_id)
        {
            var userRequest = _userFamilyRepository.FindBy(x => x.SenderId == user_id && x.MemberId == member_id).FirstOrDefault();
            if (userRequest != null)
            {
                if (userRequest.Status == "P")
                {
                    _userFamilyRepository.Delete(userRequest);
                    _syncLogRepository.Add(UserSyncLog(user_id, userRequest.Id, "UserFamilyMembers", "DELETE"));
                    _syncLogRepository.Add(UserSyncLog(member_id, userRequest.Id, "UserFamilyMembers", "DELETE"));
                    return 0;
                }
                else
                    return 2;
            }
            else
                return 1;
        }

        public dynamic GetData(int userId, int syncId, string schema, ref string lastSyncId)
        {
            return _syncLogRepository.ExecWithStoreProcedureSync("GetData", ref lastSyncId, new object[] { userId, syncId, schema });
        }

        public int AcceptRequest(int user_id, int member_id)
        {
            var userRequest = _userFamilyRepository.FindBy(x => x.SenderId == user_id && x.MemberId == member_id).FirstOrDefault();
            if (userRequest != null)
            {
                if (userRequest.Status == "A")
                    return 1;
                userRequest.Status = "A";
                userRequest.ModifiedAt = DateTime.Now.ToUniversalTime();
                _userFamilyRepository.Update(userRequest);
                _syncLogRepository.Add(UserSyncLog(user_id, userRequest.Id, "UserFamilyMembers", "UPDATE"));
                _syncLogRepository.Add(UserSyncLog(member_id, userRequest.Id, "UserFamilyMembers", "UPDATE"));
                return 0;
            }
            else
                return 2;
        }
        public List<UserFamilyMember> UserAllMembers(int user_id)
        {
            var userRequest = _userFamilyRepository.FindBy(x => x.SenderId == user_id || x.MemberId == user_id).ToList();
            return userRequest;
        }

        public void UserFeedback(dynamic data)
        {
            _userFeedbackRepository.Add(new AppFeedback()
            {
                Response = Convert.ToString(data.Response),
                RType = Convert.ToString(data.RType),
                UserId = Convert.ToInt32(data.UserId),
                CreatedAt = DateTime.UtcNow
            });
        }

        public ByDay.Data.SyncLog UserSyncLog(int userId, int rowId, string entity, string actionType)
        {
            var obj = new SyncLog()
            {
                UserId = userId,
                Entity = entity,
                EntityRowId = rowId,
                ActionType = actionType,
                SyncDate = DateTime.UtcNow
            };
            return obj;
        }
    }

    public class UserSyncResponse
    {
        public int lastSyncId { get; set; }
        public int Code { get; set; }
        public SyncLog log { get; set; }
    }

    public class SyncUserFamilyResult
    {
        public DataTable UserFamilyIU { get; set; }
        public DataTable MemberCalender { get; set; }
    }

}
