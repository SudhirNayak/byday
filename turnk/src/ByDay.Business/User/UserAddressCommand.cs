﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Business.User
{
  public  class UserAddressCommand
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int PostalCode { get; set; }
        public string LandMark { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public bool IsTemp { get; set; }

        public Nullable<System.DateTime> CreatedAt { get; set; }
    }
}
