﻿using ByDay.Utility;
using ByDay.Utility.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.ModelBinding;

namespace ByDay.API.Filters
{
    public class ValidateModelStateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = actionContext.Request.CreateResponse<ErrorOutput<ModelStateDictionary>>(HttpStatusCode.OK, new ErrorOutput<ModelStateDictionary>
                {
                    Message = "Fail",
                    Status = FieldConverter.To<bool>(ActionStatus.Error),
                    Error = actionContext.ModelState,

                });
               
            }
        }
    }
}