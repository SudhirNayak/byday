﻿using AutoMapper;
using ByDay.Data;
using ByDay.API.Models;
using ByDay.Business.EventReminder;
using ByDay.Business.User;
using ByDay.Business.EventAttendees;
using System.Collections.Generic;
using ByDay.Business.Products;
using ByDay.Business.Orders;
using System.Linq;
using ByDay.Business.RecipeDetailUnSynced;


namespace ByDay.API.App_Start.AutoMapper
{
    public class Mapping : Profile
    {
        protected override void Configure()
        {

            CreateMap<ByDayUser, UserModel>()
                .ForMember(x => x.User, opt => opt.MapFrom(y => new User() { EmailId = y.EmailId, Id = y.Id, Name = y.Name, PhotoUrl = y.PhotoUrl }))
                .ReverseMap();

            CreateMap<Data.Calendar, UserModel>()
               .ForMember(x => x.CalendarList, opt => opt.MapFrom(y => new Business.UserCalendar.Calendar() { Id = y.Id, DisplayName = y.DisplayName, TimeZone = y.TimeZone, ColorId = y.ColorId, Owner = y.Owner, IsPrimary = y.IsPrimary }))
               .ReverseMap();

            CreateMap<Color, UserModel>().ForMember(x => x.Colors, opt => opt.MapFrom(y => new Business.CalendarColor.Color()
            { Id = y.Id, Code = y.Code, Name = y.Name })).ReverseMap();

            CreateMap<ByDay.Data.Calendar, ByDay.Business.UserCalendar.Calendar>().ReverseMap();

            CreateMap<ByDay.Data.Color, ByDay.Business.CalendarColor.Color>().ReverseMap();


            CreateMap<Event, EventModel>().ReverseMap();
            CreateMap<Event, EventSyncedModel>().ReverseMap();

            CreateMap<EventAttendee, EventAttendeeCommand>().ReverseMap();
            CreateMap<Reminder, EventReminderCommand>().ReverseMap();


            CreateMap<ByDayUser, User>().ReverseMap();

            CreateMap<ProductModel, ProductCommand>().ReverseMap();
            CreateMap<Product, ProductModel>()
            .ForMember(x => x.ProductImages, c => c.MapFrom(d => (d.ProductImages.Count > 0) ? d.ProductImages.Select(x => new Business.Products.ProductImage() { Id = x.Id, ProductId = x.ProductId, URL = x.URL }).ToList() : new List<Business.Products.ProductImage>()))
                .ReverseMap();
            CreateMap<ProductSubCategory, SubcategoryModel>().ReverseMap();
            CreateMap<ProductCategory, CategoryModel>().ReverseMap();
            CreateMap<State, StateModel>().ReverseMap();
            CreateMap<Country, CountryModel>().ReverseMap();
            CreateMap<OrderModel, OrderCommand>().ReverseMap();

            CreateMap<UserAddressModel, UserAddress>().ReverseMap().ForMember(x => x.StateName, opt => opt.MapFrom(c => c.State.Name))
                .ForMember(x => x.CountryName, opt => opt.MapFrom(c => c.Country.Name)); ;



            CreateMap<MealPlanSyncedModel, MealPlan>().ReverseMap();
            CreateMap<Recipe, Business.RecipeDetailUnSynced.RecipeResponseCommand>()
            .ForMember(x => x.Images, opt => opt.MapFrom(c => c.RecipeImages.Select(d => new RecipeResponseImage()
            { Id = d.Id, Name = d.Name, RecipeId = d.RecipeId, Path = d.Path, CreatedAt = d.CreatedAt, ModifiedAt = d.ModifiedAt, Guid = d.Guid, IsDeleted = d.IsDeleted })))
            .ForMember(y => y.UserSpecificRecipeDetails, opt => opt.MapFrom(d => d.RecipeUserFeedbacks.Select(e => new RecipeResponseUserFeedBack()
            {
                Id = e.Id,
                UserId = e.UserId,
                RecipeId = e.RecipeId,
                IsFavourite = e.IsFavourite,
                IsHidden = e.IsHidden,
                Rating = e.Rating,
                Comments = e.Comments
                  ,
                Guid = e.Guid,
                CreatedAt = e.CreatedAt,
                ModifiedAt = e.ModifiedAt,
                IsDeleted = e.IsDeleted
            })))
            .ForMember(x => x.Ingredients, opt => opt.MapFrom(c => c.RecipeIngredientMappings.Select(f => new ResponseIngredient() {
                Id = f.Ingredient.Id, Name = f.Ingredient.Name, Quantity = (f.Ingredient.Type1 != null) ? f.Ingredient.Type1.Value : null, QuantityType = (f.Ingredient.Type2 != null) ? f.Ingredient.Type2.Value : null, Guid = f.Ingredient.Guid, CreatedAt = f.Ingredient.CreatedAt, ModifiedAt = f.Ingredient.ModifiedAt, IsDeleted = f.Ingredient.IsDeleted })))

            .ForMember(x => x.Categories, opt => opt.MapFrom(c => c.RecipeCategoryMappings.Select(f => new RecipeResponseCategory() { Id = f.Id, CategoryId = f.CategoryId, RecipeId = f.RecipeId, Guid = f.Guid, CreatedAt = f.CreatedAt, ModifiedAt = f.ModifiedAt, IsDeleted = f.IsDeleted })))
             .ForMember(x => x.Simplicity, opt => opt.MapFrom(src => src.Type.Value))

            .ReverseMap();


            CreateMap<ResponseRecipeCategory, Category>().ReverseMap();


            CreateMap<Recipe, Business.RecipeDetailUnSynced.RecipeRespCommand>();


            CreateMap<Data.Ingredient, Business.RecipeDetailUnSynced.RespIngredient>()
            .ForMember(x => x.RecipeId, opt => opt.MapFrom(src => src.RecipeIngredientMappings.Select(x => x.RecipeId).FirstOrDefault()))

            .ForMember(x => x.QuantityType, opt => opt.MapFrom(src => src.Type2.Value));

            // QuantityType = (f.Ingredient.Type2 != null) ? f.Ingredient.Type2.Value : null,


            CreateMap<Data.RecipeUserFeedback, Business.RecipeDetailUnSynced.RecipeRespUserFeedBack>();
            CreateMap<Data.RecipeCategoryMapping, Business.RecipeDetailUnSynced.RecipeRespCategory>();
            CreateMap<Data.RecipeImage, Business.RecipeDetailUnSynced.RecipeRespImage>();

            CreateMap<Data.UserScratchpad, ScratchPadModel>();

            CreateMap<Business.UserScratchPad.ScratchPad, ScratchPadModel>();

            CreateMap<UserNote, UserNotes>();


            CreateMap<Questionnaire, QuestionnaireDetail>()
           .ForMember(x => x.Options, opt => opt.MapFrom(c => c.QuestionOptions.Select(d => new QuestionOp()
           { Id = d.Id, Name = d.Name })))
           .ForMember(x => x.Parts, opt => opt.MapFrom(c => c.OptionParts.Select(d => new OptionParts()
           { Id = d.Id, Title = d.Title }))).ReverseMap();

            //  CreateMap<Questionnaire, QuestionnaireDetail>()
            //.ForMember(x => x.O).ReverseMap();
            CreateMap<UserResponseText, QuestionAnswers>()
                    .ForMember(x => x.SelectType, opt => opt.MapFrom(src => src.Questionnaire.SelectType));
            CreateMap<UserResponseM, QuestionAnswers>()
                    .ForMember(x => x.SelectType, opt => opt.MapFrom(src => src.Questionnaire.SelectType));
            CreateMap<Tip, TipModel>().ReverseMap();
            CreateMap<ByDay.Data.UserFamilyMember, ByDay.API.Models.UserFamilyMember>()
                    .ForMember(x => x.EmailIdM, opt => opt.MapFrom(src => src.ByDayUser.EmailId))
                    .ForMember(x => x.PhotoUrlM, opt => opt.MapFrom(src => src.ByDayUser.PhotoUrl))
                    .ForMember(x => x.EmailIdS, opt => opt.MapFrom(src => src.ByDayUser1.EmailId))
                    .ForMember(x => x.PhotoUrlS, opt => opt.MapFrom(src => src.ByDayUser1.PhotoUrl))
                    .ForMember(x => x.SenderName, opt => opt.MapFrom(src => src.ByDayUser1.Name));

        }
    }
}
