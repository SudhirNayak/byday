﻿using AutoMapper;

namespace ByDay.API.App_Start.AutoMapper
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => cfg.AddProfile<Mapping>()
                );
        }
    }
}