﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class QuestionnaireAnswerModel
    {
        public int questionId { get; set; }
        public string type { get; set; }

        public int userId { get; set; }

        public string value { get; set; }

        public int[] selectedOptions { get; set; }

        public OptionParts[] selectedOptionsParts { get; set; }
    }
}