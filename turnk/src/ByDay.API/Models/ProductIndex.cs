﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class ProductIndex
    {
        public int Count { get; set; }
        public List<ProductModel> Products { get; set; }
    }
}