﻿using ByDay.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class UserFamilyMember
    {
        public int Id { get; set; }
        public Nullable<int> SenderId { get; set; }
        public Nullable<int> MemberId { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public string MemberFirstName { get; set; }
        public string MemberlastName { get; set; }
        public string EmailIdM { get; set; }
        public string PhotoUrlM { get; set; }
        public string EmailIdS { get; set; }
        public string PhotoUrlS { get; set; }
        public string SenderName { get; set; }
    }
}