﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class EventSyncedModel
    {

        public int Id { get; set; }
        public Nullable<int> TypeId { get; set; }
        public string Title { get; set; }
        public Nullable<int> AssociatedEntityId { get; set; }
        public bool IsAllDayEvent { get; set; }
        public bool HasAlarm { get; set; }
        public bool HasAttendees { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Duration { get; set; }
        public string StartTimeZone { get; set; }
        public string EndTimeZone { get; set; }
        public string Location { get; set; }
        public Nullable<System.DateTime> ExDate { get; set; }
        public string ExRule { get; set; }
        public bool OriginalAllDay { get; set; }
        public Nullable<int> OriginalId { get; set; }
        public Nullable<System.DateTime> OriginalInstanceTime { get; set; }
        public Nullable<System.DateTime> RDateUTC { get; set; }
        public string RRule { get; set; }

        public Nullable<System.DateTime> CreatedAt { get ; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastDate { get; set; }


    }
}