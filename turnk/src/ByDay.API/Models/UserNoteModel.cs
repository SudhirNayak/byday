﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class UserNoteModel
    {
      public UserNoteModel()
        {
            UserNotes = new List<UserNotes>();
        }
        public List<UserNotes> UserNotes { get; set; }
    }

    public class UserNotes
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int NoteCategoryId { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public bool IsDeleted { get; set; }
    }
}