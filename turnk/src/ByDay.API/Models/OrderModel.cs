﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class OrderModel
    {
        public string Payment_method_nonce { get; set; }
        public int OrderId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int ProductId { get; set; }
        [Required]
        public int Qty { get; set; }
        [Required]
        public decimal TotalPrice { get; set; }
        [Required]
        public string ReceiverName { get; set; }
        [Required]
        public string ReceiverEmail { get; set; }
        [Required]
        public string ReceiverNum { get; set; }
        [Required]
        public string ReceiverAddress { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public int StateId { get; set; }
        [Required]
        public int CountryId { get; set; }
        public Nullable<int> PostalCode { get; set; }
        public string LandMark { get; set; }
        public string Message { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }

        public int StatusId { get; set; }

        public int AddressId { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public string ImageType { get; set; }
    }
}