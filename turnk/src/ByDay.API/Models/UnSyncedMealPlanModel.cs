﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class UnSyncedMealPlanModel
    {
        public UnSyncedMealPlanModel()
        {

            MealPlans = new List<MealPlanSyncedModel>();
        }
        public string LastSync
        {
            get
            {
                return DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            }
        }
        public List<MealPlanSyncedModel> MealPlans { get; set; }


       
    }
}