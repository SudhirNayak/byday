﻿using ByDay.Business.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class TaskModel
    {
      public TaskModel()
        {
            Tasks = new UserTaskVModel();
            Tasks.UserTask = new List<TaskViewModel>();
            Tasks.UserTaskAttendee = new List<TaskAttendeeViewModel>();
            Tasks.UserTaskCategory = new List<TaskCategoryViewModel>();
            Tasks.UserTaskPriority = new List<TaskPriorityViewModel>();
        }
        public UserTaskVModel Tasks { get; set; }
    }
}