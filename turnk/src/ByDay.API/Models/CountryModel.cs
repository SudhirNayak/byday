﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class CountryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<StateModel> States { get; set; }
    }
    public class StateModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
    }
}