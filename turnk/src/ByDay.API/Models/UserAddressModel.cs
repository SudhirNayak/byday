﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class UserAddressModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int PostalCode { get; set; }
        public string LandMark { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public bool IsTemp { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
    }
}