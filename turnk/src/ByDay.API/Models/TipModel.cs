﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class TipModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}