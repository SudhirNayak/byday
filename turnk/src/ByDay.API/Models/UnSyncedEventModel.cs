﻿using ByDay.Business.EventAttendees;
using ByDay.Business.EventReminder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class UnSyncedEventModel
    {

        public UnSyncedEventModel()
        {
            Events = new List<EventSyncedModel>();
            Attendees = new List<EventAttendeeCommand>();
            Reminders = new List<EventReminderCommand>();
        }


        public string LastSync
        {
            get
            {
                return DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fff"); 
            }
        }


        public List<EventSyncedModel> Events { get; set; }

        public List<EventAttendeeCommand> Attendees { get; set; }


        public List<EventReminderCommand> Reminders { get; set; }

        //public UnSyncedEventModel()
        //{
        //    CreatedEvents = new List<EventModel>();
        //    ModifiedEvents = new List<EventModel>();
        //    DeletedEvents = new List<EventModel>();
        //}


        //public string LastSync { get
        //    {
        //        return DateTime.Now.ToString("yyyy-MM-ddThh.mm.ss.fff"); ;
        //    } }

        //public List<EventModel> CreatedEvents { get; set; }

        //public List<EventModel> ModifiedEvents { get; set; }


        //public List<EventModel> DeletedEvents { get; set; }
    }
}