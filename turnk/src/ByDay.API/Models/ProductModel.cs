﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class ProductModel
    {
        public int Id { get; set; }
        public int SubCategoryId { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string Type { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public bool IsCustomizable { get; set; }
        public List<Business.Products.ProductImage> ProductImages { get; set; }


    }

    public class ProductImage
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string URL { get; set; }
    }
}