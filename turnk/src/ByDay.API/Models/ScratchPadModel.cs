﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class ScratchPadModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Content { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public bool IsDeleted { get; set; }
    }

}