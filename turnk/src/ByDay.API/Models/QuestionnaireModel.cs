﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class QuestionnaireModel
    {
        public QuestionnaireModel()
        {
            Questions = new List<QuestionnaireDetail>();
            Answer = new List<QuestionAnswers>();
        }

        public List<QuestionnaireDetail> Questions { get; set; }
        public List<QuestionAnswers> Answer { get; set; }
    }

    public class QuestionnaireDetail
    {
        public QuestionnaireDetail()
        {
            Options = new List<QuestionOp>();
            Parts = new List<OptionParts>();
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string SelectType { get; set; }
        public bool IsMandatory { get; set; }
        public List<QuestionOp> Options { get; set; }
        public List<OptionParts> Parts { get; set; }
    }

    public class QuestionOp
    {

        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class OptionParts
    {


        public int Id { get; set; }
        public string Title { get; set; }
    }
    public class QuestionAnswers
    {
        public QuestionAnswers()
        {
           // Options = new List<QuestionOp>();
           // Parts = new List<OptionParts>();
        }
        public int QuestionId { get; set; }
        public string Response { get; set; }
        public string SelectType { get; set; }
        public int OptionId { get; set; }
        public int PartId { get; set; }

     //   public List<QuestionOp> Options { get; set; }
     //   public List<OptionParts> Parts { get; set; }

    }

}