﻿using ByDay.Business.CalendarColor;
using ByDay.Business.User;
using ByDay.Business.UserCalendar;
using System.Collections.Generic;

namespace ByDay.API.Models
{
    public class UserModel
    {
        public UserModel()
        {
            this.User = new User();
            this.Colors = new List<Color>();
            this.CalendarList = new List<Business.UserCalendar.Calendar>();
            this.RecipeCategories = new List<ResponseRecipeCategory>();
            this.ScratchPad =new ScratchPadModel();
        }
        public User User { get; set; }
        public List<Calendar> CalendarList { get; set; }

        public List<Color> Colors { get; set; }

        public ScratchPadModel ScratchPad { get; set; }
        public List<ResponseRecipeCategory> RecipeCategories { get; set; }
        public bool FirstTimeUserLogin { get; set; }
    }
 
}