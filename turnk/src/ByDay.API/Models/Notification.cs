﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace ByDay.API.Models
{
    public class Notification
    {
        private class NotificationMessage
        {
            public string Title;
            public string Message;
            public long ItemId;
        }
       // public string SendNotification(List<string> deviceRegIds, string message, string title, long id)
             public string SendNotification(string deviceRegIds)

        {

            string SERVER_API_KEY = "AIzaSyAsLD2nCRDR-cqSGq9qKzh97tmjkScnCSQ";
            var SENDER_ID = "daucmdjLDzw:APA91bFpYy-hyC4ZItl11xhhxRI5R76yplL_iMQ1N2SzixYmbQ3mL9zItXIrtuNXGUmANQBqLgugxofWzfhdePEj39516akdtU-LAtVn9P5XMDCjConVbcsBf7OOiZlHHv9knbcdtOGu";
            string regIds = string.Join("\",\"", deviceRegIds);

            NotificationMessage nm = new NotificationMessage();
          //  nm.Title = title;
          //  nm.Message = message;
            //nm.ItemId = id;
            var value = new JavaScriptSerializer().Serialize(nm);
            WebRequest tRequest;
            tRequest = WebRequest.Create("https://android.googleapis.com/gcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));

            tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

            string postData = "{\"collapse_key\":\"score_update\",\"time_to_live\":108,\"delay_while_idle\":true,\"data\": { \"message\" : " + value + ",\"time\": " + "\"" + System.DateTime.Now.ToString() + "\"},\"registration_ids\":[\"" + regIds + "\"]}";

            Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;

            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse tResponse = tRequest.GetResponse();
            dataStream = tResponse.GetResponseStream();
            StreamReader tReader = new StreamReader(dataStream);
            String sResponseFromServer = tReader.ReadToEnd();

            tReader.Close();
            dataStream.Close();
            tResponse.Close();
            return sResponseFromServer;
        }

    }
  
}