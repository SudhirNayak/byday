﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }

        public virtual ICollection<SubcategoryModel> ProductSubCategories { get; set; }
    }

    public class SubcategoryModel
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
    }

    public class CategoryIndex
    {
        public int Count { get; set; }
        public List<CategoryModel> Categories { get; set; }
    }
}