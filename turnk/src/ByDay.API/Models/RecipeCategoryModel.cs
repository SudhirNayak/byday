﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class RecipeCategoryModel
    {
        public RecipeCategoryModel()
        {
            RecipeCateogories = new List<ResponseRecipeCategory>();
        }
        public List<ResponseRecipeCategory> RecipeCateogories { get; set; }
       
    }

    public class ResponseRecipeCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        //public Nullable<bool> IsActive { get; set; }
    }
}