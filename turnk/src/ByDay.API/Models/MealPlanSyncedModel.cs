﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.Models
{
    public class MealPlanSyncedModel
    {
        public int Id { get; set; }
        public int RecipeId { get; set; }

        public string RecipeName { get; set; }

        public string Thumbnail { get; set; }

        public bool IsFavourite { get; set; }

      //  public int UserId { get; set; }
        public Nullable<System.DateTime> RecipeDate { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
     //   public string Guid { get; set; }
        public string OneTimeComment { get; set; }
    }
}