﻿using ByDay.Business.MealPlans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.ViewModels
{
    public class MealViewModel
    {
        MealUnSyncedResponse response = new MealUnSyncedResponse();
        MealPlanService _mealUnSynService = new MealPlanService();
        public MealUnSyncedResponse GetMealUnSyncedResponse(MealUnSyncedCommand mealUnSyncedCommand)
        {
            if(mealUnSyncedCommand.CreatedPlans.Count()>=1)
            {
                foreach (var data in mealUnSyncedCommand.CreatedPlans)
                {
                    var _createdMeals = _mealUnSynService.Insert(data);

                    response.CreatedPlans.Add(_createdMeals);
                }

            }
            if (mealUnSyncedCommand.ModifiedPlans .Count() >= 1)
            {
                foreach (var data in mealUnSyncedCommand.ModifiedPlans)
                {
                    var _updatedMeals = _mealUnSynService.Update(data);

                    response.ModifiedPlans.Add(_updatedMeals);
                }

            }
            if (mealUnSyncedCommand.DeletedPlans.Count() >= 1)
            {
                foreach (var data in mealUnSyncedCommand.DeletedPlans)
                {
                    var _deletedMeals = _mealUnSynService.Delete(data);

                    response.DeletedPlans.Add(_deletedMeals);
                }

            }


            return response;
     
        }
    }
}