﻿using ByDay.Business.RecipeDetailUnSynced;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.ViewModels
{
    public class RecipeViewModel
    {
        RecipeUnSyncedResponse response = new RecipeUnSyncedResponse();
        RecipeService _recipeUnSynService = new RecipeService();
        public RecipeUnSyncedResponse GetRecipeUnSyncedResponse(RecipeUnSyncedCommand recipeUnSyncedCommand)
        {
            if (recipeUnSyncedCommand.CreatedRecipes.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.CreatedRecipes)
                {
                    var _createdRecipes = _recipeUnSynService.Insert(data);

                    response.CreatedRecipes.Add(_createdRecipes);
                }

            }
            if (recipeUnSyncedCommand.ModifiedRecipes.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.ModifiedRecipes)
                {
                    var _updatedrecipes = _recipeUnSynService.Update(data);

                    response.ModifiedRecipes.Add(_updatedrecipes);
                }

            }
            if (recipeUnSyncedCommand.DeletedRecipes.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.DeletedRecipes)
                {
                    var _deletedRecipes = _recipeUnSynService.Delete(data);

                    response.DeletedRecipes.Add(_deletedRecipes);
                }

            }


            return response;

        }


        public List<Data.Recipe> GetRecipeSearchResult(RecipeSearch recipe,int userId)
        {
            List<Data.Recipe> resResult = new List<Data.Recipe>();
            List<Data.Recipe> resFebResult = new List<Data.Recipe>();

            int methodValRecipe = 0;
            int scopeValFavorite = 1;

            if (recipe.Method == methodValRecipe)
            {
                recipe.Keys = recipe.Keys.ConvertAll(t => t.ToUpper());
                resResult.AddRange(_recipeUnSynService.Get().Where(x => x.Id > recipe.PageDetail.LastRecord && x.IsAdmin == true && recipe.Keys.Any(i => x.RecipeName.ToUpper().Contains(i)) && x.IsDeleted == false));
                //foreach (var recipeName in recipe.Keys)
                //{
                //    resResult.AddRange(_recipeUnSynService.Get().Where(x => x.Id > recipe.PageDetail.LastRecord && x.IsAdmin == true && x.RecipeName.ToUpper().Contains(recipeName.ToUpper()) && x.IsDeleted == false));

                //}          
                if (recipe.Scope == scopeValFavorite)
                {
                    var userfeedbackfevoriteRecipe = _recipeUnSynService.GetUserFevoriteId(userId);
                    foreach (var data in userfeedbackfevoriteRecipe)
                    {
                        resFebResult.AddRange(resResult.Where(x => x.Id == data.RecipeId));
                    }
                    resResult = resFebResult;
                    if (userfeedbackfevoriteRecipe.Count() < 1)
                    {
                        resResult.Clear();

                    }
                }

            }
            return resResult;
        }



        public RecipeNewUnSyncedResponse GetRecipeNewUnSyncedResponse(RecipeNewUnSyncedCommand recipeUnSyncedCommand)
        {
            RecipeNewUnSyncedResponse resp = new RecipeNewUnSyncedResponse();
            if (recipeUnSyncedCommand.CreatedRecipes.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.CreatedRecipes)
                {
                    var _createdRecipes = _recipeUnSynService.InsertRecipe(data);
                    resp.CreatedRecipes.Add(_createdRecipes);
                }
            }
            if (recipeUnSyncedCommand.ModifiedRecipes.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.ModifiedRecipes)
                {
                    var _updatedrecipes = _recipeUnSynService.UpdateRecipe(data);

                    resp.ModifiedRecipes.Add(_updatedrecipes);
                }

            }
            if (recipeUnSyncedCommand.DeletedRecipes.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.DeletedRecipes)
                {
                    var _deletedRecipes = _recipeUnSynService.DeleteRecipe(data);

                    resp.DeletedRecipes.Add(_deletedRecipes);
                }

            }

            if (recipeUnSyncedCommand.CreatedIngredients.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.CreatedIngredients)
                {
                    var _createdIngredient = _recipeUnSynService.InsertIngredient(data,resp.CreatedRecipes);

                    resp.CreatedIngredients.Add(_createdIngredient);
                }

            }
            if (recipeUnSyncedCommand.ModifiedIngredients.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.ModifiedIngredients)
                {
                    var _updatedIngredient = _recipeUnSynService.UpdateIngredient(data);

                    resp.ModifiedIngredients.Add(_updatedIngredient);
                }

            }
            if (recipeUnSyncedCommand.DeletedIngredients.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.DeletedIngredients)
                {
                    var _deletedIngredient = _recipeUnSynService.DeleteIngredient(data);

                    resp.DeletedIngredients.Add(_deletedIngredient);
                }

            }
            if (recipeUnSyncedCommand.CreatedCategories.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.CreatedCategories)
                {
                    var _createdCategory= _recipeUnSynService.InsertCategory(data, resp.CreatedRecipes);

                    resp.CreatedCategories.Add(_createdCategory);
                }

            }
            if (recipeUnSyncedCommand.DeletedCategories.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.DeletedCategories)
                {
                    var _deletedCategory = _recipeUnSynService.DeleteCategory(data);

                    resp.DeletedCategories.Add(_deletedCategory);
                }

            }


            if (recipeUnSyncedCommand.CreatedUserSpecificRecipeDetails.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.CreatedUserSpecificRecipeDetails)
                {
                    var _createdUserFeedBack = _recipeUnSynService.InsertUserFeedBack(data, resp.CreatedRecipes);

                    resp.CreatedUserSpecificRecipeDetails.Add(_createdUserFeedBack);
                }

            }
            if (recipeUnSyncedCommand.ModifiedUserSpecificRecipeDetails.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.ModifiedUserSpecificRecipeDetails)
                {
                    var _updatedUserFeedBack = _recipeUnSynService.UpdateUserFeedBack(data);

                    resp.ModifiedUserSpecificRecipeDetails.Add(_updatedUserFeedBack);
                }

            }
            if (recipeUnSyncedCommand.DeletedUserSpecificRecipeDetails.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.DeletedUserSpecificRecipeDetails)
                {
                    var _deletedUserFeedBack = _recipeUnSynService.DeleteUserFeedBack(data);

                    resp.DeletedUserSpecificRecipeDetails.Add(_deletedUserFeedBack);
                }

            }




            if (recipeUnSyncedCommand.CreatedImages.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.CreatedImages)
                {
                    var _createdImage = _recipeUnSynService.InsertRecipeImage(data, resp.CreatedRecipes);

                    resp.CreatedImages.Add(_createdImage);
                }

            }
            if (recipeUnSyncedCommand.ModifiedImages.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.ModifiedImages)
                {
                    var _updatedImage = _recipeUnSynService.UpdateRecipeImage(data);

                    resp.ModifiedImages.Add(_updatedImage);
                }

            }
            if (recipeUnSyncedCommand.DeletedImages.Count() >= 1)
            {
                foreach (var data in recipeUnSyncedCommand.DeletedImages)
                {
                    var _deletedImage = _recipeUnSynService.DeleteRecipeImage(data);

                    resp.DeletedImages.Add(_deletedImage);
                }

            }
            return resp;

        }

    }
}