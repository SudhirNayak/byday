﻿using ByDay.Business.EventAttendees;
using ByDay.Business.EventDetail;
using ByDay.Business.EventReminder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ByDay.API.ViewModels
{
    public class EventViewModel
    {
        EventUnSyncedResponse response = new EventUnSyncedResponse();
        EventUnSyncedService _eventUnSynService = new EventUnSyncedService();
        EventAttendeesUnSyncedService _eventAttendeesUnSyncedService = new EventAttendeesUnSyncedService();
        EventReminderUnSyncedService _eventReminderUnSyncedService = new EventReminderUnSyncedService();

        public EventUnSyncedResponse GetEventUnSyncedResponse(EventUnSyncedCommand eventUnSyncedCommand, long userId, long id)
        {
            if (_eventUnSynService.CheckLogSyncId(id, userId) > 0)
            {
                response.code = 1;
                return response;
            }
            //event
            if (eventUnSyncedCommand.CreatedEvents != null && eventUnSyncedCommand.CreatedEvents.Count() >= 1)
            {
                foreach (var data in eventUnSyncedCommand.CreatedEvents)
                {
                    var _createdEvent = _eventUnSynService.Insert(data);
                    foreach (var attendee in data.Attendees)
                        response.LastLogId = _eventUnSynService.InsertSync(_createdEvent.Id, "Event", "INSERT", attendee);

                    _createdEvent.VersionNumber = data.VersionNumber;
                    response.CreatedEvents.Add(_createdEvent);
                    Common(data, false);
                }
            }
            if (eventUnSyncedCommand.ModifiedEvents != null && eventUnSyncedCommand.ModifiedEvents.Count() >= 1)
            {
                foreach (var data in eventUnSyncedCommand.ModifiedEvents)
                {
                    if (data.Id > 0)
                    {
                        var _modifyEvent = _eventUnSynService.Update(data);
                        foreach (var attendee in data.Attendees)
                        {
                            if (data.CreatedAttendees.Where(x => x.UserId == attendee).Count()==0)
                            {
                                response.LastLogId = _eventUnSynService.InsertSync(_modifyEvent.Id, "Event", "UPDATE", attendee);
                            }
                        }

                        _modifyEvent.VersionNumber = data.VersionNumber;
                        response.ModifedEvents.Add(_modifyEvent);
                    }
                    Common(data, true);
                }
            }

            if (eventUnSyncedCommand.DeletedEvents != null && eventUnSyncedCommand.DeletedEvents.Count() >= 1)
            {
                foreach (var data in eventUnSyncedCommand.DeletedEvents)
                {
                    var _deleteEvent = _eventUnSynService.Delete(data);

                    _deleteEvent.VersionNumber = data.VersionNumber;
                    foreach (var attendee in data.Attendees)
                        response.LastLogId = _eventUnSynService.InsertSync(_deleteEvent.Id, "Event", "DELETE", attendee);

                    response.DeletedEvents.Add(_deleteEvent);
                    Common(data, false);
                }
            }
            //attendee
            response.code = 0;
            return response;
        }

        public void Common(EventCommand eventUnSyncedCommand, bool isModified)
        {
            if (eventUnSyncedCommand.CreatedAttendees != null && eventUnSyncedCommand.CreatedAttendees.Count() >= 1)
            {
                foreach (var data in eventUnSyncedCommand.CreatedAttendees)
                {
                    var _createdAttendess = _eventAttendeesUnSyncedService.Insert(data, response.CreatedEvents);
                    _createdAttendess.VersionNumber = data.VersionNumber;
                    if (isModified)
                    {
                        response.LastLogId = _eventUnSynService.InsertSync(data.EventId, "Event", "INSERT", data.UserId);
                        var reminders = _eventReminderUnSyncedService.Get(data.EventId);
                        foreach (var reminder in reminders)
                        {
                            response.LastLogId = _eventUnSynService.InsertSync(reminder.Id, "Reminder", "INSERT", data.UserId);
                        }
                        var attendees = _eventAttendeesUnSyncedService.Get(data.EventId);
                        foreach (var attendee in attendees)
                        {
                            if (_createdAttendess.Id != attendee.Id)
                            {
                                response.LastLogId = _eventUnSynService.InsertSync(attendee.Id, "EventAttendee", "INSERT", data.UserId);
                            }

                        }
                    }
                    foreach (var item in eventUnSyncedCommand.Attendees)
                    {
                        response.LastLogId = _eventUnSynService.InsertSync(_createdAttendess.Id, "EventAttendee", "INSERT", item);

                    }



                    response.CreatedAttendees.Add(_createdAttendess);
                }
            }
            if (eventUnSyncedCommand.DeletedAttendees != null && eventUnSyncedCommand.DeletedAttendees.Count() >= 1)
            {
                foreach (var data in eventUnSyncedCommand.DeletedAttendees)
                {
                    var _deleteAttendee = _eventAttendeesUnSyncedService.Delete(data);
                    _deleteAttendee.VersionNumber = data.VersionNumber;
                    foreach (var item in eventUnSyncedCommand.Attendees)
                    {
                        response.LastLogId = _eventUnSynService.InsertSync(_deleteAttendee.Id, "EventAttendee", "DELETE", item);
                    }
                    response.DeletedAttendees.Add(_deleteAttendee);
                }
            }
            if (eventUnSyncedCommand.CreatedReminders != null && eventUnSyncedCommand.CreatedReminders.Count() >= 1)
            {
                foreach (var data in eventUnSyncedCommand.CreatedReminders)
                {
                    var _createdReminder = _eventReminderUnSyncedService.Insert(data, response.CreatedEvents);
                    _createdReminder.VersionNumber = data.VersionNumber;
                    foreach (var attendee in eventUnSyncedCommand.Attendees)
                        response.LastLogId = _eventUnSynService.InsertSync(_createdReminder.Id, "Reminder", "INSERT", attendee);
                    response.CreatedReminder.Add(_createdReminder);
                }
            }
            if (eventUnSyncedCommand.ModifiedReminders != null && eventUnSyncedCommand.ModifiedReminders.Count() >= 1)
            {
                foreach (var data in eventUnSyncedCommand.ModifiedReminders)
                {
                    var _modifyReminder = _eventReminderUnSyncedService.Update(data);
                    _modifyReminder.VersionNumber = data.VersionNumber;
                    foreach (var attendee in eventUnSyncedCommand.Attendees)
                        response.LastLogId = _eventUnSynService.InsertSync(_modifyReminder.Id, "Reminder", "UPDATE", attendee);
                    response.ModifiedReminder.Add(_modifyReminder);
                }
            }
            if (eventUnSyncedCommand.DeletedReminders != null && eventUnSyncedCommand.DeletedReminders.Count() >= 1)
            {
                foreach (var data in eventUnSyncedCommand.DeletedReminders)
                {
                    var _deleteReminder = _eventReminderUnSyncedService.Delete(data);
                    _deleteReminder.VersionNumber = data.VersionNumber;
                    foreach (var attendee in eventUnSyncedCommand.Attendees)
                        response.LastLogId = _eventUnSynService.InsertSync(_deleteReminder.Id, "Reminder", "DELETE", attendee);
                    response.DeletedReminder.Add(_deleteReminder);
                }
            }
        }

        public dynamic GetEventSyncData(int userId, int syncId, string schema, ref string lastSyncId)
        {
            return _eventUnSynService.GetData(userId, syncId, schema, ref lastSyncId);
        }
    }

    public class SyncEventResult
    {
        public DataTable EventIU { get; set; }
        public DataTable EventD { get; set; }
        public DataTable EventAIU { get; set; }
        public DataTable EventAD { get; set; }
        public DataTable EventRIU { get; set; }
        public DataTable EventRD { get; set; }
    }
}