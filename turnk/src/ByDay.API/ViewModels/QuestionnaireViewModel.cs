﻿using ByDay.Business.Questionnaire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.API.ViewModels
{
    public class QuestionnaireViewModel
    {
        public const string textType = "T";
        public const string specialType = "R";
        public const string singleSelection = "SS";
        public const string MultipleSelection = "MS";
        public const string MultipleSingleSelection = "MSS";


        QuestionnaireService _Questionnaire = new QuestionnaireService();
        public void GetQuestionnaireAnswer(QuestionnaireCommand answers)
        {
            foreach (var ans in answers.Answer)
            {
                if (ans.Type == textType)
                {
                    var Questionnaire = _Questionnaire.PostQuestionnTextType(ans);
                }
                else if (ans.Type == specialType)
                {
                    var Questionnaire = _Questionnaire.PostQuestionnTextType(ans);
                }
                else if (ans.Type == singleSelection)
                {
                    var Questionnaire = _Questionnaire.PostQuestionSingleSel(ans);
                }
                else if (ans.Type == MultipleSelection)
                {
                    var Questionnaire = _Questionnaire.PostQuestionMulSel(ans);
                }
                else if (ans.Type == MultipleSingleSelection)
                {
                    var Questionnaire = _Questionnaire.PostQuestionMulSingleSel(ans);
                }
            }
        }
    }
}