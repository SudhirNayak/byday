﻿using Braintree;
using ByDay.API.Models;
using ByDay.Business.Orders;
using ByDay.Utility;
using ByDay.Utility.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
namespace ByDay.API.Controllers
{

    [System.Web.Http.RoutePrefix("api/Payment")]
    public class PaymentConfirmationController : ApiController
    {
        Business.Orders.OrderService _orderService = new Business.Orders.OrderService();
        [System.Web.Http.HttpPost]
        public HttpResponseMessage Post()
        {
            OrderModel item = new OrderModel();
            foreach (var key in System.Web.HttpContext.Current.Request.Form.AllKeys)
            {
                foreach (var val in System.Web.HttpContext.Current.Request.Form.GetValues(key))
                {
                    item = JsonConvert.DeserializeObject<OrderModel>(val);
                }
            }

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            

            var imageName = "/Image/" + System.Guid.NewGuid().ToString();
            var extension = string.Empty;
            if (HttpContext.Current.Request.Files.Count > 0)
            {
                var httpResponse = SaveImage(ref imageName, ref extension);
                if (httpResponse.StatusCode == HttpStatusCode.Created)
                {
                    item.ImageName = imageName;
                    item.ImagePath = "/Image/";
                    item.ImageType = extension;
                }
                else
                {
                    return httpResponse;
                }
            }
            
            
            

            OrderService service = new OrderService();
            // decimal price =service.GetAmountOfProduct(item.UserId, item.ProductId);
            var orderCommand = AutoMapper.Mapper.Map<OrderCommand>(item);
            int id=   _orderService.Insert(orderCommand);
            var request = new TransactionRequest
            {
                //Amount = price,
                Amount=item.TotalPrice,
                PaymentMethodNonce = item.Payment_method_nonce,
                Options = new TransactionOptionsRequest
                {
                    SubmitForSettlement = true
                }
            };
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var gateway = new BraintreeGateway
            {
                MerchantId = ConfigUtility.GetConfigValueFromWebConfig<string>(Constants.MerchantID),
                PublicKey = ConfigUtility.GetConfigValueFromWebConfig<string>(Constants.PublicKey),
                PrivateKey = ConfigUtility.GetConfigValueFromWebConfig<string>(Constants.PrivateKey),

            };
            var Environment = ConfigUtility.GetConfigValueFromWebConfig<string>(Constants.Environment);
            switch (Environment)
            {
                case "PRODUCTION":
                    gateway.Environment = Braintree.Environment.PRODUCTION;
                    break;
                case "QA":
                    gateway.Environment = Braintree.Environment.QA;
                    break;
                case "SANDBOX":
                    gateway.Environment = Braintree.Environment.SANDBOX;
                    break;
                case "DEVELOPMENT":
                    gateway.Environment = Braintree.Environment.DEVELOPMENT;
                    break;
            }
         
            Result<Transaction> result = gateway.Transaction.Sale(request);

            return Request.CreateResponse<ActionOutput<string>>(HttpStatusCode.OK, new ActionOutput<string>
            {
                Message = result.Message,
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result=id.ToString()
              
            });

        }
        private HttpResponseMessage SaveImage(ref string imagePath,ref string extension)
        {
            Directory.CreateDirectory(imagePath);
            //PostFile();
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {


                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 10; //Size = 1 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .jpg,.jpeg,.gif,.png.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please Upload a file upto 1 mb.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {


                            imagePath = imagePath + extension;
                            var filePath = HttpContext.Current.Server.MapPath(imagePath);

                            postedFile.SaveAs(filePath);

                        }
                    }

                    var message1 = string.Format("Image Updated Successfully.");
                    return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
                }
                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
            catch (Exception ex)
            {
                var res = string.Format("some Message");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
        }
    }
}