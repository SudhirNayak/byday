﻿using ByDay.API.Models;
using ByDay.Business.UserScratchPad;
using ByDay.Utility;
using ByDay.Utility.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ByDay.API.Controllers
{


    [System.Web.Http.RoutePrefix("api/ScratchPad")]
    public class ScratchPadController : ApiController
    {
        ScratchPadModel scratchPad = new ScratchPadModel();
        [System.Web.Http.HttpGet]
        public HttpResponseMessage Get(int id)
        {
            ScratchPadService _userScratchPad = new ScratchPadService();
            var userScratchPad = _userScratchPad.GetUserScratchPad(id).ToList();


            if (userScratchPad.Count() >= 1)
            {
                scratchPad = AutoMapper.Mapper.Map<ScratchPadModel>(userScratchPad[0]);
            }
            return Request.CreateResponse<ActionOutput<ScratchPadModel>>(HttpStatusCode.OK, new ActionOutput<ScratchPadModel>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = scratchPad
            });
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage Update(Business.UserScratchPad.ScratchPad scratchPadUser)
        {
            ScratchPadService _userScratchPad = new ScratchPadService();
            var userScratchPad = _userScratchPad.UpdateUserScratchPad(scratchPadUser);
            scratchPad = AutoMapper.Mapper.Map<ScratchPadModel>(userScratchPad);

            return Request.CreateResponse<ActionOutput<ScratchPadModel>>(HttpStatusCode.OK, new ActionOutput<ScratchPadModel>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = scratchPad
            });
        }
    }
}