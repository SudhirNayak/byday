﻿using ByDay.API.Models;
using ByDay.Business.Task;
using ByDay.Utility;
using ByDay.Utility.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ByDay.API.Controllers
{
    [System.Web.Http.RoutePrefix("api/Task")]
    public class TaskController : ApiController
    {
        TaskModel taskModel = new TaskModel();

        [System.Web.Http.Route("UserTasks/{userId}")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage UserTasks(int userId)
        {
            TaskService _taskService = new TaskService();
            var tasks = _taskService.GetTasks(userId);
            var taskModel = new TaskModel();
            taskModel.Tasks = tasks;
            return Request.CreateResponse<ActionOutput<TaskModel>>(HttpStatusCode.OK, new ActionOutput<TaskModel>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = taskModel
            });
        }

        [System.Web.Http.Route("SyncedTasks/{userId}/{lastSyncedId}")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage SyncedTasks(UserTaskViewModel userTasks, int userId, int lastSyncedId)
        {
            TaskService _taskService = new TaskService();
            var res = _taskService.SyncedModel(userTasks, userId, lastSyncedId);
            return Request.CreateResponse<ActionOutput<TaskSyncResponse>>(HttpStatusCode.OK, new ActionOutput<TaskSyncResponse>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = res
            });
        }

        [System.Web.Http.Route("getTaskSyncData/{userId}/{syncId}/{schema}")]
        [System.Web.Http.HttpGet]

        public HttpResponseMessage GetTaskSyncedData(int userId, int syncId, string schema)
        {
            TaskService _taskService = new TaskService();
            string lastSyncId = "";
            var res = _taskService.GetData(userId, syncId, schema, ref lastSyncId);
            SyncTaskResult result = new SyncTaskResult();
            result.TaskIU = res.Tables[0];
            result.TaskD = res.Tables[1];

            result.TaskCategoryIU = res.Tables[2];
            result.TaskCategoryD = res.Tables[3];

            result.TaskAttendeeIU = res.Tables[4];
            result.TaskAttendeeD = res.Tables[5];

            result.TaskPriorityIU = res.Tables[6];
            result.TaskPriorityD = res.Tables[7];


            lastSyncId = lastSyncId == "" ? "0" : lastSyncId;
            return Request.CreateResponse<ActionOutput<dynamic>>(HttpStatusCode.OK, new ActionOutput<dynamic>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = result,
                Code = Convert.ToInt32(lastSyncId)
            });
        }
    }
}