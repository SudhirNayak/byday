﻿using System.Net.Http;
using System.Web.Http;
using ByDay.API.Models;
using System.Net;
using ByDay.Business.User;
using ByDay.Utility.Enums;
using ByDay.Utility;
using System.Collections.Generic;
using System.Linq;
using ByDay.Business.RecipeDetailUnSynced;

namespace ByDay.API.Controllers
{

    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        UserSignUpService _userService = new UserSignUpService();
        RecipeService _recipeService = new RecipeService();
        [HttpPost]
        [Route("SignUp")]
        public HttpResponseMessage SignUp(UserCommand userCommand)
        {
            var userModel = new UserModel();
            if (userCommand.EmailId != null)
            {
                bool firstTimeUserLogin = true;
                var user = _userService.UserSignUp(userCommand,ref firstTimeUserLogin);
                var calendar = _userService.CreateDefaultCalender(user, userCommand);
                var scratchPad = _userService.CreateScrachpad(user);
                var colorValue = _userService.GetColor().ToList();
                var  users = AutoMapper.Mapper.Map<UserModel>(user);
                var category = _recipeService.GetRecipeCategory().ToList();
                userModel.User = users.User;
                userModel.CalendarList = AutoMapper.Mapper.Map<List<Business.UserCalendar.Calendar>>(calendar);
                userModel.Colors = AutoMapper.Mapper.Map<List<Business.CalendarColor.Color>>(colorValue);
                userModel.RecipeCategories =  AutoMapper.Mapper.Map<List<ResponseRecipeCategory>>(category);
                userModel.FirstTimeUserLogin = firstTimeUserLogin;
                if (scratchPad.Count() >= 1)
                {
                    userModel.ScratchPad = AutoMapper.Mapper.Map<ScratchPadModel>(scratchPad[0]);
                }
            }
            return Request.CreateResponse<ActionOutput<UserModel>>(HttpStatusCode.OK, new ActionOutput<UserModel>
            {
                Message = "Success",
                Status =FieldConverter.To<bool>((userModel.User.EmailId == null) ? FieldConverter.To<bool>(ActionStatus.Error) : FieldConverter.To<bool>(ActionStatus.Successfull)),
                   Result = userModel
            });            
        }
    }
}
