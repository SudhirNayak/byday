﻿using ByDay.API.Models;
using ByDay.Business.UserNote;
using ByDay.Utility;
using ByDay.Utility.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ByDay.API.Controllers
{

    [System.Web.Http.RoutePrefix("api/UserNote")]
    public class UserNoteController : ApiController
    {
        UserNoteModel userNote = new UserNoteModel();
        [System.Web.Http.HttpGet]
        public HttpResponseMessage Get(int id)
        {
            UserNoteService _userNote = new UserNoteService();
            var userNoteBook = _userNote.GetUserNotes(id).ToList();
        
                userNote.UserNotes = AutoMapper.Mapper.Map<List<UserNotes>>(userNoteBook);

            return Request.CreateResponse<ActionOutput<UserNoteModel>>(HttpStatusCode.OK, new ActionOutput<UserNoteModel>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = userNote
            });
        }
        [System.Web.Http.Route("SyncedNotes/{userId}/{lastSyncedId}")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage SyncedNotes(UserNotesViewModel userNotes, int userId, int lastSyncedId)
        {
            UserNoteService _userNote = new UserNoteService();
            var res = _userNote.SyncedNotes(userNotes, userId, lastSyncedId);
            return Request.CreateResponse<ActionOutput<NotesSyncResponse>>(HttpStatusCode.OK, new ActionOutput<NotesSyncResponse>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = res
            });
        }

        [System.Web.Http.Route("getNotesSyncData/{userId}/{syncId}/{schema}")]
        [System.Web.Http.HttpGet]

        public HttpResponseMessage GetNotesSyncedData(int userId, int syncId, string schema)
        {
            UserNoteService _userNote = new UserNoteService();
            string lastSyncId = "";
            var res = _userNote.GetData(userId, syncId, schema, ref lastSyncId);
            SyncNotesResult result = new SyncNotesResult();
            result.NotesIU = res.Tables[0];
            result.NotesD = res.Tables[1];
            result.CategoryIU = res.Tables[2];
            result.CategoryD = res.Tables[3];
            result.UserScratchPad = res.Tables[4];

            lastSyncId = lastSyncId == "" ? "0" : lastSyncId;
            return Request.CreateResponse<ActionOutput<dynamic>>(HttpStatusCode.OK, new ActionOutput<dynamic>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = result,
                Code = Convert.ToInt32(lastSyncId)
            });
        }


    }
}