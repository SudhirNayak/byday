﻿using ByDay.API.Models;
using ByDay.Business.Orders;
using ByDay.Utility;
using ByDay.Utility.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ByDay.API.Controllers
{
    [System.Web.Http.RoutePrefix("api/Order")]
    public class OrderController : ApiController
    {
        Business.Orders.OrderService _orderService = new Business.Orders.OrderService();

        [Route("Insert")]
        [HttpPost]
        public HttpResponseMessage Insert(OrderModel model)
        {
            var orderCommand = AutoMapper.Mapper.Map<OrderCommand>(model);
            _orderService.Insert(orderCommand);
            return Request.CreateResponse<ActionOutput<bool>>(HttpStatusCode.OK, new ActionOutput<bool>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = true
            });
        }
    }
}
