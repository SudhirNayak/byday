﻿using ByDay.API.Models;
using ByDay.API.ViewModels;
using ByDay.Business.MealPlans;
using ByDay.Utility;
using ByDay.Utility.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ByDay.API.Controllers
{

    [RoutePrefix("api/MealPlan")]
    public class MealplanController : ApiController
    {
        MealPlanService _mealService = new MealPlanService();
        [HttpGet]
        public HttpResponseMessage UnSynced(string userId, long id)
        {
            UnSyncedMealPlanModel unSyncedMeal = new UnSyncedMealPlanModel();
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, 0).AddMilliseconds(id).ToLocalTime();
            var mealsPlan = _mealService.GetUnSyncedMealPlan(userId, dt).ToList();
            List<string> recipeName = new List<string>();

            unSyncedMeal.MealPlans = AutoMapper.Mapper.Map<List<MealPlanSyncedModel>>(mealsPlan);
            unSyncedMeal.MealPlans.ForEach(x => x.RecipeName = _mealService.GetRecipeName(x.RecipeId).Select(y => y.RecipeName).FirstOrDefault());
            unSyncedMeal.MealPlans.ForEach(x => x.Thumbnail = _mealService.GetRecipeImage (x.RecipeId).Select(y => y.Path).FirstOrDefault());
            unSyncedMeal.MealPlans.ForEach(x => x.IsFavourite = _mealService.GetIsFavourite(x.RecipeId).Select(y => y.IsFavourite).FirstOrDefault());
            return Request.CreateResponse<ActionOutput<UnSyncedMealPlanModel>>(HttpStatusCode.OK, new ActionOutput<UnSyncedMealPlanModel>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = unSyncedMeal
            });
        }



        [HttpPost]
        public HttpResponseMessage UnSynced(MealUnSyncedCommand mealUnSyncedCommand, string userId, long id)
        {
            MealUnSyncedResponse response = new MealUnSyncedResponse();
            MealViewModel mealViewModel = new MealViewModel();        
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, 0).AddMilliseconds(id).ToLocalTime();
            var mealPlan = _mealService.GetUnSyncedMealPlan (userId, dt).ToList();
            bool flag = false;
            if (mealPlan.Count() == 0)
            {
                response = mealViewModel.GetMealUnSyncedResponse(mealUnSyncedCommand);
                flag = true;
            }
            return Request.CreateResponse<ActionOutput<MealUnSyncedResponse>>(HttpStatusCode.OK, new ActionOutput<MealUnSyncedResponse>
            {
                Message = "Success",
                Status = flag == false ? FieldConverter.To<bool>(ActionStatus.Error) : FieldConverter.To<bool>(ActionStatus.Successfull),
                Code = flag == false ? FieldConverter.To<int>(ActionStatus.Successfull) : FieldConverter.To<int>(ActionStatus.Error),
                Result = response

            });
        }



        [HttpPost]
        public HttpResponseMessage InsertUpdate(MealUnSyncedCommand mealUnSyncedCommand)
        {
            MealUnSyncedResponse response = new MealUnSyncedResponse();
            MealViewModel mealViewModel = new MealViewModel();
            response = mealViewModel.GetMealUnSyncedResponse(mealUnSyncedCommand);
            return Request.CreateResponse<ActionOutput<MealUnSyncedResponse>>(HttpStatusCode.OK, new ActionOutput<MealUnSyncedResponse>
            {
                Message = "Success",
                Status= FieldConverter.To<bool>(ActionStatus.Successfull),
                 Code = FieldConverter.To<int>(ActionStatus.Error),
                Result = response

            });
        }



        [HttpGet]
        public HttpResponseMessage Tip()
        {
            List<TipModel> response = new List<TipModel>();
            var tipData = _mealService.GetMealTip().ToList();
            response = AutoMapper.Mapper.Map<List<TipModel>>(tipData);

            return Request.CreateResponse<ActionOutput<List<TipModel>>>(HttpStatusCode.OK, new ActionOutput<List<TipModel>>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Code = FieldConverter.To<int>(ActionStatus.Error),
                Result = response

            });
        }

        //[HttpGet]
        //public HttpResponseMessage bulleting(string userId, long id)
        //{
        //    UnSyncedMealPlanModel unSyncedMeal = new UnSyncedMealPlanModel();
        //    DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, 0).AddMilliseconds(id).ToLocalTime();
        //    var mealsPlan = _mealService.GetUnSyncedMealPlan(userId, dt).ToList();
        //    List<string> recipeName = new List<string>();

        //    unSyncedMeal.MealPlans = AutoMapper.Mapper.Map<List<MealPlanSyncedModel>>(mealsPlan);
        //    unSyncedMeal.MealPlans.ForEach(x => x.RecipeName = _mealService.GetRecipeName(x.RecipeId).Select(y => y.RecipeName).FirstOrDefault());
        //    unSyncedMeal.MealPlans.ForEach(x => x.Thumbnail = _mealService.GetRecipeImage(x.RecipeId).Select(y => y.Path).FirstOrDefault());
        //    unSyncedMeal.MealPlans.ForEach(x => x.IsFavourite = _mealService.GetIsFavourite(x.RecipeId).Select(y => y.IsFavourite).FirstOrDefault());
        //    return Request.CreateResponse<ActionOutput<UnSyncedMealPlanModel>>(HttpStatusCode.OK, new ActionOutput<UnSyncedMealPlanModel>
        //    {
        //        Message = "Success",
        //        Status = FieldConverter.To<bool>(ActionStatus.Successfull),
        //        Result = unSyncedMeal
        //    });
        //}


    }
}
