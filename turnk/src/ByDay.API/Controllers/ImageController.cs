﻿using ByDay.Business.Image;
using ByDay.Business.RecipeDetailUnSynced;
using ByDay.Utility;
using ByDay.Utility.Enums;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace ByDay.API.Controllers
{
    [System.Web.Http.RoutePrefix("api/Image")]
    public class ImageController : ApiController
    {
        [System.Web.Http.Route("UploadImage/{userId}")]
        [System.Web.Http.HttpPost]
        // GET: Image
        public HttpResponseMessage UploadImage(int userId)
        {
            try
            {
                RecipeImage recipeImage = new RecipeImage();
                foreach (var key in System.Web.HttpContext.Current.Request.Form.AllKeys)
                {
                    foreach (var val in System.Web.HttpContext.Current.Request.Form.GetValues(key))
                    {
                        recipeImage = JsonConvert.DeserializeObject<RecipeImage>(val);
                        recipeImage.CreatedAt = DateTime.UtcNow;
                        recipeImage.ModifiedAt = DateTime.UtcNow;
                        recipeImage.ModifiedBy = userId;
                        recipeImage.CreatedBy = userId;
                    }
                }

                System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
                ImageService service = new ImageService();


                var imageName = "/Image/" + Guid.NewGuid().ToString();
                var httpResponse = SaveImage(ref imageName);

                if (httpResponse.StatusCode == HttpStatusCode.Created)
                {
                    recipeImage.Path = imageName;
                    ImageSyncResponse response = service.SyncedModel(recipeImage, userId);
                    return Request.CreateResponse<ActionOutput<RecipeImage>>(HttpStatusCode.OK, new ActionOutput<RecipeImage>
                    {
                        Message = "Success",
                        Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                        Result = response.image,
                        Code=response.lastSyncId
                    });
                }
                else
                {
                    return httpResponse;
                }
            }
            catch (Exception ex)
            {
                Dictionary<string, object> dict = new Dictionary<string, object>();
                dict.Add("error", "Please check input parameter and file type.");
                return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
            }
        }

        private HttpResponseMessage SaveImage(ref string imagePath)
        {
            Directory.CreateDirectory(imagePath);
            //PostFile();
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {


                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 10; //Size = 1 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .jpg,.jpeg,.gif,.png.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please Upload a file upto 1 mb.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {


                            imagePath = imagePath + extension;
                            var filePath = HttpContext.Current.Server.MapPath(imagePath);

                            postedFile.SaveAs(filePath);

                        }
                    }

                    var message1 = string.Format("Image Updated Successfully.");
                    return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
                }
                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
            catch (Exception ex)
            {
                var res = string.Format("some Message");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
        }
    }


}