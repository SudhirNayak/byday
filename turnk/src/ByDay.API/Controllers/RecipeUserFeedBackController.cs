﻿using ByDay.Business.RecipeDetailUnSynced;
using ByDay.Business.RecipeUserFeedBack;
using ByDay.Utility;
using ByDay.Utility.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ByDay.API.Controllers
{


    [System.Web.Http.RoutePrefix("api/RecipeUserFeedBack")]
    public class RecipeUserFeedBackController : ApiController
    {
        [System.Web.Http.HttpPost]
        public HttpResponseMessage InsertUpdate(Business.RecipeDetailUnSynced.RecipeUserFeedBack recipeUserFeedback)
        {
            RecipeUserFeedBackService _userFeeBack = new RecipeUserFeedBackService();
            int userScratchPad = _userFeeBack.UpdateUserFeedBack(recipeUserFeedback);
            return Request.CreateResponse<ActionOutput<int>>(HttpStatusCode.OK, new ActionOutput<int>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = userScratchPad
            });
        }
    }
}