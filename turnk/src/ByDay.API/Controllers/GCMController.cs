﻿using System.Net.Http;
using System.Web.Http;
using ByDay.API.Models;
using System.Net;
using ByDay.Business.DeviceSyncUser;
using ByDay.Utility.Enums;
using System.Collections.Generic;

namespace ByDay.API.Controllers
{
    [RoutePrefix("api/GCM")]
    public class GCMController : ApiController
    {
        Notification notification = new Notification();        
        [System.Web.Http.HttpPost]
        public HttpResponseMessage Notification(int id)
        {
            
            DeviceSyncService deviceSyn = new DeviceSyncService();
            var pushTokens = deviceSyn.GetPushTokens(id);
            string sResponseFromServer = null;
            foreach (var item in pushTokens)
            {
              sResponseFromServer = notification.SendNotification(item.PushToken);
            }
         
            return Request.CreateResponse(HttpStatusCode.OK, sResponseFromServer);
        }
    }
}