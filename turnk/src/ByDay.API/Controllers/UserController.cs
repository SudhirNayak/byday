﻿using ByDay.API.Models;
using ByDay.Business.User;
using ByDay.Utility;
using ByDay.Utility.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ByDay.API.Controllers
{


    [System.Web.Http.RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        [System.Web.Http.HttpPost]
        public HttpResponseMessage InsertAddress(UserAddressCommand userAddCommand)
        {
            UserAddressService _userService = new UserAddressService();
            UserAddressModel user = new UserAddressModel();
            var userAddress = _userService.Insert(userAddCommand);

            user = AutoMapper.Mapper.Map<UserAddressModel>(userAddress);
           
            return Request.CreateResponse<ActionOutput<UserAddressModel>>(HttpStatusCode.OK, new ActionOutput<UserAddressModel>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = user
            });
        }

        [System.Web.Http.HttpGet]
        public HttpResponseMessage Get(int id)
        {
            UserAddressService _userService = new UserAddressService();
            var userAddress = _userService.GetUserAddress(id);
            UserAddressModel user = new UserAddressModel();

            user = AutoMapper.Mapper.Map<UserAddressModel>(userAddress.LastOrDefault());

            return Request.CreateResponse<ActionOutput<UserAddressModel>>(HttpStatusCode.OK, new ActionOutput<UserAddressModel>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = user
            });
        }

        [System.Web.Http.Route("request/{user_id}")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage UserRequest(dynamic data,int user_id)
        {
            User user = new Business.User.User();
            int status = user.AddUserMember(user_id, data);
            return Request.CreateResponse<ActionOutput<User>>(HttpStatusCode.OK, new ActionOutput<User>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = null,
                Code = status
            });
        }

        [System.Web.Http.Route("getUserFamilyMembers/{userId}/{syncId}/{schema}")]
        public HttpResponseMessage GetUserFamilyMembers(int userId, int syncId, string schema)
        {
            User user = new User();
            string lastSyncId = "";
            dynamic res = user.GetData(userId, syncId, schema, ref lastSyncId);
            SyncUserFamilyResult result = new SyncUserFamilyResult();
            result.UserFamilyIU = res.Tables[0];
            result.MemberCalender = res.Tables[1];
           
            lastSyncId = ((lastSyncId == "") ? "0" : lastSyncId);
            return base.Request.CreateResponse(HttpStatusCode.OK, new ActionOutput<object>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = result,
                Code = Convert.ToInt32(lastSyncId)
            });
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("request/{user_id}/{member_id}")]
        public HttpResponseMessage UserAccept(int user_id,int member_id)
        {
            User user = new Business.User.User();
            int status = user.AcceptRequest(user_id, member_id);
            return Request.CreateResponse<ActionOutput<User>>(HttpStatusCode.OK, new ActionOutput<User>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = null,
                Code = status
            });
        }

        [System.Web.Http.Route("request/{user_id}/{member_id}")]
        [System.Web.Http.HttpDelete]
        public HttpResponseMessage UserDelete(int user_id, int member_id)
        {
            User user = new Business.User.User();
            var result = user.DeleteRequest(user_id, member_id);
            return Request.CreateResponse<ActionOutput<User>>(HttpStatusCode.OK, new ActionOutput<User>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = null,
                Code = result
            });
        }

        [System.Web.Http.Route("all/{user_id}")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage UserAllMembers(int user_id)
        {
            User user = new Business.User.User();
            var users = AutoMapper.Mapper.Map<List<UserFamilyMember>>(user.UserAllMembers(user_id).ToList());
            var result = Request.CreateResponse<ActionOutput<List<UserFamilyMember>>>(HttpStatusCode.OK, new ActionOutput<List<UserFamilyMember>>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = users
            });
            return result;
        }

        [System.Web.Http.Route("feedback")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage UserFeedback(dynamic data)
        {
            User user = new Business.User.User();
            user.UserFeedback(data);
            return Request.CreateResponse<ActionOutput<User>>(HttpStatusCode.OK, new ActionOutput<User>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
            });
        }
    }
}