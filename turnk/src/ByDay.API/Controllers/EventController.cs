﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using ByDay.Business.EventDetail;
using ByDay.API.Models;
using ByDay.Utility.Enums;
using System.Net;
using System.Web.Http;
using ByDay.Utility;
using System;
using ByDay.Business.EventAttendees;
using ByDay.Business.EventReminder;
using ByDay.API.ViewModels;
using Newtonsoft.Json;

namespace ByDay.API.Controllers
{

    [RoutePrefix("api/Event")]
    public class EventController : ApiController
    {
        EventService _eventService = new EventService();
       [HttpPost]
         public HttpResponseMessage Post(EventCommand  eventCommand)
        {
                var eventsModel = new EventModel();
                var events = _eventService.Insert(eventCommand);
                eventsModel = AutoMapper.Mapper.Map<EventModel>(events);
            
            return Request.CreateResponse<ActionOutput<EventModel>>(HttpStatusCode.OK, new ActionOutput<EventModel>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = eventsModel
            });
        }

        [System.Web.Http.HttpPost]

        public HttpResponseMessage UnSynced(EventUnSyncedCommand eventUnSyncedCommand, long userId, long id)
        {
            EventUnSyncedResponse response = new EventUnSyncedResponse();
            EventViewModel eventViewModel = new EventViewModel();
            response = eventViewModel.GetEventUnSyncedResponse(eventUnSyncedCommand, userId, id);
            return Request.CreateResponse<ActionOutput<EventUnSyncedResponse>>(HttpStatusCode.OK, new ActionOutput<EventUnSyncedResponse>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Code = response.code,
                Result = response
            });
        }

        //[System.Web.Http.HttpPost]

        //public HttpResponseMessage UnSynced(EventUnSyncedCommand eventUnSyncedCommand)
        //{
        //    EventUnSyncedResponse response = new EventUnSyncedResponse();
        //    EventViewModel eventViewModel = new EventViewModel();

        //    response= eventViewModel.GetEventUnSyncedResponse(eventUnSyncedCommand);
        //    return Request.CreateResponse<ActionOutput<EventUnSyncedResponse>>(HttpStatusCode.OK, new ActionOutput<EventUnSyncedResponse>
        //    {
        //        Message = "Success",
        //        Status = FieldConverter.To<bool>(ActionStatus.Successfull),
        //        Result = response
        //    });


        //}




        //[System.Web.Http.HttpGet]
        //public HttpResponseMessage Get()
        //{
        //    var eventResult = _eventService.Get().ToList();
        //    List<EventModel> eventsModel = AutoMapper.Mapper.Map<List<EventModel>>(eventResult);
        //    return Request.CreateResponse<ActionOutput<List<EventModel>>>(HttpStatusCode.OK, new ActionOutput<List<EventModel>>
        //    {
        //        Message = "Success",
        //        Status = FieldConverter.To<bool>(ActionStatus.Successfull),
        //        Result = eventsModel
        //    });

        //}

        [System.Web.Http.HttpGet]
        public HttpResponseMessage UnSynced(int id)
        {
            var eventResult = _eventService.Get(id).ToList().Distinct();
            List<EventModel> eventsModel = AutoMapper.Mapper.Map<List<EventModel>>(eventResult);
            return Request.CreateResponse<ActionOutput<List<EventModel>>>(HttpStatusCode.OK, new ActionOutput<List<EventModel>>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = eventsModel
            });

        }

        [System.Web.Http.HttpGet]

        public HttpResponseMessage UnSynced(string userId, long id)
        {
            UnSyncedEventModel unSyncedEvent = new UnSyncedEventModel();
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, 0).AddMilliseconds(id).ToLocalTime();
            var eventResult = _eventService.GetUnSyncedEvent(userId, dt,unSyncedEvent.LastSync).ToList();
            var eventAttendee = _eventService.GetUnSyncedEventAttendee(userId, dt).ToList();
            var eventReminder = _eventService.GetUnSyncedEventReminder(userId, dt).ToList();         
            unSyncedEvent.Events = AutoMapper.Mapper.Map<List<EventSyncedModel>>(eventResult);
            unSyncedEvent.Attendees = AutoMapper.Mapper.Map<List<EventAttendeeCommand>>(eventAttendee);
            unSyncedEvent.Reminders = AutoMapper.Mapper.Map<List<EventReminderCommand>>(eventReminder);
            return Request.CreateResponse<ActionOutput<UnSyncedEventModel>>(HttpStatusCode.OK, new ActionOutput<UnSyncedEventModel>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = unSyncedEvent
            });

        }

        [System.Web.Http.Route("getSyncData/{userId}/{syncId}/{schema}")]
        [System.Web.Http.HttpGet]

        public HttpResponseMessage GetEventSyncedData(int userId, int syncId, string schema)
        {
            EventViewModel eventViewModel = new EventViewModel();
            string lastSyncId = "";
            var res = eventViewModel.GetEventSyncData(userId, syncId, schema, ref lastSyncId);
            SyncEventResult result = new SyncEventResult();
            result.EventIU = res.Tables[0];
            result.EventD = res.Tables[1];
            result.EventAIU = res.Tables[2];
            result.EventAD = res.Tables[3];
            result.EventRIU = res.Tables[4];
            result.EventRD = res.Tables[5];
            //string serializer = JsonConvert.SerializeObject(result);
            lastSyncId = lastSyncId == "" ? "0" : lastSyncId;
            return Request.CreateResponse<ActionOutput<dynamic>>(HttpStatusCode.OK, new ActionOutput<dynamic>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = result,
                Code = Convert.ToInt32(lastSyncId)
            });
        }


        //[System.Web.Http.HttpGet]

        //public HttpResponseMessage UnSynced(string userId, int  id)
        //{

        //    DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0,0).AddSeconds(id).ToLocalTime();
        //    var eventResult = _eventService.GetUnSyncedData(userId, dt).ToList();
        //    var eventsByCreatedDate = eventResult.Where(x => (x.CreatedAt != null&&x.ModifiedAt==null)&&(x.IsDeleted==false)).ToList();
        //    var eventsByModifiedDate = eventResult.Where(x => (x.ModifiedAt != null) && (x.IsDeleted==false)).ToList();
        //    var eventsByDeleted = eventResult.Where(x => x.IsDeleted==true).ToList();


        //    UnSyncedEventModel unSyncedEvent = new UnSyncedEventModel();

        //    unSyncedEvent.CreatedEvents = AutoMapper.Mapper.Map<List<EventModel>>(eventsByCreatedDate);
        //    unSyncedEvent.ModifiedEvents = AutoMapper.Mapper.Map<List<EventModel>>(eventsByModifiedDate);
        //    unSyncedEvent.DeletedEvents = AutoMapper.Mapper.Map<List<EventModel>>(eventsByDeleted);
        //    return Request.CreateResponse<ActionOutput<UnSyncedEventModel>>(HttpStatusCode.OK, new ActionOutput<UnSyncedEventModel>
        //    {
        //        Message = "Success",
        //        Status = FieldConverter.To<bool>(ActionStatus.Successfull),
        //        Result = unSyncedEvent
        //    });

        //}




    }
}