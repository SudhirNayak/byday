﻿using ByDay.API.Models;
using ByDay.API.ViewModels;
using ByDay.Business.RecipeDetailUnSynced;
using ByDay.Utility;
using ByDay.Utility.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ByDay.API.Controllers
{

    [System.Web.Http.RoutePrefix("api/Recipe")]
    public class RecipeController : ApiController
    {


        RecipeService _recipeService = new RecipeService();
        UnSyncedRecipeModel unSyncedMeal = new UnSyncedRecipeModel();
        //[System.Web.Http.HttpGet]

        //public HttpResponseMessage UnSynced(string userId, long id)
        //{

        //    DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, 0).AddMilliseconds(id).ToLocalTime();
        //    var rec = _recipeService.GetUnSyncedRecipe(userId, dt).ToList();
        //    unSyncedMeal.Recipes = AutoMapper.Mapper.Map<List<RecipeResponseCommand>>(rec);
        //    return Request.CreateResponse<ActionOutput<UnSyncedRecipeModel>>(HttpStatusCode.OK, new ActionOutput<UnSyncedRecipeModel>
        //    {
        //        Message = "Success",
        //        Status = FieldConverter.To<bool>(ActionStatus.Successfull),
        //        Result = unSyncedMeal
        //    });
        //}



        [System.Web.Http.HttpGet]

        public HttpResponseMessage UnSynced(string userId, long id)
        {
            UnSyncedNewRecipeModel unSyncedRecipe = new UnSyncedNewRecipeModel();
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, 0).AddMilliseconds(id).ToLocalTime();
            var rec = _recipeService.GetUnSyncedRecipe(userId, dt).ToList();
            var ing = _recipeService.GetUnSyncedIngredient(userId, dt).ToList();
            var userFeedBack = _recipeService.GetUnSyncedUserFeedBack(userId, dt).ToList();
            var category = _recipeService.GetUnSyncedCategory(userId, dt).ToList();
            var recipeImage = _recipeService.GetUnSyncedRecipeImage(userId, dt).ToList();

            unSyncedRecipe.Recipes = AutoMapper.Mapper.Map<List<RecipeRespCommand>>(rec);
            unSyncedRecipe.Ingredients = AutoMapper.Mapper.Map<List<RespIngredient>>(ing);
            unSyncedRecipe.UserSpecificRecipeDetails = AutoMapper.Mapper.Map<List<RecipeRespUserFeedBack>>(userFeedBack);
            unSyncedRecipe.Categories = AutoMapper.Mapper.Map<List<RecipeRespCategory>>(category);
            unSyncedRecipe.Images = AutoMapper.Mapper.Map<List<RecipeRespImage>>(recipeImage);


            return Request.CreateResponse<ActionOutput<UnSyncedNewRecipeModel>>(HttpStatusCode.OK, new ActionOutput<UnSyncedNewRecipeModel>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = unSyncedRecipe
            });
        }


        [System.Web.Http.HttpGet]

        public HttpResponseMessage GetRecipeCategory()
        {
            RecipeCategoryModel response = new RecipeCategoryModel();

            var recipe = _recipeService.GetRecipeCategory().ToList();
            response.RecipeCateogories = AutoMapper.Mapper.Map<List<ResponseRecipeCategory>>(recipe);
            return Request.CreateResponse<ActionOutput<RecipeCategoryModel>>(HttpStatusCode.OK, new ActionOutput<RecipeCategoryModel>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = response
            });
        }

        //[System.Web.Http.HttpPost]

        //public HttpResponseMessage UnSynced(RecipeUnSyncedCommand recipeUnSyncedCommand, string userId, long id)
        //{
        //    RecipeUnSyncedResponse response = new RecipeUnSyncedResponse();
        //    RecipeViewModel recipeViewModel = new RecipeViewModel();
        //    DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, 0).AddMilliseconds(id).ToLocalTime();
        //    var recipe = _recipeService.GetUnSyncedRecipe(userId, dt).ToList();
        //    bool flag = false;
        //    if (recipe.Count() == 0)
        //    {
        //        response = recipeViewModel.GetRecipeUnSyncedResponse(recipeUnSyncedCommand);
        //        flag = true;
        //    }
        //    return Request.CreateResponse<ActionOutput<RecipeUnSyncedResponse>>(HttpStatusCode.OK, new ActionOutput<RecipeUnSyncedResponse>
        //    {
        //        Message = "Success",
        //        Status = flag == false ? FieldConverter.To<bool>(ActionStatus.Error) : FieldConverter.To<bool>(ActionStatus.Successfull),
        //        Code = flag == false ? FieldConverter.To<int>(ActionStatus.Successfull) : FieldConverter.To<int>(ActionStatus.Error),
        //        Result = response

        //    });


        //}

        [System.Web.Http.Route("SyncedRecipes/{userId}/{lastSyncedId}")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage SyncedRecipes(RecipeViewModel1 recipeModel, int userId, int lastSyncedId)
        {
            RecipeService recipe = new RecipeService();
            var res = recipe.SyncedRecipes(recipeModel, userId, lastSyncedId);
            return Request.CreateResponse<ActionOutput<RecipeSyncResponse>>(HttpStatusCode.OK, new ActionOutput<RecipeSyncResponse>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = res
            });
        }

        [System.Web.Http.Route("SyncedRecipes/{userId}/{syncId}/{schema}")]
        [System.Web.Http.HttpGet]

        public HttpResponseMessage GetSyncedRecipes(int userId, int syncId, string schema)
        {
            RecipeService recipe = new RecipeService();
            string lastSyncId = "";
            var res = recipe.GetData(userId, syncId, schema, ref lastSyncId);
            SyncRecipeResult result = new SyncRecipeResult();
            result.RecipeIU = res.Tables[0];
            result.RecipeD = res.Tables[1];
            result.RecipeIngredientIU = res.Tables[2];
            result.RecipeIngredientD = res.Tables[3];
            result.ImageI = res.Tables[4];
            result.ImageD = res.Tables[5];
            result.CategoryI = res.Tables[6];
            result.CategoryD = res.Tables[7];
            result.RecipeFeedbackIU = res.Tables[8];
            result.RecipeFeedbackD = res.Tables[9];
            result.MealPlanIU = res.Tables[10];
            result.MealPlanD = res.Tables[11];
            result.BulletinBoardI = res.Tables[12];
            result.BulletinBoardD = res.Tables[13];
            result.AdminRecipeI = res.Tables[14];
            result.AdminRecipeImagesI = res.Tables[15];
            result.AdminRecipeIngredientI = res.Tables[16];
            result.AdminRecipeFeedbackI = res.Tables[17];
            result.AdminRecipeCategoryI = res.Tables[18];
            

            lastSyncId = lastSyncId == "" ? "0" : lastSyncId;
            return Request.CreateResponse<ActionOutput<dynamic>>(HttpStatusCode.OK, new ActionOutput<dynamic>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = result,
                Code = Convert.ToInt32(lastSyncId)
            });
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage UnSynced(RecipeNewUnSyncedCommand recipeUnSyncedCommand, string userId, long id)
        {
            RecipeNewUnSyncedResponse response = new RecipeNewUnSyncedResponse();
            RecipeViewModel recipeViewModel = new RecipeViewModel();
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, 0).AddMilliseconds(id).ToLocalTime();
            var recipe = _recipeService.GetUnSyncedRecipe(userId, dt).ToList();
            var ing = _recipeService.GetUnSyncedIngredient(userId, dt).ToList();
            var userFeedBack = _recipeService.GetUnSyncedUserFeedBack(userId, dt).ToList();
            var category = _recipeService.GetUnSyncedCategory(userId, dt).ToList();
            var recipeImage = _recipeService.GetUnSyncedRecipeImage(userId, dt).ToList();
            bool flag = false;
            if (recipe.Count() == 0 && ing.Count() == 0 && userFeedBack.Count() == 0 && category.Count() == 0 && recipeImage.Count() == 0)
            {
                response = recipeViewModel.GetRecipeNewUnSyncedResponse(recipeUnSyncedCommand);
                flag = true;
            }
            return Request.CreateResponse<ActionOutput<RecipeNewUnSyncedResponse>>(HttpStatusCode.OK, new ActionOutput<RecipeNewUnSyncedResponse>
            {
                Message = "Success",
                Status = flag == false ? FieldConverter.To<bool>(ActionStatus.Error) : FieldConverter.To<bool>(ActionStatus.Successfull),
                Code = flag == false ? FieldConverter.To<int>(ActionStatus.Successfull) : FieldConverter.To<int>(ActionStatus.Error),
                Result = response

            });


        }




        [System.Web.Http.HttpPost]
        public HttpResponseMessage Search(RecipeSearch recipe, int id)
        {
            recipe.Keys = recipe.Keys.Distinct().ToList();
            //var userSpeficdetails = new List<RecipeResponseUserFeedBack>();
            RecipeViewModel recipeView = new RecipeViewModel();
            var recipeResult = recipeView.GetRecipeSearchResult(recipe, id).Take(recipe.PageDetail.PageSize);
            unSyncedMeal.Recipes = AutoMapper.Mapper.Map<List<RecipeResponseCommand>>(recipeResult);
            foreach (var recipeSpecific in unSyncedMeal.Recipes)
            {
                recipeSpecific.UserSpecificRecipeDetails = recipeSpecific.UserSpecificRecipeDetails.Where(t => t.UserId == id).ToList();
            }
            //var flag = false;
            //foreach (var item in unSyncedMeal.Recipes)
            //{
            //    if (item.UserSpecificRecipeDetails.Count <= 1) continue;
            //    foreach (var user in item.UserSpecificRecipeDetails)
            //    {
            //        item.UserSpecificRecipeDetails = null;
            //        if (user.UserId == id)
            //        {
            //            userSpeficdetails.Add(user);
            //            item.UserSpecificRecipeDetails = userSpeficdetails;
            //            flag = true;
            //            break;
            //        }
            //    }
            //}
            return Request.CreateResponse<ActionOutput<UnSyncedRecipeModel>>(HttpStatusCode.OK, new ActionOutput<UnSyncedRecipeModel>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = unSyncedMeal
            });
        }


        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetRecipeById(RecipeBasedSearch recipeBasedSearch)
        {
            List<Data.Recipe> recipe = new List<Data.Recipe>();
            foreach (var res in recipeBasedSearch.Recipe)
            {
                DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, 0).AddMilliseconds(res.LastSync).ToLocalTime();
                recipe.AddRange(_recipeService.GetUnSyncedRecipeByRecipeId(res.RecipeId, dt));
            }
            unSyncedMeal.Recipes = AutoMapper.Mapper.Map<List<RecipeResponseCommand>>(recipe);

            return Request.CreateResponse<ActionOutput<UnSyncedRecipeModel>>(HttpStatusCode.OK, new ActionOutput<UnSyncedRecipeModel>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = unSyncedMeal
            });
        }
    }
}