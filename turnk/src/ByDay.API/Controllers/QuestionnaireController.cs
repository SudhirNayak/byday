﻿using ByDay.API.Models;
using ByDay.API.ViewModels;
using ByDay.Business.Questionnaire;
using ByDay.Utility;
using ByDay.Utility.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ByDay.API.Controllers
{

    [System.Web.Http.RoutePrefix("api/Questionnaire")]
    public class QuestionnaireController : ApiController
    {
        QuestionnaireService _Questionnaire = new QuestionnaireService();
        QuestionnaireModel questionnaire = new QuestionnaireModel();
        [HttpGet]
    public HttpResponseMessage Meal(int id)
    {
            var Questionnaire = _Questionnaire.GetQuestionnaire().ToList();
            var QuestionnaireAnswer = _Questionnaire.GetQuestionnaireTextType(id).ToList();
            var QuestionnaireAnsMul = _Questionnaire.GetQuestionnaireSelectionType(id).ToList();
            questionnaire.Questions = AutoMapper.Mapper.Map<List<QuestionnaireDetail>>(Questionnaire);
            questionnaire.Answer.AddRange(AutoMapper.Mapper.Map<List<QuestionAnswers>>(QuestionnaireAnswer));
            questionnaire.Answer.AddRange(AutoMapper.Mapper.Map<List<QuestionAnswers>>(QuestionnaireAnsMul));

            return Request.CreateResponse<ActionOutput<QuestionnaireModel>>(HttpStatusCode.OK, new ActionOutput<QuestionnaireModel>
        {
            Message = "Success",
            Status = FieldConverter.To<bool>(ActionStatus.Successfull),
            Result = questionnaire
        });
    }


        [HttpPost]
        public HttpResponseMessage Answers(QuestionnaireCommand answers)
        {
            QuestionnaireViewModel questions = new QuestionnaireViewModel();
            questions.GetQuestionnaireAnswer(answers);
            return Request.CreateResponse<ActionOutput<string>>(HttpStatusCode.OK, new ActionOutput<string>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result="True"
            });
        }


        //[HttpPost]
        //public HttpResponseMessage Answers([FromBody] List<QuestionnaireAnswerModel> answers)
        //{
        //    try
        //    {
        //        foreach (QuestionnaireAnswerModel _ans in answers)
        //        {
        //            if (_ans.type == "T")
        //            {
        //                Data.UserResponseText response = new Data.UserResponseText();
        //                response.QuestionId = _ans.questionId;
        //                response.UserId = _ans.userId;
        //                response.Response = _ans.value;
        //                Data.ByDayRepository<Data.UserResponseText> repo = new Data.ByDayRepository<Data.UserResponseText>(new Data.Ex2ByDayEntities());
        //                repo.Add(response);
        //            }
        //            else if (_ans.type == "SS")
        //            {
        //                Data.UserResponseM response = new Data.UserResponseM();
        //                response.QuestionId = _ans.questionId;
        //                response.UserId = _ans.userId;
        //                response.OptionId = _ans.selectedOptions.First();
        //                response.UserId = _ans.userId;
        //                response.PartId = 1;
        //                Data.ByDayRepository<Data.UserResponseM> repo = new Data.ByDayRepository<Data.UserResponseM>(new Data.Ex2ByDayEntities());
        //                repo.Add(response);
        //            }
        //            else if (_ans.type == "MS")
        //            {
        //                foreach (int selectionM in _ans.selectedOptions)
        //                {
        //                    Data.UserResponseM response = new Data.UserResponseM();
        //                    response.QuestionId = _ans.questionId;
        //                    response.UserId = _ans.userId;
        //                    response.OptionId = selectionM;
        //                    response.UserId = _ans.userId;
        //                    response.PartId = 1;
        //                    Data.ByDayRepository<Data.UserResponseM> repo = new Data.ByDayRepository<Data.UserResponseM>(new Data.Ex2ByDayEntities());
        //                    repo.Add(response);
        //                }
        //            }
        //            else if (_ans.type == "MSS")
        //            {
        //                foreach (ByDay.API.Models.OptionParts selectionP in _ans.selectedOptionsParts)
        //                {
        //                    Data.UserResponseM response = new Data.UserResponseM();
        //                    response.QuestionId = _ans.questionId;
        //                    response.UserId = _ans.userId;
        //                    response.OptionId = selectionP.Id;
        //                    response.UserId = _ans.userId;
        //                    //   response.PartId = int.Parse(selectionP.Title);
        //                    response.PartId = selectionP.Id;

        //                    Data.ByDayRepository<Data.UserResponseM> repo = new Data.ByDayRepository<Data.UserResponseM>(new Data.Ex2ByDayEntities());
        //                    repo.Add(response);
        //                }
        //            }
        //        }
        //        return Request.CreateResponse<ActionOutput<string>>(HttpStatusCode.OK, new ActionOutput<string>
        //        {
        //            Message = null,
        //            Status = FieldConverter.To<bool>(ActionStatus.Successfull),
        //            Code = 0
        //        });
        //    }
        //    catch (System.Exception ex)
        //    {
        //        return Request.CreateResponse<ActionOutput<string>>(HttpStatusCode.OK, new ActionOutput<string>
        //        {
        //            Message = null,
        //            Status = FieldConverter.To<bool>(ActionStatus.Error),
        //            Code = 0
        //        });
        //    }
        //}


    }
}