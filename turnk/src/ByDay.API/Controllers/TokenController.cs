﻿

using Braintree;
using ByDay.Utility;
using ByDay.Utility.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ByDay.API.Controllers
{

    [System.Web.Http.RoutePrefix("api/Token")]
    public class TokenController : ApiController
    {
        [System.Web.Http.HttpGet]
        public HttpResponseMessage Get()
        {

            var gateway = new BraintreeGateway
            {
                MerchantId = ConfigUtility.GetConfigValueFromWebConfig<string>(Constants.MerchantID),
                PublicKey = ConfigUtility.GetConfigValueFromWebConfig<string>(Constants.PublicKey),
                PrivateKey = ConfigUtility.GetConfigValueFromWebConfig<string>(Constants.PrivateKey),
                
            };

            var Environment = ConfigUtility.GetConfigValueFromWebConfig<string>(Constants.Environment);
            switch (Environment)
            {
                case "PRODUCTION":
                    gateway.Environment = Braintree.Environment.PRODUCTION;
                    break;
                case "QA":
                    gateway.Environment = Braintree.Environment.QA;
                    break;
                case "SANDBOX":
                    gateway.Environment = Braintree.Environment.SANDBOX;
                    break;
                case "DEVELOPMENT":
                    gateway.Environment = Braintree.Environment.DEVELOPMENT;
                    break;
            }

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var clientToken = gateway.ClientToken.generate();

            return Request.CreateResponse<ActionOutput<string>>(HttpStatusCode.OK, new ActionOutput<string>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = clientToken
            });
        }
    }
}