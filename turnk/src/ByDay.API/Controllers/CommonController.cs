﻿using ByDay.API.Models;
using ByDay.Utility;
using ByDay.Utility.Enums;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ByDay.API.Controllers
{

    [RoutePrefix("api/Common")]
    public class CommonController : ApiController
    {
        Business.Countries.CountryService _countryService = new Business.Countries.CountryService();
        [HttpGet]
        [Route("Countries")]
        public HttpResponseMessage Countries()
        {
            var data = _countryService.Countries();
            var countries = AutoMapper.Mapper.Map<List<CountryModel>>(data);
            return Request.CreateResponse<ActionOutput<List<CountryModel>>>(HttpStatusCode.OK, new ActionOutput<List<CountryModel>>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = countries
            });
        }
    }
}