﻿using ByDay.API.Models;
using ByDay.Business.Products;
using ByDay.Utility;
using ByDay.Utility.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ByDay.API.Controllers
{
    [RoutePrefix("api/Product")]
    public class ProductController : BaseApiController
    {
        Business.Products.ProductService _productService = new Business.Products.ProductService();
        [Route("All")]
        [HttpPost]
        public HttpResponseMessage Get(ProductModel model)
        {
            var productCommand = AutoMapper.Mapper.Map<ProductCommand>(model);
            var products = _productService.GetProducts(productCommand);
            var productIdex = new ProductIndex
            {
                Count = products.Count(),
                Products = AutoMapper.Mapper.Map<List<ProductModel>>(products.ToList())
            };
            return Request.CreateResponse<ActionOutput<ProductIndex>>(HttpStatusCode.OK, new ActionOutput<ProductIndex>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = productIdex
            });
        }

        [Route("Insert")]
        [HttpPost]
        public HttpResponseMessage Insert(ProductModel productModel)
        {
            var productCommand = AutoMapper.Mapper.Map<ProductCommand>(productModel);
            _productService.Insert(productCommand);
            return Request.CreateResponse<ActionOutput<bool>>(HttpStatusCode.OK, new ActionOutput<bool>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = true
            });
        }

        [HttpGet]
        [Route("Categories")]
        public HttpResponseMessage Categories()
        {
          var categories=  _productService.GetCategories().ToList();
            var model = AutoMapper.Mapper.Map < List<CategoryModel>>(categories);
            var category = new CategoryIndex
            {
                Count = model.Count,
                Categories = model
            };
            return Request.CreateResponse<ActionOutput<CategoryIndex>>(HttpStatusCode.OK, new ActionOutput<CategoryIndex>
            {
                Message = "Success",
                Status = FieldConverter.To<bool>(ActionStatus.Successfull),
                Result = category
            });
        }

    }
}
