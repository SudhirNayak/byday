﻿
function IngredientsAutoSearch(id) {


    jQuery("#Ingrideints_" + id + "__IngredientName").autocomplete({

        source: function (request, response) {
            jQuery.ajax({
                url: "/Recipe/GetIngredientNames/",
                type: "POST",
                dataType: "json",
                data: { searchText: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                       
                        GetIngredientsType(item.Name, id);
                        return { label: item.Name, value: item.Name,Id:item.Id };
                    }))
                }
            })
        },
        minLength: 2,
        messages: {
            noResults: "", results: ""
        },
      
        //select: function (event, ui) {
        //    var data = ui.item.Id;
        //    GetIngredientsType(data,id);
        //}
    });



}

function GetIngredientsType(id,positionId)
{
  
    $.ajax({
        type: "POST",
        contentType: "application/json;charset=utf-8",
        url: '/Recipe/GetIngridientType/'+id,
       // data: JSON.stringify({ ingredientId: ingredientId, recipeId: recipeId }),
        success: function (data) {
            if (data[0].IngredientType != null) {
                $("#Ingrideints_" + positionId + "__IngredientType").val(data[0].IngredientType);
                $("#Ingrideints_" + positionId + "__IngredientType").attr('readonly', true);
            }
            else {

                $("#Ingrideints_" + id + "__IngredientType").attr('readonly', false);
            }
         }
    });
}
  