﻿var page = 1;
var newHtml = "";
var isMoreProducts = true;
var products = [];
$(document).ready(function () {
 
    GetProductDetails();
});


function GetProductDetails() {
    $("#autoSearchProduct").val('');
    $.ajax({
        type: "POST",
        contentType: "application/json;charset=utf-8",
        url: '/Product/GetProduct/',
        data: JSON.stringify({ page: page }),
        success: function (data) {
            if (data.length != 0 && data != null) {
                $.each(data, function (index, value) {
                    products.push(value);
                });
            }
            else {
                isMoreProducts = false;
            }
            if (isMoreProducts) {
                page++;
            }
            FilterRecord();
        }
    });
}

function FilterRecord() {
    newHtml = "";
    var unOrderdProducts = products;
    var orderdProducts = []
    var filterOption = $("#category-filter option:selected").val();
    if (filterOption == "C") {
        orderdProducts = jQuery.grep(unOrderdProducts, function (n, i) {
            return (n.Type == "C");
        });
    }
    if (filterOption == "G") {
        orderdProducts = jQuery.grep(unOrderdProducts, function (n, i) {
            return (n.Type == "G");
        });
    }
    if (filterOption == "") {
        orderdProducts = jQuery.grep(unOrderdProducts, function (n, i) {
            return (n.Type == "G" || n.Type == "C");
        });
    }
    $.each(orderdProducts, function (index, value) {
        newHtml += '<li><div class="product-img"><p class="product-name">' + value.Name + '</p><img src="' + value.URL + '"></div>' +
               ' <div class="product-info"><p>' +
               '<div class="product-id">' + (value.Type == "G" ? "Gift" : "Card") + '</div><div class="product-price">' + value.Price + '</div></p></div></li>'
    });
    $("#pro-list").html(newHtml);
}


//get auto complete recipe name when user type in search field.
jQuery("#autoSearchProduct").autocomplete({

    source: function (request, response) {
        jQuery.ajax({
            //url: '/Product/GetAllProduct/',
            "url": "/Product/GetFilterProduct?searchLike=" + $('#autoSearchProduct').val(),
            type: "POST",
            dataType: "json",
            data: { searchText: request.term },
            success: function (data) {
                response($.map(data, function (item) {
                    return { label: item.Name, value: item.Name };

                }))

            }
        })
    },
    minLength: 2,
    messages: {
        noResults: "", results: ""
    },
    select: function (event, ui) {
        var data = ui.item.value;
        $('#autoSearchProduct').val(data);
        GetFilterProductDetails();

    }
});




function GetFilterProductDetails() {
    $.ajax({
        "url": "/Product/GetFilterProduct?searchLike=" + $('#autoSearchProduct').val(),
        "type": "POST",
        "datatype": "json",
        success: function (data) {
            if (data.length != 0 && data != null) {
                while (products.length > 0) {
                    products.pop();
                }

                FilterRecord();
                $.each(data, function (index, value) {
                    newHtml += '<li><div class="product-img"><p class="product-name">' + value.Name + '</p><img src="' + value.URL + '"></div>' +
                   ' <div class="product-info"><p>' +
                   '<div class="product-id">' + (value.Type == "G" ? "Gift" : "Card") + '</div><div class="product-price">' + value.Price + '</div></p></div></li>'
                    products.push(value);

                });
                $("#pro-list").html(newHtml);
                $("#category-filter").val("");

            }

        }
    });
}

$("#autoSearchProduct").keyup(function () {
    if ($('#autoSearchProduct').val() == '') {
        while (products.length > 0) {
            products.pop();
        }
        FilterRecord();
        page = 1;
        GetProductDetails();

    }
});
