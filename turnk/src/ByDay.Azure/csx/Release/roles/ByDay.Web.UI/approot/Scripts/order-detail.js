﻿var istrue = false;
$(document).ready(function () {

    DisplayRecipeDetails();



    $("input").keyup(function () {
        if ($('#autoSearchOrder').val().trim() != "") {

            $('#btnSearch').attr({ 'disabled': false });
        }
        //if ($('#autoSearchOrder').val().trim() == "")
        
        //    {
        //        var table = $('#myTable').DataTable();
        //        table.destroy();
        //        DisplayRecipeDetails();
        //        $('#btnSearch').attr({ 'disabled': true });

        //    }
        
    });


    function CheckLenght() {
        $('autoSearchOrder')
    }


    //get auto complete oder name when user type in search field.
    //jQuery("#autoSearchOrder").autocomplete({

    //    source: function (request, response) {
    //        $('#btnSearch').attr({ 'disabled': false });
    //        jQuery.ajax({
    //            url: "/Recipe/GetRecipeNames/",
    //            type: "POST",
    //            dataType: "json",
    //            data: { searchText: request.term },
    //            success: function (data) {
    //                response($.map(data, function (item) {
    //                    return { label: item.RecipeName, value: item.RecipeName };


    //                }))

    //            }
    //        })
    //    },
    //    minLength: 2,
    //    messages: {
    //        noResults: "", results: ""
    //    },
    //    select: function (event, ui) {
    //        var data = ui.item.value;
    //        $('#autoSearchOrder').val(data);
    //        var table = $('#myTable').DataTable();
    //        table.destroy();
    //        DisplayRecipeDetails();
    //    }
    //});



    //getting recipe detail based on search in search button.
    $('#btnSearch').click(function () {
        var table = $('#myTable').DataTable();
        table.destroy();
        DisplayRecipeDetails();
    });
});






//display recipe detail(pagination) in index view. 
function DisplayRecipeDetails() {

    $('#myTable').DataTable({
        "processing": false, // for show processing bar
        "serverSide": true, // for process on server side
        "orderMulti": false, // for disable multi column order
        "dom": '<"top"i>rt<"bottom"lp><"clear">', // for hide default global search box // little confusion? don't worry I explained in the tutorial website
        "ajax": {
            "url": "/Order/LoadData?searchLike=" + $('#autoSearchOrder').val(),
            "type": "POST",
            "datatype": "json"
        },
        "aoColumnDefs": [
    { 'bSortable': false, 'aTargets': [0, 1, 2, 3,4,5] }
        ],
        //"fnRowCallback": function (nRow, aData, iDisplayIndex) {
        //    var index = iDisplayIndex + 1;
        //    $('td:eq(0)', nRow).html(index);
        //    return nRow;
        //},
        "initComplete": function (settings, json) {
            if ($('.dataTables_empty').html() == "No data available in table")
                $('#myTable_info').hide();
        },
    

        "columns": [

             {
                 "autoWidth": true, "width": '16.667%', "render": function (data, type, full, meta) {
                     return '<a href="/Order/View/' + full.OrderID + '">' + '<img  src="' + full.Url + '">' + '</a>';
                 }
             },

                 {
                     "autoWidth": true, "width": '12.667%', "render": function (data, type, full, meta) {
                         return '<a id="order" href="/Order/View/' + full.OrderID + '">' + full.OrderID + '</a>';
                        // return '<a href="/Recipe/View/' + full.OrderID + '">' + full.OrderID + '</a>';

                     }
                 },

                  {
                      "autoWidth": true, "width": '15.667%', "render": function (data, type, full, meta) {
                        
                        
                          return '<span>'+ '<a  href="/Order/View/' + full.OrderID + '">' + full.OrderBy +'</a> '+ '</span>';
                      }
                  },
                  {
                      "autoWidth": true, "width": '15.667%', "render": function (data, type, full, meta) {
                          return '<span>' + '<a href="/Order/View/' + full.OrderID + '">' + full.OrderDate +'</a>'+ '</span>';
                      }
                  },
                    {
                        "autoWidth": true, "width": '15.667%', "render": function (data, type, full, meta) {
                            return '<span>'+'<a href="/Order/View/' + full.OrderID + '">' + full.DeliveryDate +'</a>'+ '</span>';
                        }
                    },
                    {
                        "autoWidth": true, "width": '22.667%', "render": function (data, type, full, meta) {
                          
                          
                          //  <li class="status" onclick="SelectStatus(@item.Id,'@item.Name')" >@item.Name.</li>
                           
                            if (istrue == false) {
                                $.each(full.StatusList, function () {
                                    $(".dropdown-menu").append($('<li class="status" onclick="SelectStatus(' + this.Id + "," + "'" + this.Name + "'" + ')">').text(this.Name));
                                });
                           
                                istrue = true;
                            }

                     
                            return ' <div class="dropdown "> <span type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="statusSelected" class=' + full.Status + '> ' + full.Status + '</span>' +

                              ' <ul class="dropdown-menu" aria-labelledby="dLabel">' + '</ul> </div>';
                        
                            

                        }
                    }]
    });
}


function SelectStatus(statusId, statusName) {
    
    var orderId = $("#order").text();
    $.ajax({
        type: "POST",
        contentType: "application/json;charset=utf-8",
        url: '/Order/StatusOfOrder/',
        data: JSON.stringify({ statusId: statusId, orderId: orderId }),
        success: function (data) {

            if (data == 1) {
                $('#statusSelected').removeClass();

                $('#statusSelected').addClass(statusName + '');
                $('#statusSelected').html(statusName);
            }

        }
    });

}
