﻿$(document).ready(function () {
  
    DisplayRecipeDetails();

    $("input").keyup(function () {
        if ($('#autoSearch').val().trim() != "") {

            $('#btnSearch').attr({ 'disabled': false });
        }
        if ($('#autoSearch').val().trim() == "") {
            {
                var table = $('#myTable').DataTable();
                table.destroy();
                DisplayRecipeDetails();
                $('#btnSearch').attr({ 'disabled': true });

            }
        }
    });


    function CheckLenght() {
        $('autoSearch')
    }

    //$('#btnSearch').click(function () {
    //    jQuery.ajax({
    //        url: "/Recipe/SearchResult/",
    //        type: "POST",
    //        data: { recipeName: $('#autoSearch').val() },
    //        success: function (data) {
    //            // alert(data);
    //            jQuery('#results').html(data);

    //        },
    //        error: function (request, status, errorThrown) {
    //        }

    //    })
    //});
    //get auto complete recipe name when user type in search field.
    jQuery("#autoSearch").autocomplete({
       
        source: function (request, response) {
            $('#btnSearch').attr({ 'disabled': false });
            jQuery.ajax({
                url: "/Recipe/GetRecipeNames/",
                type: "POST",
                dataType: "json",
                data: { searchText: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.RecipeName, value: item.RecipeName };

             
                    }))
                  
                }
            })
        },
        minLength: 2,
        messages: {
            noResults: "", results: ""
        },
        select: function (event, ui) {
            var data = ui.item.value;
            $('#autoSearch').val(data);
            var table = $('#myTable').DataTable();
            table.destroy();
            DisplayRecipeDetails();
        }
    });



    //getting recipe detail based on search in search button.
    $('#btnSearch').click(function () {
        var table = $('#myTable').DataTable();
        table.destroy();
        DisplayRecipeDetails();
         });
});





//display recipe detail(pagination) in index view. 
function DisplayRecipeDetails() {

    $('#myTable').DataTable({
        "processing": false, // for show processing bar
        "serverSide": true, // for process on server side
        "orderMulti": false, // for disable multi column order
        "dom": '<"top"i>rt<"bottom"lp><"clear">', // for hide default global search box // little confusion? don't worry I explained in the tutorial website
        "ajax": {
            "url": "/Recipe/LoadData?searchLike=" + $('#autoSearch').val(),
            "type": "POST",
            "datatype": "json"
        },
        "aoColumnDefs": [
    { 'bSortable': false, 'aTargets': [0, 1, 2, 3] }
        ],
        //"fnRowCallback": function (nRow, aData, iDisplayIndex) {
        //    var index = iDisplayIndex + 1;
        //    $('td:eq(0)', nRow).html(index);
        //    return nRow;
        //},
        "initComplete": function (settings, json) {
            if ($('.dataTables_empty').html() == "No data available in table")
                $('#myTable_info').hide();
        },
        "columns": [
                 {
                     "autoWidth": true, "width": '25%', "render": function (data, type, full, meta) {
                         return '<span>' + full.Sn + '</span>';
                     }
                 },

                  {
                      "autoWidth": true, "width": '25%', "render": function (data, type, full, meta) {
                          return '<a href="/Recipe/View/' + full.Id + '">' + full.RecipeName + '</a>';
                      }
                  },
                    {
                        "autoWidth": true, "width": '25%', "render": function (data, type, full, meta) {
                            return '<img src="' + full.Path[0] + '">';
                        }
                    },
                    {
                        "autoWidth": true, "width": '25%', "render": function (data, type, full, meta) {
                            return '<td><a href="/Recipe/Edit/' + full.Id + '">Edit </a> | <a href="">Delete </a>  </td>';
                        }
                    }]
    });
}

