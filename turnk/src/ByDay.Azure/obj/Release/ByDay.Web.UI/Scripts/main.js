$(document).ready(function(){
	
	//Answer option show on select
	 $("#answertype").change(function() {
		if ($(".single_choice").is(":selected")) {
		   $(".answer-type").hide();
			$("#singlechoice_form").show();

		} 
		else if ($(".multiple_choice").is(":selected")) {
		   $(".answer-type").hide();
			$("#multichoice_form").show();

		} 
		 else if ($(".choice_text").is(":selected")) {
		   $(".answer-type").hide();
			$("#text_choice").show();

		} 
		 else {
			  $(".answer-type").hide();
		}
  	}).trigger('change');
	//Answer option remove
		$('.delete-option').click(function() {
			$(this).closest('.input-wrap').remove();
			return false;
		});
	//tip text editor
	$("#txtEditor").Editor(); 
	 
	
});
