﻿var rowCount;
if ($("#images-count").length > 0) {
    rowCount = $("#images-count").val();
} else {
     rowCount = 2;
}

//adding more image field in add recipe page.
function AddMoreRowsImage() {
    var recRow = '<div class="form-group upload-image-container upload-image-gp"><div class="gp-remove-image-btn">Delete</div><div class="col-md-2 col-xs-2"><img class="upload-image" src="http://www.barebooks.com/wp-content/uploads/2013/10/GM05.gif"  /></div><div class="col-md-4 col-xs-4"><label for="file-upload-' + (rowCount - 1) + '" class="custom-file-upload" id ="custom-file-upload-'+(rowCount-1)+'"><i class="fa fa-image"></i> Image Upload</label> <input type="file" id="file-upload-' + (rowCount - 1) + '" class="form-control" name="images[' + (rowCount - 1) + '].FileName" onchange="SelectImage(this);"></div><div class="col-md-6 col-xs-6"><div class="form-control multiple-select"><ul>';
    if ($("#images-count").length > 0) {
        recRow += $('#ageData').html().replace(/images\[[^\]]+\]/g, 'images[' + (rowCount - 1) + ']');
    }
 else {
        recRow += $('#ageData').html().replace(/images\[[^\]]+\]/g, 'images[' + (rowCount - 1) + ']');
    }
    //recRow += ' <div class="gp-remove-btn"> Delete</div>';
    recRow += '</ul></div></div></div>';
    jQuery(recRow).insertBefore('#more-option').last();
    $('.chk-p').each(function (i) {
        var appendChkName = 'images[' + (rowCount - 1) + '].PermissionIds';
        currentCheckBoxName=$(this).attr("name");
            if (appendChkName == currentCheckBoxName) {
            $(this).prop("checked",false)
        }
        
    });
    rowCount++;
}


var editCount;
if ($("#images-count").length > 0) {
    editCount = $("#images-count").val();
} else {
    editCount = $("#images-count").val();
}


//adding more image field in edit recipe page.
function AddMoreRowsEditImage() {
   
    var recRow = '<div class="uploadImages upload-image-gp" id="imageContainer"><div class="gp-remove-image-btn">Delete</div> <div class="form-group upload-image-container"><div class="col-md-2 col-xs-2"><img class="upload-image img-pos" id="img-src-' + (editCount - 1) + '" ></div><div class="col-md-4 col-xs-4"><label for="file-upload-' + (editCount - 1) + '" class="custom-file-upload" id="custom-file-upload-'+(editCount-1)+'"><i class="fa fa-image"></i> Image Upload</label><input type="file" id ="file-upload-' + (editCount - 1) + '"  class="form-control" name="images[' + (editCount - 1) + '].FileName" onchange="SelectImage(this);"></div><div class="col-md-6 col-xs-6"><div class="form-control multiple-select"><ul id="ageData">';
    if ($("#images-count").length > 0) {
        recRow += $('#ageData').html().replace(/images\[[^\]]+\]/g, 'images[' + (editCount - 1) + ']');
    }
    else {
        recRow += $('#ageData').html().replace(/images\[[^\]]+\]/g, 'images[' + (editCount - 1) + ']');
    }
    recRow += '</ul></div></div></div>';
    jQuery(recRow).insertBefore('#more-option').last();
    $('.chk-p').each(function (i) {
        var appendChkName = 'images[' + (editCount - 1) + '].PermissionIds';
        currentCheckBoxName = $(this).attr("name");
        if (appendChkName == currentCheckBoxName) {
            $(this).prop("checked", false)
        }

    });
    AddRemoveOption(editCount);
    editCount++;
}

//hide remove button when single image field is left otherwise display remove button in edit recipe page.
function AddRemoveOption(indexValue) {
 
    if (indexValue == 1) {
 $(".gp-remove-image-btn").hide();
    }
    else {
        $(".gp-remove-image-btn").show();
    }
}


//hide remove button when single ingredient field is left otherwise display remove button in edit recipe page.
function AddRemoveOptionOnIngrediant(indexValue) {
    if (indexValue == 1) {
  $(".gp-remove-btn").hide();

    }
    else {

        $(".gp-remove-btn").show();
    }
}



//adding more ingredient field in add recipe page.
function AddMoreIngredients() {
    var rowcount = parseInt( $("#addCount").val());
    $("#addCount").val(rowcount + 1);
    $.ajax({
        url: '/Recipe/PartialIngridients/' + rowcount,
        success: function (data) {
            $("#ingredientsId").append(data);
            //$('form').removeData("validator");
            //$('form').removeData("unobtrusiveValidation");
            //$.validator.unobtrusive.parse('form');
        }
    });
}

// delete ingredient in database based on ingredientid.
function DeleteIngredient(ingredientId,recipeId) {
    $.ajax({
        type: "POST",
        contentType: "application/json;charset=utf-8",
        url: '/Recipe/DeleteIngridients/',
        data: JSON.stringify({ ingredientId: ingredientId, recipeId: recipeId }),
        success: function (data) {
            if(data<=1)
            {
                $(".gp-remove-btn").css("display", "none");
            }
        }
    });
}


// delete upload image in database based on ingredientid.
function DeleteUploadImage(ImageId,recipeId) {
    $.ajax({
        type: "POST",
        contentType: "application/json;charset=utf-8",
        url: '/Recipe/DeleteUploadImage/',
        data: JSON.stringify({ ImageId: ImageId, recipeId: recipeId }),
        success: function (data) {
            if (data <= 1) {
                $(".gp-remove-image-btn").css("display", "none");
            }
        }
    });
}



//adding more ingredient field in edit recipe page.
function EditMoreIngredients() {
    var rowEditCount = parseInt($("#editCount").val());
    $("#editCount").val(rowEditCount + 1);

   rowEditCount -= 1;
    $.ajax({
        url: '/Recipe/PartialIngridients/' + rowEditCount ,
        success: function (data) {
            $("#ingredientsId").append(data);
             AddRemoveOptionOnIngrediant($(".udate-ingredient").length);
            $('form').removeData("validator");
            $('form').removeData("unobtrusiveValidation");
            $.validator.unobtrusive.parse('form');
        }
    });
}

//allow only number in numeric field in recipe add and edit form.
function AllowOnlyNumber(event) {
    var key = window.event ? event.keyCode : event.which;

    if (event.keyCode == 8
     || event.keyCode == 37 || event.keyCode == 39) {
        return true;
    }
    else if (key >= 48 && key < 58) {
        return true;
    }
    else return false;
}

// selected image update in current upload image and unchecked validation.
function SelectImage(sender) {
    if (sender.files && sender.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(sender).closest('.upload-image-container').find('.upload-image').attr('src', e.target.result);
        }

        reader.readAsDataURL(sender.files[0]);

        $('#upload-image').html(''); $('#uploadImage').html('');
        $(".custom-file-upload").css('border-color', '');

    }
}


//update next index position of ingredint when user delete ingredient field in add recipe page.
function UpdateAddIngredientData() {
    $(".udate-ingredient").each(function (index, val) {
        var currentForm = this;
        $(currentForm).find(':input').each(function () {
            var currentPostion = index;
            var ingredient = $(this).attr("name").substr(0, 11);
            var name = $(this).attr("name").split('.')[1];
            $(this).attr("name", ingredient + "[" + currentPostion + "]." + name);
            $(this).attr("id", ingredient + "_" + currentPostion + "__" + name);
            $(this).attr("onkeyup", "return CheckValidation(" + currentPostion + ")");
            $(this).attr("data-valmsg-for", ingredient + "[" + currentPostion + "]." + name);
            $(this).next(".field-validation-valid").attr("data-valmsg-for", ingredient + "[" + currentPostion + "]." + name);
            $(this).next(".field-validation-error").attr("id", "ingredient-errorid_" + currentPostion);

        });
    })
    var updatedCount = $(".udate-ingredient").length - 1;
    $("#addCount").val(updatedCount);
}

var updatedCount = $(".udate-ingredient").length - 1;
$("#addCount").val(updatedCount);

//update next index position of image when user delete ingredient field in add recipe page.
function UpdateAddImageRecipeData() {
    $(".upload-image-container").each(function (index, val) {
        $(this).find(".control-label").text(index + 1);
        var currentForm = this;
        $(currentForm).find(':input').each(function () {
            var currentPostion = index;
            var image = $(this).attr("name").substr(0, 6);
            var Name = $(this).attr("name").split('.')[1];
            $(this).attr("name", image + "[" + currentPostion + "]." + Name);

            $(this).prev(".custom-file-upload").attr('for', 'file-upload-' + currentPostion);

            $(this).prev(".custom-file-upload").attr('id', 'custom-file-upload-' + currentPostion);


            $(this).attr("id", "file-upload-" + currentPostion);

        });
    });

    rowCount = $(".upload-image-container").length + 1;
    //var updatedCount = $(".upload-image-container").length;
    //$("#images-count").val(updatedCount);


}


//update next index position of ingredint when user delete ingredient field in edit recipe page.
function UpdateEditIngredientData() {
    $(".udate-ingredient").each(function (index, val) {
        
        var currentForm = this;
        
        $(currentForm).find(':input').each(function () {
            var currentPostion = index;
            
            var ingredient = $(this).attr("name").substr(0, 11);
            var name = $(this).attr("name").split('.')[1];
            $(this).attr("name", ingredient + "[" + currentPostion + "]." + name);
            $(this).attr("id", ingredient + "_" + currentPostion + "__" + name);
            $(this).attr("onkeyup", "return CheckValidation(" + currentPostion + ")");
            $(this).attr("data-valmsg-for", ingredient + "[" + currentPostion + "]." + name);
            $(this).next(".field-validation-valid").attr("data-valmsg-for", ingredient + "[" + currentPostion + "]." + name);
            $(this).next(".field-validation-error").attr("id", "ingredient-errorid_" + currentPostion);     
        });
    })
   AddRemoveOptionOnIngrediant($(".udate-ingredient").length);
    var updatedCount = $(".udate-ingredient").length;
    $("#editCount").val(updatedCount);

}


//update next index position of image when user delete ingredient field in edit recipe page.
function UpdateEditImageData() {
    $(".upload-image-container").each(function (index, val) {
        $(this).find(".control-label").text(index + 1);
        var currentForm = this;
        $(currentForm).find(':input').each(function () {
            var currentPostion = index;
            var image = $(this).attr("name").substr(0, 6);
            var Name = $(this).attr("name").split('.')[1];
            $(this).attr("name", image + "[" + currentPostion + "]." + Name);
            $(this).prev(".custom-file-upload").attr('for', 'file-upload-' + currentPostion);

            $(this).prev(".custom-file-upload").attr('id', 'custom-file-upload-' + currentPostion);
            $(this).attr("id", "file-upload-" + currentPostion);

           
        });
        $(currentForm).find(".img-pos").attr("id", "img-src-" + index);
    });
    
    AddRemoveOption($(".upload-image-container").length);
    editCount = $(".upload-image-container").length + 1;

}

//validation message display in required input field based keyup.
$("#rec-validid").keyup(function () {
    if ($('#rec-validid').val().trim() != "") {
        $("#recipeName-error").html("");
        $("#rec-validid").css('border-color', '');
    }
    if($('#rec-validid').val().trim() == "")
    {
        $("#recipeName-error").html("This field is required.");
        $("#rec-validid").css('border-color','red');

    }
});

//validation message display in required input field based on keyup.
$('#ins-validid').keyup(function () {
    if ($('#ins-validid').val().trim() != "") {
        $("#instruction-error").html("");
        $("#ins-validid").css('border-color', '');
    }
    else {
        $("#instruction-error").html("This field is required.");
        $("#ins-validid").css('border-color', 'red');
    }

});

//validation message display in required input field based on keyup.
$('#description-validid').keyup(function () {
    if ($('#description-validid').val().trim() != "") {
        $("#description-error").html("");
        $("#description-validid").css('border-color', '');
    }
    else {
        $("#description-error").html("This field is required.");
        $("#description-validid").css('border-color', 'red');
    }

});


$('#readygethome-validid').change(function () {
    if ($('#readygethome-validid').val().trim() == "") {
        $("#readygethome-error").html("This field is required.");
        $("#readygethome-validid").css('border-color', 'red');
    }

    else {
        $("#readygethome-error").html("");
        $("#readygethome-validid").css('border-color', '');
    }

});






//validation for all required  field  when user submit the save button in add recipe.
$('#saveRecipeBtn').click(function () {
   
    var checkCondition = false;
    var checkUpload = false;
    if ($('#rec-validid').val().trim() == "") {
        $("#recipeName-error").html("This field is required.");
        $("#rec-validid").css('border-color', 'red');
    }

    if ($('#ins-validid').val().trim()== "") {
        $("#instruction-error").html("This field is required.");
        $("#ins-validid").css('border-color', 'red');
    }
   

    if ($('#description-validid').val().trim() == "") {
        $("#description-error").html("This field is required.");
        $("#description-validid").css('border-color', 'red');
    }

    if ($('#readygethome-validid').val().trim() == "") {
            $("#readygethome-error").html("This field is required.");
            $("#readygethome-error").css('border-color', 'red');
    }
  
    $('.udate-ingredient').each(function (index, val) {
        if ($("#Ingrideints_" + (index) + "__IngredientName").val().trim() == "") {
            $("#ingredient-errorid_" + index).html("This field is required");
            $("#Ingrideints_" + index + "__IngredientName").css('border-color', 'red');
             }
        else {
            $("#ingredient-errorid_" + index).html("");
            $("#Ingrideints_" + index + "__IngredientName").css('border-color', '');
        }
    });
 
    $('.upload-image-gp').each(function (index, val) {
            if ($('#file-upload-' + (index) + '').val().trim() == "") {
                $("#upload-image").html("Please upload atleast one image.");
                $("#custom-file-upload-" + (index) + "").css('border-color', 'red');

            }
            if ($('#file-upload-' + (index) + '').val().trim() != "") {
                $("#upload-image").html("");
                $("#custom-file-upload-" + (index) + "").css('border-color', '');
                return false;
            }
    });

    // remove image border red color if any single image is uploaded.
    $('.upload-image-gp').each(function (index, val) {
        if ($('#file-upload-' + (index) + '').val().trim() != "") {

            checkUpload = true; return false;
        }
    });
    if (checkUpload == true) {
        $(".custom-file-upload").css('border-color', '');

    }



    //submit the form if all required field is not empty. 
    if ($('#rec-validid').val().trim() != "" && $('#ins-validid').val() != "" && $('#description-validid').val().trim() != "" && $('#readygethome-validid').val().trim() != "")
         {            
             $('.upload-image-gp').each(function (index, val) {
                 if ($('#file-upload-' + (index) + '').val().trim() != "") {

                     checkUpload = true;  return false; 
                 }
             });
             if (checkUpload == false) {
                 $('html, body').animate({
                     scrollTop: ($('#upload-image').first().offset().top-110)
                 }, 300);               
             }
             if (checkUpload == true) {
                 $(".custom-file-upload").css('border-color', '');

             }

             $('.udate-ingredient').each(function (index, val) {
                 if ($('#Ingrideints_' + (index) + '__IngredientName').val().trim() == "") {

                     if (checkUpload != false) {
                         $('html, body').animate({
                             scrollTop: ($('#Ingrideints_' + (index) + '__IngredientName').first().offset().top-60)
                         }, 400);
                     }
                     checkCondition = true; return false;
                 }
             });

           
             if (checkCondition != true && checkUpload == true)  
                 $('#recipeForm').submit();
            }
          
});


// check validation for current ingredient Name in list.
function CheckValidation(id)
{
    IngredientsAutoSearch(id);
    var currentIngridients = $("#Ingrideints_" + id + "__IngredientName").val().trim();
                if (currentIngridients != "")
                {
                    $("#ingredient-errorid_" + id).html("");
                    $("#Ingrideints_" + id + "__IngredientName").css('border-color', '');

                    $("#Ingrideints_" + id + "__IngredientType").attr('readonly', false);
                    $("#Ingrideints_" + id + "__IngredientType").val('');

                }
                else
                {
                      $("#ingredient-errorid_" + id).html("This field is required.");
                      $("#Ingrideints_" + id + "__IngredientName").css('border-color', 'red');

                      $("#Ingrideints_" + id + "__IngredientType").attr('readonly', false);
                      $("#Ingrideints_" + id + "__IngredientType").val('');
                }
       
            

               //$('#Ingrideints_" + id + "__IngredientName').keypress(function () {
               //    $("#Ingrideints_" + id + "__IngredientType").val('');

               //});
}



function CheckIngreTypeValidation(id) {
 
    var currentIngridients = $("#Ingrideints_" + id + "__IngredientName").val().trim();
 
 //  IngredientsTypeSearch(id);

}








//enable and disable save and reset button based on fill value in recipeName.       
function CheckFormDisable() {
    if ($('input[name=RecipeName]').val().trim() != "") {
        $('#saveRecipeBtn').prop('disabled', false);
        $('#resetBtn').prop('disabled', false);
        $('#saveDuplicateRecipeBtn').prop('disabled', false);
        $('#reset-duplicate-btn').prop('disabled', false);


    } else {
        $('#saveRecipeBtn').prop('disabled', true);
        $('#resetBtn').prop('disabled', true);
        $('#saveDuplicateRecipeBtn').prop('disabled', true);
        $('#reset-duplicate-btn').prop('disabled', true);

    }
}


//validation for edit recipe when submit the save button in edit recipe page.
$('#saveEditRecipeBtn').click(function () {
   
    var checkCondition = false;
    var checkUpload = false;
    if ($('#rec-validid').val().trim() == "") {
        $("#recipeName-error").html("This field is required.");
        $("#rec-validid").css('border-color', 'red');
    }

    if ($('#ins-validid').val().trim() == "") {
        $("#instruction-error").html("This field is required.");
        $("#ins-validid").css('border-color', 'red');
    }

    if ($('#description-validid').val().trim() == "") {
        $("#description-error").html("This field is required.");
        $("#description-validid").css('border-color', 'red');
    }

    $('.udate-ingredient').each(function (index, val) {
        if ($("#Ingrideints_" + (index) + "__IngredientName").val().trim() == "") {
            $("#ingredient-errorid_" + index).html("This field is required");
            $("#Ingrideints_" + index + "__IngredientName").css('border-color', 'red');
        }
        else {
            $("#ingredient-errorid_" + index).html("");
            $("#Ingrideints_" + index + "__IngredientName").css('border-color', '');
        }
    });
    
    $('.upload-image-gp').each(function (index, val) {
        var $img = $("#img-src-" + index + "");
        var src = $img.attr("src");
        if (src == undefined) {
            $("#upload-image").html("Please upload atleast one image.");
            $("#custom-file-upload-" + (index) + "").css('border-color', 'red');
        }
        if (src!=undefined) {
            $("#upload-image").html("");
            $("#custom-file-upload-" + (index) + "").css('border-color', '');
            return false;
        }
    });
    

    // remove image border red color if any single image is uploaded.
    $('.upload-image-gp').each(function (index, val) {
        var $img = $("#img-src-" + index + "");
        var src = $img.attr("src");
        if (src != undefined) {
            checkUpload = true; return false;
        }
    });
    if (checkUpload == true) {
        $(".custom-file-upload").css('border-color', '');

    }

    //submit the form if all required field is not empty. 
        if ($('#rec-validid').val().trim() != "" && $('#ins-validid').val().trim() != "" && $('#description-validid').val().trim() != "") {
          
            $('.upload-image-gp').each(function (index, val) {
                var $img = $("#img-src-" + index + "");
                var src = $img.attr("src");
                if (src!=undefined) {
                    checkUpload = true; return false;
                }
            });
            if (checkUpload == false) {
                $('html, body').animate({
                    scrollTop: ($('#upload-image').first().offset().top-110)
                }, 500);
            }
            $('.udate-ingredient').each(function (index, val) {
                if ($('#Ingrideints_' + (index) + '__IngredientName').val().trim() == "") {
                    if (checkUpload != false) {
                        $('html, body').animate({
                            scrollTop: ($('#Ingrideints_' + (index) + '__IngredientName').first().offset().top-60)
                        }, 600);
                    }
                    checkCondition = true; return false;
                }
            });
            if (checkUpload == true) {
                $(".custom-file-upload").css('border-color', '');
            }
            if (checkCondition != true && checkUpload == true)
                $('#recipeForm').submit();
        }
});






//validation for all required  field  when user submit the save duplicate button in add recipe.
$('#saveDuplicateRecipeBtn').click(function () {
   
    var checkCondition = false;
    var checkUpload = false;
    if ($('#rec-validid').val().trim() == "") {
        $("#recipeName-error").html("This field is required.");
        $("#rec-validid").css('border-color', 'red');
    }

    if ($('#ins-validid').val().trim() == "") {
        $("#instruction-error").html("This field is required.");
        $("#ins-validid").css('border-color', 'red');

        $('html, body').animate({
            scrollTop: ($('#rec-validid').first().offset().top - 10)
        }, 500);
    }

    if ($('#description-validid').val().trim() == "") {
        $("#description-error").html("This field is required.");
        $("#description-validid").css('border-color', 'red');
        $('html, body').animate({
            scrollTop: ($('#rec-validid').first().offset().top - 10)
        }, 500);
    }

    if ($('#readygethome-validid').val().trim() == "") {
        $("#readygethome-error").html("This field is required.");
        $("#readygethome-error").css('border-color', 'red');
        $('html, body').animate({
            scrollTop: ($('#rec-validid').first().offset().top - 10)
        }, 500);
    }

    $('.udate-ingredient').each(function (index, val) {
        if ($("#Ingrideints_" + (index) + "__IngredientName").val().trim() == "") {
            $("#ingredient-errorid_" + index).html("This field is required");
            $("#Ingrideints_" + index + "__IngredientName").css('border-color', 'red');
        }
        else {
            $("#ingredient-errorid_" + index).html("");
            $("#Ingrideints_" + index + "__IngredientName").css('border-color', '');
        }
    });

    $('.upload-image-gp').each(function (index, val) {
        if ($('#file-upload-' + (index) + '').val().trim() == "") {
            $("#upload-image").html("Please upload atleast one image.");
            $("#custom-file-upload-" + (index) + "").css('border-color', 'red');

        }
        if ($('#file-upload-' + (index) + '').val().trim() != "") {
            $("#upload-image").html("");
            $("#custom-file-upload-" + (index) + "").css('border-color', '');
            return false;
        }
    });

    // remove image border red color if any single image is uploaded.
    $('.upload-image-gp').each(function (index, val) {
        if ($('#file-upload-' + (index) + '').val().trim() != "") {

            checkUpload = true; return false;
        }
    });
    if (checkUpload == true) {
        $(".custom-file-upload").css('border-color', '');

    }



    //submit the form if all required field is not empty. 
    if ($('#rec-validid').val().trim() != "" && $('#ins-validid').val().trim() != "" && $('#description-validid').val().trim() != "" && $('#readygethome-validid').val().trim()!= "") {
        $('.upload-image-gp').each(function (index, val) {
            if ($('#file-upload-' + (index) + '').val().trim() != "") {

                checkUpload = true; return false;
            }
        });
        if (checkUpload == false) {
            $('html, body').animate({
                scrollTop: ($('#upload-image').first().offset().top - 110)
            }, 300);
        }
        if (checkUpload == true) {
            $(".custom-file-upload").css('border-color', '');

        }

        $('.udate-ingredient').each(function (index, val) {
            if ($('#Ingrideints_' + (index) + '__IngredientName').val().trim() == "") {

                if (checkUpload != false) {
                    $('html, body').animate({
                        scrollTop: ($('#Ingrideints_' + (index) + '__IngredientName').first().offset().top - 60)
                    }, 400);
                }
                checkCondition = true; return false;
            }
        });


        if (checkCondition != true && checkUpload == true)
            $('#recipeForm').submit();
    }

});







//validation for  edit recipe when submit the duplicate save button in edit recipe page.
$('#saveDuplicateEditRecipeBtn').click(function () {
    var checkCondition = false;
    var checkUpload = false;
    if ($('#rec-validid').val().trim() == "") {
        $("#recipeName-error").html("This field is required.");
        $("#rec-validid").css('border-color', 'red');
    }

    if ($('#ins-validid').val().trim() == "") {
        $("#instruction-error").html("This field is required.");
        $("#ins-validid").css('border-color', 'red');
        $('html, body').animate({
            scrollTop: ($('#rec-validid').first().offset().top - 10)
        }, 500);
    }

    if ($('#description-validid').val().trim() == "") {
        $("#description-error").html("This field is required.");
        $("#description-validid").css('border-color', 'red');
        $('html, body').animate({
            scrollTop: ($('#rec-validid').first().offset().top - 10)
        }, 500);
    }

    if ($('#readygethome-validid').val().trim() == "") {
        $("#readygethome-error").html("This field is required.");
        $("#readygethome-error").css('border-color', 'red');
        $('html, body').animate({
            scrollTop: ($('#rec-validid').first().offset().top - 10)
        }, 500);
    }

    $('.udate-ingredient').each(function (index, val) {
        if ($("#Ingrideints_" + (index) + "__IngredientName").val().trim() == "") {
            $("#ingredient-errorid_" + index).html("This field is required");
            $("#Ingrideints_" + index + "__IngredientName").css('border-color', 'red');
        }
        else {
            $("#ingredient-errorid_" + index).html("");
            $("#Ingrideints_" + index + "__IngredientName").css('border-color', '');
        }
    });

    $('.upload-image-gp').each(function (index, val) {
        var $img = $("#img-src-" + index + "");
        var src = $img.attr("src");
        if (src == undefined) {
            $("#upload-image").html("Please upload atleast one image.");
            $("#custom-file-upload-" + (index) + "").css('border-color', 'red');
        }
        if (src != undefined) {
            $("#upload-image").html("");
            $("#custom-file-upload-" + (index) + "").css('border-color', '');
            return false;
        }
    });


    // remove image border red color if any single image is uploaded.
    $('.upload-image-gp').each(function (index, val) {
        var $img = $("#img-src-" + index + "");
        var src = $img.attr("src");
        if (src != undefined) {
            checkUpload = true; return false;
        }
    });
    if (checkUpload == true) {
        $(".custom-file-upload").css('border-color', '');

    }

    //submit the form if all required field is not empty. 
    if ($('#rec-validid').val().trim() != "" && $('#ins-validid').val().trim() != "" && $('#description-validid').val().trim() != "") {

        $('.upload-image-gp').each(function (index, val) {
            var $img = $("#img-src-" + index + "");
            var src = $img.attr("src");
            if (src != undefined) {
                checkUpload = true; return false;
            }
        });
        if (checkUpload == false) {
            $('html, body').animate({
                scrollTop: ($('#upload-image').first().offset().top - 110)
            }, 500);
        }
        $('.udate-ingredient').each(function (index, val) {
            if ($('#Ingrideints_' + (index) + '__IngredientName').val().trim() == "") {
                if (checkUpload != false) {
                    $('html, body').animate({
                        scrollTop: ($('#Ingrideints_' + (index) + '__IngredientName').first().offset().top - 60)
                    }, 600);
                }
                checkCondition = true; return false;
            }
        });
        if (checkUpload == true) {
            $(".custom-file-upload").css('border-color', '');
        }
        if (checkCondition != true && checkUpload == true)
            $('#recipeForm').submit();
    }
});

