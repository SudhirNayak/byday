﻿    //This method used for when reload the login page then call prepopulaecheckdataform() method for fill localstorage(username,password) data.
    $(document).ready(function () {
        prepopulateCheckDataForm()
    });

//Get username,password and Ischecked data.
function setLocalStorage() {

    this.email = $("#inputEmail").val();
    this.password = $("#inputPassword").val();
    this.Ischecked = $("#remember-id").val();
}

// Store user and password data in localstorage when checked rememberme checkbox.
function submitCheckData() {

    var obj = new setLocalStorage();
    var data = JSON.stringify(obj);
    localStorage.setItem('AdminData', data);


}
//This method perform when reload the login page then specify if rememberMe checkbox is checked then fill the localstorage(username,password) data.
function prepopulateCheckDataForm() {
    var loadData = JSON.parse(localStorage.getItem('AdminData'));
    if (loadData != null) {
        $("#inputEmail").val(loadData.email);
        $("#inputPassword").val(loadData.password);
        $("#remember-id").prop('checked', loadData.Ischecked);
    }
}

//Submit login user and specify if rememberMe checkbox is checked then store localstorage(username,password) data.
//otherwise remove local storage data.
function checkRemember() {
    var x = $("#remember-id").is(":checked");
    if (x == false) {
        var obj = new setLocalStorage();
        localStorage.removeItem('AdminData', JSON.stringify(obj));
    }
    else {
        submitCheckData();
    }

}
