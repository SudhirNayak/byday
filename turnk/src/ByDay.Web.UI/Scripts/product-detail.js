﻿
$(document).ready(function () {
    if (parseInt($("#extraImages_").css("height").split('p')) >= 274)
        $("#extraImages_").css("height", 345);
    // perform slide up and down when add new category.
    $("#category-form-btn").click(function () {
        $("#category-form").slideToggle("slow");
        $("#pro-validid").val("");
        $("#proName-error").html("");
        $("#pro-validid").css('border-color', '');

    });
    // perform slide up and down when add new subcategory.
    $("#subcategory-form-btn").click(function () {

        if ($("#selectName").val() == '') {
            $(this).attr("disabled", true);
            return false;
        }
        else {
            $(this).attr("disabled", false);
        }
        $("#subcategory-form").slideToggle("slow");
        ClearAllValidation();

    });
});
// clear all validation of add product field.
function ClearAllValidation()
{
    $("#proSub-validid").val("");
    $("#proName-error").html("");
    $("#pro-validid").css('border-color', '');
    $("#proSub-validid").css('border-color', '');
    $("#proSubName-error").html("");
    $("#pro-subCatgory").css('border-color', '');
    $("#selectName").css('border-color', '');
    $("#productName-error").html("");
    $("#proname-validid").css('border-color', '');
    $("#price-error").html("");
    $("#price-validid").css('border-color', '');
    $("#producttype-error").html("");
    $("#producttype-validid").css('border-color', '');
    $("#uploadimage-error").html("");
    $("#custom-file-upload").css('border-color', '');
   $(".upload-image").attr('src', '/Content/images/byDefault.png');
}

// save product category name and selected in dropdown.
function submitProductCategoryData() {
    var data = $('#pro-validid').val();
    if (data == '') {
        $("#proName-error").html("This field is required.");
        $("#pro-validid").css('border-color', 'red');
    } else {
       $.ajax({
            type: "POST",
            contentType: "application/json;charset=utf-8",
            url: '/Product/AddProductCategory/',
            data: JSON.stringify({ categoryName: data }),
            success: function (data) {
                if (data.Id == 0) {
                    $("#proName-error").html("Category already exist .");
                    $("#pro-validid").css('border-color', 'red');
                }
                else {

                    $("#category-form").hide();
                    $('#selectName').append('<option selected="selected" value="' + data.Id + '">' + data.Name + '</option>');
                    $("#pro-validid").val("");
                    $("#pro-subCatgory").empty();
                    $("#pro-subCatgory").html("<option value=''>--Select--</option>");
                    $('#catId').val(data.Id);
                }
                }
        });
        $('#pro-subCatgory').attr("disabled", false);
        $('#subcategory-form-btn').unbind('click', true);
        $("#pro-validid").css('border-color', '');
      
    }
}




// save product sab category  name and selected that dropdown list.
function submitProductSubCategoryData() {
    var data = $('#proSub-validid').val();
    var categoryId = $('#catId').val();
   
    if (data == '') {
        $("#proSubName-error").html("This field is required.");
        $("#proSub-validid").css('border-color', 'red');
    } else {

        $.ajax({
            type: "POST",
            contentType: "application/json;charset=utf-8",
            url: '/Product/AddProductSubCategory/',
            data: JSON.stringify({ subCategoryName: data, categoryId: categoryId }),
            success: function (data) {
                if (data.Id == 0) {
                    $("#proSubName-error").html("Sub Category already exist .");
                    $("#proSub-validid").css('border-color', 'red');
                  
                }
                else {
                    EnableInputControll();
                    $("#subcategory-form").hide();         
                    $('#pro-subCatgory').append('<option selected="selected" value="' + data.Id + '">' + data.Name + '</option>');
                
             
                }

            }
        });
    }
}

// fill data in prodcut subcategory dropdown based on selected product category.
$("#selectName").on('change', function () {
    var id = $(this).find('option:selected').val();   
    if (id == '') {
           $("#pro-subCatgory").empty();       
           $('#pro-subCatgory').attr("disabled", true);
           $("#pro-subCatgory").html("<option value=''>--Select--</option>");
           $("#pro-subCatgory").css('border-color', '');
           $("#category-form").hide();
         $("#subcategory-form").hide();
         $("#pro-validid").css('border-color', '');
         DisableInputControll();
    }
    else {
        $('#pro-subCatgory').attr("disabled", false);
        $('#subcategory-form-btn').unbind('click', true);
        $("#proSub-validid").css('border-color', '');
        $("#category-form").hide();
    }
    $.ajax({
        type: "POST",
        datatype: 'json',
        contentType: "application/json;charset=utf-8",
        url: '/Product/GetProductSubCategory/',
        data: JSON.stringify({ categoryId: id }),
        success: function (res) {
            $("#pro-subCatgory").empty();
            $("#pro-subCatgory").html("<option value=''>--Select--</option>");
            $.each(res, function (key, value) {
                $("#pro-subCatgory").append($("<option></option>").val(value.Id).html(value.Name));
            });
            $('#catId').val(id);
        }
    });
});


//validation message display in required input field (product category add field)  based keyup.
$("#pro-validid").keyup(function () {
    if ($('#pro-validid').val().trim() != "") {
        $("#proName-error").html("");
        $("#pro-validid").css('border-color', '');
    }
    if ($('#pro-validid').val().trim() == "") {
        $("#proName-error").html("This field is required.");
        $("#pro-validid").css('border-color', 'red');

    }
});

// bydefault disable product subcategory dropdown list and add button.
if ($('#pro-subCatgory').val() == '') {
    $('#pro-subCatgory').attr("disabled", true);
    $('#subcategory-form-btn').bind('click', true);
    DisableInputControll();
}

//validation message display in required input field (product sub category add field) based keyup.
$("#proSub-validid").keyup(function () {

    if ($('#proSub-validid').val().trim() != "") {
        $("#proSubName-error").html("");
        $("#proSub-validid").css('border-color', '');
    }
    if ($('#proSub-validid').val().trim() == "") {
        $("#proSubName-error").html("This field is required.");
        $("#proSub-validid").css('border-color', 'red');

    }
});

// enable and disable other field based on selection of product subcateogory dropdown list.
$("#pro-subCatgory").on('change', function () {
    $("#proName-error").html("");
    $("#pro-validid").css('border-color', '');
    $("#subcategory-form").hide();
    $("#proSub-validid").css('border-color', '');
    $("#proSubName-error").html("");


    if ($("#pro-subCatgory").val() != '') {
        EnableInputControll();
    }
    else {
        ClearAllValidation();
        DisableInputControll();
        
    }
});
// function for enable all input controll when select subcategory selection

function EnableInputControll()
{
    $('#proname-validid').attr("disabled", false);
    $('#price-validid').attr("disabled", false);
    $('#producttype-validid').attr("disabled", false);
    $('#iscustom-validid').attr("disabled", false);
    $('#file-upload').attr("disabled", false);
    $('#btnAddMore').attr("disabled", false);
    $('.enableUpload').attr("disabled", false);
}

function DisableInputControll()
{
    $('#proname-validid').attr("disabled", true);
    $('#price-validid').attr("disabled", true);
    $('#producttype-validid').attr("disabled", true);
    $('#iscustom-validid').attr("disabled", true);
    $('#file-upload').attr("disabled", true);
    $('#btnAddMore').attr("disabled", true);
    $('.enableUpload').attr("disabled", true);

}


// selected image update in current upload image and unchecked validation.
function SelectProductImage(sender) {
  
    if (sender.files && sender.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(sender).closest('.upload-product-image').find('.upload-image').attr('src', e.target.result);
        }

        reader.readAsDataURL(sender.files[0]);

    }
}

//validation message display in required input field (product name)  based keyup.
$('#proname-validid').keyup(function () {
    if ($('#proname-validid').val().trim() != "") {
        $("#productName-error").html("");
        $("#proname-validid").css('border-color', '');
    }
    else {
        $("#productName-error").html("This field is required.");
        $("#proname-validid").css('border-color', 'red');
    }

});


//validation message display in required input field (product price)  based keyup. and take only numeric value.
$('#price-validid').keyup(function () {
  
    if ($('#price-validid').val().trim() != "") {
        $("#price-error").html("");
        $("#price-validid").css('border-color', '');
        
    }
    else {
        $("#price-error").html("This field is required.");
        $("#price-validid").css('border-color', 'red');
     
    }
    var val = $(this).val();
    if (isNaN(val)) {
        val = val.replace(/[^0-9\.]/g, '');
        if (val.split('.').length > 2)
            val = val.replace(/\.+$/, "");
    }
    $(this).val(val);
});


//validation message display in required input field (product type)  based keyup.
$('#producttype-validid').change(function () {
    if ($('#producttype-validid').val().trim() != "") {
        $("#producttype-error").html("");
        $("#producttype-validid").css('border-color', '');
    }
    else {
        $("#producttype-error").html("This field is required.");
        $("#producttype-validid").css('border-color', 'red');
    }

});
$('#file-upload').change(function () {

    if ($('#file-upload').val().trim() != "") {
        $("#uploadimage-error").html("");
        $("#custom-file-upload").css('border-color', '');
    }
});
// submit add product
$('#savePrductBtn').click(function () {
    if ($('#pro-subCatgory').val().trim() != "") {

        if ($('#proname-validid').val().trim() == "") {
            $("#productName-error").html("This field is required.");
            $("#proname-validid").css('border-color', 'red');
        }

        if ($('#price-validid').val().trim() == "") {
            $("#price-error").html("This field is required.");
            $("#price-validid").css('border-color', 'red');
        }
        if ($('#producttype-validid').val().trim() == "") {
            $("#producttype-error").html("This field is required.");
            $("#producttype-validid").css('border-color', 'red');
        }
     
        if ($('#file-upload').val().trim() == "") {
            $("#uploadimage-error").html("This field is required.");
            $("#custom-file-upload").css('border-color', 'red');
        }
    }
    if ($('#selectName').val().trim() != "") {

        if ($('#proname-validid').val().trim() != "" && $('#price-validid').val().trim() != "" && $('#producttype-validid').val().trim() != "" && $('#file-upload').val().trim() != "") {
            $('#productForm').submit();
        }
    } 
});


// submit edit product
$('#savePrductBtnEdit').click(function () {
    if ($('#pro-subCatgory').val().trim() != "") {

        if ($('#proname-validid').val().trim() == "") {
            $("#productName-error").html("This field is required.");
            $("#proname-validid").css('border-color', 'red');
        }

        if ($('#price-validid').val().trim() == "") {
            $("#price-error").html("This field is required.");
            $("#price-validid").css('border-color', 'red');
        }
        if ($('#producttype-validid').val().trim() == "") {
            $("#producttype-error").html("This field is required.");
            $("#producttype-validid").css('border-color', 'red');
        }

        if ($('.upload-image img').attr('src') == "") {
           // $("#uploadimage-error").html("This field is required.");
           // $("#custom-file-upload").css('border-color', 'red');
        }
    }
    if ($('#selectName').val().trim() != "") {

        if ($('#proname-validid').val().trim() != "" && $('#price-validid').val().trim() != "" && $('#producttype-validid').val().trim() != "" && $('.upload-image img').attr('src') != "") {
            //EnableInputControll();
            //$('#pro-subCatgory').attr("disabled", false);
            $('#productEditForm').submit();
        }
    }
});
var rowCount = $(".custom-file-upload").length;
function AddMoreRowsImage() {
    var recRow = '<div class="form-group upload-image-container upload-image-gp"><div class="gp-remove-image-btn" onclick="deleteImage(this)">Delete</div><div class="col-md-2 col-xs-2"><img class="upload-image" src="http://www.barebooks.com/wp-content/uploads/2013/10/GM05.gif"  /></div><div class="col-md-4 col-xs-4"><label for="file-upload-' + (rowCount - 1) + '" class="custom-file-upload" id ="custom-file-upload-' + (rowCount - 1) + '"><i class="fa fa-image"></i> Image Upload</label> <input type="file" id="file-upload-' + (rowCount - 1) + '" class="form-control" name="images[' + (rowCount - 1) + '].FileName" onchange="SelectImage(this);"></div><div class="col-md-6 col-xs-6"><div class="multiple-select"><ul>';
    recRow += '</ul></div></div></div>';
    //jQuery(recRow).insertBefore('#more-option').last();
    if (parseInt($("#extraImages_").css("height").split('p')) >= 274)
        $("#extraImages_").css("height", 345);
    jQuery("#extraImages_").prepend(jQuery(recRow));
    rowCount++;
}

function SelectImage(sender) {
    if (sender.files && sender.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(sender).closest('.upload-image-container').find('.upload-image').attr('src', e.target.result);
        }

        reader.readAsDataURL(sender.files[0]);

        $('#upload-image').html(''); $('#uploadImage').html('');
        $(".custom-file-upload").css('border-color', '');

    }
}
function deleteImage(obj) {
    $(obj).parent().remove();
}