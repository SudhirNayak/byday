﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace ByDay.Web.UI.Authentication
{
    public interface ICustomPrincipal : IPrincipal
    {
        int UserId { get; set; }
        string[] Roles { get; set; }
        string Email { get; }
    }
}