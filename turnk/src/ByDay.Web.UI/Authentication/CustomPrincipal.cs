﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace ByDay.Web.UI.Authentication
{
    public class CustomPrincipal : ICustomPrincipal
    {
        public System.Security.Principal.IIdentity Identity { get; private set; }

        public CustomPrincipal()
        {

        }
        public CustomPrincipal(string email)
        {
            Identity = new GenericIdentity(email);

        }
        public bool IsInRole(string role)
        {
            return Roles.Contains(role);
        }


        public int UserId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string[] Roles { get; set; }

    }
}