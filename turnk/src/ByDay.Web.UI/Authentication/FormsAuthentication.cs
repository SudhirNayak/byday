﻿using ByDay.Business;
using ByDay.Data;
using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace ByDay.Web.UI.Authentication
{
    public class FormsAuthenticationService
    {

        UserService _userService = null;
        private const int CookieVersion = 1;

        public FormsAuthenticationService(UserService userService)
        {
            _userService = new UserService();
        }

        // assumes user has authenticated in some way, i.e. via the confirmation
        // process. Call this method with care
        public void SetAuthenticated(ByDayUser user, bool persistent = false)
        {
            if (user != null)
            {
                var cookieName = CookieName(user);
                var encryptedTicket = GenerateEncryptedTicket(user, cookieName, persistent);
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                HttpContext.Current.Response.Cookies.Add(authCookie);
            }
        }

        public void Reauthenticate(ByDayUser user, bool isPersistent = false)
        {
            var authCookie = FormsAuthentication.GetAuthCookie(user.EmailId, false);
            authCookie.Value = GenerateEncryptedTicket(user, user.EmailId, isPersistent);
            HttpContext.Current.Response.Cookies.Set(authCookie);
        }
        public ByDayUser Login(string username, string password, bool persistent = false)
        {
            ByDayUser user = _userService.Login(username, password);

            //   status = user.Status;
            // if (user.Status == (int)LoginStatusEnum.Success)
            //if (user.Status == Convert.ToBoolean(LoginStatusEnum.Success))
            //{
                SetAuthenticated(user, persistent);
                //return true;
           // }
            return user;
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
        }

        private string GenerateEncryptedTicket(ByDayUser user, string cookieName, bool persistent)
        {
            if (user == null) return null;

            var serializeModel = new CustomPrincipalSerializeModel
            {
                UserId=user.Id,
                Email = user.EmailId,
                Name=user.Name,
            };

            var serializer = new JavaScriptSerializer();
            var userData = serializer.Serialize(serializeModel);

            var authTicket = new FormsAuthenticationTicket(
              CookieVersion, // version
              cookieName, // name
              DateTime.UtcNow, //created
              DateTime.UtcNow.AddMinutes(240), // expires
              persistent, // persistent?
              userData // user data
              );

            return FormsAuthentication.Encrypt(authTicket);

        }

        private string CookieName(ByDayUser user)
        {
            var name = user.Name ?? Guid.NewGuid().ToString();
            return string.Format("{0}", name);
        }

    }
}