﻿using ByDay.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.Web.UI.Models
{
    public class UserModel
    {

        public UserModel()
        {
            BreadCrumbLinks = new List<BreadCrumb>();
        }

        public List<BreadCrumb> BreadCrumbLinks { get; set; }
        public string Name { get; set; }
        public string EmailId { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
    }
}