﻿using ByDay.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.Web.UI.Models
{
    public class OrderModel
    {
        public OrderModel()
        {
            BreadCrumbLinks = new List<BreadCrumb>();
            UserAddress = new ViewModel.VMUserAddress();
        }


        public List<BreadCrumb> BreadCrumbLinks { get; set; }

        public int Id { get; set; }
        public Nullable<int> Qty { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverEmail { get; set; }
        public string ReceiverNum { get; set; }
        public string ReceiverAddress { get; set; }
        public string City { get; set; }
        public int StateId { get; set; }
        //public int CountryId { get; set; }
        public Nullable<int> PostalCode { get; set; }
        public string LandMark { get; set; }
        public string Message { get; set; }
        public string OrderDate { get; set; }
        //public int AddressId { get; set; }
        //public int StatusId { get; set; }


        public string CurrentStatus { get; set; }
        public string UserName { get; set; }
        public string ProductName { get; set; }
        public string ProductURL { get; set; }
        public Nullable<decimal> ProductPrice { get; set; }



        // public virtual ByDayUser ByDayUser { get; set; }
        // public virtual Country Country { get; set; }
        // public virtual Product Product { get; set; }
        // public virtual State State { get; set; }
        // public virtual Status Status { get; set; }
        public ViewModel.VMUserAddress UserAddress { get; set; }

        public List<ViewModel.VMStatus> StatusList { get; set; }



    }


}