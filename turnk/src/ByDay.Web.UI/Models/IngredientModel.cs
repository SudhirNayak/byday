﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ByDay.Business.RecipeDetail;
using ByDay.Data;
using ByDay.Web.UI.ViewModel.Recipe;

namespace ByDay.Web.UI.Models
{
    public class IngredientModel
    {
        RecipeService _recipeService = new RecipeService();
        public IngridientCommand GetIngredientDetails()
        {
            return  new IngridientCommand
            {
                Quantities = _recipeService.GetQuanities().ToList(),
                QuantityType = _recipeService.GetQuanityTypes().ToList(),
                IngridientType = _recipeService.GetIngridientType().ToList()
            };
        }


        public List<IngridientCommand> GetIngredientsOfRecipe(int id )
        {
            var Recipe = _recipeService.GetMeal(id);

           return  Recipe.RecipeIngredientMappings.Select(x => new IngridientCommand()
            {
                Id = x.Id,
                IngredientName = x.Ingredient.Name,
                IngredientType=x.Ingredient.IngredientType,
                QuantityId = x.Ingredient.Quantity ?? 0,
                QuanityTypeId = x.Ingredient.QuantityType,
                IngredientCost = x.Ingredient.Cost,
                Tip = x.Ingredient.Tip,
                HardToFind = x.Ingredient.HardToFind,
                Omittable = x.Ingredient.Ommitable,
                IngridientTypeId = x.Ingredient.IngredientTypeMappings.Select(y => y.TypeId).ToList(),
                Quantities = _recipeService.GetQuanities().ToList(),
                QuantityType = _recipeService.GetQuanityTypes().ToList(),
                IngridientType = _recipeService.GetIngridientType().ToList()
            }).ToList();

        }
    }
}