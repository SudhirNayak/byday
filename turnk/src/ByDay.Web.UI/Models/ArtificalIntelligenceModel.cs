﻿using ByDay.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.Web.UI.Models
{
    public class ArtificalIntelligenceModel
    {
        public ArtificalIntelligenceModel()
        {
            BreadCrumbLinks = new List<BreadCrumb>();
        }


        public List<BreadCrumb> BreadCrumbLinks { get; set; }
    }
}