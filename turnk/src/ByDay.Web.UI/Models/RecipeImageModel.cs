﻿using System.Collections.Generic;
using System.Linq;
using ByDay.Data;
using ByDay.Web.UI.ViewModel;


namespace ByDay.Web.UI.Models
{
    public class RecipeImageModel
    {

        public IEnumerable<UploadImage> GetImagesOfRecipe(Recipe recipeDetails,int id)
        {
            return recipeDetails.RecipeImages.Where(x => x.RecipeId == id).Select(x => new UploadImage()
            {
                Id = x.Id,
                Name = x.Name,
                Path = x.Path,
                RecipeId = x.RecipeId,
                PermissionIds = x.ImagePermissionMappings.Where(y => y.ImageId == x.Id).Select(y => y.PermissionId.ToString()).ToList()
            }).ToList();

        }
    }
}