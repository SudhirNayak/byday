﻿using System.Linq;
using ByDay.Web.UI.ViewModel.Recipe;
using ByDay.Business.RecipeDetail;

namespace ByDay.Web.UI.Models
{
    public class RecipeModel
    {

        RecipeService _recipeService = new RecipeService();
        public RecipeEdit GetRecipeDetails(RecipeEdit recipeEdit)
        {
               recipeEdit.count = 0;
            recipeEdit.RecipesList = _recipeService.GetAll().Where(t => t.IsAdmin == true).ToList();
               recipeEdit.AgeAppropriateness = _recipeService.GetAgeGroups().ToList();
               recipeEdit.PermissionType = _recipeService.GetPermissionType();
               recipeEdit.CuisineList = _recipeService.GetCuisines().ToList();
               recipeEdit.Category = _recipeService.GetCategory().ToList();
               recipeEdit.SpecialDiet = _recipeService.GetSpecialDiet().ToList();
               recipeEdit.SpicyLevel = _recipeService.GetSpicyLevel().ToList();
               recipeEdit.SimplicityLevelList = _recipeService.GetSimplicityLevel().ToList();
               recipeEdit.CostLevel = _recipeService.GetIngredientCost().ToList();
               recipeEdit.IngredientPreferenceIdLevel = _recipeService.GetIngredientPreference().ToList();
               return recipeEdit;
           
        }
    }
}