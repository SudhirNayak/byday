﻿using ByDay.Model;
using System;
using System.Collections.Generic;
using System.Web;
using ByDay.Web.UI.ViewModel;
using ByDay.Business.Products;

namespace ByDay.Web.UI.Models
{
    public class ProductModel
    {

        public ProductModel()
        {
            BreadCrumbLinks = new List<BreadCrumb>();
            ProductCategoryList = new List<VMProductCategory>();
            ProductSubCategoryList = new List<VMProductSubCategory>();
            ProductImages = new List<ProductImage>();
        }
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public string ProductName { get; set; }
        public int SubCategoryId { get; set; }
        public HttpPostedFileBase FileName { get; set; }
        public string URL { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string Type { get; set; }
        public bool IsActive { get; set; }
        public bool IsCustomizable { get; set; }
        public List<BreadCrumb> BreadCrumbLinks { get; set; }
        public List<VMProductCategory> ProductCategoryList { get; set; }
        public List<VMProductSubCategory> ProductSubCategoryList { get; set; }
        public  List<ProductImage> ProductImages { get; set; }



    }

    //public class ProductImage
    //{
    //    public int Id { get; set; }
    //    public int ProductId { get; set; }
    //    public string URL { get; set; }
    //}
}