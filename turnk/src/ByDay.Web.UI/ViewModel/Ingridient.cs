﻿using System.Collections.Generic;

namespace ByDay.Web.UI.ViewModel
{
    public class Ingridient
    {
        public string IngredientName { get; set; }
        public int Id { get; set; }
        public List<int> IngridientId { get; set; }
        public int? QuanityTypeId { get; set; }
        public int? QuantityId { get; set; }
        public IEnumerable<ByDay.Data.Type> Quantities { get; set; }
        public IEnumerable<ByDay.Data.Type> QuantityType { get; set; }
        public int? IngredientCost { get; set; }
        public bool? Omittable { get; set; }
        public string Tip { get; set; }
        public bool? HardToFind { get; set; }
        public IEnumerable<ByDay.Data.Type> IngridientType { get; set; }
    }
}