﻿using System.Collections.Generic;
using System.Web;

namespace ByDay.Web.UI.ViewModel
{
    public class UploadImage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public HttpPostedFileBase FileName { get; set; }
        public string Path { get; set; }
        public int RecipeId { get; set; }
        public List<string> PermissionIds { get; set; }

    }
}