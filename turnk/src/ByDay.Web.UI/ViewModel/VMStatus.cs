﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.Web.UI.ViewModel
{
    public class VMStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}