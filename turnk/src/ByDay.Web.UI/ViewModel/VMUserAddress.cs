﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.Web.UI.ViewModel
{
    public class VMUserAddress
    {


        public string Address { get; set; }
        public string City { get; set; }
        public int PostalCode { get; set; }
        public string LandMark { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
    }
}