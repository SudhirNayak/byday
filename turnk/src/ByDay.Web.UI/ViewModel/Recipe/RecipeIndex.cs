﻿using System.Collections.Generic;
using System.Linq;

namespace ByDay.Web.UI.ViewModel.Recipe
{
    public class RecipeIndex
    {
        public int Count { get; set; }
        public IEnumerable<ByDay.Data.Recipe> Recipe { get; set; }
        public IEnumerable<ByDay.Data.Recipe> RecipeList
        {
            get
            {
                return Recipe.OrderBy(c => c.RecipeName);
            }
        }
        public string RecipeName {get; set;}
    }
}