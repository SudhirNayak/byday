﻿using ByDay.Model;
using ByDay.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ByDay.Web.UI.ViewModel.Recipe
{
    public class RecipeEdit
    {
        public RecipeEdit()
        {
            RecipePermission = new List<RecipePermission>();
            images = new List<UploadImage>();
            BreadCrumbLinks = new List<BreadCrumb>();
            this.Ingrideints = new List<ByDay.Business.RecipeDetail.IngridientCommand>();
            IsMostLikeIds = new List<int>();
            LeftOverRecipeGroupId = new List<int>();
        }
        public string DeleteImageIds { get; set; }
        public int count { get; set; }
        public List<BreadCrumb> BreadCrumbLinks { get; set; }
        public string Name { get; set; }
        public int? RecipeId { get; set; }
        public string RecipeName { get; set; }
        public int? SimplicityId { get; set; }
        public List<int> CategoryId { get; set; }
        public List<int> SpecialDietId { get; set; }
        public int Id { get; set; }
        public int? SpiceLevelId { get; set; }
        
       // [UIHint("tinymce_full_compressed"), AllowHtml]
        public string Instructions { get; set; }
        public string Description { get; set; }
        [Required]
        public bool? ReadyWhenGetHome { get; set; }
        public Nullable<int> QuickFixMatch { get; set; }
        public string IsMostLike { get; set; }
        public int? NumServings { get; set; }
        public Nullable<int> Cost { get; set; }
        public string PickyEaterTip { get; set; }
        public string GeneralTips { get; set; }
        public int? CookingTime { get; set; }
        public int? PrepTime { get; set; }
        public bool? UniqueToDatabase { get; set; }
        public List<AgeAppropriate> AgeAppropriateness { get; set; }
        public int? CuisineId { get; set; }
        public bool? Top10PercentOfCuisine { get; set; }
        public bool? Freezable { get; set; }
        public List<int> IsMostLikeIds { get; set; }
      //  public bool? BackupPlanWorthiness { get; set; }
        public bool? Publish { get; set; }
        public int? LeftoverRecipeId { get; set; }
        public bool? WeekendPreparable { get; set; }   
        public List<int> AgeGroupIds { get; set; }
        public List<int> PermissionGroupIds { get; set; }
        public List<string> IsMostLikeGroupId { get; set; }
        public string LeftOverRecipe { get; set; }
        public List<int> LeftOverRecipeGroupId { get; set; }
        public List<int> CusineGroupId { get; set; }
        public virtual ICollection<RecipeCategoryMapping> MealsCategories { get; set; }
        public virtual ICollection<RecipePermissionMapping> permissions { get; set; }
        public List<ByDay.Data.Type> CuisineList { get; set; }
        public string  AgeIds { get; set; }
        public IEnumerable<Permission> PermissionType { get; set; }       
        public List<ByDay.Data.Type> CategoryType { get; set; }
        public List<ByDay.Data.Category> Category { get; set; }
        public List<ByDay.Data.SpecialDiet> SpecialDiet { get; set; }
        public List<ByDay.Data.Type> SpicyLevel { get; set; }
        public List<ByDay.Data.Type> SimplicityLevelList { get; set; }
        public List<ByDay.Data.Type> CostLevel { get; set; }
        public Nullable<int> IngredientPreferenceId { get; set; }
        public List<ByDay.Data.Type> IngredientPreferenceIdLevel { get; set; }
        public int UserId { get; set; }
        public List<RecipePermission> RecipePermission { get; set; }
        public IEnumerable< UploadImage > images { get; set; }
        public List<ByDay.Data.Recipe> RecipesList { get; set; }
        public List<ByDay.Business.RecipeDetail.IngridientCommand> Ingrideints { get; set; }

    }





}