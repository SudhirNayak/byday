﻿namespace ByDay.Web.UI.ViewModel
{
    public class RecipePermission
    {
       public int Id { get; set; }
       public PhotoPermType PhotoPerms { get; set; }
       public int PhotoPerm { get
            {
                return (int)this.PhotoPerms;
            }
                
           set {
                PhotoPerms = (PhotoPermType)value;
            }
        }      
        public int RecipePerm { get; set; }
        public RecipePermType RecipePermTypes { get; set; }
        public int RecipeId { get { return (int)this.RecipePermTypes; }
                            set { RecipePermTypes = (RecipePermType)value; } }

        public enum PhotoPermType
        {
            PermisisonGranted=0,
            PinterestSMGranted=1

        }
        public enum RecipePermType
        {
            FullRecPermGranted = 0,
            IntroDesAdapted = 1

        }
    }
}