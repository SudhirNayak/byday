﻿using System;

namespace ByDay.Web.UI.ViewModel
{
    public class VMProductCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ModifiedAt { get; set; }
    }
}