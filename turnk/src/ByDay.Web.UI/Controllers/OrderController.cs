﻿using ByDay.Business.Orders;
using ByDay.Business.RecipeDetail;
using ByDay.Model;
using ByDay.Web.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ByDay.Web.UI.Controllers
{

    [Authorize]
    public class OrderController : Controller
    {
        RecipeService _recipeService = new RecipeService();
        OrderService _orderService= new OrderService();

        // GET: OrderManagement
        public ActionResult Index()
        {
            OrderModel order = new OrderModel();

            order.BreadCrumbLinks.Add(new BreadCrumb
            {
                Text = string.Format("Home"),
                // Seperator = " > ",
                Url = string.Format("/Recipe/Index")
            });
            order.BreadCrumbLinks.Add(new BreadCrumb { Text = "View Order", Url = "" });
            ViewBag.BreadCrumbs = order.BreadCrumbLinks;
            return View(order);
        }



        public ActionResult view(int id)
        {
            OrderModel order = new OrderModel();
            var OrderData = _orderService.GetOrderById(id).ToList();

            var status = _orderService.GetAllStatus().ToList();

            var orderselectedData = AutoMapper.Mapper.Map<List<OrderModel>>(OrderData);
       
             order.StatusList= AutoMapper.Mapper.Map<List<ViewModel.VMStatus>>(status);

            order.BreadCrumbLinks.Add(new BreadCrumb
            {
                Text = string.Format("Home"),
                // Seperator = " > ",
                Url = string.Format("/Recipe/Index")
            });
            order.BreadCrumbLinks.Add(new BreadCrumb { Text = "View Order", Url = "" });
            ViewBag.BreadCrumbs = order.BreadCrumbLinks;
            ViewBag.StatusList = order.StatusList;
            return View(orderselectedData);

    
        }

        [HttpPost]
        public ActionResult StatusOfOrder(int statusId,int orderId)
        {

            _orderService.UpdateOrderOfStatus(statusId, orderId);
            
            return Json(1, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult GetOrderUserName(string searchText)
        {
            var recipies = _orderService.SearchOrder(searchText).Select(x => new { Id = x.Id, Name = x.ByDayUser.Name }).ToList();
            return Json(recipies, JsonRequestBehavior.AllowGet);
        }




        [HttpPost]
        public ActionResult LoadData(string searchLike)
       {
            var drawNumber = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int recordsTotal = 0;
            if (searchLike != "")
            {
       
                var status = _orderService.GetAllStatus().ToList();
                var StatusList = AutoMapper.Mapper.Map<List<ViewModel.VMStatus>>(status);
                var listData = _orderService.SearchOrder(searchLike).ToList();
                var filterData = AutoMapper.Mapper.Map<List<OrderModel>>(listData);
                var getData = filterData.Select(x => new
                {
                    Url = x.ProductURL,
                    OrderID = x.Id,
                    OrderBy = x.UserName,
                    OrderDate = x.OrderDate,
                    DeliveryDate = "",
                    Status = x.CurrentStatus,
                    StatusList = StatusList,
                }).OrderBy(x => x.OrderID).ThenBy(y => y.OrderBy).ToList();
                recordsTotal = getData.Count();
                var data = getData.Skip(skip).Take(pageSize).ToList();
                return Json(new { drawNumber = drawNumber, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data },
                    JsonRequestBehavior.AllowGet);
            }
            else
            {
                var status = _orderService.GetAllStatus().ToList();
                var StatusList = AutoMapper.Mapper.Map<List<ViewModel.VMStatus>>(status);
                var listData = _orderService.GetAllOrder().ToList();
                var filterData = AutoMapper.Mapper.Map<List<OrderModel>>(listData);
                // int idCount = 1;
                var getData = filterData.Select(x => new
                {
                    Url = x.ProductURL,
                    OrderID = x.Id,
                    OrderBy = x.UserName,
                    OrderDate = x.OrderDate,
                    DeliveryDate = "",
                    Status=x.CurrentStatus,
                    StatusList=StatusList,
                }).OrderBy(x => x.OrderID).ThenBy(y => y.OrderBy).ToList();
                recordsTotal = getData.Count();
                var data = getData.Skip(skip).Take(pageSize).ToList();
                return Json(new { drawNumber = drawNumber, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data },
                    JsonRequestBehavior.AllowGet);
            }
        }

    }
}