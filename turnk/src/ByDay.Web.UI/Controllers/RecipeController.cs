﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ByDay.Web.UI.ViewModel;
using ByDay.Business;
using ByDay.Web.UI.ViewModel.Recipe;
using ByDay.Business.RecipeDetail;
using System.IO;
using AutoMapper;
using ByDay.Model;
using ByDay.Web.UI.Models;
using ByDay.Business.Tips;

namespace ByDay.Web.UI.Controllers
{

    [Authorize]
    public class RecipeController : BaseController
    {
        RecipeService _recipeService = new RecipeService();
        TipsService _tipsService = new TipsService();
        UserService _userService = new UserService();
        RecipeModel recipeModel = new RecipeModel();
        IngredientModel ingredientModel = new IngredientModel();
        RecipeImageModel recipeImageModel = new RecipeImageModel();
        

        public ActionResult Index(RecipeIndex recipeIndex)
        {
              return View();
        }
        [HttpGet]
        public ActionResult AddRecipe()
        {
            RecipeEdit recipes = new RecipeEdit();
            recipes = recipeModel.GetRecipeDetails(recipes);
            var ingredient = ingredientModel.GetIngredientDetails();
            recipes.Ingrideints.Add(ingredient);
            recipes.BreadCrumbLinks.Add(new BreadCrumb
            {
                Text = string.Format("Recipe"),
                //Seperator = " > ",
                Url = string.Format("/Recipe/Index")
            });
            recipes.BreadCrumbLinks.Add(new BreadCrumb { Text = "Add Recipe", Url = "" });
            ViewBag.BreadCrumbs = recipes.BreadCrumbLinks;
            return View(recipes);
        }
        [HttpPost]
        public ActionResult AddRecipe(RecipeEdit recipe)
        {
            recipe = recipeModel.GetRecipeDetails(recipe);
            var ingredient = ingredientModel.GetIngredientDetails();
            recipe.Ingrideints.Add(ingredient);
            recipe.UserId = _userService.GetByUserName(User.Email).Id;
            var recipeCommand = AutoMapper.Mapper.Map<RecipeCommands>(recipe);
            var id = _recipeService.Insert(recipeCommand);
            UploadImage(recipe.images.ToList(), id,recipeCommand.UserId);
            return RedirectToAction("Index");

        }
        

                    [HttpGet]
        public ActionResult AddTip(RecipeEdit recipe)
        {
            RecipeEdit recipes = new RecipeEdit();
            recipes.BreadCrumbLinks.Add(new BreadCrumb
            {
                Text = string.Format("Home"),
                Url = string.Format("/Recipe/Index")
            });
            recipes.BreadCrumbLinks.Add(new BreadCrumb { Text = "Add Tip", Url = "" });
            ViewBag.BreadCrumbs = recipes.BreadCrumbLinks;

            return View();

        }


        [HttpPost]
        public ActionResult DeleteRecipe(int recipeId)
        {

            bool res = _recipeService.DeleteRecipe(recipeId);
            return Json(1, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult AddTip(string tips)
        {
             bool res=   _tipsService.Insert(tips);
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        
              [HttpPost]
        public ActionResult EditTip(string tips,int tipId)
        {
            bool res = _tipsService.Update(tips,tipId);
            return Json(1, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult DeleteTip(int tipId)
        {

            bool res = _tipsService.Delete(tipId);
            return Json(1, JsonRequestBehavior.AllowGet);


        }
        [HttpGet]
        public ActionResult IndexTip(RecipeEdit recipe)

        {
            RecipeEdit recipes = new RecipeEdit();
            recipes.BreadCrumbLinks.Add(new BreadCrumb
            {
                Text = string.Format("Home"),
                Url = string.Format("/Recipe/Index")
            });
            recipes.BreadCrumbLinks.Add(new BreadCrumb { Text = "Tips Detail", Url = "" });
            ViewBag.BreadCrumbs = recipes.BreadCrumbLinks;

            return View();

        }

        [HttpGet]
        public ActionResult ViewTip(int id)

        {
            var tipsDesciption = _tipsService.GetMealTipById(id).ToList();
            var resTip = Mapper.Map<TipsModel>(tipsDesciption[0]);
            ViewBag.TipDescription = resTip.Description;
            ViewBag.tipId = resTip.Id;
            RecipeEdit recipes = new RecipeEdit();
            recipes.BreadCrumbLinks.Add(new BreadCrumb
            {
                Text = string.Format("Home"),
                Url = string.Format("/Recipe/Index")
            });
            recipes.BreadCrumbLinks.Add(new BreadCrumb { Text = "View Tip", Url = "" });
            ViewBag.BreadCrumbs = recipes.BreadCrumbLinks;

            return View();

        }
        public ActionResult View(int id)
        {
            var recipeDetails = _recipeService.GetMeal(id);
            var recipe = Mapper.Map<RecipeEdit>(recipeDetails);
            recipe =recipeModel.GetRecipeDetails(recipe);
            recipe.AgeGroupIds= _recipeService.GetSelectedAgeOfRecipe(recipeDetails,id);
            recipe.CategoryId = _recipeService.GetSelectedCategoryOfRecipe(recipeDetails,id);
            recipe.CusineGroupId = _recipeService.GetSelectedCusineOfRecipe(recipeDetails, id);
            recipe.SpecialDietId = _recipeService.GetSelectedSpecialDiet(recipeDetails, id);
            recipe.Ingrideints = ingredientModel.GetIngredientsOfRecipe(id);
            recipe.images = recipeImageModel.GetImagesOfRecipe(recipeDetails,id);
            recipe.BreadCrumbLinks.Add(new BreadCrumb
            {
                Text = string.Format("Recipe"),
                // Seperator = " > ",
                Url = string.Format("/Recipe/Index")
            });
            recipe.BreadCrumbLinks.Add(new BreadCrumb { Text = "View Recipe", Url = "" });
            ViewBag.BreadCrumbs = recipe.BreadCrumbLinks;
            return View(recipe);
        }

        public ActionResult PartialIngridients(int id)
        {
            RecipeEdit model = new RecipeEdit();
            model.count = id + 1;
            var ingredient = ingredientModel.GetIngredientDetails();
            model.Ingrideints.Add(ingredient);
            return PartialView("_ingredients", model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var Recipe = _recipeService.GetMeal(id);
            var recipeEdit = Mapper.Map<RecipeEdit>(Recipe);
            recipeEdit.IsMostLikeIds = string.IsNullOrEmpty(recipeEdit.IsMostLike) ? new List<int>() : recipeEdit.IsMostLike.Split(',').Select(Int32.Parse).ToList();
            recipeEdit.LeftOverRecipeGroupId = string.IsNullOrEmpty(recipeEdit.LeftOverRecipe) ? new List<int>() : recipeEdit.LeftOverRecipe.Split(',').Select(Int32.Parse).ToList();


            recipeEdit = recipeModel.GetRecipeDetails(recipeEdit);
            recipeEdit.RecipesList = recipeEdit.RecipesList.Select(c => c).Where(x => x.RecipeName != Recipe.RecipeName).ToList();
            recipeEdit.AgeGroupIds = _recipeService.GetSelectedAgeOfRecipe(Recipe, id);
            recipeEdit.CusineGroupId= _recipeService.GetSelectedCusineOfRecipe(Recipe, id);

            recipeEdit.CategoryId = _recipeService.GetSelectedCategoryOfRecipe(Recipe, id);
            recipeEdit.SpecialDietId = _recipeService.GetSelectedSpecialDiet(Recipe,id);
            recipeEdit.PermissionGroupIds= _recipeService.GetSelectedPermissionOfRecipe(Recipe,id);
            recipeEdit.Ingrideints = ingredientModel.GetIngredientsOfRecipe(id);
            recipeEdit.images = recipeImageModel.GetImagesOfRecipe(Recipe, id);
            recipeEdit.BreadCrumbLinks.Add(new BreadCrumb
            {
                Text = string.Format("Recipe"),
                // Seperator = " > ",
                Url = string.Format("/Recipe/Index")
            });
            recipeEdit.BreadCrumbLinks.Add(new BreadCrumb { Text = "Edit Recipe", Url = "" });
            ViewBag.BreadCrumbs = recipeEdit.BreadCrumbLinks;
            return View(recipeEdit);
        }
        [HttpPost]
        public ActionResult Edit(RecipeEdit recipe, int id)
        {
            var Recipe = _recipeService.GetMeal(id);
            var recipeEdit = Mapper.Map<RecipeEdit>(Recipe);
                var recipeobject = Mapper.Map<RecipeCommands>(recipe);
                if (recipe.DeleteImageIds != null)
                {
                    var imageSepIds = recipe.DeleteImageIds.Split(',').ToList<string>();
                    List<int> imageIds = imageSepIds.Select(s => int.Parse(s)).ToList();
                    foreach (int imageId in imageIds)
                    {
                        _recipeService.DeleteUploadImage(imageId);
                    }
                }
                UploadEditImage(recipe.images.ToList(), id,recipeEdit.UserId);
                _recipeService.Update(recipeobject);
                return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult SearchResult(string recipeName)
        {
            var recipies = _recipeService.SearchRecipies(recipeName).ToList();
            var recipe = new RecipeIndex
            {
                Count = recipies.Count(),
                Recipe = recipies.ToList(),
            };
            return PartialView("_results", recipe);
        }
        public void UploadImage(List<UploadImage> images, int mealId,int userId)
        {
            foreach (var item in images)
            {
                var uploadImage = AutoMapper.Mapper.Map<ImageCommand>(item);
                if (item.FileName != null)
                {
                    var file = item.FileName;

                    String timeStamp = Guid.NewGuid().ToString();
                    var path = Path.Combine("/RecipeImages/", string.Format("{0}_{1}", timeStamp, item.FileName.FileName));

                  //  var path = Path.Combine("/ReceipeImages/", string.Format("{0}_{1}", 1, item.FileName.FileName));
                    System.Web.HttpContext.Current.Server.MapPath(path);
                    file.SaveAs(Server.MapPath(path));
                    uploadImage.Path = path;
                    uploadImage.MealId = mealId;
                    uploadImage.Name = file.FileName;
                }
                if (item.Id == 0 && item.FileName != null)
                {
                    _recipeService.UploadImage(uploadImage,userId);
                }

            }
        }


        public void UploadEditImage(List<UploadImage> images, int mealId,int userId)
        {

            foreach (var item in images)
            {
                var uploadImage = AutoMapper.Mapper.Map<ImageCommand>(item);
                if (item.FileName != null)
                {
                    var file = item.FileName;
                    String timeStamp = Guid.NewGuid().ToString();
                    var path = Path.Combine("/RecipeImages/", string.Format("{0}_{1}", timeStamp, item.FileName.FileName));
                    System.Web.HttpContext.Current.Server.MapPath(path);
                    file.SaveAs(Server.MapPath(path));
                    uploadImage.Path = path;
                    uploadImage.MealId = mealId;
                    uploadImage.Name = file.FileName;
                }

                if (item.Id == 0 && item.FileName != null)
                {
                    _recipeService.UploadImage(uploadImage,userId);
                }
                else if (item.Id != 0 && item.FileName != null)
                {
                    _recipeService.UpdateUploadedImage(uploadImage, mealId,userId);
                }
                else if (item.FileName == null)
                {
                    _recipeService.UpdateUploadedImagePermission(uploadImage, mealId);
                }

            }
        }
        [HttpPost]
        public ActionResult GetRecipeNames(string searchText)
        {
            var recipies = _recipeService.SearchRecipies(searchText).Select(x => new { Id = x.Id, RecipeName = x.RecipeName }).ToList();
            return Json(recipies, JsonRequestBehavior.AllowGet);
        }
        


              [HttpPost]
        public ActionResult GetIngredientNames(string searchText)
        {
            var ingrName = _recipeService.SearchIngredients(searchText).Select(x => new { Id = x.Id, Name = x.Name }).ToList();
            var result = ingrName.GroupBy(test => test.Name)
                   .Select(grp => grp.First())
                   .ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        

        [HttpPost]
        public ActionResult GetIngridientType(string id)
        {
            var ingrType = _recipeService.SearchIngredientsType(id).Select(x => new { Id = x.Id, IngredientType = x.IngredientType }).ToList();
            return Json(ingrType, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult LoadData(string searchLike)
        {
            var drawNumber = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int recordsTotal = 0;
            int idSearchCount = 1;
            if (searchLike != "")
            {
                var recipies = _recipeService.SearchRecipiesResult(searchLike).ToList();
                var getSarchData = recipies.Select(x => new
                {
                    Sn = idSearchCount++,
                    x.Id,
                    RecipeName = string.IsNullOrEmpty(x.RecipeName) ? "" : x.RecipeName,
                    Path = x.RecipeImages.Select(y => string.IsNullOrEmpty(y.Path) ? "" : y.Path).ToList()
                }).OrderBy(x => x.Sn).ThenBy(y => y.RecipeName).ToList();
                recordsTotal = getSarchData.Count();
                var data = getSarchData.Skip(skip).Take(pageSize).ToList();
                return Json(new { drawNumber = drawNumber, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data },
                    JsonRequestBehavior.AllowGet);
            }
            else
            {   var listData = _recipeService.GetAll().ToList();
                int idCount = 1;
                var getData = listData.Select(x => new
                {
                    Sn = idCount++,
                    x.Id,
                    RecipeName = string.IsNullOrEmpty(x.RecipeName) ? "" : x.RecipeName,
                    Path = x.RecipeImages.Select(y => string.IsNullOrEmpty(y.Path) ? "" : y.Path).ToList()
                }).OrderBy(x => x.Sn).ThenBy(y => y.RecipeName).ToList();
                recordsTotal = getData.Count();
                var data = getData.Skip(skip).Take(pageSize).ToList();
                return Json(new { drawNumber = drawNumber, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadTipsResult(string searchLike)
        {
            var drawNumber = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int recordsTotal = 0;
            int idSearchCount = 1;
            if (searchLike != "")
            {
                var recipies = _recipeService.SearchRecipiesResult(searchLike).ToList();
                var getSarchData = recipies.Select(x => new
                {
                    Sn = idSearchCount++,
                    x.Id,
                    RecipeName = string.IsNullOrEmpty(x.RecipeName) ? "" : x.RecipeName,
                    Path = x.RecipeImages.Select(y => string.IsNullOrEmpty(y.Path) ? "" : y.Path).ToList()
                }).OrderBy(x => x.Sn).ThenBy(y => y.RecipeName).ToList();
                recordsTotal = getSarchData.Count();
                var data = getSarchData.Skip(skip).Take(pageSize).ToList();
                return Json(new { drawNumber = drawNumber, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data },
                    JsonRequestBehavior.AllowGet);
            }
            else
            {
                var listData = _tipsService.GetMealTip().ToList();
                int idCount = 1;
                var getData = listData.Select(x => new
                {
                    Sn = idCount++,
                    x.Id,
                    Description = string.IsNullOrEmpty(x.Description) ? "" : x.Description,
                    CreateDate = x.CreatedAt.ToString()
                }).OrderBy(x => x.Sn).ThenBy(y => y.Description).ToList();
                recordsTotal = getData.Count();
                var data = getData.Skip(skip).Take(pageSize).ToList();
                return Json(new { drawNumber = drawNumber, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data },
                    JsonRequestBehavior.AllowGet);
            }
        }
    }
}