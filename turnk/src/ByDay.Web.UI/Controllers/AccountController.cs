﻿using ByDay.Business;
using ByDay.Web.UI.Authentication;
using ByDay.Web.UI.ViewModel;
using System.Web.Mvc;
using System.Web.Security;


namespace ByDay.Web.UI.Controllers
{
    public class AccountController : BaseController
    {
     
        FormsAuthenticationService authService = null;
        private UserService _userRepository = new UserService();

        public AccountController()
        {
                 authService = new FormsAuthenticationService(_userRepository);
        }
        /// <summary>
        /// Login method used for showing admin login view.
        /// </summary>
        /// <returns>admin login view</returns>
        [AllowAnonymous]
        public ActionResult Login()
        {
            if (UserContext.IsAuthenticated)
                return Redirect("/");
            return View();
        }
        /// <summary>
        /// method for submit login of valid admin.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns>valid admin redirect to admin home page</returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.Email, model.Password))
                {
                    FormsAuthenticationService service = new FormsAuthenticationService(_userRepository);
                    var user = _userRepository.GetByUserName(model.Email);
                    service.SetAuthenticated(user, true);

                    return RedirectToLocal(returnUrl);
                }
                else { ModelState.AddModelError("", "Invalid login credentials."); return View(model); }
            }
            return View(model);
        }
        /// <summary>
        /// method for logout the admin page.
        /// </summary>
        /// <returns>redirct page in log in</returns>
        public ActionResult LogOut()
        {
            authService.Logout();
            return RedirectToAction("Login");

        }
        /// <summary>
        /// redirect the page based on valid admin.
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("index", "Recipe");
        }
        #endregion
    }

}