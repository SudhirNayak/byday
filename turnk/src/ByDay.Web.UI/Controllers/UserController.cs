﻿using ByDay.Business;
using ByDay.Model;
using ByDay.Web.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ByDay.Web.UI.Controllers
{
    public class UserController : Controller
    {
        UserService userService = new UserService();
        // GET: UserManagement
        public ActionResult Index()
        {
            UserModel user = new UserModel();

            user.BreadCrumbLinks.Add(new BreadCrumb
            {
                Text = string.Format("Home"),
                // Seperator = " > ",
                Url = string.Format("/Recipe/Index")
            });
            user.BreadCrumbLinks.Add(new BreadCrumb { Text = "View User", Url = "" });
            ViewBag.BreadCrumbs = user.BreadCrumbLinks;
            return View();
        }

        [HttpPost]
        public ActionResult GetUser()
        {
            var getUserInfo = userService.GetAllUser();
            var user = AutoMapper.Mapper.Map<List<UserModel>>(getUserInfo);

            return Json(user, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult GetFilterUser(string searchLike)
        { 
            var userList = AutoMapper.Mapper.Map<List<UserModel>>(userService.GetSelectUser(searchLike));
            return Json(userList, JsonRequestBehavior.AllowGet);
        }



    }
}