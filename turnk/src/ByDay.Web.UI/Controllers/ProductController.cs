﻿using ByDay.Business.Products;
using ByDay.Model;
using ByDay.Web.UI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ByDay.Web.UI.Controllers
{

    [Authorize]
    public class ProductController : Controller
    {
        ProductModel product = new ProductModel();
        ProductService productService = new ProductService();
        public ActionResult Index()
        {
     
            product.BreadCrumbLinks.Add(new BreadCrumb
            {
                Text = string.Format("Home"),
                // Seperator = " > ",
                Url = string.Format("/Recipe/Index")
            });
            product.BreadCrumbLinks.Add(new BreadCrumb { Text = "View product", Url = "" });
            ViewBag.BreadCrumbs = product.BreadCrumbLinks;
            var res = productService.GetAll().ToList();
            ViewBag.productList = AutoMapper.Mapper.Map<List<ProductModel>>(res);
            return View();
        }
        [HttpGet]
        public ActionResult AddProduct()
        {
             var categoryList   = AutoMapper.Mapper.Map<List<ViewModel.VMProductCategory>>(productService.GetCategories().ToList());
            product.ProductCategoryList = categoryList;

            product.BreadCrumbLinks.Add(new BreadCrumb
            {
                Text = string.Format("Home"),
                // Seperator = " > ",
                Url = string.Format("/Recipe/Index")
            });
            product.BreadCrumbLinks.Add(new BreadCrumb { Text = "Add product", Url = "" });
            ViewBag.BreadCrumbs = product.BreadCrumbLinks;
            return View(product);

        }
        [HttpPost]
        public ActionResult AddProduct(ProductModel product)
        {
             product.URL = UploadProductImage(product);
            if (Request.Files.Count > 1)
            {
                product.ProductImages = new List<ProductImage>();
                for (int i = 1; i < Request.Files.Count; i++)
                {
                    if (!string.IsNullOrEmpty(Request.Files[i].FileName))
                        product.ProductImages.Add(UploadOtherProductImages(Request.Files[i]));
                }
            }
            var productCommand = AutoMapper.Mapper.Map<ProductCommand>(product);
            productService.Insert(productCommand);
                return RedirectToAction("Index");
        }
     

        [HttpPost]
        public ActionResult AddProductCategory(string categoryName)
        {
            var res = productService.InsertProductCategory(categoryName);
            return Json(res, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult GetProduct(int page)
        {
            int pageSize = 15;
            int skipRecord = pageSize * (page- 1);
            var productList = AutoMapper.Mapper.Map<List<ProductModel>>(productService.GetAll());
            productList = productList.OrderBy(x => x.Name).Skip(skipRecord).Take(pageSize).ToList();
            return Json(productList, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult GetFilterProduct(string searchLike)
       {
          //  var recipies = _recipeService.SearchRecipiesResult(searchLike).ToList();
            var productList = AutoMapper.Mapper.Map<List<ProductModel>>(productService.GetSelectProduct(searchLike));
            return Json(productList, JsonRequestBehavior.AllowGet);
        }




        [HttpPost]
        public ActionResult GetAllProduct()
        {
            var productList = AutoMapper.Mapper.Map<List<ProductModel>>(productService.GetAll());
            return Json(productList, JsonRequestBehavior.AllowGet);

        }



        [HttpPost]
        public ActionResult AddProductSubCategory(string subCategoryName, int categoryId)
        {
            var UpdatedProductSubCategory = productService.InsertProductSubCategory(subCategoryName,categoryId);
            var res = AutoMapper.Mapper.Map<ViewModel.VMProductSubCategory>(UpdatedProductSubCategory);
            return Json(res, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult GetProductSubCategory(int categoryId )
        {
            var data = productService.GetSubCategories(categoryId).Select(x => new ProductModel() { Name = x.Name, Id = x.Id }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public ActionResult EditProduct(int id)
        {


            var products = productService.GetProductById(id).ToList();
            var productList = AutoMapper.Mapper.Map<ProductModel>(products[0]);
            productList.ProductName = products[0].Name;
            var subCategory = AutoMapper.Mapper.Map<ViewModel.VMProductSubCategory>(productService.GetSubCategoriesById(productList.SubCategoryId).FirstOrDefault());
            var Category = AutoMapper.Mapper.Map<ViewModel.VMProductCategory>(productService.GetCategoriesById(subCategory.CategoryId).FirstOrDefault());

            ViewBag.CategoryName = Category.Name;
            ViewBag.CategorySubName = subCategory.Name;
            ViewBag.CategoryId = Category.Id;
            ViewBag.CategorySubId = subCategory.Id;
            product.BreadCrumbLinks.Add(new BreadCrumb
            {
                Text = string.Format("Home"),
                // Seperator = " > ",
                Url = string.Format("/Recipe/Index")
            });
            product.BreadCrumbLinks.Add(new BreadCrumb { Text = "Edit product", Url = "" });
            ViewBag.BreadCrumbs = product.BreadCrumbLinks;
            var categoryList = AutoMapper.Mapper.Map<List<ViewModel.VMProductCategory>>(productService.GetCategories().ToList());
            productList.ProductCategoryList = categoryList;
            return View(productList);
        }
        [HttpPost]
        public ActionResult EditProduct(ProductModel product)
        {
            if (product.FileName != null)
            {
                product.URL = UploadProductImage(product);
            }
            if (Request.Files.Count > 1)
            {
                //product.ProductImages = new List<ProductImage>();
                for (int i = 1; i < Request.Files.Count; i++)
                {
                    if (!string.IsNullOrEmpty(Request.Files[i].FileName))
                        product.ProductImages.Add(UploadOtherProductImages(Request.Files[i]));
                }
            }
            var productCommand = AutoMapper.Mapper.Map<ProductCommand>(product);
            productService.Update(productCommand);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult DeleteProduct(int productId)
        {
            var proRes = productService.Delete(productId);
            return Json(1, JsonRequestBehavior.AllowGet);
        }


        public string UploadProductImage(ProductModel product)
        {
            var file = product.FileName;
            String timeStamp = Guid.NewGuid().ToString();
            var path = Path.Combine("/ProductImages/", string.Format("{0}_{1}", timeStamp, product.FileName.FileName));
            System.Web.HttpContext.Current.Server.MapPath(path);
            file.SaveAs(Server.MapPath(path));
            return path;
        }

        public ProductImage UploadOtherProductImages(HttpPostedFileBase filename)
        {
            ProductImage image = new ProductImage();
            var file = filename;
            String timeStamp = Guid.NewGuid().ToString();
            var path = Path.Combine("/ProductImages/", string.Format("{0}_{1}", timeStamp, filename.FileName));
            System.Web.HttpContext.Current.Server.MapPath(path);
            file.SaveAs(Server.MapPath(path));
            image.URL = path;
            return image;
        }
    }
}