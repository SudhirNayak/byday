﻿using ByDay.Web.UI.Authentication;
using ByDay.Web.UI.Models;
using System.Web.Mvc;

namespace ByDay.Web.UI.Controllers
{
    public class BaseController : Controller
    {
        private UserContext _userContext;
        protected virtual new CustomPrincipal User
        {
            get { return HttpContext.User as CustomPrincipal; }
        }

        public UserContext UserContext
        {
            get { return _userContext ?? (_userContext = new UserContext(User)); }
        }

    }
}