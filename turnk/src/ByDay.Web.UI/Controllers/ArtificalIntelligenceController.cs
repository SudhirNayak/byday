﻿using ByDay.Model;
using ByDay.Web.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ByDay.Web.UI.Controllers
{
    public class ArtificalIntelligenceController : Controller
    {
        // GET: ArtificalIntelligence
        public ActionResult Index()
        {
            ArtificalIntelligenceModel artificalModel = new ArtificalIntelligenceModel();

            artificalModel.BreadCrumbLinks.Add(new BreadCrumb
            {
                Text = string.Format("Home"),
                // Seperator = " > ",
                Url = string.Format("/Recipe/Index")
            });
            artificalModel.BreadCrumbLinks.Add(new BreadCrumb { Text = "Artifical Intelligence", Url = "" });
            ViewBag.BreadCrumbs = artificalModel.BreadCrumbLinks;
            return View();
        }
    }
}