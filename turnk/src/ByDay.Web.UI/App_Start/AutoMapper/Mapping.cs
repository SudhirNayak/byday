﻿using AutoMapper;
using ByDay.Data;
using ByDay.Business.RecipeDetail;
using ByDay.Web.UI.ViewModel;
using ByDay.Web.UI.ViewModel.Recipe;
using ByDay.Web.UI.Models;
using ByDay.Business.Products;
using System.Collections.Generic;
using ViewModels = ByDay.Business.Products;
using System.Linq;

namespace ByDay.Web.UI.App_Start.AutoMapper
{
    public class Mapping : Profile
    {
        protected override void Configure()
        {

            CreateMap<RecipeCommands, RecipeEdit>().ReverseMap();
            CreateMap<ImageCommand, UploadImage>().ReverseMap();
            CreateMap<RecipeEdit, Recipe>().ReverseMap();


            CreateMap<ProductModel, ProductCommand>()
            .ForMember(x => x.Name, opt => opt.MapFrom(src => src.ProductName));


            CreateMap<ProductModel, Product>()
                .ReverseMap();

            CreateMap<Product, ProductModel>()
            .ForMember(x => x.ProductImages, c => c.MapFrom(d => (d.ProductImages.Count > 0) ? d.ProductImages.Select(x=> new ViewModels.ProductImage() {Id=x.Id, ProductId=x.ProductId, URL=x.URL }).ToList() : new List<ViewModels.ProductImage>()))
             .ReverseMap();

            CreateMap<VMProductCategory, ProductCategory>().ReverseMap();

            CreateMap<VMProductSubCategory, ProductSubCategory>().ReverseMap();

            CreateMap<OrderListing, OrderModel>()
                 .ForMember(x => x.ProductURL, opt => opt.MapFrom(src => (src.Product.URL != null) ? src.Product.URL : null))
                           .ForMember(x => x.ProductName, opt => opt.MapFrom(src => (src.Product.Name != null) ? src.Product.Name : null))
                            .ForMember(x => x.UserName, opt => opt.MapFrom(src => (src.ByDayUser.Name != null) ? src.ByDayUser.Name : null))
                            .ForMember(x => x.UserAddress, opt => opt.MapFrom(c => new ViewModel.VMUserAddress()
                            {
                                Address = (c.UserAddress.Address != null ? c.UserAddress.Address : null),
                                LandMark = (c.UserAddress.LandMark != null ? c.UserAddress.LandMark : null),
                                City = (c.UserAddress.City != null ? c.UserAddress.City : null),
                                PostalCode = c.UserAddress.PostalCode,
                                StateName = (c.UserAddress.State.Name != null ? c.UserAddress.State.Name : null),
                                CountryName = (c.UserAddress.Country.Name != null ? c.UserAddress.Country.Name : null),
                            }))
                           .ForMember(x => x.CurrentStatus, opt => opt.MapFrom(src => (src.Status.Name != null) ? src.Status.Name : null))




                           .ReverseMap();
            CreateMap<Status, VMStatus>().ReverseMap();
            CreateMap<Tip, TipsModel>().ReverseMap();
            CreateMap<ByDayUser, UserModel>().ReverseMap();

        }
    }
}