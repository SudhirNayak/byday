﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ByDay.Web.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            //);



            routes.MapRoute(
           name: "Recipe",
           url: "{controller}/{action}/{id}",
           defaults: new { controller = "Recipe", action = "Index", id = UrlParameter.Optional }
       );

            routes.MapRoute(
          name: "ProSample",
          url: "{controller}/{action}/{id}",
          defaults: new { controller = "Product", action = "EditProduct", id = UrlParameter.Optional }
      );


            routes.MapRoute(
        name: "RecipeTip",
        url: "{controller}/{action}/{id}",
        defaults: new { controller = "Recipe", action = "AddTip", id = UrlParameter.Optional }
    );
            routes.MapRoute(
       name: "RecipeTipView",
       url: "{controller}/{action}/{id}",
       defaults: new { controller = "Recipe", action = "ViewTip", id = UrlParameter.Optional }
   );
            routes.MapRoute(
       name: "RecipeTipIndex",
       url: "{controller}/{action}/{id}",
       defaults: new { controller = "Recipe", action = "IndexTip", id = UrlParameter.Optional }
   );

            routes.MapRoute(
       name: "RecipeTipEdit",
       url: "{controller}/{action}/{id}",
       defaults: new { controller = "Recipe", action = "EditTip", id = UrlParameter.Optional }
   );

            routes.MapRoute(
                   name: "ArtificalIntelligence",
                   url: "{controller}/{action}/{id}",
                   defaults: new { controller = "ArtificalIntelligence", action = "Index", id = UrlParameter.Optional }
               );

            routes.MapRoute(
            name: "logout",
            url: "logout",
      defaults: new { controller = "Account", action = "LogOut", id = UrlParameter.Optional }
        );

            routes.MapRoute(
                     name: "User",
                    url: "{controller}/{action}/{id}",
               defaults: new { controller = "User", action = "Index", id = UrlParameter.Optional }
                 );

        }
    }
}
