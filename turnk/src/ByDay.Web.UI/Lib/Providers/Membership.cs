﻿using ByDay.Business;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;


namespace ByDay.Web.UI.Lib.Providers
{
    public class Membership : MembershipProvider
    {
        #region Class Variables

        private bool _enablePasswordReset;
        private readonly bool _enablePasswordRetrieval;
        private readonly bool _requiresQuestionAndAnswer;
        private bool _requiresUniqueEmail;
        private int _maxInvalidPasswordAttempts;
        private int _passwordAttemptWindow;
        private string _providerName;
        private MembershipPasswordFormat _passwordFormat;
        private int _minRequiredNonAlphanumericCharacters;
        private int _minRequiredPasswordLength;
        private string _passwordStrengthRegularExpression;
        private MachineKeySection _machineKey; //Used when determining encryption key values.
        private UserService _userRepository;

        //private IUserRoleRepository _userRoleRepository;
        //private IRoleRepository _roleRepository;
        #endregion


        #region Properties

        public override string ApplicationName { get; set; }





        public override bool EnablePasswordReset
        {
            get
            {
                return _enablePasswordReset;
            }
        }

        public override bool EnablePasswordRetrieval
        {
            get
            {
                return _enablePasswordRetrieval;
            }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get
            {
                return _requiresQuestionAndAnswer;
            }
        }

        public override bool RequiresUniqueEmail
        {
            get
            {
                return _requiresUniqueEmail;
            }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get
            {
                return _maxInvalidPasswordAttempts;
            }
        }

        public override int PasswordAttemptWindow
        {
            get
            {
                return _passwordAttemptWindow;
            }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get
            {
                return _passwordFormat;
            }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get
            {
                return _minRequiredNonAlphanumericCharacters;
            }
        }

        public override int MinRequiredPasswordLength
        {
            get
            {
                return _minRequiredPasswordLength;
            }
        }

        public override string PasswordStrengthRegularExpression
        {
            get
            {
                return _passwordStrengthRegularExpression;
            }
        }



        #endregion

        #region Initialization

        public override void Initialize(string name, NameValueCollection config)
        {
            _userRepository = new UserService();
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            if (name.Length == 0)
            {
                name = "Membership";
            }

            //Initialize the abstract base class.
            base.Initialize(name, config);
            _providerName = name;



            string tempFormat = config["passwordFormat"] ?? "Hashed";

            switch (tempFormat)
            {
                case "Hashed":
                    _passwordFormat = MembershipPasswordFormat.Hashed;
                    break;
                case "Encrypted":
                    _passwordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Clear":
                    _passwordFormat = MembershipPasswordFormat.Clear;
                    break;
                default:
                    throw new ProviderException("Password format not supported.");
            }


        }

        #endregion


        #region Implemented Abstract Methods from MembershipProvider

        /// <summary>
        /// Change the user password.
        /// </summary>
        /// <param name="username">UserName</param>
        /// <param name="oldPwd">Old password.</param>
        /// <param name="newPwd">New password.</param>
        /// <returns>T/F if password was changed.</returns>
        public override bool ChangePassword(string username, string oldPwd, string newPwd)
        {
            var args = new ValidatePasswordEventArgs(username, newPwd, true);
            OnValidatingPassword(args);
            if (args.Cancel)
            {
                if (args.FailureInformation != null)
                {
                    throw args.FailureInformation;
                }
                else
                {
                    throw new Exception("Change password canceled due to new password validation failure.");
                }
            }

            var user = _userRepository.GetByUserName(username);

            if (!CheckPassword(oldPwd, user.Password))
            {
                return false;
            }
            user.Password = EncodePassword(newPwd);
            try
            {
                //_userRepository.Update(user);
            }
            catch (Exception)
            {
                return false;
            }
            return true;

        }

        /// <summary>
        /// Change the question and answer for a password validation.
        /// No question is in our schema, so it's not implemented
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="password">Password.</param>
        /// <param name="newPwdQuestion">New question text.</param>
        /// <param name="newPwdAnswer">New answer text.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPwdQuestion,
          string newPwdAnswer)
        {
            throw new NotSupportedException("Method not supported");
        }

        /// <summary>
        /// Create a new user.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="password">Password.</param>
        /// <param name="email">Email address.</param>
        /// <param name="passwordQuestion">Security quesiton for password.</param>
        /// <param name="passwordAnswer">Security quesiton answer for password.</param>
        /// <param name="isApproved"></param>
        /// <param name="status"></param>
        /// <returns>MembershipUser</returns>
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotSupportedException("Method not supported");

        }

        public MembershipUser CreateUser(string username, string password, string email, string firstName, string lastname, string phoneNumber,
            int partnerId, int roleId, out MembershipCreateStatus status, out int? userId)
        {

            //var args = new ValidatePasswordEventArgs(username, password, true);

            //OnValidatingPassword(args);
            //userId = -1;
            //if (args.Cancel)
            //{

            //    status = MembershipCreateStatus.InvalidPassword;
            //    return null;
            //}

            //if ((RequiresUniqueEmail && (GetUserNameByEmail(email) != String.Empty)))
            //{

            //    status = MembershipCreateStatus.DuplicateEmail;
            //    return null;
            //}

            //MembershipUser membershipUser = GetUser(username, false);
            //if (membershipUser == null)
            //{
            //    var user = new User
            //    {
            //        PartnerID = partnerId,
            //        FirstName = firstName,
            //        LastName = lastname,
            //        PhoneNumber = phoneNumber,
            //        Email = email,
            //        Password = EncodePassword(password),
            //        UserName = username,
            //        ActivityState = activityState
            //    };
            //    try
            //    {
            //        userId = _userRepository.Insert(user);
            //        if (userId != null)
            //        {
            //            var userRole = new UserRole { RoleID = roleId, UserID = userId.Value };
            //            _userRoleRepository.Insert(userRole);
            //        }
            //        status = MembershipCreateStatus.Success;
            //    }
            //    catch (SqlException)
            //    {
            //        //Add exception handling here.

            //        status = MembershipCreateStatus.ProviderError;
            //    }
            //    return GetUser(username, false);
            //}
            //status = MembershipCreateStatus.DuplicateUserName;
            status = MembershipCreateStatus.DuplicateUserName;
            userId = 1;
            return null;
        }


        /// <summary>
        /// Delete a user.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="deleteAllRelatedData">Whether to delete all related data.</param>
        /// <returns>T/F if the user was deleted.</returns>
        public override bool DeleteUser(
         string username,
         bool deleteAllRelatedData
        )
        {
            throw new NotSupportedException("Method not supported");

        }
        /// <summary>
        /// Get a collection of users.
        /// </summary>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="totalRecords">Total # of records to retrieve.</param>
        /// <returns>Collection of MembershipUser objects.</returns>

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {

            throw new NotSupportedException("Method not implemented");

        }
        /// <summary>
        /// Gets the number of users currently on-line.
        /// </summary>
        /// <returns>  /// # of users on-line.</returns>
        public override int GetNumberOfUsersOnline()
        {
            throw new NotSupportedException("Method not supported");
        }
        /// <summary>
        /// Get the password for a user. Not implemented
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="answer">Answer to security question.</param>
        /// <returns>Password for the user.</returns>
        public override string GetPassword(
         string username,
         string answer
        )
        {
            throw new NotSupportedException("Method not supported");
        }

        public override MembershipUser GetUser(
        string username,
         bool userIsOnline
        )
        {
            //var user = _userRepository.GetByUserName(username);
            //if (user != null)
            //{
            //    var user2 = new MembershipUser(_providerName, user.UserName, null, user.Email, string.Empty,
            //    string.Empty, true, false, new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime());
            //    return user2;
            //}
            return null;
        }
        /// <summary>
        /// Get a user based upon provider key and if they are on-line.
        /// </summary>
        /// <param name="userID">Provider key.</param>
        /// <param name="userIsOnline">T/F whether the user is on-line.</param>
        /// <returns></returns>
        public override MembershipUser GetUser(
         object userID,
         bool userIsOnline
        )
        {

            throw new NotSupportedException("Method not supported");
        }

        /// <summary>
        /// Unlock a user.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <returns>T/F if unlocked.</returns>
        public override bool UnlockUser(
         string username
        )
        {
            throw new NotSupportedException("Users are not locked up");
        }


        public override string GetUserNameByEmail(string email)
        {
            throw new NotSupportedException("Method not supported");
        }
        /// <summary>
        /// Reset the user password.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="answer">Answer to security question.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public override string ResetPassword(
         string username,
         string answer
        )
        {

            throw new NotSupportedException("Method not supported");
        }

        /// <summary>
        /// Update the user information.
        /// </summary>
        /// <param name="_membershipUser">MembershipUser object containing data.</param>
        public override void UpdateUser(MembershipUser membershipUser)
        {
            throw new NotSupportedException("Method not supported");

        }
        public void UpdateUser(int userId, string userName, string email, string firstName, string lastName, string phoneNumber, string password, int? partnerId, int? activityStateID, int? IsPartnerSuperAdmin)
        {
            UpdateUser(userId, userName, email, firstName, lastName, phoneNumber, partnerId, password, activityStateID, IsPartnerSuperAdmin);

        }

        public void UpdateUser(int userId, string userName, string email, string firstName, string lastName, string phoneNumber, int? partnerId, string password, int? activityStateID, int? IsPartnerSuperAdmin)
        {
            throw new NotSupportedException("Method not supported");
        }

        /// <summary>
        /// Validate the user based upon username and password.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="password">Password.</param>
        /// <returns>T/F if the user is valid.</returns>
        public override bool ValidateUser(
         string username,
         string password
        )
        {
            var user = _userRepository.GetByUserName(username);
            if (user != null)
                return CheckPassword(password, user.Password);
            return false;

        }
        /// <summary>
        /// Find all users matching a search string.
        /// </summary>
        /// <param name="usernameToMatch">Search string of user name to match.</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords">Total records found.</param>
        /// <returns>Collection of MembershipUser objects.</returns>

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotSupportedException("Method not supported");


        }

        /// <summary>
        /// Find all users matching a search string of their email.
        /// </summary>
        /// <param name="emailToMatch">Search string of email to match.</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords">Total records found.</param>
        /// <returns>Collection of MembershipUser objects.</returns>

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotSupportedException("Method not supported");
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Converts a hexadecimal string to a byte array. Used to convert encryption key values from the configuration
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private byte[] HexToByte(string hexString)
        {
            var returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        public static string GetSHA1(string str)
        {
            SHA1 sha1 = SHA1Managed.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = sha1.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }

        /// <summary>
        /// Check the password format based upon the MembershipPasswordFormat.
        /// </summary>
        /// <param name="password">Password</param>
        /// <param name="dbpassword"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private bool CheckPassword(string password, string dbpassword)
        {
            string pass1 = password;
            string pass2 = dbpassword;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Encrypted:
                    pass2 = UnEncodePassword(dbpassword);
                    break;
                case MembershipPasswordFormat.Hashed:
                    pass1 = EncodePassword(password);
                    break;
                default:
                    break;
            }

            if (pass1 == pass2)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Encode password.
        /// </summary>
        /// <param name="password">Password.</param>
        /// <returns>Encoded password.</returns>
        private string EncodePassword(string password)
        {
            string encodedPassword = password;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    encodedPassword =
                      Convert.ToBase64String(EncryptPassword(Encoding.Unicode.GetBytes(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    /* var hash = new HMACSHA1 {Key = HexToByte(_machineKey.ValidationKey)};
                     encodedPassword =
                       Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));
                     */
                    encodedPassword = GetSHA1(password);
                    break;
                default:
                    throw new ProviderException("Unsupported password format.");
            }

            return encodedPassword;
        }

        /// <summary>
        /// UnEncode password.
        /// </summary>
        /// <param name="encodedPassword">Password.</param>
        /// <returns>Unencoded password.</returns>
        private string UnEncodePassword(string encodedPassword)
        {
            string password = encodedPassword;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    password =
                      Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    throw new ProviderException("Cannot unencode a hashed password.");
                default:
                    throw new ProviderException("Unsupported password format.");
            }

            return password;
        }

        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
            {
                return defaultValue;
            }

            return configValue;
        }

        #endregion

    }
}