﻿namespace ByDay.Model
{
    public enum LoginStatusEnum
    {
        Success = 0,
        InvalidEmail = 1,
        InvalidPassword = 2
    }
}