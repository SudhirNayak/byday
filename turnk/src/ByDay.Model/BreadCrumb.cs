﻿namespace ByDay.Model
{
    public class BreadCrumb
    {
        public string Text { get; set; }
        public string Url { get; set; }
        //  public string Seperator { get; set; }
        public BreadCrumb()
        {
          //  Seperator = ">";
        }
    }
}