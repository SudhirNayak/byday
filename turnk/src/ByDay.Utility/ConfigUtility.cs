﻿using System.Configuration;

namespace ByDay.Utility
{
    public class ConfigUtility
    {
        public static T GetConfigValueFromWebConfig<T>(string key)
        {
            return FieldConverter.To<T>(ConfigurationManager.AppSettings.Get(key));
        }
    }
}
