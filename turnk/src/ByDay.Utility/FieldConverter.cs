﻿using System;

namespace ByDay.Utility
{
    public abstract class FieldConverter
    {
        /// <summary>
        /// Convert the value to type specified
        /// </summary>
        /// <typeparam name="T">the type of object to convert</typeparam>
        /// <param name="value">value to convert</param>
        /// <param name="defaultValue">default value to use if specified value is null</param>
        /// <returns>The converted value</returns>
        public static T To<T>(object value, T defaultValue)
        {
            if (Convert.IsDBNull(value))
            {
                return defaultValue;
            }

            Type conversionType = typeof(T);

            if (conversionType.IsGenericType &&
                conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return defaultValue;
                }
                conversionType = Nullable.GetUnderlyingType(conversionType);
            }

            return (T)Convert.ChangeType(value, conversionType);
        }

        /// <summary>
        /// Convert the value to type specified using the C# generics default value
        /// </summary>
        /// <typeparam name="T">the type of object to convert</typeparam>
        /// <param name="value">value to convert</param>
        /// <returns>The converted value</returns>
        public static T To<T>(object value)
        {
            if (typeof(T) == typeof(string))
            {
                return To<T>(value, (T)Convert.ChangeType(string.Empty, typeof(T)));
            }

            return To<T>(value, default(T));
        }

        /// <summary>
        /// Converts the specified value to DBNull if value is null or equivalent
        /// </summary>
        /// <typeparam name="T">the type of object to convert</typeparam>
        /// <param name="value">value to convert</param>
        /// <returns>DBNull or value of object</returns>
        public static object ToDBNull<T>(T value)
        {
            object returnValue = DBNull.Value;

            if (value != null)
            {
                if (value is int)
                {
                    if (Convert.ToInt32(value) != 0)
                    {
                        returnValue = value;
                    }
                }
                else if (value is string)
                {
                    if (value.ToString() != string.Empty)
                    {
                        returnValue = value;
                    }
                }
                else if (value is DateTime)
                {
                    if (Convert.ToDateTime(value).Date != DateTime.MinValue)
                    {
                        returnValue = value;
                    }
                }
                else //return the value for all other types
                {
                    returnValue = value;
                }
            }

            return returnValue;
        }
    }
}
