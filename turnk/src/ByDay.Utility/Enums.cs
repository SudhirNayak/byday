﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByDay.Utility.Enums
{
    public enum ActionStatus
    {
        Error = 0,
        Successfull = 1,
        LoggedOut = 3,
        Unauthorized = 4
    }
    public enum ActionId
    {
        ColorId=1,
        RoleDefinedId=2,
        IsPrimary=1
    }
    public class ActionOutputBase
    {
      //  public ActionStatus Status { get; set; }
          public bool Status { get; set; }
         public String Message { get; set; }
       // public List<Object> Results { get; set; }
    }

    public class ActionOutput<T> : ActionOutputBase
    {
        public T Result { get; set; }

        public int Code { get; set; }
    }
    public class ErrorOutput<T> : ActionOutputBase
    {
        public T Error { get; set; }

        public int Code { get; set; }
    }
    public class ActionOutput : ActionOutputBase
    {
        public Int32 ID { get; set; }
    }
  
    public class ApiActionOutput
    {
        public ActionStatus Status { get; set; }
        public String Message { get; set; }
    }
}