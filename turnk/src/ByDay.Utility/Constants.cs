﻿namespace ByDay.Utility
{
    public class Constants
    {
        public static readonly string DisplayName = "DisplayName";
        public static readonly string MerchantID = "MerchantID";
        public static readonly string PublicKey = "PublicKey";
        public static readonly string PrivateKey = "PrivateKey";
        public static readonly string Environment = "Environment";
        public static readonly string ImagePath = "ImagePath";
    }
}
