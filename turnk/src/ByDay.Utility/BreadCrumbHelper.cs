﻿using ByDay.Model;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace ByDay.Utility
{
    public static class BreadCrumbHelper
    {
        public static MvcHtmlString RenderBreadCrumbs(this HtmlHelper helper, List<BreadCrumb> breadCrumbList)
        {
            StringBuilder breadCrumb = new StringBuilder();
            int index = 0;
            if (breadCrumbList.Count > 1)
            {
                foreach (BreadCrumb crumb in breadCrumbList)
                {
                    if (index == breadCrumbList.Count - 1)
                    {
                        breadCrumb.AppendFormat("<li>{0}</li>", crumb.Text);
                    }
                    else
                    {
                        breadCrumb.AppendFormat("<li><a href=\"{0}\">{1}</a></li>", crumb.Url, crumb.Text);
                    }
                    //  breadCrumb.AppendFormat("<span>{0}</span>", crumb.Seperator);
                    index++;
                }
            }
            else
            {
                BreadCrumb crumb = breadCrumbList[0];
                breadCrumb.AppendFormat("<li><a href=\"{0}\">{1}</a></li>", crumb.Url, crumb.Text);
            }
            return MvcHtmlString.Create(breadCrumb.ToString());
        }
    }
}